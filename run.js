const Logger = require("./generator/lib/logger"),
  program = require("caporal"),
  name = require("./generator/package.json").name,
  version = require("./generator/package.json").version;
const logger = new Logger();

program._helper._getGlobalOptions = function () {
  return "";
};
program._help = function (cmdStr) {
  const cmd = this._commands.filter(
    (c) => c.name() === cmdStr || c.getAlias() === cmdStr
  )[0];
  const help = this._helper.get(cmd).trimEnd();
  console.log(help);
  return help;
};

/* Init CLI */
program
  .name(name)
  .version(version)
  .help("Run with 'node --max-old-space-size=4096 run.js ...' if memory drains")
  .command("innhold", "Generer Markdown-filer fra Word-dokumenter")
  .option("--debug", "")
  .action((args, options) => {
    require("./generator/lib/innhold").process({ debug: options.debug });
  })
  .command("backup", "Ta backup av /Innhold")
  .action((args, options) => {
    require("./generator/lib/backup").process();
  })
  .command("oppvask", "Rydd i /docs")
  .action((args, options) => {
    try {
      require("./generator/lib/oppvask").process();
    } catch (error) {
      logger.trace(error);
    }
  })
  .command("indeks", "Indekser /docs")
  .action((args, options) => {
    try {
      require("./generator/lib/indeks").process();
    } catch (error) {
      logger.trace(error);
    }
  })
  .command("pdf", "Bygg PDF")
  .action((args, options) => {
    try {
      require("./generator/lib/pdf").process();
    } catch (error) {
      logger.trace(error);
    }
  })
  .command("lesbarhet", "Bygg lesbarhet")
  .option("--debug", "")
  .option("--force", "")
  .action((args, options) => {
    try {
      require("./generator/lib/readability").process({
        debug: options.debug,
        force: options.force,
      });
    } catch (error) {
      logger.trace(error);
    }
  })
  .command("lesbarhetsrapport", "Bygg lesbarhetsrapport")
  .option("--debug", "")
  .action((args, options) => {
    try {
      require("./generator/lib/readability/report").process({
        debug: options.debug,
      });
    } catch (error) {
      logger.trace(error);
    }
  });

program.parse(process.argv);

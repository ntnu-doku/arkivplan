# Bidrag og utvikling

**For bidrag til eller spørsmål om innholdet på sidene, [kontakt DOKU](https://www.ntnu.no/web/adm-doku/hjelp-og-kontakt) direkte.**

Automasjonen som bygger arkivplanen er skrevet i Node.js, og anvender Pandoc for effektiv konvertering av Word- til Markdown-filer. Begge er løsninger med åpen kildekode og gratis i bruk. Enkelte deler av kodegrunnlaget krever nyere versjon av begge.

## Node.js

Er en implementering av JavaScript for servere, forenklet forklart, og svært effektivt for automasjon i mindre og mellomstor skala, samt velegnet for programmering.

Kan installeres på nær sagt alle operativsystemer, også uten administratortilgang. Lastes ned fra [NodeJs.org](https://nodejs.org/en/). Bruker semantisk versjonering, som betyr at alle versjoner innenfor samme større versjon – 15 i 15.12.0 – skal være kompatible. Foruten uforutsette endringer vil typisk nyere, større versjoner enn dette også fungere.

## Pandoc

Et program for konvertering mellom ulike dokumenttyper, med svært bred støtte for formater.

Kan også installeres på nær sagt alle operativsystemer og uten administratortilgang, lastes ned fra [Pandoc.org](https://pandoc.org/). Bruker en annen type semantisk versjonering, men også her skal større versjoner være kompatible.

## Tilgjengelighet

Begge ovennevnte programmer må være tilgjengelig via kommandolinjen der automasjonen kjøres. Det betyr at hvis man skriver `node --version` og `pandoc --version` i et terminalvindu, så får man tilbake informasjon om hvilken versjon av programmet som er installert. Hvis programmet installeres, fremfor å bare pakkes ut, vil det typisk være tilgjengelig på denne måten. Node.js må også ha tilgang til å skrive og endre mapper og filer.

## Kode og standarder

All nødvendig kode ligger i `/generator/lib` og diverse tester for disse i `/tests`. I tillegg ligger `run.js` på samme nivå som `package.js`. Filen `run.js` er hovedprogrammet som anvender all annen kode, tester kjøres uavhengig hvis de bare rapporterer til kommandolinjen.

All kode følger moderne standarder for JavaScript – ECMAScript 6 og oppover, og bruker innebygde API-er der de er tilgjengelige i Node.js. For mange funksjoner brukes bibliotek eller rammeverk som effektivt utfører automasjonen. [JSDoc](https://jsdoc.app/) er brukt for all kode, og tilgjengelig i [API-dokumentasjonen](https://ntnu-doku.gitlab.io/arkivplan/api).

// const Attachments = require("../lib/attachments");

var str = `
::: ADVARSEL Tittel

Innhold

:::

::: ADVARSEL: Du må hjemle tittel

Sensitive innhold i titler må hjemles riktig, se guide til hjemling.

Mer tekst?

Enda mer Tekst.

:::

::: BRA: Suksess

Grønn boks.

:::

asd

::: INFO: Informasjon

Blå boks.

:::

::: ADVARSEL: Advarsel

Gul boks.

:::

asd

::: FARE: Fare

Rød boks.

:::
`;

const replaceAll = require("string.prototype.replaceall"),
  matchAll = require("string.prototype.matchall");
replaceAll.shim();
matchAll.shim();

str = str.replaceAll("::: BRA: ", "::: tip ");
str = str.replaceAll("::: INFO: ", "::: info ");
str = str.replaceAll("::: ADVARSEL: ", "::: warning ");
str = str.replaceAll("::: FARE: ", "::: danger ");

console.log(str);

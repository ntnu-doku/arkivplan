const Attachments = require("../generator/lib/attachments");

const str = `Ved NTNU er det foreløpig bare arkivmateriale som kommer inn under felles bevarings- og kassasjonsregler for statsforvaltningen og arkivmateriale som er gitt kassasjonsvedtak fra Riksarkivaren som kan kasseres./
/
[Felles bevarings- og kassasjonsregler står i Riksarkivarens forskrift kapittel 7 del II](https://lovdata.no/dokument/SF/forskrift/2017-12-19-2286/KAPITTEL_7-3#%C2%A77-5)
[Arkivverkets veileder i bevaring og kassasjon](https://www.arkivverket.no/for-arkiveiere/bevaring-og-kassasjon/a-lage-en-bevaringsplan)

[Vedtak - Kassasjon og bevaring av eksamensbesvarelser med mer (vedlegg)](https://studntnu.sharepoint.com/sites/o365_ArkivplanNTNU-Revisjonogoppdatering/Shared%20Documents/General/Publisering/Innhold/Regelverk/Bevaring%20og%20kassasjon/Vedlegg/Vedtak%20-%20Kassasjon%20og%20bevaring%20av%20eksamensbesvarelser%20med%20mer.pdf)

[Arkivoversikt](https://studntnu.sharepoint.com/sites/o365_ArkivplanNTNU-Revisjonogoppdatering/Shared%20Documents/General/Publisering/Innhold/Arkivoversikt.docx)

[Elektroniske arkivserier og -perioder](https://studntnu.sharepoint.com/sites/o365_ArkivplanNTNU-Revisjonogoppdatering/Shared Documents/General/Publisering/Innhold/Arkivoversikt/Arkivserier og -perioder/Arkivserier og -perioder.docx)

[Funksjonsanalyse -- prosesser, aktiviteter, transaksjoner, saksdokumenter](https://studntnu.sharepoint.com/sites/o365_ArkivplanNTNU-Revisjonogoppdatering/Shared%20Documents/General/Publisering/Innhold/Rutiner/Saksdokumenter/Vedlegg/Funksjonsanalyse.pdf)
`;

Attachments.process({ str: str, debug: true });

const Readability = require("../generator/lib/readability");

const files = [
  "./docs/rutiner/saksbehandling-og-arkivering/taushetsplikt/index.md",
  "./docs/rutiner/saksomrader/personaldokumentasjon/index.md",
  "./docs/rutiner/saksomrader/personal/index.md",
  "./docs/rutiner/saksomrader/vitenskapsmuseet/index.md",
];
Readability.process({ files: files, debug: true });

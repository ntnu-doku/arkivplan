const Logger = require("../generator/lib/logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  { msToTime } = require("../generator/lib/utilities"),
  chalk = require("chalk"),
  { build } = require("vuepress"),
  link = require("linkinator"),
  http = require("http"),
  statik = require("node-static");
const start = performance.now();
const logger = new Logger();
const base = path.resolve("./");
const internal = "http://127.0.0.1:8080/arkivplan";

class Links {
  static async process() {
    await Links.generate()
      .then(async () => {
        await Links.start();
      })
      .then(async () => {
        await Links.test();
      })
      .then(() => {
        console.log(
          chalk`{whiteBright ${"Total Time:"}} {cyan ${msToTime(
            performance.now() - start
          )}}`
        );
        process.exit(0);
      });
  }

  /**
   * Build site to ./public/arkivplan
   */
  static async generate() {
    logger.notice("Bygger VuePress ...");
    try {
      await build({
        sourceDir: "./docs",
        dest: "public/arkivplan",
        clearScreen: false,
        theme: "@vuepress/default",
      });
    } catch (error) {
      logger.trace(error);
    }
    logger.notice("Bygget VuePress");
    await new Promise((resolve) => {
      resolve();
    });
  }

  /**
   * Spawn HTTP server and serve ./public
   */
  static async start() {
    logger.notice("Starter HTTP Server ...");
    const file = new statik.Server("./public");
    http
      .createServer(function (request, response) {
        request
          .addListener("end", function () {
            file.serve(request, response);
          })
          .resume();
      })
      .listen(8080);
  }

  /**
   * Test links and log results
   */
  static async test() {
    logger.notice("Starter lenkesjekk ...");
    try {
      const results = await link.check({
        path: internal,
        recurse: true,
      });
      const skipped = results.links.filter((link) => link.state === "SKIPPED");
      const valid = results.links.filter((link) => link.state === "OK");
      const broken = results.links.filter((link) => link.state === "BROKEN");
      let output = [];
      for (let i = 0; i < skipped.length; i++) {
        console.log(
          chalk`[{gray ${skipped[i].status}}] ${skipped[i].url.replace(
            internal,
            ""
          )}`
        );
        output.push(
          `[${skipped[i].status}] ${skipped[i].url.replace(internal, "")}`
        );
      }
      for (let i = 0; i < valid.length; i++) {
        console.log(
          chalk`[{green ${valid[i].status}}] ${valid[i].url.replace(
            internal,
            ""
          )}`
        );
        output.push(
          `[${valid[i].status}] ${valid[i].url.replace(internal, "")}`
        );
      }
      for (let i = 0; i < broken.length; i++) {
        console.log(
          chalk`[{red ${broken[i].status}}] ${broken[i].url.replace(
            internal,
            ""
          )}`
        );
        output.push(
          `[${broken[i].status}] ${broken[i].url.replace(internal, "")}`
        );
      }
      console.log(
        chalk.whiteBright`${broken.length}/${results.links.length} links broken.`
      );
      output.push(`${broken.length}/${results.links.length} links broken.`);
      if (!FS.existsSync(`${base}/tests/data`)) {
        FS.mkdirSync(`${base}/tests/data`, { recursive: true });
      }
      FS.writeFileSync(`${base}/tests/data/links.txt`, output.join("\n"));
      logger.entry({
        message: `${base}/tests/data/links.txt`,
        source: "Lenkesjekk resultater",
        type: "saved",
        start: start,
        store: true,
      });
    } catch (error) {
      logger.trace(error);
    }
    await new Promise((resolve) => {
      resolve();
    });
  }
}

Links.process();

const Logger = require("../generator/lib/logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  { msToTime } = require("../generator/lib/utilities"),
  chalk = require("chalk"),
  Table = require("cli-table3"),
  firstBy = require("thenby"),
  stripAnsi = require("strip-ansi"),
  Concordance = require("../generator/local_modules/text-analysis/concordance");
const start = performance.now();
const logger = new Logger();
const base = path.resolve("./");
const query = "doku";
let output = [];
let data, keys, testsResults;

function concordance({ data: data }) {
  let content = Concordance.groupContent(data);
  const concordance = new Concordance();
  concordance.process(content, "no");
  let dict = concordance.associativeDict.slice(0, 10);
  logAndStore(
    chalk`{whiteBright ${"Concordance:"}} {cyan ${dict.length}} entries`
  );
  let table = new Table({
    head: ["Word", "Count"],
    style: { head: [], border: [] },
  });
  for (let i = 0; i < dict.length; i++) {
    table.push([chalk.green(dict[i].key), chalk.yellow(dict[i].count)]);
  }
  return table.toString();
}

/**
 * Generate search-data with various search-engines
 * @param {object} params
 * @param {array} params.data Array of objects with search-data
 * @param {array} params.keys Fields to search in
 * @param {string} params.query Term to search for
 * @param {boolean} params.highlight Include highlit data in return
 * @param {boolean} params.originals Include original data in return
 * @param {boolean} params.matches Include match information in return
 * @param {boolean} params.ansi Mark highlights with color for console
 * @returns {array} results Search-results from each search-engine
 */
function testsGenerator({
  data = [],
  keys = [],
  query = "",
  highlight = false,
  originals = false,
  matches = false,
  ansi = false,
} = {}) {
  logAndStore(chalk`{whiteBright ${"Count:"}} {cyan ${data.length}} documents`);
  const tests = [];
  tests["Fuse"] = require("../generator/lib/search/Fuse").bootstrap({
    content: data.map((obj) => ({ ...obj })),
    keys: keys,
  });
  tests["Fuse"].data = require("../generator/lib/search/Fuse").search({
    engine: tests["Fuse"].engine,
    query: query,
    content: data.map((obj) => ({ ...obj })),
    originals: originals,
    matches: matches,
  });
  if (highlight) {
    tests["Fuse"].highlit = require("../generator/lib/search/Fuse").matcher({
      data: tests["Fuse"].data.results,
      query: query,
      ansi: ansi,
    });
  }
  tests["MatchSorter"] =
    require("../generator/lib/search/MatchSorter").bootstrap({
      content: data.map((obj) => ({ ...obj })),
      keys: keys,
      query: query,
      originals: originals,
      matches: matches,
    });
  if (highlight) {
    tests["MatchSorter"].highlit =
      require("../generator/lib/search/MatchSorter").matcher({
        data: tests["MatchSorter"].data.results,
        query: query,
        ansi: ansi,
      });
  }
  tests["FuzzySearch"] =
    require("../generator/lib/search/FuzzySearch").bootstrap({
      content: data.map((obj) => ({ ...obj })),
      keys: keys,
    });
  tests["FuzzySearch"].data =
    require("../generator/lib/search/FuzzySearch").search({
      engine: tests["FuzzySearch"].engine,
      query: query,
      content: data.map((obj) => ({ ...obj })),
      originals: originals,
      matches: matches,
    });
  if (highlight) {
    tests["FuzzySearch"].highlit =
      require("../generator/lib/search/FuzzySearch").matcher({
        data: tests["FuzzySearch"].data.results,
        query: query,
        ansi: ansi,
      });
  }
  tests["FastFuzzy"] = require("../generator/lib/search/FastFuzzy").bootstrap({
    content: data.map((obj) => ({ ...obj })),
    keys: keys,
  });
  tests["FastFuzzy"].data = require("../generator/lib/search/FastFuzzy").search(
    {
      engine: tests["FastFuzzy"].engine,
      query: query,
      content: data.map((obj) => ({ ...obj })),
      originals: originals,
      matches: matches,
    }
  );
  if (highlight) {
    tests["FastFuzzy"].highlit =
      require("../generator/lib/search/FastFuzzy").matcher({
        data: tests["FastFuzzy"].data.results,
        query: query,
        ansi: ansi,
      });
  }
  tests["FlexSearch"] = require("../generator/lib/search/FlexSearch").bootstrap(
    {
      content: data.map((obj) => ({ ...obj })),
      keys: keys,
    }
  );
  tests["FlexSearch"].data =
    require("../generator/lib/search/FlexSearch").search({
      engine: tests["FlexSearch"].engine,
      query: query,
      content: data.map((obj) => ({ ...obj })),
      originals: originals,
      matches: matches,
    });
  tests["Lunr"] = require("../generator/lib/search/Lunr").bootstrap({
    content: data.map((obj) => ({ ...obj })),
    keys: keys,
  });
  tests["Lunr"].data = require("../generator/lib/search/Lunr").search({
    engine: tests["Lunr"].engine,
    query: query,
    content: data.map((obj) => ({ ...obj })),
    originals: originals,
    matches: matches,
  });
  // if (highlight) {
  //   tests["Lunr"].highlit = require("../generator/lib/search/Lunr").matcher({
  //     data: tests["Lunr"].data.results,
  //     query: query,
  //     ansi: ansi,
  //   });
  // }
  tests["ElasticLunr"] =
    require("../generator/lib/search/ElasticLunr").bootstrap({
      content: data.map((obj) => ({ ...obj })),
      keys: keys,
    });
  tests["ElasticLunr"].data =
    require("../generator/lib/search/ElasticLunr").search({
      engine: tests["ElasticLunr"].engine,
      query: query,
      content: data.map((obj) => ({ ...obj })),
      originals: originals,
      matches: matches,
    });
  if (highlight) {
    tests["ElasticLunr"].highlit =
      require("../generator/lib/search/ElasticLunr").matcher({
        data: tests["ElasticLunr"].data.results,
        query: query,
        ansi: ansi,
      });
  }
  tests["Sifter"] = require("../generator/lib/search/Sifter").bootstrap({
    content: data.map((obj) => ({ ...obj })),
    keys: keys,
    query: query,
    originals: originals,
    matches: matches,
    highlight: highlight,
    ansi: ansi,
  });
  tests["MiniSearch"] = require("../generator/lib/search/MiniSearch").bootstrap(
    {
      content: data.map((obj) => ({ ...obj })),
      keys: keys,
    }
  );
  tests["MiniSearch"].data =
    require("../generator/lib/search/MiniSearch").search({
      engine: tests["MiniSearch"].engine,
      query: query,
      content: data.map((obj) => ({ ...obj })),
      originals: originals,
      matches: matches,
    });
  if (highlight) {
    tests["MiniSearch"].highlit =
      require("../generator/lib/search/MiniSearch").matcher({
        data: tests["MiniSearch"].data.results,
        query: query,
        ansi: ansi,
      });
  }
  return tests;
}

/**
 * From each test, render results
 * @param {object} params
 * @param {object} params.tests Search-engine results
 * @param {string} params.query Term searched for
 * @param {boolean} params.highlight Include highlit data in output
 * @returns {void} Outputs table of results directly
 */
function testsIterator({ tests = {}, query = "", highlight = false } = {}) {
  logAndStore(
    chalk`{whiteBright ${"Query:"}} '{cyan ${query}}' running on {cyan ${
      Object.keys(tests).length
    }} search-engine(s), results ordered by highest relevance first`
  );
  let table = new Table({
    head: ["Engine", "Count", "Scored", "Fuzzy", "Locale", "Time"],
    style: { head: [], border: [] },
  });
  for (const [engine, test] of Object.entries(tests)) {
    let time = test.time + test.data.time;
    if (
      test.hasOwnProperty("highlit") &&
      test.highlit !== null &&
      test.highlit.hasOwnProperty("time")
    ) {
      time = time + test.highlit.time;
    }
    table.push([
      chalk.whiteBright(engine),
      chalk.yellow(test.data.count),
      test.score ? chalk.black.bgGreen(test.score) : chalk.bgRed(test.score),
      test.fuzzy ? chalk.black.bgGreen(test.fuzzy) : chalk.bgRed(test.fuzzy),
      test.locale ? test.locale : chalk.bgRed(test.locale),
      msToTime(time),
    ]);
    if (
      highlight &&
      test.hasOwnProperty("highlit") &&
      test.highlit !== null &&
      test.highlit.hasOwnProperty("matches")
    ) {
      table.push([
        {
          colSpan: 6,
          content: (function (data, highlit) {
            let output = "";
            for (let i = 0; i < data.length; i++) {
              if (data[i].hasOwnProperty("score") && data[i].score > 0) {
                output +=
                  chalk.yellow(Number.parseFloat(data[i].score).toFixed(4)) +
                  " ";
              }
              output += data[i].path + "\n";
              if (
                typeof highlit[data[i].path] !== "undefined" &&
                Object.entries(highlit[data[i].path]).length > 0
              ) {
                for (const [key, highlight] of Object.entries(
                  highlit[data[i].path]
                )) {
                  output += `  ${chalk.yellow(key)} ${highlight}\n`;
                }
              }
            }
            return output.trim();
          })(test.data.results, test.highlit.matches || false),
        },
      ]);
    }
  }
  return (
    table.toString() +
    "\n" +
    chalk`{bgRed ${"Numbers and scores are not comparable between tests, only within."}} Results formatted as (SCORE) PATH, with optional document-properties and highlit matches indented below.`
  );
}

/**
 * Find common paths in data
 * @param {object} params
 * @param {array} params.data Array of objects with search-data
 * @returns {void} Outputs table of results directly
 */
function consensus({ data = [] } = {}) {
  let consensus = [];
  let table = new Table({
    head: ["Engine", "Path"],
    style: { head: [], border: [] },
  });
  for (let i = 0; i < data.length; i++) {
    const name = data[i].name;
    const path = data[i].data.results[0].path;
    if (!consensus.includes(path)) {
      consensus.push(path);
      table.push([name, path]);
      if (
        data[i].hasOwnProperty("highlit") &&
        data[i].highlit.hasOwnProperty(path)
      ) {
        table.push([
          {
            colSpan: 2,
            content: (function (data) {
              let output = "";
              for (const [property, highlight] of Object.entries(data)) {
                output += `${chalk.yellow(property)} ${highlight}\n`;
              }
              return output.trim();
            })(data[i].highlit[path]),
          },
        ]);
      }
    }
  }
  return (
    chalk`{whiteBright ${"Consensus:"}} {cyan ${
      consensus.length
    }} documents, listing engines and common paths` +
    "\n" +
    table.toString()
  );
}

/**
 * Sort data by set properties
 * @param {object} params
 * @param {array} params.data Array of objects with search-data
 * @returns {array}
 */
function sort({ data = [] } = {}) {
  let result = [];
  for (const key in data) {
    result.push({
      name: key,
      data: data[key].data,
      count: data[key].count,
      score: data[key].score,
      fuzzy: data[key].fuzzy,
      time: data[key].time,
      locale: data[key].locale,
      highlit: data[key].highlit ? data[key].highlit : {},
    });
  }
  const collator = new Intl.Collator(undefined, {
    numeric: true,
    sensitivity: "base",
  });
  result.sort(
    firstBy(function (a, b) {
      return Object.keys(b.highlit).length - Object.keys(a.highlit).length;
    })
      .thenBy(function (a, b) {
        return b.score - a.score;
      })
      .thenBy(function (a, b) {
        return b.fuzzy - a.fuzzy;
      })
      .thenBy(function (a, b) {
        return collator.compare(b.locale, a.locale);
      })
      .thenBy(function (a, b) {
        return a.time - b.time;
      })
      .thenBy(function (a, b) {
        return collator.compare(a.count, b.count);
      })
  );
  return result;
}

function logAndStore(input) {
  console.log(input);
  output.push(stripAnsi(input));
}

class Search {
  static async process() {
    await Search.data()
      .then(async () => {
        logAndStore(
          concordance({
            data: data,
            keys: keys,
          })
        );
      })
      .then(async () => {
        let tests = await Search.test();
        logAndStore(tests);
      })
      .then(async () => {
        let tests = await Search.consensus();
        logAndStore(tests);
      })
      .then(async () => {
        if (!FS.existsSync(`${base}/tests/data`)) {
          FS.mkdirSync(`${base}/tests/data`, { recursive: true });
        }
        FS.writeFileSync(`${base}/tests/data/search.txt`, output.join("\n"));
        logger.entry({
          message: `${base}/tests/data/search.txt`,
          source: "søkemotortest",
          type: "saved",
          start: start,
          store: true,
        });
      })
      .then(() => {
        logAndStore(
          chalk`{whiteBright ${"Total Time:"}} {cyan ${msToTime(
            performance.now() - start
          )}}`
        );
        process.exit(0);
      });
  }

  static async data() {
    if (!FS.existsSync(`${base}/docs/.vuepress/public/index.json`)) {
      logger.entry({
        message: `${base}/docs/.vuepress/public/index.json`,
        source: "File not found",
        type: "error",
        start: start,
        store: true,
      });
      throw new Error("File not found");
    }
    data = require("../docs/.vuepress/public/index.json");
    data = data.map((item) => ({
      path: item.path ?? "",
      date: item.date ?? "",
      title: item.title ?? "",
      category: item.category ?? "",
      tags: item.tags ?? "",
      author: item.author ?? "",
      content: item.content ?? "",
    }));
    keys = Object.keys(data[0]);
    logAndStore(chalk`{whiteBright ${"Data:"}} {cyan ${data.length}} items`);
  }

  static async test() {
    testsResults = testsGenerator({
      data: data,
      keys: keys,
      query: query,
      originals: true,
      matches: true,
      highlight: true,
      ansi: true,
    });
    const tests = testsIterator({
      tests: testsResults,
      query: query,
      highlight: true,
    });
    return tests;
  }

  static async consensus() {
    return consensus({
      data: sort({
        data: testsResults,
      }),
    });
  }
}

Search.process();

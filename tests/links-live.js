const Logger = require("../generator/lib/logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  { msToTime } = require("../generator/lib/utilities"),
  chalk = require("chalk"),
  link = require("linkinator");
const start = performance.now();
const logger = new Logger();
const base = path.resolve("./");
const url = "https://ntnu-doku.gitlab.io/arkivplan";

class Links {
  static async process() {
    await Links.test().then(() => {
      console.log(
        chalk`{whiteBright ${"Total Time:"}} {cyan ${msToTime(
          performance.now() - start
        )}}`
      );
    });
  }

  /**
   * Test links and log results
   */
  static async test() {
    logger.notice("Starter live lenkesjekk ...");
    try {
      const results = await link.check({
        path: url,
        recurse: true,
      });
      console.log(results);

      const skipped = results.links.filter((link) => link.state === "SKIPPED");
      const valid = results.links.filter((link) => link.state === "OK");
      const broken = results.links.filter((link) => link.state === "BROKEN");
      let output = [];
      for (let i = 0; i < skipped.length; i++) {
        console.log(
          chalk`[{gray ${skipped[i].status}}] ${skipped[i].url.replace(
            url,
            ""
          )}`
        );
        output.push(
          `[${skipped[i].status}] ${skipped[i].url.replace(url, "")}`
        );
      }
      for (let i = 0; i < valid.length; i++) {
        console.log(
          chalk`[{green ${valid[i].status}}] ${valid[i].url.replace(url, "")}`
        );
        output.push(`[${valid[i].status}] ${valid[i].url.replace(url, "")}`);
      }
      for (let i = 0; i < broken.length; i++) {
        console.log(
          chalk`[{red ${broken[i].status}}] ${broken[i].url.replace(url, "")}`
        );
        output.push(`[${broken[i].status}] ${broken[i].url.replace(url, "")}`);
      }
      console.log(
        chalk.whiteBright`${broken.length}/${
          results.links.length
        } links broken, in ${url}, at ${new Date().toISOString()}.`
      );
      output.push(
        `${broken.length}/${
          results.links.length
        } links broken, in ${url}, at ${new Date().toISOString()}.`
      );
      if (!FS.existsSync(`${base}/tests/data`)) {
        FS.mkdirSync(`${base}/tests/data`, { recursive: true });
      }
      FS.writeFileSync(`${base}/tests/data/links.txt`, output.join("\n"));
      logger.entry({
        message: `${base}/tests/data/links.txt`,
        source: "Lenkesjekk resultater",
        type: "saved",
        start: start,
        store: true,
      });
    } catch (error) {
      logger.trace(error);
    }
    await new Promise((resolve) => {
      resolve();
    });
  }
}

Links.process();

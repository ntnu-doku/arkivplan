---
date: '2021-02-04T00:00:00'
title: Om NTNU
status: Ferdig
category: Organisering
tags:
  - Generelt
author: Geir Ekle
---

NTNU er et internasjonalt orientert universitet med hovedsete i Trondheim og campuser i Gjøvik og Ålesund.

NTNU tilbyr 371 studieprogram (2019), samt etter- og videreutdanninger. NTNU har teknisk-naturvitenskapelig hovedprofil, en rekke profesjonsutdanninger og en stor faglig bredde som også inkluderer humaniora, samfunnsvitenskap, økonomi, medisin, helsevitenskap, utdanningsvitenskap, arkitektur, entreprenørskap, kunstfag og kunstnerisk virksomhet.

Vel 48 000 ansatte og studenter har sitt daglige virke ved NTNU.

[Les mer på NTNU.no!](https://www.ntnu.no/om/offentlig-journal/innsyn-i-dokumenter-ved-ntnu)

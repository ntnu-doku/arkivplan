---
date: '2022-12-05T00:00:00'
title: Organisering
status: Ferdig
category: Organisering
tags:
  - Administrativt
author: Ole Vik
---

## Organisasjonskart

![NTNUs organisasjonskart av 2022.](./vedlegg/media/image1.png)

Les mer om NTNUs organisering på [NTNU.no/om](https://www.ntnu.no/om/) og på [Innsida.NTNU.no/organisasjon](https://innsida.ntnu.no/organisasjon).

## Styret

Styret er oppnevnt i medhold av Lov av 01.04.2005 nr. 15 om universiteter og høyskoler kapittel 9. Styret tar avgjørelser i saker av prinsipiell og overordnet karakter. Styret har ansvaret for virksomheten ved universitetet, at den drives innenfor de rammer og retningslinjer som er gitt av departement og storting. Styret skal trekke opp strategiene, fastsette mål og resultatkrav og legge fram regnskap og forslag til budsjett. Det er styret som ansetter rektor.

Styret består av 11 medlemmer. Tre medlemmer representerer det vitenskapelige personalet, én representerer midlertidig ansatte i undervisnings- og forskerstilling og én representerer de teknisk-administrativt ansatte. Studentene har to representanter, mens fire kommer utenfra universitetet (eksterne representanter). Alle styrerepresentantene velges for en fireårsperiode, med unntak av representanten fra de midlertidig ansatte og de to studentene. Disse sitter i ett år.

Rektor er styrets sekretær og daglig leder ved NTNU.

Det er rektor i egenskap av daglig leder som opptrer på vegne av NTNU og som mottar og formidler kontakt mellom styret og omverden i saker der styret treffer vedtak. Rektor rapporterer til styret.

[Styrets forretningsorden](https://www.ntnu.no/adm/styret/forretningsorden)

[Rammer for arbeidet til styrene for universiteter og høyskoler](https://www.regjeringen.no/no/dokumenter/Rammer-for-arbeidet-til-styrene-for-universiteter-og-hoyskoler/id2001906/)

[NTNUs styringsreglement](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Styringsreglement)

## Rektor og rektors lederteam 

Rektor og rektors lederteam består av rektoratet med rektor, tre prorektorer, to direktører, to viserektorer og én prosjektdirektør

- Rektor er daglig leder for både NTNUs faglige og administrative virksomhet. Rektor er sekretær for NTNUs styre dit hen også rapporterer. Som daglig leder opptrer rektor på vegne av NTNU og mottar og formidler kontakt mellom styret og omverdenen i saker der styret treffer vedtak. Rektor har ansvar for NTNUs økonomi og holder styret løpende orientert om regnskap og økonomi. Rektor er ansatt på åremål.

- Prorektor for utdanning har et særlig ansvar for utdanning ved NTNU og skal ivareta strategisk utvikling og kvalitet innenfor utdanningsområdet. Hen leder NTNUs utdanningsutvalg og er medlem i flere nasjonale nettverk på utdanningssiden.  
  Prorektor har også ansvar for NTNU Universitetsbiblioteket.

- Prorektor for forskning er rektors stedfortreder. Hen har et særlig ansvar for oppfølging av forskningsvirksomhet og -strategi ved NTNU. Dette inkluderer også forskerutdanningen. I tillegg koordinerer hen NTNUs internasjonale kontakt.

- Prorektor for nyskaping har i oppgave å ivareta relasjonene til næringslivsaktører regionalt, nasjonalt og internasjonalt og nyskapingsnettverk. Hen ivaretar dessuten kontakten med NTNU Technology Transfer Office (TTO) og koordinerer arbeidet med NTNUs intellektuelle rettigheter (IPR).

- Organisasjonsdirektøren har et overordnet ansvar for organisasjonsutvikling ved NTNU. Hun har i tillegg ansvar for HR og HMS, kommunikasjon, IT og dokumentasjonsforvaltning.

- Økonomi- og eiendomsdirektøren har ansvar for NTNUs virksomhetsstyring, budsjett-, regnskaps- og innkjøpsprosesser, lønn og for eiendomsforvaltning og teknisk drift av bygningsmassen.

- Viserektorenes viktigste oppgaver er å representere og posisjonere NTNU som institusjon overfor regionalt arbeidsliv, koordinere intern faglig virksomhet og utvikle tverrfaglig samarbeid, initiere medvirkningsbaserte prosesser og delta i institusjonens strategiske arbeid.

- Prosjektdirektør for campusutvikling leder campusutviklingen ved NTNU. I dette inngår blant annet samling av campus i Trondheim og NTNUs arbeid med Ocean Space Centre.

## Fellesadministrasjonen

Fellesadministrasjonen driver rådgiving og gir praktisk bistand til NTNUs ledelse og fakulteter. Den drifter og utvikler teknisk-administrative tjenester for hele NTNU.

Prorektor for utdanning, Økonomi- og eiendomsdirektør og organisasjonsdirektør har ansvar for avdelingene i fellesadministrasjonen.

## Ombud

Studentombudet er en uavhengig, nøytral og konfidensiell bistandsperson for studenter ved NTNU, og skal arbeide for å sikre at NTNU ivaretar studentenes rettigheter i studiesituasjonen.

Personvernombudet skal være et kontaktpunkt for de registrerte (de personer som det behandles persondata om) og for Datatilsynet. I mange tilfeller har NTNU en rådføringsplikt med personvernombudet. Dette trer inn i stedet for meldeplikt og konsesjonsplikt. Personvernombudet skal informere om forpliktelsene man har etter de nye reglene. Ombudet skal også kontrollere at regelverket overholdes og at personvernkonsekvenser vurderes der det kan være høy risiko for personers personvern og øvrige interesser. Man kan søke generell veiledning hos personvernombudet. NTNU som behandlingsansvarlig skal sørge for at personvernombudet involveres i rett tid og på riktig måte i personvernspørsmål.

## Faglige enheter

NTNU er organisert i åtte fakulteter, femtifem institutter og NTNU Vitenskapsmuseet. Fakultetene ledes av en dekan. Dekan ansettes på åremål av NTNUs styre. Fakultetene har prodekaner innenfor områder som forskning undervisning og innovasjon. Instituttene ledes av en instituttleder. Instituttleder ansettes på åremål.

Vitenskapsmuseet ledes av en museumsdirektør.

### Det humanistiske fakultet

Forskning ved Det humanistiske fakultet spenner fra grunnforskning innen filosofi, via historie, kunstfag og litteratur, til anvendt forskning innen blant annet språkvitenskap. I tillegg har vi kunstfaglige miljøer som jobber med kunstnerisk utviklingsarbeid av høy internasjonal kvalitet. Vi tilbyr studier på bachelor- master- og ph.d.-nivå innen et bredt spekter av humanistiske fagdisipliner, i tillegg til etter- og videreutdanning.

### Fakultet for medisin og helsevitenskap

Fakultet for medisin og helsevitenskap har et bredt spekter av forskningsmiljøer innenfor fagområdene medisin og helse. Forskningen spenner fra grunnforskning til translasjonsforskning og anvendt forskning. Vi tilbyr utdanning innenfor medisin og helse, og utdanner kvalifiserte kandidater til de fleste områder i helsevesenet, samt helserelatert forskning, næringsliv og forvaltning.

### Fakultet for arkitektur og design

Med en kritisk og tverrfaglig tilnærming til samfunnsutfordringene utvikler vi ny kunnskap innen arkitektur, byplanlegging, design og billedkunst med mennesket i sentrum og med vekt på bærekraft, helse, estetikk, etikk, teknologi, samt innovative prosesser og læringsformer. Vi tilbyr 15 studieprogram på master- og bachelornivå, innen arkitektur, byplanlegging, design og billedkunst. I tillegg tilbyr vi etter- og videreutdanning. Vi utfordrer studentene til å være aktive og utforskende, og vi legger vekt på læring gjennom handling.

### Fakultet for informasjonsteknologi og elektroteknikk

Forskningen ved Fakultet for informasjonsteknologi og elektroteknikk adresserer utfordringer som spenner fra basisforskning innen matematikk, IKT, kybernetikk, nano- og mikroelektronikk, til internasjonale forskningsutfordringer innen energi, helse og velferd, IKT-sikkerhet, maritime og arktiske operasjoner. Vi er Norges største aktør på utdanning innenfor våre fagområder både på bachelor-, master- og ph.d.-nivå.

### Fakultet for ingeniørvitenskap

Fakultet for ingeniørvitenskap tilbyr bachelor- og masterstudier i teknologi, realfagstudier, internasjonale masterprogrammer og en rekke doktorgradsstudier. Det finnes mer enn 50 studieprogram å velge mellom. Fakultetet driver langsiktig forskning med vekt på forskningsområder som bidrar til en samfunnsutvikling preget av bærekraft og nyskaping. For å skape sterkeste mulige faggrupper og oppnå best mulig kvalitet, utnyttes fakultetets og institusjonens samlede kompetanse organisert i forskningsprogrammer.

### Fakultet for naturvitenskap

NV-Fakultetets forskning adresserer globale utfordringer innenfor bærekraft, energi, klima, miljø, mat, vann, helse og velferd. Våre forskningsaktiviteter spenner fra grunnleggende forskning innenfor biologi, fysikk og kjemi, til muliggjørende teknologier som bioteknologi, materialteknologi og nanoteknologi. Ved Fakultet for naturvitenskap tilbyr vi en rekke framtidsrettede studieprogram innenfor realfag og teknologi.

### Fakultet for samfunns- og utdanningsvitenskap

Forskningen vår skal bidra til å løse de sentrale utfordringene samfunnet står overfor globalt og lokalt, og vi leverer forskning av høy internasjonal kvalitet. i tilbyr studier på bachelor- master- og ph.d.-nivå innen samfunns- og utdanningsvitenskapelige disipliner og profesjoner i tillegg til etter- og videreutdanning.

### Fakultet for økonomi

Forskningen ved Fakultet for økonomi favner bredt innen økonomi, ledelse og teknologi, og er i stor grad preget av tverrfaglighet og regionalt, nasjonalt og internasjonalt samarbeid. Fakultetets fagmiljøer er viktige bidragsytere til norsk industri og økonomisk politikk. Vi tilbyr 26 ulike bachelor-, masterstudier og profesjonsutdanninger. I tillegg har vi et bredt videreutdanningstilbud for ledere og andre som er i jobb.

### NTNU Vitenskapsmuseet

Vitenskapsmuseet er ett av Norges seks universitetsmuseer og en del av Norges teknisk-naturvitenskapelige universitet, NTNU. De ansatte ved Institutt for arkeologi og kulturhistorie studerer forhistorisk, historisk, maritim og samisk arkeologi. Instituttet har et konserverings laboratorium samt arkeologiske samlinger og samlinger med andre kulturgjenstander. Institutt for naturhistorie har forskning innenfor biogeografi, biosystematikk og økologi med særlig fokus på bevaringsbiologi. Instituttet har ansvaret for å bygge opp og vedlikeholde museets naturhistoriske samlinger, inkludert de botaniske hagene. Nasjonallaboratoriene utfører datering av arkeologiske og naturmaterialer ved hjelp av både karbondatering (14C) og dendrokronologi. Laboratoriet for 14C-datering utfører karbondatering av organisk natur- og kulturmateriale. Seksjonen er ansvarlig for museets aktiviteter og arrangementer for publikum, samt for skoleprogrammene og andre læringstilbud. Seksjonen har også ansvar for utforming og vedlikehold av museets utstillinger.

## Sentrale råd og utvalg

Lenkene under peker til beskrivelser på NTNU.no og Innsida.

[NTNUs styre](https://www.ntnu.no/adm/styret)

[Rektoratet](https://www.ntnu.no/rektor)

[Dekanmøtet](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Dekanm%C3%B8tet)

### Råd

[Ansettelsesråd](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Tilsettingsr%C3%A5d+for+teknisk-administrative+stillinger)

[NTNUs råd for samarbeid med arbeidslivet](https://www.ntnu.no/rsa) (RSA)

[Studieprogramrådene](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Studieprogramr%C3%A5d)

[Studentrådene](https://studentrad.no/)

### Utvalg

[Arbeidsmiljøutvalget](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Arbeidsmilj%C3%B8utvalget+-+AMU)

[Ansettelsesutvalg](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Ansettelsesutvalg+for+undervisnings-+og+forskerstillinger)

[Utvalg for likestilling og mangfold](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Utvalg+for+likestilling+og+mangfold)

[Forskningsutvalget](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Forskningsutvalget)

[Forskningsetisk utvalg](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Forskningsetisk+utvalg)

[Felles forskningsutvalg for St- Olavs hospital og Fakultet for medisin og helsevitenskap](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Felles+forskningsutvalg+for+St.+Olavs+Hospital+og+Fakultet+for+medisin+og+helsevitenskap)

[Felles utdanningsutvalg for St- Olavs hospital og Fakultet for medisin og helsevitenskap](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Felles+utdanningsutvalg+for+St.+Olavs+Hospital+og+Fakultet+for+medisin+og+helsevitenskap)

[Forvaltningsutvalget for de 5-årige lektorutdanningene](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Forvaltningsutvalget+for+de+5-%C3%A5rige+lektorutdanningene+-+FUL) (FUL)

[Forvaltningsutvalget for siv.ing.-utdanningen](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Forvaltningsutvalget+for+sivilingeni%C3%B8rutdanningen+-+FUS) (FUS)

[Forvaltningsutvalget for ingeniørutdanningen](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Forvaltningsutvalget+for+ingeni%C3%B8rutdanningen+-+FUI) (FUI)

[Læringsmiljøutvalget](https://innsida.ntnu.no/wiki/-/wiki/Norsk/L%C3%A6ringsmilj%C3%B8utvalget+-+LMU) (LMU)

[Utdanningsutvalget](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Utdanningsutvalget+-+UU) (UU)

### Nemder

[Klagenemnd](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Klagenemnda)

[Skikkethetsnemnd](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Skikkethetsvurdering)

[Nemd for sidegjøremål](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Saksbehandling+av+sidegj%C3%B8rem%C3%A5l)

### Annet

[Sentralt samarbeidsorgan](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Sentralt+samarbeidsutvalg+-+SESAM) (SESAM)

[Lokalt samarbeidsorgan](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Lokalt+samarbeidsutvalg+-+LOSAM) (LOSAM)

[Fakultetsforum](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Fakultetsforum)

[Tilretteleggingsforum](https://o365addins.it.ntnu.no/RadUtvalg/Tilretteleggingsforum)

[Studenttinget](https://studenttinget.no/)

[IT-styret](https://o365addins.it.ntnu.no/RadUtvalg/IT-styret)

[Prosjektstyret Campusutvikling](https://www.ntnu.no/campusutvikling)

[Styringsgruppe samlokaliseringsprosjektet](https://web.archive.org/web/20200926092027/https:/www.ntnu.no/samlokalisering)

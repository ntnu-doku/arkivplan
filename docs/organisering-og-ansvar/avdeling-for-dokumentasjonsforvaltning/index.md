---
date: '2020-12-17T00:00:00'
title: Avdeling for dokumentasjonsforvaltning
status: Ferdig
category: Organisering
tags:
  - DOKU
  - Ansvarsområder
  - Bevare
author: Geir Ekle
---

Avdeling for dokumentasjonsforvaltning, heretter omtalt som DOKU, ble etablert som avdeling 01.01.2017 som følge av fusjonen mellom NTNU, Høgskolen i Sør-Trøndelag, Høgskolen i Ålesund og Høgskolen i Gjøvik.

DOKU er en støttetjeneste innenfor fellesadministrasjonen og yter tjenester til alle enheter ved NTNU. DOKUs oppgave er å sørge for at dokumentasjonsplikten innenfor administrative prosesser — forvaltningen — overholdes.

DOKU har overordnet ansvar for sak- og arkivsystemet, samt for historiske arkiver inntil de avleveres til Arkivverket. Private arkiver og forskningsarkiver ivaretas ikke av avdelingen, men av Universitetsbiblioteket på NTNU.

Dokumentasjonsforvaltning er mer enn å arkivere et sluttprodukt — det er en dynamisk og gjennomgripende del av enhver arbeidsprosess, for å sikre autentisk og tilstrekkelig dokumentasjon av alle transaksjoner og handlinger. Avdelingen støtter saksbehandlere og ledere i daglig dokumentasjonsflyt, gjennom opplæring i systemer, rutiner og lovverk, og gjennom å bidra direkte inn i utviklingen av gode saksprosesser.

Avdelingen er inndelt i tre grupper: Dokumentasjonsforvaltning, Kompetanse og Utvikling.

## Dokumentasjonsforvaltning

- Yter tjenester til saksbehandlere og ledere i NTNU når det gjelder dokumentasjonsflyt og -sikkerhet

- Kvalitetssikring av og tilgangsstyring til ePhorte

- Oppfylle NTNUs krav om meroffentlighet gjennom å kvalitetssikre og publisere offentlig journal

- Koordinerende rolle ved henvendelser av innsyn i dokumenter

- Ansvar for å holde vedlike og gjøre tilgjengelig NTNUs historiske arkiv og fjernarkiv

- Veiledning innen offentlig forvaltning av dokumentasjon og lovverk

## Kompetanse

- Ivareta avdelingens utadrettede virksomhet når det gjelder opplæring, brukerstøtte, veiledning og formidling

- Sørge for ansvar for sikker og effektiv saksbehandling i de systemene NTNU til enhver tid benytter

- Kursing, både i klasserom og digitalt, i bruk av ePhorte

- Tar imot henvendelser på telefon og e-post, og gir personlig oppfølging eller hjelp via skjermdeling eller Skype

- Råd om forvaltning og arkivdanning

- Pedagogisk arbeid med opplæring og støtte av saksbehandlere, lager eLæringskurs, veiledningsvideoer og skriftlige brukerveiledninger

## Utvikling

- Ansvar for dokumentering av prosesser rundt integrasjoner av systemer og arkivarkitektur, men bidrar også til utvikling av slike prosesser

- Ivaretar systemadministrasjonen i arkivkjernen

- Styrer prosjekter internt på avdelingen og eksternt med samarbeidspartnere

- Kompetanse på planlegging, gjennomføring, evaluering og dokumentering av prosjekter, samt effektivisering, automatisering og digitalisering av arbeidsprosesser. Dette omhandler ofte dokumentasjonsforvaltning og rutineutvikling, men også videreutvikling av disse og kompetansebygging

---
date: '2020-11-26T00:00:00'
title: Ansvar og fullmakter
status: Ferdig
category: Formelt
tags:
  - Delegasjon
  - Styring
  - Ansvarsområder
author: Geir Ekle
---

**Overordnet arkivansvar**  
  
Arkivloven § 6 og arkivforskriften § 1 hjemler arkivplikt og overordnet ansvar for arkivarbeidet.

Offentlige organ plikter å ha arkiv. Arkiv skal være ordnet og innrettet slik at dokumentene er sikret som informasjonskilder både for samtid og ettertid.

Det er den øverste ledelsen i et organ som har det overordnede ansvaret for arkivarbeidet i organet. Ved NTNU er dette organisasjonsdirektøren. I ansvaret ligger organisering og oppfølging av at arkivarbeidet blir utført i tråd med de lover og regler som gjelder for offentlig arkiv. Det vil si Arkivloven og andre lover som vedrører arkiv og forskrifter til disse lovene. Ledelsen må forsikre seg om at arkivarbeidet holder mål, og det er ledelsen som står ansvarlig utad på organets vegne.

Arkivarbeidet utføres av Avdeling for dokumentasjonsforvaltning, heretter omtalt som DOKU.

## **Arkivleder – avdeling for dokumentasjonsforvaltning**

Ansvar for at arkivfunksjonen er løst i henhold til de krav som stilles i lov, forskrift og regelverk er lagt til DOKU ved avdelingsleder. Avdelingsleder ivaretar fagansvaret for NTNUs arkiver og ivaretakelse av dokumentasjonsforvaltning, kvalitet, kompetanse og servicenivå innenfor funksjonsområdet til avdelingen. Avdelingsleder har ansvar for arkivfaglig/strategisk utvikling og kvalitetssikring, samt systemansvar for NTNUs elektroniske sak- og arkivsystem. Avdelingsleder har ansvar for at avdelingen utvikler tjenester til det beste for NTNUs samlede virksomhet. Avdelingsleder har overordnet personal-, tjeneste- og økonomiansvar i avdelingen.

## **Leders ansvar - enhetene**

Alle som har lederansvar og eller beslutningsmyndighet ut fra stilling eller delegering, har ansvar for å:

- Benytte NTNUs elektroniske sak- og arkivsystem i saksbehandlingsprosesser

- Sjekke om det er mottatte dokumenter til behandling eller oppgaver til godkjenning

- Sørge for at mottatte dokumenter blir fordelt

- Holde oversikt over restanser og saksbehandling, og at restanseoppfølging gjennomføres ved enheten

- Holde DOKU orientert om relevante organisasjonsendringer eller omorganisering av ansvarsområder

- Informere DOKU om avsluttede arbeidsforhold og nytilsettinger for å sikre korrekte tilganger til sak- og arkivsystemet

- Sørge for at saksbehandlingen og arkivarbeidet overholder gjeldende lover, regelverk og rutiner

## **Saksbehandlers ansvar - enhetene**

Saksbehandlerne har et viktig medansvar for at NTNUs elektroniske sak- og arkivsystem skal fungere som et effektivt og komplett arkiv. Dette innebærer å arkivere dokumenter som er saksdokumenter for NTNU. Offentlighetsloven § 4 hjemler hva som er saksdokument.

Saksbehandlerne har ansvar for å:

- Bruke sak- og arkivsystemet i saksbehandlingen og følge rutiner for saksområdene

- Sjekke om saksdokumenter er mottatt til behandling eller oppgaver er mottatt til oppfølging og godkjenning

- Kontrollere mottatte saksdokumenter for å sikre at dokumentene framstår korrekt, komplett og er lesbare

- Registrere direkte mottatte saksdokumenter selv for journalføring og arkivering

- Avskrive restanser og sørge for at dokumenter under arbeid ferdigstilles

- Foreta offentlighetsvurdering av saksdokumenter og bidra til at dokumenter unntatt offentlighet blir påført riktig lovhjemmel

- Sørge for at det ikke gjenstår dokumenter under arbeid, og påse at alle restanser er avskrevet eller omfordelt ved arbeidsforholdets avslutning

Sjekkliste for saksbehandlere (vedlegg)

::: info Sjekkliste for saksbehandlere

Vi har også en sjekkliste du kan følge daglig, ukentlig, ved registrering og ved avslutning av arbeidsforholdet! Se siden «[Sjekkliste for saksbehandlere](/arkivplan/rutiner/sjekkliste-for-saksbehandlere)».

:::

## **Systemeier** 

DOKU er systemeier for NTNUs sak- og arkivsystem. IT-Forvaltning forvalter NTNUs IT-tjenester og infrastruktur. Dette innbefatter koordinering og styring av både administrative systemer og basis IT-systemer gjennom hele IT-systemenes livsløp. DOKU i samarbeid med IT-Forvaltning har følgende ansvar:

- Kontraktsmessige forhold overfor leverandøren

- Kontakt med leverandøren

- Forvaltning og drift av systemet

- Dokumentasjonen av systemet

- Systemadministrasjon, teknisk drift og vedlikehold

- Budsjettmessige forhold

## **Systemansvar** 

DOKU har systemeierskap- og -ansvar for sak- og arkivsystemet:

- Konfigurasjon og oppsett

- Tilgjengeliggjøring og oppdatering av maler

- Opprette, autorisere og avslutte brukere etter anvisning fra ansvarlig innmelder

- Kurs og veiledning

- Utarbeide og vedlikehold av veiledninger

- Vedlikeholde NTNUs nettsider for DOKU

- Periodisering av databasen

- Test av moduler og nye versjoner

- Rapportere eventuelle tekniske feil, mangler og nedetid til IT-avdelingen

- Brukerstøtte

- Kontakt overfor leverandørens brukerstøtte

## **Teknisk drift og vedlikehold**

Ansvaret for teknisk drift og vedlikehold av systemet hører inn under IT-avdelingen. Det er inngått egen avtale mellom DOKU, IT Forvaltning og IT Drift om drift og forvaltning av systemet. IT Drift skal:

- Bistå DOKU ved konfigurasjon av systemet og integrasjon og samspill med andre systemer

- Bistå DOKU i relasjon til leverandør av systemet

- Rådgi DOKU med hensyn til oppfyllelse av krav til arkitektur og sikkerhet

- Rådgi DOKU vedr dataleveranser til/fra andre systemer

- Ha ansvar for drift av systemet slik at det er operativt og tilgjengelig for brukerne

- Ha ansvar for drift av applikasjonen og håndtere tekniske feil

- I applikasjonen inngår ePhorte-web, ePhorteOutlook, databasen og dokument-konvertering, samt modulene:

  - eInnsyn

  - Møte og utvalg

  - eFormidling

  - eSignering

- Ha ansvar for nødvendig lagringsplass og sikkerhetskopiering

- Installere nye versjoner av sak- og arkivsystemet etter bestilling fra DOKU

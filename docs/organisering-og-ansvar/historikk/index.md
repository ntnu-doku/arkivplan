---
date: '2025-01-06T00:00:00'
title: Historikk
status: Ferdig
category: Organisering
tags:
  - Navneendring
author: Geir Ekle
---

##  NTNUs historie

NTNU er en ung institusjon med en lang historie. Norges teknisk-naturvitenskapelige universitet ble etablert 1. januar 1996, ved en omdanning av Universitetet i Trondheim. Fundamentet for universitetet var de tre tradisjonsrike institusjonene Norges tekniske høgskole (NTH), Den allmennvitenskapelige høgskolen (AVH), Vitenskapsmuseet samt Det medisinske fakultetet. I det nye universitetet inngikk også Musikkonservatoriet og Kunstakademiet.

Røttene går imidlertid mye lenger tilbake. I 1760 ble Det Kongelige Norske Videnskabers Selskab stiftet. Vitenskapsmuseet og Gunnerusbiblioteket stammer herfra. Ingeniørutdanningen i Trondheim begynte med Trondhjems Tekniske Læreanstalt i 1870. I 1910 åpnet NTH, og tolv år senere kom Norges Lærerhøgskole (senere AVH). NTNUs eldste fagmiljø, Bergteknikk, kan føre sin historie tilbake til Bergseminaret som åpnet på Kongsberg i 1757.

I 1968 ble Universitetet i Trondheim (UNIT) opprettet på bakgrunn av vedtak i Stortinget 28. mars. UNIT fungerte som en administrativ overbygning over NTH, Lærerhøgskolen, og museet og biblioteket ved vitenskapsselskapet. Høyskolene og museet fortsatte å fungere som relativt autonome institusjoner, fram til opprettelsen av NTNU 1. januar 1996.

I 2016 ble NTNU og høyskolene i Sør-Trøndelag, Gjøvik og Ålesund slått sammen.

## Tidslinje – overordnet

### 1700-tallet

1757 Det Kongelige Norske Bergseminarium åpner på Kongsberg. NTNUs eldste fagmiljø - bergteknikk - sporer sin historie hit  
1760 DKNVS stiftes - Norges første vitenskapsselskap  
1768 DKNVs’ bibliotek grunnlagt - i dag Gunnerus-biblioteket - Norges eldste bibliotek

### 1800-tallet

1833 Første forslag om et norsk polyteknisk institutt  
1870 Trondhjems tekniske Læreanstalt grunnlegges. Nedlagt 1916

### 1900-1949

1900 Vedtak om opprettelse av Den tekniske høiskole i Trondhjem 31. mai  
1910 NTH åpner 15. september  
1910 Studentersamfundet i Trondhjem stiftet  
1921 Tapir blir stiftet - studenter og ansatte på NTH etablerer sitt eget utsalg av skrivesaker og rekvisita  
1922 Norges Lærerhøgskole (NLHT) etableres på Lade gård  
1948 Studentsamskipnaden i Trondheim (SiT) blir etablert  
1949 Sivilingeniørtittelen blir beskyttet som yrkestittel

### 1950-1999

1950 SINTEF stiftes - Selskapet for industriell og teknisk forskning  
1956 Berg studentby åpner  
1960 Norges lærerhøgskole flytter til Rosenborg  
1960-tallet - Stor utbygging av NTH  
1968 Universitetet i Trondheim (UNIT) etableres  
1968 Tidligere NTH student Lars Onsager tildeles Nobelprisen i kjemi  
1973 Tidligere NTH student Ivar Gievær tildeles Nobelprisen i fysikk  
1973 Musikkonservatoriet  
1974 Avdeling for medisin (senere Det medisinske fakultet) opprettes under UNIT  
1978 Universitetssenteret på Dragvoll åpnes  
1984 Lærerhøgskolen bytter navn til Den allmennvitenskapelige høgskolen (AVH)  
1996 NTNU etableres jf. Innst. O. nr. 40 (1994-1995), Ot.prp. nr. 85 (1993-94)  
1999 Realfagbygget åpner på campus Gløshaugen

### 2010 Jubileumsår

2014 Nobelprisen i fysiologi eller medisin 2014 ble tildelt hjerneforskerne May-Britt Moser og Edvard Moser ved NTNU og John O’Keefe ved University College London

2015 Fusjon. NTNUs styre vedtar 28. januar fusjon med Høgskolen i Gjøvik, Høgskolen i Ålesund og Høgskolen i Sør-Trøndelag

Ved kongelig resolusjon 19. juni 2015 ble det bestemt at NTNU, Høgskolen i Sør-Trøndelag, Høgskolen i Ålesund og Høgskolen i Gjøvik fra 1. januar 2016 skal organiseres som ett universitet under navnet Norges teknisk-naturvitenskapelige universitet (NTNU).

2016 Fra 1. januar slo NTNU seg sammen med høgskolene i Ålesund, Gjøvik og Sør-Trøndelag. Fusjonsprosjektet ble samtidig avsluttet

## Tidslinje – enhetene

### NTH 1910-1995

#### 1910

Arkitektavdelingen  
Bergavdelingen  
Bygningsingeniøravdelingen  
Elektroteknisk avdeling  
Kjemiavdelingen  
Maskinavdelingen  
Almenavdelingen

#### 1973

Skipsteknisk avdeling (utskilt fra Maskinavdelingen)

#### 1984

Avdeling for økonomiske og administrative fag  
Avdeling for fysikk og matematikk  
Avdeling for elektroteknikk og datateknikk  
Marinteknisk avdeling

Almenavdelingen delt i Avdeling for økonomiske og administrative fag og Avdeling for fysikk og matematikk i 1984. Skipsteknisk avdeling skifter navn til Marinteknisk avdeling. Avdeling for elektroteknikk og datateknikk skiftet navn til Avdeling for elektro- og datateknikk i 1986.

#### 1992

Fakultet for arkitektur  
Fakultet for berg-, petroleums- og metallurgifag  
Fakultet for bygningsingeniørfag  
Fakultet for elektro- og datateknikk  
Fakultet for fysikk og matematikk  
Fakultet for kjemi og kjemisk teknologi  
Fakultet for marin teknikk  
Fakultet for maskinteknikk  
Fakultet for økonomi og arbeidslivsvitenskap (Fusjonert med Det samfunnsvitenskapelige fakultet ved opprettelsen NTNU i 1996)

### NLHT 1922-1984

#### 1963

Avdeling for realfag  
Avdeling for filologiske fag  
Avdeling for samfunnsfag

### AVH 1984-1995

#### 1984

Det historisk filosofiske fakultet  
Det matematisk- naturvitenskapelige fakultet  
Det samfunnsvitenskapelige fakultet (Fusjonert med Fakultet for økonomi og arbeidslivsvitenskap ved opprettelsen av NTNU i 1996)

### Vitenskapsmuseet

#### 1760

Museet etablert som del av Det Kongelige Norske Vitenskabers Selskap (DKNVS)

#### 1926

DKNVS delt - Vitenskapsselskapet og museet. Museet egen administrasjon i 1960

#### 1984

Museet gikk fra stiftelse til å bli en del av Universitetet i Trondheim (UNiT)

#### 1992

Fakultet for arkeologi og kunsthistorie  
Fakultet for naturhistorie

Ved overgang til NTNU, mistet Vitenskapsmuseet sine fakulteter

### Medisin

#### 1974

Avdeling for medisin  
Underlagt Interimsstyret

#### 1984

Det medisinske fakultet  
Egen enhet direkte under Universitetets styre, som NTH, AVH og Vitenskapsmuseet

### NTNU

#### 1996

Fakultet for arkitektur, plan og billedkunst  
Fakultet for marin teknikk  
Det medisinske fakultet  
Fakultet for samfunnskunnskap og teknologiledelse  
Historisk-filosofisk fakultet  
Fakultet for bygg- og miljøteknikk  
Fakultet for elektroteknikk og telekommunikasjon  
Fakultet for fysikk, informatikk og matematikk  
Fakultet for geofag og petroleumsteknologi  
Fakultet for kjemi og biologi  
Fakultet for maskinteknikk

#### 2002

HF - Historisk-filosofisk fakultet  
SVT - Fakultet for samfunnskunnskap og teknologiledelse  
AB – Fakultet for arkitektur og billedkunst  
IME – Fakultet for informasjonsteknologi, matematikk og elektronikk  
IVT – Fakultet for ingeniørvitenskap og teknologi  
NT – Fakultet for naturvitenskap og teknologiledelse  
DMF – Det medisinske fakultet

#### 2010

HF - Det humanistiske fakultet (kun navneendring)

#### 2013

Regionalt kunnskapssenter for barn og unge – Psykisk helse og barnevern (RKBU Midt-Norge).  
Fusjon Regionsenter for barn og unges psykiske helse Midt-Norge (RBUP) ved NTNU, og Barnevernets utviklingssenter i Midt-Norge (BUS) ved NTNU Samfunnsforskning AS

Det humanistiske fakultet endrer instituttstrukturen. Tre nye institutt opprettet. Institutt for språk– og kommunikasjonsstudier, Institutt for nordistikk og litteraturvitenskap og Institutt for moderne fremmedspråk slått sammen til ett institutt; Institutt for språk og litteratur. Institutt for arkeologi og religionsvitenskap delt opp. Arkeologi slått sammen med Institutt for historie og klassiske fag. Nytt institutt med navn Institutt for historiske studier. Religionsvitenskap slått sammen med Filosofisk institutt. Nytt institutt med navn Institutt for filosofi og religionsvitenskap. Institutt for kunst- og medievitenskap, Institutt for musikk og Institutt for tverrfaglige kulturstudier videreført.

#### 2014

Institutt for bevegelsesvitenskap ble overført fra Fakultet for samfunnsvitenskap og teknologiledelse til Det medisinske fakultet med virkning fra 1. januar 2014.

#### 2015

Med bakgrunn i SAKS-prosessen (samarbeid, arbeidsfordeling, konsentrasjon, sammenslåing) vedtar NTNUs styre 28. januar følgende: Styret ønsker at NTNU fusjonerer med Høgskolen i Gjøvik, Høgskolen i Ålesund og Høgskolen i Sør-Trøndelag. Fusjonsprosess igangsettes.

Ved kongelig resolusjon 19. juni ble det bestemt at NTNU, Høgskolen i Sør-Trøndelag, Høgskolen i Ålesund og Høgskolen i Gjøvik fra 1. januar 2016 skal organiseres som ett universitet under navnet Norges teknisk-naturvitenskapelige universitet (NTNU).

#### 2016

1/. januar slo NTNU seg sammen med høgskolene i Ålesund, Gjøvik og Sør-Trøndelag. Fusjonsprosjektet ble samtidig avsluttet.  
  
1. januar opphørte Pedagogisk institutt og Institutt for voksnes læring og rådgivningsvitenskap. Nytt institutt etablert fra same dato under navnet Institutt for pedagogikk og livslang læring.

14/. april 2016 vedtok NTNUs styre nye fakultetsnavn.

6/. oktober 2016 besluttet rektor nye instituttnavn.

18/. oktober 2016 besluttet rektor nye navn på avdelinger i fellesadministrasjonen.

#### 2017

Fra 1. august 2017 blir Institutt for kreftforskning og molekylærmedisin og Institutt for laboratoriemedisin, barne- og kvinnesykdommer slått sammen til ett institutt - Institutt for klinisk og molekylær medisin.

#### 2018

Fra 1. januar 2018 ble Artsdatabanken skilt ut fra NTNU som et selvstendig forvaltningsorgan underlagt Klima- og miljødepartementet. Fra samme tidspunkt ble BIBSYS, CERES og deler av UNINETT A/S slått sammen til et nytt tjenesteorgan. Nasjonalt senter for matematikk i opplæringen, Matematikksenteret, og Nasjonalt senter for skriveopplæring og skriveforsking, Skrivesenteret, ble fra 1. januar tilknyttet Institutt for lærerutdanning. Nasjonalt senter for realfagsrekruttering ble fra 1. januar tilknyttet Fakultet for informasjonsteknologi og elektroteknikk.

#### 2019

Fra 1. november 2019 ble Institutt for historiske studier delt etter NTNUs styrevedtak 31.10.2019. Nytt institutt med basis i forskergruppa “Fate of Nations” opprettes. Resterende miljø fortsetter som eget institutt.

#### 2020

Fra 12. februar 2020. NTNUs styrevedtak for nye instituttnavn for tidligere Institutt for historiske studier: Institutt for historiske og klassiske studier og Institutt for moderne samfunnshistorie.

#### 2021

Universell har siden 2003 hatt i oppdrag fra Kunnskapsdepartementet å være nasjonal pådriver for inkludering, universell utforming, tilrettelegging og læringsmiljøutvalg i høyere utdanning og fagskoleutdanning. Universell har vært organisatorisk tilknyttet NTNU. Fra 1. juli 2021 inngår Universell som del av Direktoratet for høgare utdanning og kompetanse (HK-dir).

Regionalt samarbeidsorgan Midt-Norge er et rådgivende organ for saker innen utdanning, forskning og innovasjon, og er et samarbeid mellom Helse Midt-Norge RHF og universiteter og høgskoler i regionen. Fra 1. oktober 2021 overføres sekretariatet for samarbeidsorganet fra NTNU MH-fakultetet til Helse Midt-Norge, Stjørdal.

#### 2022

Ved IE-fakultetet skiftet Institutt for elkraftteknikk navn til Institutt for elektrisk energi.

#### 2024

Ved IV-fakultetet skiftet Institutt for geovitenskap og petroleum navn til Institutt for Geovitenskap.

Ny organisering i fellesadministrasjonen fra 1. mars 2024. Avdeling for virksomhetsstyring etablert med følgende fire seksjoner: Seksjon for virksomhetsstyring, Seksjon for organisasjons- og tjenesteutvikling, Seksjon for sikkerhet og beredskap og Seksjon for arkiv og dokumentasjonsforvaltning.

#### 2025

Institutt for geografi og Institutt for sosialantropologi slås sammen til ett institutt fra 1. januar 2025. Nytt er navn er Institutt for geografi og sosialantropologi.

Prorektor for samfunnsansvar og formidling etablert fra 1. januar 2025.

Omorganisering i fellesadministrasjonen fra 1. januar 2025. Seksjon for virksomhetsstyring og Seksjon for organisasjons- og tjenesteutvikling slås sammen til en seksjon. Nytt navn er Seksjon for utvikling og styring. Seksjon for arkiv og dokumentasjonsforvaltning endrer navn til Seksjon for saksbehandling og dokumentasjonsforvaltning.

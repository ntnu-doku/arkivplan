---
date: '2021-01-29T00:00:00'
title: Organisering og ansvar
status: Ferdig
category: Oversikt
author: Geir Ekle
---

NTNU er underlagt Kunnskapsdepartementet. Styret er NTNUs øverste organ og representerer institusjonen overfor offentlige myndigheter. Rektor er styrets sekretær og daglig leder ved NTNU. NTNUs virksomhet er organisert i åtte fakulteter og Vitenskapsmuseet.

---
date: '2021-07-30T00:00:00'
title: 'Lov, forskrift, regelverk - overordnet'
status: Ferdig
category: Regelverk
tags:
  - Bestemmelse
author: Geir Ekle
---

Disse lover og forskrifter er førende for hvordan saksbehandling og forvaltning, samt arkivering, gjennomføres.

## Arkivlova

- [Forskrift om offentlige arkiv](https://lovdata.no/dokument/SF/forskrift/2017-12-15-2105?q=forskrift%20om%20arkiv)

- [Forskrift om utfyllende tekniske og arkivfaglige bestemmelser om behandling av offentlige arkiver](https://lovdata.no/dokument/SF/forskrift/2017-12-19-2286?q=forskrift%20om%20utfyllende) (riksarkivarens forskrift)

## Lov om elektroniske tillitstjenester

- [Forskrift om tillitstjenester for elektroniske transaksjoner](https://lovdata.no/dokument/SF/forskrift/2019-11-21-1577)

- [Forskrift om elektronisk kommunikasjon med og i forvaltningen](https://lovdata.no/dokument/SF/forskrift/2004-06-25-988) (eForvaltningsforskriften)

- [Forskrift om krav til utsteder av kvalifiserte sertifikater mv.](https://lovdata.no/dokument/LTI/forskrift/2001-06-15-611)

## Forvaltningsloven

- [Forskrift til forvaltningsloven](https://lovdata.no/dokument/SF/forskrift/2006-12-15-1456) (forvaltningslovforskriften)

- [Forskrift om elektronisk kommunikasjon med og i forvaltningen](https://lovdata.no/dokument/SF/forskrift/2004-06-25-988) (eForvaltningsforskriften)

- [Forskrift om IT-standarder i offentlig forvaltning](https://lovdata.no/dokument/SF/forskrift/2013-04-05-959)

## Offentlighetsloven

- [Forskrift til offentlighetsloven](https://lovdata.no/dokument/SF/forskrift/2008-10-17-1119) (offentlighetsforskriften)

- [Rettleder til offentlighetsloven](https://www.regjeringen.no/no/dokumenter/rettleiar-til-offenntleglova/id590165/)

## Personopplysningloven

- [Forskrift om behandling av personopplysninger](https://lovdata.no/dokument/SF/forskrift/2018-06-15-876) (personopplysningsforskriften)

## Pliktavleveringsloven

- [Forskrift om avleveringsplikt for allment tilgjengelige dokument](https://lovdata.no/dokument/SF/forskrift/2018-07-01-1139?q=forskrift%20om%20avleveringsplikt)

- [Instruks for institusjoner som forvalter dokument innkomne etter lov om avleveringsplikt for allment tilgjengelige dokument](https://lovdata.no/dokument/INS/forskrift/1990-05-25-4696)

## Sikkerhetsloven

- [Instruks for behandling av dokumenter som trenger beskyttelse av andre grunner enn nevnt i sikkerhetsloven med forskrifter](https://lovdata.no/dokument/INS/forskrift/1972-03-17-3352) (beskyttelsesinstruksen)

## Universitets- og høyskoleloven

- [Forskrift om studier ved Norges teknisk-naturvitenskapelige universitet (NTNU)](https://lovdata.no/dokument/SF/forskrift/2015-12-08-1449)

- [Forskrift om opptak til studier ved Norges teknisk-naturvitenskapelige universitet (NTNU)](https://lovdata.no/dokument/SF/forskrift/2016-08-25-1051)

- [Forskrift for graden doctor philosophiae (dr.philos.) ved Norges teknisk-naturvitenskapelige universitet (NTNU)](https://lovdata.no/dokument/SF/forskrift/2014-01-21-117)

- [Forskrift for gradene philosophiae doctor (ph.d.) og philosophiae doctor (ph.d.) i kunstnerisk utviklingsarbeid ved Norges teknisk–naturvitenskapelig universitet (NTNU)](https://lovdata.no/dokument/SF/forskrift/2018-12-05-1878)

- [Midlertidig forskrift til forskrift 8. desember 2015 nr. 1449 om studier ved Norges teknisk-naturvitenskapelige universitet (NTNU) – ekstraordinære tiltak i forbindelse med koronavirus](https://lovdata.no/dokument/LTI/forskrift/2020-03-12-290)

- [Forskrift om tilsyn med utdanningskvaliteten i høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2017-02-07-137) (studietilsynsforskriften)

- [Forskrift om kvalitetssikring og kvalitetsutvikling i høyere utdanning og fagskoleutdanning](https://lovdata.no/dokument/SF/forskrift/2010-02-01-96)

- [Forskrift om krav til mastergrad](https://lovdata.no/dokument/SF/forskrift/2005-12-01-1392)

- [Forskrift om opptak til høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2017-01-06-13?q=forskrift%20om%20opptak%20til%20h%C3%B8yere)

## Åndsverksloven

- [Forskrift til åndsverkloven](https://lovdata.no/dokument/SF/forskrift/2021-08-26-2608)

## Helsepersonelloven

## Helseregisterloven

## Forskrifter

- [Forskrift om avgiftsfritak, østerrikske stud. Bestemmelse om fritak for avgifter ved universitet og høgskoler for østerrikske studenter](https://lovdata.no/dokument/SFO/forskrift/1973-08-24-5)

- [Forskrift til universitets- og høyskoleloven, Kapittel 8. Felles klagenemnd](https://lovdata.no/dokument/SF/forskrift/2024-06-28-1392/KAPITTEL_8)

- [Forskrift om godskriving og fritak av høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2018-12-21-2221)

- [Forskrift om grader og yrkesutdanninger, beskyttet tittel og normert studietid ved universiteter og høyskoler](https://lovdata.no/dokument/SF/forskrift/2005-12-16-1574)

- [Forskrift om offentlighet i rettspleien](https://lovdata.no/dokument/SF/forskrift/2001-07-06-757)

- [Forskrift om pasientjournal](https://lovdata.no/dokument/SF/forskrift/2019-03-01-168)

- [Vedtak om utvidelse av det geografiske virkeområdet for lov om arkiv til også å omfatte Svalbard, Jan Mayen og de norske biland i Antarktis](https://lovdata.no/dokument/SF/forskrift/2001-03-09-192)

## Annet

- [Rundskriv G-69/98 Innskjerping av praktiseringen av offentlighetsloven](https://www.regjeringen.no/no/dokumenter/g-6998/id108328/)

- [Statens personalhåndbok](https://lovdata.no/dokument/SPH)

- [Digitaliseringsrundskrivet (Kommunal- og moderniseringsdepartementet)](https://www.regjeringen.no/no/dokumenter/digitaliseringsrundskrivet/id2826781/)

- [Partsinnsyn i saker om tilsetting i den offentlige forvaltning (forvaltningsforskriften kap. 5)](https://lovdata.no/dokument/SF/forskrift/2006-12-15-1456/KAPITTEL_5#KAPITTEL_5)

## Retningslinjer ved NTNU

- [Personopplysninger](https://innsida.ntnu.no/wiki/-/wiki/Norsk/retningslinje+for+behandling+av+personopplysninger)

- [Personvern ved NTNU](https://www.ntnu.no/personvern)

- [Språkpolitiske retningslinjer](https://www.ntnu.no/sprakpolitiske-retningslinjer)

- [IKT-reglement](https://i.ntnu.no/wiki/-/wiki/Norsk/IKT-reglement)

- [Politikk for informasjonssikkerhet](https://i.ntnu.no/wiki/-/wiki/Norsk/Politikk+for+informasjonssikkerhet)

- [Politikk for digitalisering og prosessutvikling](https://i.ntnu.no/wiki/-/wiki/Norsk/Digital+politikk)

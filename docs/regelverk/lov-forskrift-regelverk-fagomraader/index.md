---
date: '2021-07-30T00:00:00'
title: 'Lov, forskrift, regelverk - fagområder'
status: Ferdig
category: Regelverk
tags:
  - Bestemmelse
author: Ole Vik
---

## Regelverk om utdanning

### Lover

- [Lov om universiteter og høgskoler](https://lovdata.no/dokument/NL/lov/2024-03-08-9/)

- [Offentlighetsloven](https://lovdata.no/lov/2006-05-19-16)

- [Forvaltningsloven](https://lovdata.no/dokument/NL/lov/1967-02-10)

### Forskrifter

- [Forskrift om opptak til høgre utdanning](https://lovdata.no/dokument/SF/forskrift/2017-01-06-13/)

- [Forskrift om godskriving og fritak av høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2018-12-21-2221)

- [Forskrift om krav til mastergrad](https://lovdata.no/dokument/SF/forskrift/2005-12-01-1392)

- [Forskrift om grader og yrkesutdanninger, beskyttet tittel og normert studietid ved universiteter og høgskoler](https://lovdata.no/dokument/SF/forskrift/2005-12-16-1574)

- [Forskrift om skikkethetsvurdering i høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2024-06-28-1392/KAPITTEL_7)

- [Forskrift om kvalitetssikring og kvalitetsutvikling i høyere utdanning og fagskoleutdanning](https://lovdata.no/dokument/SF/forskrift/2010-02-01-96)

- [Forskrift om tilsyn med utdanningskvaliteten i høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2017-02-07-137)

- [Forskrift om målform i eksamensoppgaver](https://lovdata.no/dokument/SF/forskrift/1987-07-07-4148)

- [Universitets- og høyskoleforskriften kapittel 4](https://lovdata.no/dokument/SF/forskrift/2024-06-28-1392/KAPITTEL_4)

- [Forvaltningslovforskriften](https://lovdata.no/dokument/SF/forskrift/2006-12-15-1456)

- [Offentlighetsforskriften](https://lovdata.no/dokument/SF/forskrift/2008-10-17-1119)

- [Forskrift om Nasjonalt kvalifikasjonsrammeverk for livslang læring og om henvisningen til Det europeiske kvalifikasjonsrammeverket for livslang læring](https://lovdata.no/dokument/SF/forskrift/2017-11-08-1846)

- [Forskrift om kvalifikasjoner og sertifikater for sjøfolk](https://lovdata.no/dokument/SF/forskrift/2011-12-22-1523)

### Rundskriv

- [Rundskriv til forskrift om skikkethetsvurdering](https://www.regjeringen.no/no/dokumenter/rundskriv-f-07-16–forskrift-om-skikkethetsvurdering-i-hoyere-utdanning/id2516838/)

- [Rundskriv til behandling av klagesaker](https://www.regjeringen.no/no/dokumenter/rundskriv-f-005-06/id109587/)

- [Rundskriv til forskrift om opptak til høgre utdanning](https://www.regjeringen.no/contentassets/3760da1a0ddb4a959f8a43cc101654f5/rundskriv-f-06-19.pdf)

- [Forskrift om godskriving og fritak av høyere utdanning - merknader til bestemmelsene](https://www.regjeringen.no/contentassets/a6450cba06d54ebab58f22005c336822/rundskriv-ny-forskrift-om-godskriving-og-fritak-av-hoyere-utdanning.pdf)

### Rammeplaner for høyere utdanning

Kunnskapsdepartementet har fastsatt nasjonale rammeplaner for enkelte utdanninger.  
I [studieplanene](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Studieh%C3%A5ndb%C3%B8ker+og+studieplaner) finner du en mer detaljert oversikt over innholdet i studieprogrammet.

[Rammeplaner for høyere utdanning](https://www.regjeringen.no/no/tema/utdanning/hoyere-utdanning/rammeplaner/id435163/) fastsettes av Kunnskapsdepartementet. Rammeplanene er forskrifter til loven. På [Regjeringen.no](https://www.regjeringen.no/no/tema/utdanning/hoyere-utdanning/rammeplaner/id435163/) finner du alle rammeplaner som er gjeldende for studier innen høyere utdanning i dag, samt tidligere, ikke lenger gyldige rammeplaner.

Når det kommer en ny rammeplan for en utdanning, vil den gamle være gyldig i en viss periode, dette er for at personer som er i utdanningsløp skal kunne fullføre det løpet de har startet på. Det vil derfor være overlapping av gyldighetsperiodene til noen gyldige rammeplaner for samme utdanning. Gyldigheten til foregående rammeplan er beskrevet i siste paragraf i den etterfølgende. Rammeplanene er listet opp per utdanningstype. Først grunnutdanningene, deretter videreutdanninger som det finnes rammeplaner for.

For noen rammeplaner er det også utarbeidet tilhørende rundskriv. Rundskrivene er orienteringer fra departementet til berørte parter om tolkninger av forskrifter. Du finner dem direkte under den forskriften/de forskriftene de gjelder for. For noen av lærerutdanningene har Universitets- og høgskolerådet ved Nasjonalt råd for lærerutdanning [utarbeidet nasjonale retningslinjer](https://www.uhr.no/temasider/nasjonale-retningslinjer/nasjonale-retningslinjer-for-larerutdanningene/).

## Reglement ved NTNU

### NTNUs studieforskrift

- [Forskrift om studier ved NTNU](https://lovdata.no/dokument/SF/forskrift/2015-12-08-1449)

- [Veiledning til studieforskrift ved NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Veiledning+til+ny+studieforskrift+ved+NTNU)

### Utfyllende regler til studieforskriften

Se underoverskriften Utfyllende regler til studieforskriften på siden [Generelle lover og regler - studier](https://i.ntnu.no/wiki/-/wiki/norsk/generelle+lover+og+regler+-+studier) på Innsida.

### Opptak

- [Forskrift om opptak til studier ved NTNU](https://lovdata.no/dokument/SF/forskrift/2016-08-25-1051)

### Andre bestemmelser

Se underoverskriften Andre bestemmelser ved NTNU på siden [Generelle lover og regler - studier](https://i.ntnu.no/wiki/-/wiki/norsk/generelle+lover+og+regler+-+studier) på Innsida.

### Eksamensreglement

- [Generelt for eksamenskandidater](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Regler+for+eksamen#section-Regler+for+eksamen-Oppm%C3%B8te)

- [Forsinket eksamensmelding](https://i.ntnu.no/wiki/-/wiki/Norsk/Forsinket+eksamensmelding)

- [Tillatte hjelpemidler på eksamen](https://innsida.ntnu.no/wiki/-/wiki/Norsk/tillatte+hjelpemidler)

- [Begrunnelse og klage på karakter](http://www.ntnu.no/studier/eksamen/klage)

- [Eksamen - Ofte stilte spørsmål for studenter](https://i.ntnu.no/wiki/-/wiki/Norsk/Eksamen+-+Ofte+stilte+sp%C3%B8rsm%C3%A5l+for+studenter)

- [Faglærers tilstedeværelse](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Fagl%C3%A6rers+oppm%C3%B8te+under+eksamen)

- [Retningslinjer for 4. og 5. eksamensforsøk](https://i.ntnu.no/wiki/-/wiki/Norsk/antall+fors%C3%B8k+p%C3%A5+eksamen)

- [Retningslinjer for digital eksamensavvikling ved NTNU – fordeling av studenter på NTNUs utstyr og studentenes eget utstyr ved samme eksamensavvikling](https://i.ntnu.no/documents/1306938287/1306941279/Retningslinjer+for+digital+eksamensavvikling+ved+NTNU_2019.12.07.PDF/)

  - [Bruk av studentens eget utstyr](https://i.ntnu.no/documents/1306938287/1306941279/Bruk+av+studentens+eget+utstyr_2019.12.07.PDF/)

- [Retningslinjer for digital muntlig eksamen ved NTNU – for studenter](https://i.ntnu.no/wiki/-/wiki/Norsk/digital+muntlig+eksamen+-+for+studenter)

### NTNUs klagenemnd

Klagenemnda behandler klagesaker for studenter. Du finner mer [informasjon om Klagenemda på Innsida](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Klagenemnda).

## Regelverk ph.d.-utdanning

### Forskrifter

- [Endringer i ph.d.-forskriften som følge av koronaviruset](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Endringer+i+ph.d.-forskriften+p%C3%A5+grunn+av+koronaviruset)

- [Forskrift for gradene philosophiae doctor (ph.d.) og philosophiae doctor (ph.d.) i kunstnerisk utviklingsarbeid](https://lovdata.no/dokument/SF/forskrift/2018-12-05-1878)

- Fakultet som har egne utfyllende retningslinjer til forskriften for graden ph.d.:

  - [Fakultet for informasjonsteknologi og elektroteknikk](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Doktorgrad+-+skjema+-+IE)

  - [Fakultet for ingeniørvitenskap](https://www.ntnu.no/documents/389341/4788367/191217+Forskrift+m+adm+bestemmelser+for+IV-fak_norsk.pdf/c49ce2f5-b527-194c-5ebf-2421c31b35e8?t=1578310586028)

  - [Fakultet for medisin og helsevitenskap](https://www.ntnu.no/documents/1284731177/1291010166/NTNUs+reviderte+ph.d.-forskrift+med+fakultetets+bestemmelser.pdf/fcb03c58-c5bd-8357-bbfe-903348832e88?t=1576480269519)

  - [Fakultet for samfunns- og utdanningsvitenskap](https://www.ntnu.no/documents/389341/4788367/Utfyllende+retningslinjer+phd-forskrift+SU+vedtatt+5-6-2020.pdf/f79ebf01-91b6-3342-1812-ba13810158f6?t=1591862689331)

  - [Fakultet for økonomi](https://www.ntnu.no/documents/389341/4788367/Ph.d.-forskrift+vedtatt+5.+desember+2018+med+utfyllende+retningslinjer.pdf/4a931ee1-4f8e-1e9b-c97d-68d807b60260?t=1580997972931)

- [Forskrift for graden integrert ph.d.-utdanning](https://www.ntnu.no/documents/389341/4788367/Forskrift+for+integrert+ph.d.+-utdanning+ved+NTNU_Jan2019.docx/132c2809-6d1c-4fe6-abc8-1cbd0fa9e5aa)

- [Forskrift for graden dr.philos](http://lovdata.no/dokument/SF/forskrift/2014-01-21-117)

- [Veiledning om bedømmelse av norske doktorgrader](https://www.ntnu.no/documents/389341/4788367/20190527+Revidert+veiledning+for+bed%C3%B8mmelseskomiteer+phd_NTNU.pdf/51cf6d77-ad6e-4566-8090-39897fc22b53?t=1568816369009) (revidert 27.08.2019)

- [Fellesgrader og cotutelle-avtaler (felles veiledning) innenfor doktorgradsutdanningen](https://innsida.ntnu.no/wiki/-/wiki/English/cotutelle+and+joint+doctoral+degrees)

### Tidligere forskrifter

- [Forskrift for stipendiaters pliktarbeid og ansettelsesforhold](https://lovdata.no/dokument/SFO/forskrift/2009-06-17-959) (Gjelder kun stipendiater som er ansatt ved NTNUI)

- [Forskrift for graden philosophiae doctor (ph.d.) (2012–2018)](https://lovdata.no/dokument/LTI/forskrift/2012-01-23-206)

- [Forskrift for graden philosophiae doctor (ph.d.) (2005–2012)](https://www.ntnu.no/studieavd/dok/phd_forskrift_2005.pdf)

- [Forskrift for graden dr.philos (2005–2014)](https://lovdata.no/dokument/NLO/lov/2005-04-01-15/)

## Administrativt regelverk

- [Arbeidsmiljøloven](https://lovdata.no/dokument/NL/lov/2005-06-17-62?q=arbeidsmilj%C3%B8loven)

- [Ferieloven](https://lovdata.no/dokument/NL/lov/1988-04-29-21?q=ferieloven)

- [Folketrygdloven](https://lovdata.no/dokument/NL/lov/1997-02-28-19?q=folketrygdloven)

- [Forvaltningsloven](https://lovdata.no/dokument/NL/lov/1967-02-10?q=forvaltningsloven)

- [Hovedtariffavtalen i staten](https://lovdata.no/dokument/SPH/sph-2016/KAPITTEL_7#KAPITTEL_7)

- [Informasjonssikkerhet](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Informasjonssikkerhet+-+rutiner+og+reglement)

- [Lov om yrkesskadeforsikring](https://lovdata.no/dokument/NL/lov/1989-06-16-65)

- [Offentlighetsloven](https://lovdata.no/dokument/NL/lov/2006-05-19-16?q=offentlighetsloven)

- [Personopplysningsloven](https://lovdata.no/dokument/NL/lov/2018-06-15-38)

- [Personalreglement](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Personalreglement)

<ul>
<li><a href="https://lovdata.no/dokument/NL/lov/2017-06-16-67?q=lov%20om%20statens%20ansatte">Statsansatteloven</a> /| <a href="https://lovdata.no/dokument/SF/forskrift/2017-06-21-838?q=statens%20ansatte">Forskrift</a></li>
</ul>

- [Statens personalhåndbok](https://lovdata.no/dokument/SPH/)

- [Universitets- og høgskoleloven](https://lovdata.no/dokument/NL/lov/2024-03-08-9/)

- [Forskrift til universitets- og høyskoleloven, kapittel 3](https://lovdata.no/dokument/SF/forskrift/2024-06-28-1392/KAPITTEL_3)

- [Forskrift om ansettelsesvilkår for stillinger som postdoktor, stipendiat, vitenskapelig assistent og spesialistkandidat](https://lovdata.no/dokument/SFO/forskrift/2006-01-31-102)

- [Internkontrollforskriften](https://lovdata.no/dokument/SF/forskrift/1996-12-06-1127)

- [NTNUs retningslinje for rekrutteringsstillinger](https://i.ntnu.no/wiki/-/wiki/Norsk/NTNUs+retningslinje+for+rekrutteringsstillinger)

- [NTNUs særavtaler for feltarbeid i Norge, i utlandet og kortvarige tilsettinger](https://i.ntnu.no/wiki/-/wiki/Norsk/Lover+og+reglementer)

- [Særavtale om honorering for arbeid på eksternt finansierte prosjekter](https://i.ntnu.no/wiki/-/wiki/Norsk/Tilleggsl%C3%B8nn)

- [Særavtale om minstelønn for postdoktorer](https://innsida.ntnu.no/wiki?p_p_id=36&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_36_struts_action=%2Fwiki%2Fedit_page&_36_redirect=https%3A%2F%2Finnsida.ntnu.no%2Fwiki%2F-%2Fwiki%2FNorsk%2Flover%2Bog%2Breglementer&p_r_p_185834411_nodeId=24647&p_r_p_185834411_title=S%C3%A6ravtale+om+minstel%C3%B8nn+for+postdoktor+01.12.19)

### Økonomi

- [Bevilgningsreglement for staten](https://www.regjeringen.no/no/tema/okonomi-og-budsjett/statlig-okonomistyring/bevilgningsreglementet/id439274/)

- [Regelverket for økonomistyring i staten](https://dfo.no/fagomrader/okonomiregelverket)

- [Hovedinstruks for økonomiforvaltningen ved statlige universiteter og høyskoler](https://www.regjeringen.no/no/dokumenter/Hovedinstruks-for-okonomiforvaltningen-ved-statlige-universiteter-og-hoyskoler/id765252/)

- [Styringsreglement ved NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Styringsreglement) (fastsatt av styret 07.12.2016 - S-sak 83/2016)

- [Delegasjonsreglement ved NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Delegasjonsreglement) (Fastsatt av styret 07.12.2016, S-sak 83/2016)

- [Budsjettdisponeringsmyndighet](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Budsjettdisponeringsmyndighet) (vedtatt av direktør for økonomi og eiendom 1.12.2007)

- [Strategi og styrende dokumenter ved NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Strategi+og+styrende+dokumenter)

- [Regnskap - lover og reglement](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Regnskap+-+lover+og+reglement)

- [Lønn - lover og instrukser](https://innsida.ntnu.no/wiki/-/wiki/Norsk/L%C3%B8nn+-+Lover+og+instrukser)

- [Retningslinjer for reiser](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Retningslinjer+for+reise)

- [Retningslinjer for anskaffelser](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Retningslinjer+for+anskaffelser)

- [Interne prosjekter NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Interne+prosjekt)

- [Eksterne prosjekter - lover og reglement](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Lover+og+reglementer+for+bidrags-+og+oppdragsfinansiert+aktivitet) (bidrags- og oppdragsfinansierte aktiviteter)

- [Etikk ved NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Etikk+ved+NTNU)

### HR

- [Ansattgoder](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Fordeler+og+rabatter) og [trening](https://innsida.ntnu.no/trening-ansatte)

- [Arbeidsvilkår for ansatte](https://innsida.ntnu.no/wiki/-/wiki/Norsk/arbeidsvilk%c3%a5r+for+ansatte) - Samleside med oversikt over sentrale og lokale dokumenter som lov- og avtaleverk, forskrifter med mer, som er gjeldende for ansatte ved NTNU

- [Arbeidstid](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Arbeidstid)

- [Etiske retningslinjer for ansatte ved NTNU](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Etiske+retningslinjer+for+ansatte+ved+NTNU)

- [Fagforeninger](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Fagforeninger)

- [Ferie](https://innsida.ntnu.no/ferie) og [permisjon](https://innsida.ntnu.no/permisjon)

- [Forskertermin](https://innsida.ntnu.no/forskertermin)

- [Innstegsstilling](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Innstegsstilling?_36_pageResourcePrimKey=2552752220)

- [Kurs og kompetanseutvikling](https://innsida.ntnu.no/kurs)

- [Ledig stilling](https://innsida.ntnu.no/ledige-stillinger)

- [Likestilling](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Likestilling+-+tiltak+og+midler)

- [Lønn](https://innsida.ntnu.no/lonn)

- [Medarbeidersamtale for ansatte](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Medarbeidersamtale+for+ansatte)

- [Ny ved NTNU - for nyansatte](https://innsida.ntnu.no/ny-ved-ntnu)

- [Paga - veiledninger](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Paga)

- [Pensjon](https://innsida.ntnu.no/pensjon), [forsikringer](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Forsikringer)

- [Personalpolitikk](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Personalpolitikk), [personalreglement,](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Personalreglement) [lønnspolitikk](https://innsida.ntnu.no/wiki/-/wiki/Norsk/L%C3%B8nnspolitikk) og [seniorpolitikk](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Seniorpolitikk+ved+NTNU)

- [Råd og utvalg](https://innsida.ntnu.no/raad-og-utvalg)

- [Sidegjøremål](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Sidegj%C3%B8rem%C3%A5l)

- [Sykdom](https://innsida.ntnu.no/sykdom) og [inkluderende arbeidsliv](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Inkluderende+arbeidsliv)

- [Sider tagget med HR](https://innsida.ntnu.no/wiki/-/wiki/tag/hr)

- [Temasider for ansatte - Alfabetisk rekkefølge](https://innsida.ntnu.no/alt-innhold)

### Rekruttering

- [Ansette medarbeider](https://innsida.ntnu.no/ansette-medarbeider)

- [Vitenskapelige stillinger](https://studntnu.sharepoint.com/sites/rekruttering_vit/SitePages/Hjemmeside.aspx#/level/1)

- [Teknisk/administrative stillinger](https://innsida.ntnu.no/ansette-medarbeider)

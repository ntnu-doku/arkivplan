---
date: '2021-01-29T00:00:00'
title: Regelverk
status: Ferdig
category: Oversikt
author: Geir Ekle
---

Arkivloven, arkivforskriften og Riksarkivarens forskrift er kjernen i regelverket som regulerer håndteringen av arkiv. Regelverksoversikten viser til disse og andre lover, forskrifter og bestemmelser som får følger for arkiv og saksbehandling.

---
date: '2023-09-16T00:00:00'
title: Bevaring og kassasjon
status: Ferdig
category: Bevare
tags:
  - Beholde
  - Kaste
  - Slette
  - BK-plan
author: Linea Kristine Solgård
---

Bevaring betyr å sikre at arkiv som har kulturell og forskningsmessig verdi, eller som inneholder viktig rettslig eller forvaltningsmessig dokumentasjon, blir tatt vare på og gjort tilgjengelig.

::: info Bevarings- og kassasjonsplan for UH-sektoren

Bevarings- og kassasjonsplan for UH-sektoren trer i kraft når NTNU tar i bruk ny arkivkjerne innenfor samarbeidsprosjektet BOTT Saksbehandling og arkiv, nå kalt UH Sak. Dokumentet er [gjengitt i fulltekst her](/arkivplan/regelverk/bevaring-og-kassasjon/bevarings-og-kassasjonsplan-for-uh-sektoren), og [original PDF er tilgjengelig](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/bevarings-og-kassasjonsplan-for-uh-sektoren.pdf). For en utgave av tabellen som er enklere å navigere i, se UiBs gjengivelse av [klassifikasjonssystemet](https://fupperåd.no/).

:::

Kassasjon betyr at arkivmateriale som har vært gjenstand for saksbehandling eller har hatt verdi som dokumentasjon, skal tas ut av arkivet/slettes/destrueres på et gitt tidspunkt. Arkivloven med forskrifter danner det rettslige grunnlaget for arbeidet med bevaring og kassasjon i offentlig forvaltning.

Arkivlovens § 9 slår fast at arkivmateriale ikke kan kasseres, dvs. slettes eller destrueres,  
med mindre dette er hjemlet i arkivloven med forskrifter, eller at Riksarkivaren har gitt særskilt samtykke til kassasjon.

Arkivforskriftens § 15 gir påbud om bevaring av bestemte typer materiale,  
mens § 14 sier hva slags materiale som skal arkivavgrenses - det vil si å holde utenfor eller fjerne dokumenter fra arkivet som verken er gjenstand for saksbehandling eller har verdi som dokumentasjon.

Riksarkivarens forskrift, kapittel 7, del II omfatter felles kassasjons- og  
bevaringsbestemmelser for statsforvaltningen – «egenforvaltningssaker»; aktiviteter som omfatter forvaltning av eget organ, det vil si egen administrasjon, egen økonomiforvaltning og egen personalforvaltning.

## Bevaringsformål

Følgende hovedformål (F1 - F4) utgjør hovedkriterier for bevaring (Hovedprinsipper for Riksarkivarens arbeid med bevaring og kassasjon):

- F1: Å dokumentere offentlige organers funksjoner i samfunnet, deres utøvelse av myndighet, deres rolle i forhold til det øvrige samfunn og deres rolle i samfunnsutviklingen

- F2: Å holde tilgjengelig materiale som gir informasjon om forhold i samfunnet på et gitt tidspunkt, og som belyser samfunnsutviklingen

- F3: Å dokumentere personers og virksomheters rettigheter og plikter i forhold til det offentlige, og i forhold til hverandre

- F4: Å dokumentere de arkivskapende organers rettigheter og plikter i forhold til andre instanser.

## Kassasjon

En kassasjonsvurdering skal alltid gjøres med hjemmel i gjeldende bevarings- og kassasjonsbestemmelser.

::: fare Uhjemlet kassering

Det er ulovlig å kassere noe uten å ha hjemmel for det.

Alt som kasseres **må** ha hjemmel i lov før det destrueres eller slettes.

:::

Selv om man kasserer i henhold til regelverket, vil det alltid være nødvendig å utøve skjønn i bevarings- og kassasjonsarbeidet, særlig der emner og temaer ikke er helt eksplisitt nevnt i reglene. Men man må også huske at tidligere praksis og skjønn ikke nødvendigvis er korrekt. Også Riksarkivarens prinsipper for bevaring og kassasjon kan være til hjelp.

For papirarkiver skal arkivmateriale som skal kasseres skilles ut ved periodisering. Materialet skal ikke kasseres før etter at lengste kassasjonsdato er utløpt.

For elektronisk arkiv vil kassasjonskodene bety at elektroniske dokumenter i saker som er påført kassasjonskode, ikke følger med i et datauttrekk ved deponering og avlevering til Arkivverket.

Ved deponering overføres arkivmateriale, fysisk og/eller elektronisk, fra statlige etater til Arkivverket for oppbevaring uten at eierskap og råderett over materialet overføres. Det er ved avlevering at eierskap og råderett av arkivmaterialet overføres endelig til Arkivverket. Arkivforskriften § 18 hjemler deponering og avlevering.

Arkiv, personvern og GDPR

GDPR-forordningen («General Data Protection Regulation») – personvernforordningen på norsk – har bestemmelser som omhandler behandling av personopplysninger til arkivformål i allmennhetens interesse. Personopplysninger er alle opplysninger og vurderinger som kan knyttes til og identifisere en enkeltperson.

Offentlige organ nevnt i arkivloven § 5 har en direkte lovpålagt plikt etter arkivloven § 6 til å holde arkiv. Dette skal skje i henhold til reglene i arkivloven kap. II, føringer i arkivforskriften og i Riksarkivarens forskrift.  
  
Når NTNU utfører søknads- og/eller saksbehandling eller en vedtaksprosess, vil dette være registrert i sak/arkivsystemet. NTNU er forpliktet etter arkivloven å ta vare på slike opplysninger og saksdokumenter.

Plikten til å sikre at flere typer dokumentasjon blir bevart for ettertiden går tydelig fram av Riksarkivarens forskrift – bevaring og kassasjonsbestemmelser om egenforvaltningssaker, og plikten til å lage en bevarings- og kassasjonsplan som skal godkjennes av Riksarkivaren.

Arkivloven § 9 slår fast at arkivmateriale ikke kan kasseres med mindre dette er hjemlet i arkivloven med forskrifter, eller at Riksarkivaren har gitt særskilt samtykke til kassasjon.

Arkivloven med forskrifter gir et godt grunnlag i nasjonal rett om hva som kan kasseres og hva som må bevares for ettertiden. Personvernforordningen innebærer at, så lenge man har et behandlingsgrunnlag, vil arkivlovens regler om bevaring gå foran retten til sletting. Arkivplikten består, slik at opplysninger ikke kan kasseres uten at det foreligger hjemmel for kassasjon i eller i medhold av arkivloven.

## Personvernerklæring

NTNUs personvernerklæring beskriver hvilke personopplysninger som behandles, hvordan de behandles, hvem som er ansvarlig for behandlingen, hvilke rettigheter du har og hvem du kan kontakte om personopplysningene dine. Rektor er behandlingsansvarlig for NTNUs behandling av personopplysninger. Behandlingsansvarliges oppgaver er delegert organisasjonsdirektøren, og daglig oppfølgingsansvar er delegert videre til linjelederne.

Les mer om [NTNUs personvernerklæring, personvern og GDPR på NTNUs nettsider](https://innsida.ntnu.no/wiki/-/wiki/Norsk/personvern+og+gdpr).

::: info Retten til å bli glemt i forhold til Arkivloven

GDPR gir rett til slette opplysninger om seg selv.

Arkivloven gir ikke rom for å slette opplysninger som er av allmenn interesse, rettslig verdi, forskningsinteresse eller av kultur-/historisk interesse.

GDPR viser til unntak (Artikkel 17, nummer 3) dersom følgende behandlinger er nødvendig: «b) for å oppfylle en rettslig forpliktelse /[…/]» eller «d) for arkivformål i allmennhetens interesse, for formål knyttet til vitenskapelig eller historisk forskning eller for statistiske formål /[…/]».

Derfor betyr det at ikke alle som ønsker informasjon om seg selv skal slettes, blir slettet, dersom Arkivlova mener det er andre hensyn som hindrer sletting av slik informasjon.

Ønske om sletting må derfor behandles og vurderes individuelt før eventuell sletting blir gjennomført.

:::

## Bestemmelser og vedtak om bevaring og kassasjon

Ved NTNU er det foreløpig bare arkivmateriale som kommer inn under felles bevarings- og kassasjonsregler for statsforvaltningen og arkivmateriale som er gitt kassasjonsvedtak fra Riksarkivaren som kan kasseres.

Felles bevarings- og kassasjonsregler står i [Riksarkivarens forskrift kapittel 7 del II](https://lovdata.no/dokument/SF/forskrift/2017-12-19-2286/KAPITTEL_7-3#%C2%A77-5).

[Riksarkivarens hovedprinsipper for bevaring og kassasjon i offentlig forvaltning](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/riksarkivarens-hovedprinsipper-for-bevaring-og-kassasjon.pdf) (vedlegg)

[Arkivverkets veileder i bevaring og kassasjon](https://www.arkivverket.no/for-arkiveiere/bevaring-og-kassasjon/a-lage-en-bevaringsplan)

[Rundskriv om generell bevaring og kassasjon i Noark-baserte løsninger](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/rundskriv-om-generell-bevaring-og-kassasjon-i-noark-baserte-loesninger.pdf) (vedlegg)

[Vedtak - Kassasjon av søknader om opptak til universiteter og høgskoler](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-kassasjon-av-soeknader-om-opptak-til-universiteter-og-hoegskoler.pdf) (vedlegg)

[Vedtak - Kassasjon og bevaring av eksamensbesvarelser med mer](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-kassasjon-og-bevaring-av-eksamensbesvarelser-med-mer.pdf) (vedlegg)

[Vedtak - Kassasjon og bevaring av praksis- og besøksrapporter ved profesjonsutdanninger](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-kassasjon-og-bevaring-av-praksis-og-besoeksrapporter-ved-profesjonsutdanninger.pdf) (vedlegg)

[Vedtak - Kassasjon av karfysiologiske undersøkelser fra perioden 1986-2006](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-kassasjon-av-karfysiologiske-undersoekelser-fra-perioden-1986-2006.pdf) (vedlegg)

[Vedtak - Kassasjonsregler for NTH tilknyttet arkivnøkkelen fra 1981](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-kassasjonsregler-for-nth-tilknyttet-arkivnoekkelen-fra-1981.pdf) (vedlegg)

[Vedtak - Retningslinjer for arkivbegrensning og kassasjons i NTHs arkiv](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-retningslinjer-for-arkivbegrensing-og-kassasjon-i-nths-arkiv.pdf) (vedlegg)

[Vedtak - Kassasjon og bevaring av studentmapper ved NTH](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-kassasjon-og-bevaring-av-studentmapper-ved-nth.pdf) (vedlegg)

[Vedtak - Regnskapsbilag tom 1916-1917 kasseres ikke](/arkivplan/vedlegg/regelverk/bevaring-og-kassasjon/vedtak-regnskapsbilag-tom-1916-1917-kasseres-ikke.pdf) (vedlegg)

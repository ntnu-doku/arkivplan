---
date: '2023-09-16T00:00:00'
title: >-
  Bevarings- og kassasjonsplan for universiteter, vitenskapelige høgskoler og
  ikke-etatsinterne statlige høgskoler i Norge
status: Ferdig
category: Regelverk
tags:
  - Dokument
  - Arkiv
  - Bevare
  - Beholde
  - Slette
  - BK-plan
  - Struktur
author: Ole Vik
layout: FUP
---

![Virksomheter tilknyttet bevarings- og kassasjonsplanen.](./vedlegg/media/image1.png)

## Innledning

### Universitetenes og høgskolenes virksomhet

Virksomheten ved universiteter, vitenskapelige høgskoler og statlige høgskoler er hjemlet i lov om universiteter og høyskoler, §§ 1-3 og 1-4.

Primærfunksjonene kan inndeles som følger:

**Utdanning**: universitetene og høgskolene skal tilby høyere utdanning som er basert på det fremste innen forskning, faglig og kunstnerisk utviklingsarbeid og erfaringskunnskap, bidra til utvikling av høyere utdanningstilbud og tilby etter- og videreutdanning innenfor institusjonens virkeområde.

**Forskning**: universitetene og høgskolene skal utføre forskning og faglig og kunstnerisk utviklingsarbeid, bidra til innovasjon og verdiskapning basert på resultater fra forskning og bidra til at norsk høyere utdanning og forskning følger den internasjonale forskningsfronten.

**Formidling**: universitetene og høgskolene skal bidra til å spre og formidle resultater og faglig og kunstnerisk utviklingsarbeid, og legge til rette for at institusjonens ansatte og studenter kan delta i samfunnsdebatten.

**Museums- og samlingsvirksomhet**: enkelte av universitetene har et særskilt nasjonalt ansvar for å bygge opp, drive og vedlikeholde museer og vitenskapelige samlinger/arkiver. Institusjoner uten museums- og samlingsvirksomhet behøver ikke implementere denne funksjonen i klassifikasjonssystem og bevarings- og kassasjonsplan.

### Prinsipper for utarbeidelsen av funksjonsbasert klassifikasjonssystem og bevarings- og kassasjonsplan

Arbeidet er basert på funksjonsanalyse utført ved institusjonene 2019-2020 (alle unntatt Universitetet i Oslo, hvor analysen ble utført 2015-2016) basert på ISO TR 26122 *Information and documentation - Work process analysis for records.* Dette er et prosessanalyseverktøy som tar utgangspunkt i hva som er relevant for dokumentasjonsforvaltning.

Med funksjoner menes her et overordnet ansvarsområde som bidrar til å oppnå virksomhetens mål. Med prosess menes en rekke handlinger som utføres for å oppnå et bestemt resultat knyttet til en funksjon. Med underfunksjon menes en gruppe prosesser som hører sammen som del av et overordnet ansvarsområde.

Det ble gjennomført en kartlegging av institusjonenes hovedfunksjoner, underfunksjoner, prosesser og transaksjoner. I dette arbeidet ble det gjennomført intervjuer og workshops med medarbeidere innen de ulike hovedfunksjonene. UiB, UiT og NTNU gjennomførte egne kartlegginger. En arbeidsgruppe for de 17 institusjonene tilknyttet Units tjenesteleveranse av systemer for saksbehandlings- og dokumentasjonsforvaltning gjennomførte en felles kartlegging.

Videre ble kartleggingene sammenstilt til en felles funksjonsanalyse av en arbeidsgruppe satt sammen av representanter for funksjonsområdene og institusjonene.

Riksarkivet utarbeider regler for bevaring og kassasjon av såkalte egenforvaltningssaker i staten (riksarkivarens forskrift kap. 7). I denne bevarings- og kassasjonsplanen er egenforvaltningssakene innlemmet og detaljert, etter samme prinsipp om en funksjonsbasert tilnærming, basert på en analyse foretatt av Unit i 2020. De er samlet i funksjonen *internadministrasjon*, et begrep som er enklere å kommunisere utenfor dokumentasjonsforvaltningen enn egenforvaltning.

Den sammenstilte funksjonsanalysen ble kvalitetssikret av institusjonene. Den felles funksjonsanalysen ble så grunnlag for arbeidet til en arbeidsgruppe satt sammen av representanter for dokumentasjonsforvaltningsområdet og institusjonene. Denne gruppen utarbeidet utkast til felles funksjonsbasert klassifikasjonssystem for universiteter og statlige høgskoler, også kvalitetssikret ved institusjonene.

Videre har denne gruppen utarbeidet utkast til felles bevarings- og kassasjonsplan for universiteter, vitenskapelige høgskoler og ikke-etatsinterne statlige høgskoler.

### Vesentlige prinsipper for felles funksjonsbasert klassifikasjonssystem og felles bevarings- og kassasjonsplan med kassasjonsfrister

- Klassifikasjonssystem og bevarings- og kassasjonsplan skal være levende dokumenter og ta opp endringer i institusjonenes funksjoner, underfunksjoner og prosesser

- Bevarings- og kassasjonsvurdering foregår på prosessnivået

- Bevarings- og kassasjonsvurdering er satt på bakgrunn av en vurdering av sannsynlighet for historisk og/eller administrativ informasjonsverdi, i tråd med bevaringsformålene og gjeldende lov- og regelverk

- Kassasjonsfrister er satt etter en samlet vurdering av administrativt behov, personvern og individers rettigheter overfor institusjonene ved den enkelte prosess

- Kassasjonsfrister løper fra saken avsluttes

- Funksjonsanalysen, klassifikasjonssystemet og bevaring- og kassasjonsplanen er systemuavhengige for å ivareta endringer i systembruk over tid

- Funksjonsanalysen, klassifikasjonssystemet og bevaring- og kassasjonsplanen er uavhengige av roller og organisasjonsstruktur ved den enkelte institusjon for å ivareta endringer i organisering over tid

- Egennavn og spesifikke benevnelser unngås så langt det er mulig, for å sikre at ulikheter i systemer og behandlingsinstanser for samme prosess ved de ulike institusjonene dekkes, og for å redusere behovet for oppdatering av planen ved endringer i egennavn og behandlingsinstanser

- Ikke alle prosesser vil være relevante ved alle institusjoner. I enkelte av disse tilfellene er det nødvendig å bruke egennavn og spesifikke benevnelser

Den enkelte institusjon kan ha behov for å overstyre den felles bevarings- og kassasjonsplanen for enkelte prosesser og transaksjoner. Disse kan ha særskilt bevaringsverdi fordi de er presedenssettende eller fordi de var av stor viktighet, eller ved saksforløp av lang varighet og/eller stor betydning for individer, etter den enkelte institusjons vurdering. Den enkelte institusjon vil også kunne vurdere å overstyre felles terminologi.

Bevarings- og kassasjonsplan er utarbeidet med hjemmel i Lov om arkiv (arkivloven), Forskrift om offentlege arkiv (arkivforskriften) og Forskrift om utfyllende tekniske og arkivfaglige bestemmelser om behandling av offentlige arkiver (riksarkivarens forskrift). Dokumentet er utformet slik at det skal gi en oversikt over hva som skal bevares for ettertiden, og hva som skal kasseres etter at arkivskapernes egen bruk er avsluttet.

Formålet med bevaring av offentlig arkivmateriale er å sørge for at arkiver som har betydelig kulturell eller forskningsmessig verdi, eller som inneholder rettslig eller viktig forvaltningsmessig dokumentasjon, blir bevart for fremtiden og gjort tilgjengelige for ettertiden, jf. arkivloven § 1. Materiale som kommer inn under bestemmelse om bevaring kan dermed ikke kasseres.

Arkivlovens paragraf 9 fastslår at offentlig arkivmateriale ikke kan kasseres uten at det skjer i samsvar med bestemmelser i forskrift, eller etter særskilt samtykke fra Riksarkivaren.

Kassasjon betyr at arkivmateriale som har vært gjenstand for saksbehandling etter å ha hatt verdi som dokumentasjon, blir tatt ut av arkivet og slettet eller destruert. Formålet med kassasjonsbestemmelser er å begrense omfanget av arkivene, ivareta personvern og samtidig sikre at alt arkivmateriale av verdi blir bevart for ettertiden. Reglene tar utgangspunkt i at følgende forhold skal ivaretas:

- Dersom materialet har stor dokumentasjonsverdi og informasjonsverdi og samtidig er unikt, må det bevares for ettertiden.

- Dersom materialet ikke har stor nok dokumentasjonsverdi og informasjonsverdi til å oppbevares for ettertiden, men dokumenterer rettigheter for andre enn arkivskaperen, bør det bevares inntil dokumentasjonsbehovet opphører og deretter kasseres.

- Dersom materialet ikke har bevaringsverdi utover arkivskapers eget behov bevares det til arkivskaperens behov har opphørt, for deretter å kasseres.

### Bevaringsformål

Bevarings- og kassasjonsplanen er basert på Rapport fra Bevaringsutvalget fra 2002 (Bevaringsrapporten). Rapporten er utarbeidet på forespørsel fra Riksarkivaren, og foreslår hvilke prinsipper og kriterier som bør ligge til grunn for bevaring- og kassasjonsvurderingene.

Bevaringsrapporten tar utgangspunkt i fire bevaringsformål som arkivmaterialet skal vurderes mot. Formålene springer ut fra arkivlovens formålsparagraf og det er knyttet ulike kriterier til hvert av formålene.

**Formål 1 (F1): Dokumentasjonsverdi**

Man skal dokumentere offentlige organers funksjon i samfunnet, utøvelse av myndighet og rolle i forhold til det øvrige samfunn og samfunnsutviklingen.

Tilhørende kriterier: Administrativt nivå, saksbehandlingstype, saksbehandlingsledd, ekstraordinære/ordinære aktiviteter, pionérvirksomhet, primær/internfunksjon i virksomheten.

**Formål 2 (F2): Informasjonsverdi**

Man skal bevare materiale som gir informasjon om forhold i samfunnet på et gitt tidspunkt og som belyser samfunnsutviklingen. Her vil det også være snakk om informasjon som går utover virksomhetens funksjon og virke.

Tilhørende kriterier: Tidsspenn/kontinuitet, omfang, informasjonstetthet og tematisk variasjon, lenkbarhet med annet materiale, kvalitative egenskaper og alder.

**Formål 3 (F3): Rettigheter og plikter I**

Man skal dokumentere personers og virksomheters rettigheter og plikter i forhold til det offentlige og hverandre.

Tilhørende kriterier: Saksbehandlingens konsekvenser og hjemmelsgrunnlag.

**Formål 4 (F4): Rettigheter og plikter II**

Man skal dokumentere arkivskapende virksomheters rettigheter og plikter i forhold til andre instanser.

Tilhørende kriterier: Organets administrative og driftsmessige behov.

Formålene skal vurderes trinnvis fra F1–F4. Faller materiale inn under kriteriene for bevaring i F1 og F2 skal det bevares for ettertiden, enten i sin helhet eller i form av prøver. Hvis materiale bevares etter F3 eller F4 er det ofte behov for kun en tidsbegrenset oppbevaring. Materiale kan kasseres når det ikke lenger er gyldig som juridisk dokumentasjon eller det har tapt sin verdi for arkivskaper. I noen tilfeller kan dette være et behov som kan bestå over lang tid, noe som kan tilsi langvarig bevaring eller lang kassasjonfrist.

For universitets- og høgskolesektoren har parallell vurdering i enkelte tilfeller vært mer relevant enn trinnvis, for å belyse materialets ulike funksjoner i virksomheten. Dette vil være tilfelle blant annet når materialet dokumenterer rettigheter og plikter etter F3 og F4 så vel som arkivskapers funksjon, F1, eller informasjon om forhold i samfunnet, F2.

I tillegg skisserer Bevaringsrapporten enkelte tilleggskriterier som skal vurderes uavhengig av de ulike bevaringsformålene. Det mest relevante er kriteriet knyttet til redundans/unikhet, det vil si å bevare dokumentasjonen kun på ett sted. I tillegg må materialets kontekstuelle informasjon tas i betraktning når man vurderer redundans.

### Funksjoner og overordnet vurdering av dokumentasjonen

Felles for alle funksjoner er at strategi dokumenteres gjennom utarbeidelse av planverk, periodisk rapportering og beslutninger fattet i styrer, råd og utvalg. Strategidokumentasjon bevares som vesentlig dokumentasjon av institusjonenes oppfyllelse av primærfunksjonene og deres prioriteringer og strategiske valg for kjernevirksomheten og egen forvaltning.

**Internadministrasjon  
**Bevaring og kassasjon av egenforvaltningssaker, her benevnt som internadministrasjon og beskrevet med tilhørende underfunksjoner, prosesser og stikkord, er i vesentlig grad styrt av riksarkivarens forskrift. Arbeidsgruppen har likevel foretatt en selvstendig vurdering av den enkelte prosess.

**Forskning**

- Forskningsstrategi og –kvalitet

- Forskningsprosjekt

- Forskningsstøtte

- Forskningsrettigheter og kommersialisering

I tillegg til vesentlig informasjon om strategi og kvalitetssystemer omfatter funksjonen planlegging og løpende drift av forskningsprosjekter. Også driftsdokumentasjon av forskningsstøtte, nasjonale og internasjonale samarbeider mv. vil ha varig interesse både som dokumentasjon av en primærfunksjon (bevaringsprinsipp F1) og av forhold i samfunnet (bevaringsprinsipp F2).

**Utdanning**

- Utdanningsstrategi og –kvalitet

- Opptak

- Undervisning og læringsaktiviteter

- Oppfølging av studenter og ph.d.-kandidater

- Vurdering

- Grad og vitnemål

I tillegg til vesentlig informasjon om strategi og kvalitetssystemer omfatter funksjonen planlegging og forvaltning av undervisningstilbudet, individuell oppfølging av studenter og ph.d.-kandidater gjennom deres studieløp, gjennomføring av eksamener og andre former for vurdering og utstedelse av vitnemål. Dokumentasjon av institusjonenes studieportefølje er vesentlig som dokumentasjon av en primærfunksjon (bevaringsprinsipp F1) og av forhold i samfunnet (bevaringsprinsipp F2).

Dokumentasjonen som dannes av individuell oppfølging vil i overveiende grad, med unntak av nemndbehandling, kasseres. Kassasjonsfrister er satt ut fra en vurdering av hvor lenge institusjonene vil ha administrativt behov for dokumentasjonen for å ivareta individenes rettigheter (bevaringsprinsipp F3).

**Formidling**

- Faglig formidling

- Kommunikasjon og samfunnskontakt

- Utstillinger og forestillinger

I tillegg til vesentlig informasjon om strategi omfatter funksjonen løpende kommunikasjonsaktivitet og ivaretar formidlingsansvaret for de av institusjonene som har et særskilt ansvar for museer og samlinger. Dokumentasjonen for flere av disse prosessene vurderes som vesentlige som dokumentasjon av en primærfunksjon (bevaringsprinsipp F1) og av forhold i samfunnet (bevaringsprinsipp F2). Publikumshenvendelser, veiledning mv. kasseres.

**Museum og samling**

- Samlingsforvaltning

- Kulturminneforvaltning

- Naturforvaltning

- Samlingsforvaltning – spesialsamlinger og depotarkiv

Funksjonen gjelder ikke alle sektorens institusjoner. I tillegg til vesentlig informasjon om strategi omfatter funksjonen dokumentasjon av enkelte av institusjonenes særlige ansvar for å ivareta kulturminnevern og naturområdevern, samt særskilt bibliotek- og arkivmateriale. Forvaltningen av disse særlige ansvarsområdene vurderes som vesentlige som dokumentasjon av en primærfunksjon (bevaringsprinsipp F1) og av forhold i samfunnet (bevaringsprinsipp F2).

### Iverksettelse

Bestemmelsene trer i kraft for den enkelte institusjon fra institusjonen tar i bruk ny arkivkjerne innenfor samarbeidsprosjektet BOTT Saksbehandling og arkiv.

### Relevant lov- og regelverk

[Lov om universiteter og høyskoler (universitets- og høyskoleloven)](https://lovdata.no/dokument/NL/lov/2005-04-01-15)

[Lov om behandlingsmåten i forvaltningssaker (forvaltningsloven)](https://lovdata.no/dokument/NL/lov/1967-02-10)

[Forskrift til forvaltningsloven (forvaltningslovforskriften)](https://lovdata.no/dokument/SF/forskrift/2006-12-15-1456)

[Lov om rett til innsyn i dokument i offentleg verksemd (offentleglova)](https://lovdata.no/dokument/NL/lov/2006-05-19-16)

[Forskrift til offentleglova (offentlegforskrifta)](https://lovdata.no/dokument/SF/forskrift/2008-10-17-1119)

[Forskrift om offentlege arkiv](https://lovdata.no/dokument/SF/forskrift/2017-12-15-2105)

[Forskrift om utfyllende tekniske og arkivfaglige bestemmelser om behandling av offentlige arkiver (riksarkivarens forskrift)](https://lovdata-no.ezproxy.uio.no/dokument/SF/forskrift/2017-12-19-2286#KAPITTEL_7)

[Lov om avleveringsplikt for allment tilgjengelege dokument (pliktavleveringslova)](https://lovdata.no/dokument/NL/lov/1989-06-09-32)

[Forskrift om elektronisk kommunikasjon med og i forvaltningen (eForvaltningsforskriften)](https://lovdata.no/dokument/SF/forskrift/2004-06-25-988)

[Lov om behandling av personopplysninger (personopplysningsloven)](https://lovdata.no/dokument/NL/lov/2018-06-15-38)

[Forskrift om opptak til høgare utdanning](https://lovdata.no/dokument/SF/forskrift/2017-01-06-13)

[Forskrift om grader og yrkesutdanninger, beskyttet tittel og normert studietid ved universiteter og høyskoler](https://lovdata.no/dokument/SF/forskrift/2005-12-16-1574)

[Forskrift om kvalitetssikring og kvalitetsutvikling i høyere utdanning og fagskoleutdanning](https://lovdata.no/dokument/SF/forskrift/2010-02-01-96)

[Forskrift om skikkethetsvurdering i høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2006-06-30-859)

[Forskrift om godskriving og fritak av høyere utdanning](https://lovdata.no/dokument/SF/forskrift/2018-12-21-2221)

[Forskrift om felles klagenemnd for behandling av klagesaker](https://lovdata.no/dokument/SF/forskrift/2005-10-10-1192)

[Lov om organisering av forskningsetisk arbeid (forskningsetikkloven)](https://lovdata.no/dokument/NL/lov/2017-04-28-23)

[Lov om medisinsk og helsefaglig forskning (helseforskningsloven)](https://lovdata.no/dokument/NL/lov/2008-06-20-44)

[Hovedinstruks for økonomiforvaltningen ved statlige universiteter og høyskoler](https://www.regjeringen.no/no/dokumenter/Hovedinstruks-for-okonomiforvaltningen-ved-statlige-universiteter-og-hoyskoler/id765252/)

[Forskrift om bokføring](https://lovdata.no/dokument/SF/forskrift/2004-12-01-1558)

Lokale regler og forskrifter ved den enkelte institusjon

## Bevarings- og kassasjonsvurdering

<table>
<thead>
<tr>
<th><strong>Kode</strong></th>
<th><strong>Funksjon</strong></th>
<th><strong>Underfunksjon</strong></th>
<th><strong>Prosess</strong></th>
<th><strong>Stikkord</strong></th>
<th><strong>B/K</strong></th>
<th><strong>Frist for kassasjon</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>A.a.01</td>
<td>Intern-administrasjon</td>
<td>Styring og utvikling av virksomheten</td>
<td>Styring av virksomheten fra overordnede organer</td>
<td>Departementets direktiver og styring, tildelingsbrev, styringsdialog</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.02</td>
<td></td>
<td></td>
<td>Utarbeide og vedlikeholde strategi og plan</td>
<td>Strategidokument, virksomhetsplan, årsplan/flerårsplan, handlingsplan, arbeidsplan, årshjul, innspill/høring, saksfremlegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.03</td>
<td></td>
<td></td>
<td>Utarbeide og vedlikeholde interne instrukser og retningslinjer og andre styrende dokumenter</td>
<td>Utredning, saksfremlegg, retningslinjer, regelverk, informasjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.04</td>
<td></td>
<td></td>
<td>Organisere strategisk samarbeid</td>
<td>Invitasjon, intensjonsbrev, partneravtale, samarbeidsavtale, konsortiumavtale, endringsavtale, mandat/formål, handlingsplan, referat, budsjett, tildelingsbrev</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.05</td>
<td></td>
<td></td>
<td>Etablere og endre organisasjonsstruktur</td>
<td>Plan, organisasjonskart, utredning, evaluering, omorganisering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.06</td>
<td></td>
<td></td>
<td>Utrede og evaluere ledelses- og styringsstruktur</td>
<td>Myndighets-, oppgave- og ansvarsfordeling, delegering, delegeringsmatrise, utredning, evaluering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.07</td>
<td></td>
<td></td>
<td>Administrere og vedlikeholde medlemmer til styrer, råd, utvalg og egne nemnder</td>
<td>Oppnevning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.08</td>
<td></td>
<td></td>
<td>Avholde møter i styrer, råd, utvalg og egne nemnder</td>
<td>Innkalling, saksdokumenter, referat/protokoll, orientering om vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.09</td>
<td></td>
<td></td>
<td>Avholde møter i fora og nettverk</td>
<td>Innkalling, saksdokumenter, referat, informasjon/tilråding til besluttende myndighet</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.a.10</td>
<td></td>
<td></td>
<td>Samarbeide med studentparlament /studentråd/studentutvalg</td>
<td>Referat, avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.11</td>
<td></td>
<td></td>
<td>Utarbeide forslag og innspill til statsbudsjett</td>
<td>Referat, innspill</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.12</td>
<td></td>
<td></td>
<td>Utarbeide og vedlikeholde kvalitetssystem</td>
<td>Systembeskrivelse, rutiner og retningslinjer, revisjon, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.13</td>
<td></td>
<td></td>
<td>Utarbeide og vedlikeholde internkontroll</td>
<td>Systembeskrivelse, rutiner og retningslinjer, revisjon, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.14</td>
<td></td>
<td></td>
<td>Håndtere avvik</td>
<td>Avvik fra gjeldende regler, rutiner og prosedyrer - registrering, oppfølging</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.a.15</td>
<td></td>
<td></td>
<td>Gjennomføre internkontroll</td>
<td>Vurdering av rutiner, evaluering av gjeldende praksis</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.16</td>
<td></td>
<td></td>
<td>Behandle og følge opp revisjon</td>
<td>Revisjonsrapport, oppfølging av anbefalinger og vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.17</td>
<td></td>
<td></td>
<td>Behandle og følge opp tilsyn</td>
<td>Tilsynsrapport, oppfølging av anbefalinger og vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.18</td>
<td></td>
<td></td>
<td>Administrere høringer</td>
<td>Høringsbrev, høringsnotat, vurdering, innspill, høringsuttalelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.19</td>
<td></td>
<td></td>
<td>Planlegge og gjennomføre interne prosjekter</td>
<td>Mandat, oppnevning av styringsgruppe, prosjektplan, tidsplan, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), endringsmelding, budsjett, prosjektroller, ressursavtale, kontrakt, referat, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.20</td>
<td></td>
<td></td>
<td>Planlegge og gjennomføre prosessforbedring</td>
<td>Kartlegging, tiltak, forankring, prosessdesign, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), automasjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.a.21</td>
<td></td>
<td></td>
<td>Rapportere</td>
<td>Statistikk, rapporter/nøkkeltall til ulike interessenter, rapport til Riksrevisjonen</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.b.01</td>
<td></td>
<td>Økonomi</td>
<td>Utarbeide budsjett</td>
<td>Satsningsforslag til departement, foreløpig tildeling, orientering om statsbudsjett, tildelingsbrev, budsjettrammer, budsjettinnspill, rapport, rammeendringer, supplerende tildelingsbrev, intern fordeling, budsjettforslag, revidert budsjett, vedtatt budsjett</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.b.02</td>
<td></td>
<td></td>
<td>Gjennomføre internkontroll for økonomiområdet</td>
<td>Vurdering av rutiner, evaluering av gjeldende praksis, avstemming, årsoppgjør</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.b.03</td>
<td></td>
<td></td>
<td>Utføre daglig drift</td>
<td>Daglig inntektsføring, hovedbok, korrigering og avstemming, merverdiavgift</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>A.b.04</td>
<td></td>
<td></td>
<td>Gjennomføre bank og betaling</td>
<td>Grunnlag, godkjenning, utbetaling, kapitalvarer, innførselsdeklarasjoner</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.b.05</td>
<td></td>
<td></td>
<td>Gjennomføre bank og betaling - EU-transaksjoner</td>
<td>Grunnlag, godkjenning, utbetaling</td>
<td>K</td>
<td>15 år</td>
</tr>
<tr>
<td>A.b.06</td>
<td></td>
<td></td>
<td>Håndtere faktura</td>
<td>Inngående faktura, utgående faktura, fakturagrunnlag</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.b.07</td>
<td></td>
<td></td>
<td>Betale ut lønn</td>
<td>Avtale, bilag, utbetaling</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.b.08</td>
<td></td>
<td></td>
<td>Håndtere refusjon</td>
<td>Grunnlag, godkjenning, utbetaling</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.b.09</td>
<td></td>
<td></td>
<td>Rapportere innenfor økonomiområdet</td>
<td>Årsrapport, likviditetsrapport, tertialrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.c.01</td>
<td></td>
<td>Personalpolitikk og arbeidsmiljø</td>
<td>Utarbeide personalpolitikk</td>
<td>Utredning, saksfremlegg, Charter and Code (The European Charter for Researchers and Code of Conduct for the Recruitment of Researchers), tiltaksavtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.c.03</td>
<td></td>
<td></td>
<td>Organisere samarbeid mellom arbeidsgiver og arbeidstakerorganisasjoner</td>
<td>Oppnevning, informasjon, drøfting, forhandling, særavtale, referat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.c.04</td>
<td></td>
<td></td>
<td>Kartlegge arbeidsmiljø</td>
<td>Handlingsplan, kartlegging, vernerunde, rapport, tiltaksrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.c.05</td>
<td></td>
<td></td>
<td>Utføre velferdstiltak</td>
<td>Velferdskomité, utlysning av midler, søknad, tildeling</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.c.06</td>
<td></td>
<td></td>
<td>Gjennomføre kompetanseutvikling</td>
<td>Kartlegging av kompetanse, kartlegging av kompetansebehov, plan for kompetanseutvikling, tiltak, søknad om midler, tildeling, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.c.07</td>
<td></td>
<td></td>
<td>Forberede lønnsforhandlinger</td>
<td>Krav, statistikk, leders anbefaling</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.c.08</td>
<td></td>
<td></td>
<td>Gjennomføre lønnsforhandlinger</td>
<td>Møteinnkalling, protokoll, informasjon til ansatt, tilbud, voldgiftsbehandling</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.d.01</td>
<td></td>
<td>Rekruttering og ansettelse av personale</td>
<td>Rekruttere</td>
<td>Behovskartlegging, ressursplanlegging, kapasitetskartlegging, oppdragsbekreftelse, utlysning, offentlig søkerliste, utvidet søkerliste, søknad med CV for den som ansettes, fortrinnsrett, trekk av søknad, arbeidstillatelse, oppnevning av komité, habilitetserklæring, vurdering, merknad til vurdering, innstilling, protokoll</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.d.02</td>
<td></td>
<td></td>
<td>Ansette</td>
<td>Ansettelsesbrev, arbeidsavtale, forhandling av lønn og andre vilkår, ansiennitetsberegning, oppdragsavtale, avslag på tilbud om stilling</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.01</td>
<td></td>
<td>Personalforvaltning</td>
<td>Administrere mottak av ansatt</td>
<td>Mottaksplan, bestille oppretting av bruker, tildeling av utstyr/arbeidsplass, mentor, informasjonspakke, opplæring</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>A.e.02</td>
<td></td>
<td></td>
<td>Administrere arbeidsordning</td>
<td>Avtale om arbeidssted, avtale om arbeidstid, individuell arbeidsplan, vaktplan</td>
<td>K</td>
<td>60 år</td>
</tr>
<tr>
<td>A.e.03</td>
<td></td>
<td></td>
<td>Administrere kortere fravær og ferieavvikling</td>
<td>Fravær uten betydning for den ansattes pensjonsforhold, kortere permisjon med lønn, velferdspermisjon med lønn, søknad, søknad om overføring, søknad om forskudd, søknad om utsettelse, inndragning</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>A.e.04</td>
<td></td>
<td></td>
<td>Administrere lærlinger</td>
<td>Lærlingkontrakt, oppfølging, dialog med fylkeskommune, intern opplæringsplan, fagprøve</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.05</td>
<td></td>
<td></td>
<td>Administrere praktikanter og personer i arbeidsforberedende trening (AFT)</td>
<td>Kontrakt/praksisavtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.06</td>
<td></td>
<td></td>
<td>Forlenge midlertidig ansettelse</td>
<td>Anmodning om forlengelse, forlengelsesbrev, beregning av forlenget stipendperiode, melding om forlenget ansettelse, ny avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.07</td>
<td></td>
<td></td>
<td>Behandle søknad om permisjon</td>
<td>Fravær med betydning for den ansattes pensjonsforhold, søknad</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.08</td>
<td></td>
<td></td>
<td>Behandle søknad om personlig kompetanseopprykk</td>
<td>Søknad, oppnevning av komité, vurdering, merknad til vurdering, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.09</td>
<td></td>
<td></td>
<td>Behandle søknad om endring i stillingsforhold</td>
<td>Søknad</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.10</td>
<td></td>
<td></td>
<td>Administrere sidegjøremål og bierverv</td>
<td>Melding om sidegjøremål, melding om eierinteresse, melding om bierverv, avtale, avslag, unntak av sidegjøremål/bierverv fra offentlighet</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.11</td>
<td></td>
<td></td>
<td>Forvalte ansattmobilitet</td>
<td>Søknad, avtale, oppfølging, tildeling av midler, rapportering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.12</td>
<td></td>
<td></td>
<td>Følge opp sykefravær</td>
<td>Sykmelding, referat fra dialogmøte, oppfølgingsplan, kartlegging av tilretteleggingsbehov, tilretteleggingstiltak, arbeidsevnevurdering, IA-tiltak (Inkluderende arbeidsliv), rapport</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.e.13</td>
<td></td>
<td></td>
<td>Følge opp ansatte ved arbeidsulykke, yrkesskade eller yrkessykdom</td>
<td>Skademelding, avdekking og verifisering, rapport, tiltak, erstatningskrav</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.14</td>
<td></td>
<td></td>
<td>Tilrettelegge arbeidsplass</td>
<td>Tiltak</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.e.15</td>
<td></td>
<td></td>
<td>Behandle økonomisk tap i jobbsammenheng</td>
<td>Krav om erstatning</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>A.e.16</td>
<td></td>
<td></td>
<td>Behandle helt eller delvis opphør av arbeidsforhold</td>
<td>Oppsigelse, pensjon, avtalefestet pensjon (AFP), delvis avtalefestet pensjon, uføretrygd</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.17</td>
<td></td>
<td></td>
<td>Behandle disiplinærsak</td>
<td>Referat, ordensstraff, suspensjonsvedtak, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.18</td>
<td></td>
<td></td>
<td>Behandle avskjed</td>
<td>Avskjedigelse, rettsdokumenter, klage, tjenestetidsattest</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.e.19</td>
<td></td>
<td></td>
<td>Behandle bortgang</td>
<td>Registrering av bortgang (mors), interne varsler, sluttoppgjørsberegning, utlevering av personlige eiendeler og data</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.f.01</td>
<td></td>
<td>Helse, miljø og sikkerhet</td>
<td>Utføre opplæring i helse, miljø og sikkerhet (HMS)</td>
<td>Opplæringsplan, deltakerliste, kursmateriell</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.f.02</td>
<td></td>
<td></td>
<td>Utføre arbeidsmiljøundersøkelser</td>
<td>Undersøkelse, rapport, tiltaksrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.f.03</td>
<td></td>
<td></td>
<td>Utføre løpende oppfølging av helse, miljø og sikkerhet (HMS)</td>
<td>HMS-rundeplan, rapport</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.f.04</td>
<td></td>
<td></td>
<td>Følge opp strålevern</td>
<td>Oppnevning av strålevernskoordinator, dosimetri, avhending av radioaktivt avfall</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.f.05</td>
<td></td>
<td></td>
<td>Føre tilsyn med helse, miljø og sikkerhet (HMS)</td>
<td>Plan, dokumentasjon, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.f.06</td>
<td></td>
<td></td>
<td>Håndtere avvik</td>
<td>Avvik fra gjeldende regler, rutiner og prosedyrer - registrering, oppfølging</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.f.07</td>
<td></td>
<td></td>
<td>Håndtere hendelse</td>
<td>Uønsket og uventet hendelse - registrering, oppfølging</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.f.08</td>
<td></td>
<td></td>
<td>Håndtere konflikt</td>
<td>Dokumentasjon, oppfølging</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.f.09</td>
<td></td>
<td></td>
<td>Behandle varsling av kritikkverdige forhold</td>
<td>Varsel, dokumentasjon, referat, tiltak, svar til varsler, sluttrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.g.01</td>
<td></td>
<td>Sikkerhet og beredskap</td>
<td>Utarbeide og vedlikeholde sikkerhet- og beredskapsstrategi</td>
<td>Strategidokument, krisedokumentasjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.g.02</td>
<td></td>
<td></td>
<td>Utarbeide overordnede risiko- og sårbarhetsvurderinger</td>
<td>Risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA)</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.g.03</td>
<td></td>
<td></td>
<td>Planlegge gjennomføring av sikkerhets- og beredskapsarbeid</td>
<td>Beredskapsplan, kriseplan, evakueringsplan, referat, rapport, sikkerhetsrevisjon, kontinuitetsplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.g.04</td>
<td></td>
<td></td>
<td>Gjennomføre beredskapsøvelse</td>
<td>Referat, program, plan, rapport, evaluering, samhandling med eksterne</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.g.05</td>
<td></td>
<td></td>
<td>Håndtere sikkerhetsavvik</td>
<td>Avvik fra gjeldende regler, rutiner og prosedyrer - registrering, oppfølging</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.g.06</td>
<td></td>
<td></td>
<td>Håndtere sikkerhetshendelse</td>
<td>Uønsket og uventet hendelse - registrering, oppfølging</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.h.01</td>
<td></td>
<td>Bygg- og eiendomsforvaltning</td>
<td>Styre eiendomsforvaltning</td>
<td>Strategidokument</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.h.02</td>
<td></td>
<td></td>
<td>Styre bærekraft og miljø</td>
<td>Strategidokument, miljøtiltaksplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.h.03</td>
<td></td>
<td></td>
<td>Drifte og vedlikeholde bygninger og anlegg</td>
<td>Renholdsplan, vaktholdsplan, driftsplan, energibruksanalyse, parkeringsplan, arealbruksplan</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.h.04</td>
<td></td>
<td></td>
<td>Forvalte bygninger og anlegg</td>
<td>Bygging, ombygging, utvidelse, behovsanalyse, forprosjekt, detaljprosjekt, arealplan, ikke antatte tilbud, prosjektplan, risiko- og sårbarhetsanalyse (ROS), teknisk dokumentasjon, evaluering, arbeidstegning, tegning og beskrivelse av oppbygging og kvalitet, overtakelsesdokumentasjon, avtale om leie av areal/bygning, avtale om utleie av areal/bygning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.h.05</td>
<td></td>
<td></td>
<td>Administrere romreservasjon</td>
<td>Søknad, dokumentasjon</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>A.h.06</td>
<td></td>
<td></td>
<td>Drifte og vedlikeholde inventar, utstyr og transportmidler</td>
<td>Behovsanalyse, driftsavtale, supportavtale, vedlikeholdsplan</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.h.07</td>
<td></td>
<td></td>
<td>Forvalte kunstsamling og utsmykning av bygninger og anlegg</td>
<td>Register, konservering, restaurering, innlånsavtale, utlånsavtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.i.01</td>
<td></td>
<td>Anskaffelse av varer og tjenester</td>
<td>Avklare behov</td>
<td>Behovsbeskrivelse, budsjettramme</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.i.02</td>
<td></td>
<td></td>
<td>Forberede og gjennomføre anskaffelse av varer og tjenester</td>
<td>Markedsundersøkelse/ -dialog, befaring, finansiering, kravspesifikasjon, konkurransegrunnlag, risiko- og sårbarhetsanalyse (ROS), anbudsdokument, tilbud, dialog med tilbydere, evaluering, tildeling, meddelelse om valg, klage, avtale, anskaffelsesprotokoll</td>
<td>K</td>
<td>Vurderes i den enkelte saken, minimum 5 år</td>
</tr>
<tr>
<td>A.i.03</td>
<td></td>
<td></td>
<td>Bestille varer</td>
<td>Spesifikasjon, rekvisisjon, godkjenning, bestilling, ordrebekreftelse, varemottak, reklamasjon</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.i.04</td>
<td></td>
<td></td>
<td>Følge opp avtaler</td>
<td>Avtale, behovsavklaring, revidert avtale, varsel om prisregulering</td>
<td>K</td>
<td>Vurderes i den enkelte saken, minimum 5 år</td>
</tr>
<tr>
<td>A.i.05</td>
<td></td>
<td></td>
<td>Drive innkjøpsstøtte</td>
<td>Behovsmelding, rådgivning, tilbakemelding om avtale</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>A.j.01</td>
<td></td>
<td>Informasjons- og dokumentasjons-forvaltning</td>
<td>Styre virksomhetens data, informasjon og dokumentasjon</td>
<td>Strategidokument, instruks for informasjonssikkerhet, instruks for personvern, politikk og rutine for tildeling av tilgang, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), intern tilgangsstyring</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.j.02</td>
<td></td>
<td></td>
<td>Motta data, informasjon og dokumentasjon</td>
<td>Brukerstøtte, rådgivning, opplæring</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>A.j.03</td>
<td></td>
<td></td>
<td>Planlegge og administrere lagring og gjenbruk av data, informasjon og dokumentasjon</td>
<td>Plan, bevaring- og kassasjonsplan, klassifikasjonssystem, prosedyre for å sikre korrekte data og gjenfinning, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), evaluering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.j.04</td>
<td></td>
<td></td>
<td>Tilgjengeliggjøre data, informasjon og dokumentasjon</td>
<td>Rutine for journalføring og offentlig journal, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), avtale om publisering av elektronisk journal og dokumenter</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.j.05</td>
<td></td>
<td></td>
<td>Behandle innsynskrav</td>
<td>Innsynskrav, vedtak, klage, oversendelse til Felles klagenemnd</td>
<td>B</td>
<td>Ev. 2 år</td>
</tr>
<tr>
<td>A.j.06</td>
<td></td>
<td></td>
<td>Avlevere dokumentasjon for langtidslagring</td>
<td>Periodisering, test av uttrekk, uttrekk, dialog med arkivdepot, kassasjonsliste, dokumentasjon, bortsetting, deponering, avlevering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.j.07</td>
<td></td>
<td></td>
<td>Overføre arkiv</td>
<td>Avtale om overføring til annet offentlig organ, avtale om overføring til privat rettssubjekt</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.j.08</td>
<td></td>
<td></td>
<td>Behandle tap av arkivmateriale og uhjemlet kassasjon</td>
<td>Dokumentasjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.k.01</td>
<td></td>
<td>Bibliotek</td>
<td>Planlegge og styre bibliotekfunksjonen</td>
<td>Strategidokument</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.k.02</td>
<td></td>
<td></td>
<td>Utføre drift og vedlikehold</td>
<td>Klassifisering og registrering av anskaffelser, abonnementer, digitalisering, forvaltning og drift av databaser</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.k.03</td>
<td></td>
<td></td>
<td>Forvalte forskningsdataarkiv</td>
<td>Administrasjon av forskningsdataarkiv</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.l.01</td>
<td></td>
<td>IT-planlegging og -forvaltning</td>
<td>Styre IT-funksjonen</td>
<td>Strategidokument</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.l.02</td>
<td></td>
<td></td>
<td>Planlegge og implementere IT-løsninger</td>
<td>Behovsanalyse, systemarkitektur, kontrakt, driftsavtale, supportavtale, testplan, evaluering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.l.03</td>
<td></td>
<td></td>
<td>Drifte og forvalte IT-løsninger</td>
<td>Vedlikeholdsplan, oppdateringsplan, sikkerhetsplan, feilretting, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), oppretting og validering av bruker, opplæring, brukerstøtte, avslutning av drift og forvaltning</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.l.04</td>
<td></td>
<td></td>
<td>Forvalte avtaler og systemer for forskningsdata</td>
<td>Avtale, vedlikeholdsplan, oppdateringsplan, sikkerhetsplan, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), feilretting, opplæring, brukerstøtte, avslutning av drift og forvaltning</td>
<td>K</td>
<td>30 år</td>
</tr>
<tr>
<td>A.l.05</td>
<td></td>
<td></td>
<td>Tilby digitale forskningsressurser</td>
<td>Vedlikeholdsplan, oppdateringsplan, sikkerhetsplan, feilretting, risiko- og sårbarhetsanalyse (ROS), vurdering av personvernkonsekvenser (Data Protection Impact Assessment, DPIA), opplæring, brukerstøtte, avslutning av drift og forvaltning</td>
<td>K</td>
<td>30 år</td>
</tr>
<tr>
<td>A.l.06</td>
<td></td>
<td></td>
<td>Tilby digitale læringsressurser</td>
<td>Vedlikeholdsplan, oppdateringsplan, sikkerhetsplan, feilretting, opplæring, brukerstøtte, avslutning av drift og forvaltning</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>A.m.1</td>
<td></td>
<td>Intern kommunikasjon</td>
<td>Planlegge, styre og evaluere internkommunikasjonsfunksjonen</td>
<td>Strategidokument, retningslinjer, innholdsveiledning, plan, utredning, evaluering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>A.m.2</td>
<td></td>
<td></td>
<td>Planlegge og gjennomføre interne informasjonstiltak</td>
<td>Planlegge og gjennomføre informasjonsmøter, undersøkelser, utforme og gjennomføre interne kommunikasjonskampanjer, planlegge og gjennomføre andre interne kommunikasjonstiltak</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>B.a.01</td>
<td>Forskning</td>
<td>Forskningsstrategi og -kvalitet</td>
<td>Utarbeide forskningsstrategi</td>
<td>Medvirkningsplan, innspill, referat, strategidokument, saksfremlegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.02</td>
<td></td>
<td></td>
<td>Analysere og identifisere finansieringskilder for forskning</td>
<td>Saksfremlegg, informasjon fra fagmiljøer</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.03</td>
<td></td>
<td></td>
<td>Informere og mobilisere om forskningsprogrammer og utlysninger</td>
<td>Saksfremlegg, referat, protokoll</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.04</td>
<td></td>
<td></td>
<td>Prioritere potensielle forskningsprogrammer og prosjekter</td>
<td>Betingelser for prioritering, intern utlysning, saksfremlegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.05</td>
<td></td>
<td></td>
<td>Tildele interne forsknings- og utviklingsmidler og -stipend</td>
<td>Utlysning, søknad, vurdering, tildeling, avslag</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.06</td>
<td></td>
<td></td>
<td>Etablere og utvikle forskningssamarbeid</td>
<td>Invitasjon, intensjonsbrev, Memorandum of Understanding (MOU)/intensjonsavtale, partneravtale, samarbeidsavtale, konsortiumavtale, endringsavtale, revidert/fornyet avtale, Material Transfer Agreement, mandat/formål, handlingsplan, referat, budsjett, tildelingsbrev, avslutte samarbeid</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.07</td>
<td></td>
<td></td>
<td>Forvalte midler til internasjonale stipendprogram og samarbeidsprosjekter</td>
<td>Intern opplæring og informasjon, utlysning, søknad, tildelingsbrev, kontrakt, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.08</td>
<td></td>
<td></td>
<td>Evaluere og forbedre kvaliteten på forskningsaktiviteter og -prosjekter</td>
<td>Prosessdesign, statistikk, evaluering, analyse, referat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.09</td>
<td></td>
<td></td>
<td>Støtte kvalitet i forskning</td>
<td>Systembeskrivelse, rutiner og retningslinjer, veiledning, forskningsgruppebeskrivelse, internasjonal overenskomst, godkjenning, evaluering, forsøksprotokoll, revisjon, rapport, monitoreringsrapport, forskningsetiske vurderinger</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.10</td>
<td></td>
<td></td>
<td>Sikre samsvar med juridiske og etiske retningslinjer</td>
<td>Referat, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.11</td>
<td></td>
<td></td>
<td>Opprette og administrere Advisory Board</td>
<td>Oppnevning, mandat, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.12</td>
<td></td>
<td></td>
<td>Følge opp rankinger</td>
<td>Invitasjon, datainnsending, datavalidering, rapport/kunngjøring</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.13</td>
<td></td>
<td></td>
<td>Administrere politikk for åpen vitenskap</td>
<td>Retningslinjer for publiseringsstøtte, retningslinjer for tildeling av incentivmidler</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.14</td>
<td></td>
<td></td>
<td>Utarbeide politikk for immaterielle rettigheter</td>
<td>Forslag, saksfremlegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.15</td>
<td></td>
<td></td>
<td>Samarbeide med Mattilsynet</td>
<td>Tilsynsrapport, eksporttillatelse, importtillatelse, kjøttkontrollrapport, kassasjonsvedtak, vedtak om godtgjørelse for nødhjelp til dyr, rapportering av forsøksdyr</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.16</td>
<td></td>
<td></td>
<td>Styrke forskningspublisering</td>
<td>Analyse, handlingsplan, saksfremlegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.17</td>
<td></td>
<td></td>
<td>Fastsette rammer for forsknings- og utviklingstermin</td>
<td>Avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.18</td>
<td></td>
<td></td>
<td>Tildele forsknings- og utviklingstermin</td>
<td>Utlysning, søknad, saksfremlegg, søknad om endring, faglig rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.19</td>
<td></td>
<td></td>
<td>Tildele forsknings- og utviklingstermin - oppgjør</td>
<td>Oppgjørsskjema</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>B.a.20</td>
<td></td>
<td></td>
<td>Administrere program for å utvikle unge forskere</td>
<td>Saksfremlegg, kursplan, informasjon til fagmiljøer, evaluering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.a.21</td>
<td></td>
<td></td>
<td>Opprette og administrere forskerskole</td>
<td>Konsortiumavtale, kontrakt, tildelingsbrev, budsjett, søknad om finansiering, oppnevning av styre, midtveisevaluering, framdriftsrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.01</td>
<td></td>
<td>Forskningsprosjekt</td>
<td>Forvalte og tildele forskningsmidler</td>
<td>Saksfremlegg, informasjon til fagmiljøer</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.02</td>
<td></td>
<td></td>
<td>Forvalte og tildele interne forskningsmidler</td>
<td>Saksfremlegg, utlysning, evaluering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.03</td>
<td></td>
<td></td>
<td>Forvalte prosjektsamarbeidsavtaler</td>
<td>Avtale, Non-disclosure Agreement, referat, revidert/fornyet avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.04</td>
<td></td>
<td></td>
<td>Søke om midler til og administrere Senter for fremragende forskning</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, kontrakt/tilsagnsbrev, framdriftsrapport, midtveisevaluering, forlengelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.05</td>
<td></td>
<td></td>
<td>Søke om midler til og administrere Senter for forskningsdrevet innovasjon</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, kontrakt/tilsagnsbrev, framdriftsrapport, midtveisevaluering, forlengelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.06</td>
<td></td>
<td></td>
<td>Søke om midler til og administrere prosjekter finansiert av Norges forskningsråd</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, dialog med finansiør og samarbeidspartnere, kontrakt/tilsagnsbrev, framdriftsrapport, endringsmelding midtveisevaluering, forlengelse, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.07</td>
<td></td>
<td></td>
<td>Søke om midler til og administrere EU-finansierte prosjekter</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, dialog med finansiør og samarbeidspartnere, kontrakt/tilsagnsbrev, framdriftsrapport, endringsmelding midtveisevaluering, forlengelse, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.08</td>
<td></td>
<td></td>
<td>Søke om midler til og administrere andre eksternt finansierte prosjekter</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, dialog med finansiør og samarbeidspartnere, kontrakt/tilsagnsbrev, framdriftsrapport, endringsmelding midtveisevaluering, forlengelse, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.09</td>
<td></td>
<td></td>
<td>Innhente godkjenning (regionale komiteer (REK)/Datatilsynet/Personvernombudet mv.)</td>
<td>Søknad, godkjenning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.10</td>
<td></td>
<td></td>
<td>Innhente tillatelse eksternt (innsamling av data mv.)</td>
<td>Samtykke fra forsøkspersoner, trekk av samtykke</td>
<td>K</td>
<td>Ved prosjektets avslutning</td>
</tr>
<tr>
<td>B.b.11</td>
<td></td>
<td></td>
<td>Gjennomføre internt finansiert forskningsprosjekt</td>
<td>Periodisk faglig og økonomisk rapportering, sluttmelding, rapport, videre planer</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.12</td>
<td></td>
<td></td>
<td>Utføre oppdragsvirksomhet (fagekspertise)</td>
<td>Sjekkliste for bidrags- og oppdragsfinansierte prosjekter, svar på anbud, vurdering, kontrakt/tildelingsbrev, godkjenning av kontrakt, budsjett, forlengelse, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.13</td>
<td></td>
<td></td>
<td>Håndtere forskningsdata</td>
<td>Prinsipper og retningslinjer for håndtering av forskningsdata, datahåndteringsplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.14</td>
<td></td>
<td></td>
<td>Gjøre forskningsetiske vurderinger</td>
<td>Oppnevning, lovpålagt opplæring, juridisk rådgivning, vurdering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.15</td>
<td></td>
<td></td>
<td>Håndtere avvik og behandle forskningsetiske brudd</td>
<td>Varsel, melding om uønskede medisinske hendelser, referat, saksfremlegg, innstilling, klage, ekstern kildeinformasjon, juridisk rådgivning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.b.16</td>
<td></td>
<td></td>
<td>Rapportere forskningsresultater</td>
<td>Rapport, forskningsmelding, deliverables</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.c.01</td>
<td></td>
<td>Forskningsstøtte</td>
<td>Veilede og informere</td>
<td>Spesialisert veiledning, opplæringsopplegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.c.02</td>
<td></td>
<td></td>
<td>Rapportere til nasjonalt forskningsinformasjonssystem (Cristin)</td>
<td>Publikasjonsliste, godkjenning fra fagmiljø, evaluering av publikasjonsform, revidert/godkjent liste, nominering av publiseringskanaler til opprykk</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.c.03</td>
<td></td>
<td></td>
<td>Opprette og vedlikeholde forskningsinfrastruktur</td>
<td>Utlysning av midler, bevilgning, administrere og vedlikeholde forskningsanlegg og laboratorier, administrere og vedlikeholde utstyr og databaser, tillatelser, evaluering, søkerstøtte, søknad, prosjektbeskrivelse, lisens</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.c.04</td>
<td></td>
<td></td>
<td>Publisere forskning</td>
<td>Søknad om støtte til publisering, vitenskapelig publikasjon, fagfellevurdering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.c.05</td>
<td></td>
<td></td>
<td>Publisere forskning - driftsoppgaver</td>
<td>Språkvask, korrektur, publiseringsbonus</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>B.c.06</td>
<td></td>
<td></td>
<td>Følge opp politikk for åpen vitenskap</td>
<td>Publisering, søknad om midler, tildeling, institusjonens tidsskrifter</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.c.07</td>
<td></td>
<td></td>
<td>Følge opp politikk for åpen vitenskap - driftsoppgaver</td>
<td>Refusjon av publiseringskostnader</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>B.d.01</td>
<td></td>
<td>Forskningsrettigheter og kommersialisering</td>
<td>Kommersialisere forskningsresultater</td>
<td>Avtale, Disclosure of Invention, oppfinneravtale, lisensavtale, bedriftsetablering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.d.02</td>
<td></td>
<td></td>
<td>Realisere ikke-kommersielt forskningspotensiale</td>
<td>Referat, avtale, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.d.03</td>
<td></td>
<td></td>
<td>Forvalte interne virkemidler for innovasjon</td>
<td>Utlysning, jury, tildelingsbrev, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.d.04</td>
<td></td>
<td></td>
<td>Etablere og utvikle innovasjonssamarbeid</td>
<td>Programplan, utlysning, jury, tildelingsbrev, referat, rapport, evaluering, avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.d.05</td>
<td></td>
<td></td>
<td>Følge opp politikk for immaterielle rettigheter</td>
<td>Registrering av opphav til forskningsideer, registrering av innmeldte forskningsideer, bedriftsetablering, copyright, Disclosure of Invention, lisensavtale, oppfinneravtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.d.06</td>
<td></td>
<td></td>
<td>Søke patent</td>
<td>Søknad, oversikt over patentsøknader, meddelt patent</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>B.d.07</td>
<td></td>
<td></td>
<td>Støtte studentinnovasjon</td>
<td>Søknad, tildeling, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.01</td>
<td>Utdanning</td>
<td>Utdanningsstrategi og -kvalitet</td>
<td>Akkreditere og opprette studietilbud</td>
<td>Søknad, studieplan, emneplan, emnebeskrivelse, samarbeidsavtale, vurdering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.02</td>
<td></td>
<td></td>
<td>Revidere studietilbud</td>
<td>Revidert emnebeskrivelse, revidert studieplan, revidert emneplan, ny utdanningsplan/ emnekombinasjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.03</td>
<td></td>
<td></td>
<td>Reakkreditere studietilbud</td>
<td>Vurdering/redegjørelse av akkrediteringskrav</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.04</td>
<td></td>
<td></td>
<td>Legge ned studietilbud</td>
<td>Begrunnelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.05</td>
<td></td>
<td></td>
<td>Fastsette studieportefølje og opptaksrammer</td>
<td>Bestillingsbrev, forslag om endringer, orientering om vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.06</td>
<td></td>
<td></td>
<td>Følge opp evaluering av studietilbud</td>
<td>Rapport, student- og kandidatundersøkelse, programrapport, emnerapport, oppfølgingsplan/tiltaksplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.07</td>
<td></td>
<td></td>
<td>Administrere ordninger for arbeidsrelevans</td>
<td>Avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.09</td>
<td></td>
<td></td>
<td>Følge opp varsel om læringsmiljø</td>
<td>Varsel, dokumentasjon, tiltak, svar til varsler, sluttrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.10</td>
<td></td>
<td></td>
<td>Følge opp tilsyn</td>
<td>Tilsynsrapport, oppfølging av anbefalinger og vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.11</td>
<td></td>
<td></td>
<td>Dokumentere institusjonens systematiske kvalitetsarbeid</td>
<td>Egenvurderinger, dialogmøter, rapport fra ekstern fagfelle, kvalitetsdialog, oppnevning ekstern fagfelle, tiltaksliste, kvalitetsmelding, plan for periodisk evaluering, handlingsplan for læringsmiljø, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.12</td>
<td></td>
<td></td>
<td>Opprette og administrere Advisory Board</td>
<td>Oppnevning, mandat, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.13</td>
<td></td>
<td></td>
<td>Følge opp rankinger</td>
<td>Invitasjon, datainnsending, datavalidering, rapport/kunngjøring</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.a.14</td>
<td></td>
<td></td>
<td>Inngå og følge opp utdanningssamarbeid</td>
<td>Intensjonsavtale, samarbeidsavtale, konsortiumavtale, referat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.16</td>
<td></td>
<td></td>
<td>Opprette bidrags- og oppdragsfinansierte studietilbud</td>
<td>Prosjektbeskrivelse, oppdragsavtale, budsjett, godkjenning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.17</td>
<td></td>
<td></td>
<td>Administrere mentorordning for studenter</td>
<td>Planlegging, evaluering, opplæringsopplegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.18</td>
<td></td>
<td></td>
<td>Administrere meritteringsordning for undervisere</td>
<td>Kriterier, oppnevning av komité, utlysning, søknad, intervju, innstilling, kunngjøring</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.19</td>
<td></td>
<td></td>
<td>Administrere interne konkurransearenaer</td>
<td>Utlysning, søknad, vurdering, innstilling, tildeling</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.20</td>
<td></td>
<td></td>
<td>Søke om midler og administrere Senter for fremragende utdanning</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, kontrakt/tilsagnsbrev, framdriftsrapport, midtveisevaluering, forlengelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.21</td>
<td></td>
<td></td>
<td>Søke om eksterne midler til utdanningsprosjekter</td>
<td>Søkerstøtte, søknad, avslag med evaluering, budsjett, samarbeidsavtale, kontrakt/tilsagnsbrev, kommunikasjon med finansiør og samarbeidspartnere, framdriftsrapport, midtveisevaluering, forlengelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.22</td>
<td></td>
<td></td>
<td>Forvalte midler til internasjonale stipendprogram og samarbeidsprosjekter</td>
<td>Utlysning, søknad, tildeling, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.23</td>
<td></td>
<td></td>
<td>Inngå og forvalte samarbeidsavtaler med partnerinstitusjoner</td>
<td>Avtale om studentutveksling, avtale om lærerutveksling, avtale om praksisutveksling, referat, revidert/fornyet avtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.24</td>
<td></td>
<td></td>
<td>Forvalte Erasmus +</td>
<td>Utlysning av midler, tildeling fra direktorat, rapport, søknad om Erasmus-charter, finansieringsplan fra direktorat, kontrakt</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.25</td>
<td></td>
<td></td>
<td>Forvalte internasjonale partnerskaps- og stipendprogram (utenfor Erasmus+)</td>
<td>Utlysning av midler, tildeling, søknad, finansieringsplan, kontrakt</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.26</td>
<td></td>
<td></td>
<td>Følge opp deltakelse i internasjonale samarbeidsnettverk</td>
<td>Referat, saksfremlegg, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.27</td>
<td></td>
<td></td>
<td>Følge opp samarbeid med studentorganisasjoner</td>
<td>Retningslinjer, forvaltning, referat, utlysning av midler, søknad om midler</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.a.28</td>
<td></td>
<td></td>
<td>Administrere studentombudsordning</td>
<td>Mandat, årsrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.01</td>
<td></td>
<td>Opptak</td>
<td>Administrere opptakskomiteer</td>
<td>Oppnevning, referat</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.b.02</td>
<td></td>
<td></td>
<td>Fastsette tilbudstall</td>
<td>Dialog med fagmiljø, stipulering, overbooking</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.b.03</td>
<td></td>
<td></td>
<td>Gjennomføre lokalt opptak</td>
<td>Utlysning, søknad, dialog med fagmiljø, validering av utdanning, verifikasjon av vitnemål, tildeling av studierett, supplering og etterfylling av studieplasser, klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.b.04</td>
<td></td>
<td></td>
<td>Gjennomføre lokalt opptak - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.05</td>
<td></td>
<td></td>
<td>Gjennomføre nasjonalt opptak (Nasjonal opptaksmodell, NOM)</td>
<td>Innmelding av studier og opptaksrammer, utlysning, søknad, validering av utdanning, verifikasjon av vitnemål, tildeling av studierett, supplering og etterfylling av studieplasser, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.b.06</td>
<td></td>
<td></td>
<td>Gjennomføre nasjonalt opptak (Nasjonal opptaksmodell, NOM) - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.07</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til studier med opptaksprøver</td>
<td>Søknad, ferdighetsprøve, validering av utdanning, verifikasjon av vitnemål, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.b.08</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til studier med opptaksprøver - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.09</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til utøvende og skapende studier</td>
<td>Utlysning, søknad, intervju, porteføljemappe, spilleprøve, prøve teori/gehør, validering av utdanning, verifikasjon av vitnemål, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.b.10</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til utøvende og skapende studier - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.11</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til ph.d.-program</td>
<td>Søknad, vurdering fra opptakskomité, innstilling, validering av utdanning, verifikasjon av vitnemål, opptaksavtale, ph.d.-avtale, kontrakt for nærings- og offentlig ph.d., klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.12</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til ph.d.-program - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.13</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til etter- og videreutdanning</td>
<td>Utlysning, søknad, kontrakt, tildeling av studierett, deltagerliste</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.b.14</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til spesialistutdanning</td>
<td>Søknad, validering av utdanning, verifikasjon av vitnemål, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.b.15</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til spesialistutdanning - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.16</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til forskerskole</td>
<td>Søknad, validering av utdanning, verifikasjon av vitnemål, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.17</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til forskerskole - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.18</td>
<td></td>
<td></td>
<td>Gjennomføre opptak til talentutviklingsprogram</td>
<td>Søknad, validering av utdanning, tildeling av stipend</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.b.19</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra søknadsfrist</td>
<td>Søknad, klage</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.b.20</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra søknadsfrist - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.21</td>
<td></td>
<td></td>
<td>Behandle søknad om reservasjon av studieplass</td>
<td>Søknad</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.b.22</td>
<td></td>
<td></td>
<td>Håndtere politiattest</td>
<td>Politiattest, supplerende informasjon</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.b.23</td>
<td></td>
<td></td>
<td>Trekke studier fra årets opptak</td>
<td>Søknad, eventuelt trekk</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.b.24</td>
<td></td>
<td></td>
<td>Gjennomføre overgang mellom studieprogram (lokalt)</td>
<td>Utlysning, søknad</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.b.25</td>
<td></td>
<td></td>
<td>Gjennomføre overflytting mellom institusjoner</td>
<td>Søknad, validering av utdanning, verifikasjon av vitnemål, tildeling av studierett, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.01</td>
<td></td>
<td>Undervisning og læringsaktiviteter</td>
<td>Planlegge undervisning</td>
<td>Bemanningsplan, undervisningsopplegg, utstyrsregister</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.02</td>
<td></td>
<td></td>
<td>Lage timeplaner</td>
<td>Timeplandata, dialog med fagmiljø, opprette undervisningsaktiviteter, opprette undervisningsenheter, opprette kull/klasser, revidert timeplan, publisert timeplan</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.03</td>
<td></td>
<td></td>
<td>Behandle oppmelding til undervisning</td>
<td>Undervisningsmelding, rangering</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.04</td>
<td></td>
<td></td>
<td>Utarbeide og tilgjengeliggjøre oversikt over læringsressurser</td>
<td>Litteraturliste, andre læringsressurser (film, opptak av forelesning mv.)</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.05</td>
<td></td>
<td></td>
<td>Bestille kompendier</td>
<td>Innholdsfortegnelse, klarering for bruk av kilder</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.06</td>
<td></td>
<td></td>
<td>Innhente og dokumentere samtykke ved opptak av undervisning</td>
<td>Samtykkeerklæring</td>
<td>K</td>
<td>15 år</td>
</tr>
<tr>
<td>C.c.07</td>
<td></td>
<td></td>
<td>Behandle obligatoriske læringsaktiviteter</td>
<td>Innlevert oppgave, gjennomført aktivitet, oppmøteliste, avtale om spesialpensum, varsel om fare for underkjenning, søknad om utsettelse, søknad om gjentak, klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.c.08</td>
<td></td>
<td></td>
<td>Behandle obligatoriske læringsaktiviteter - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.09</td>
<td></td>
<td></td>
<td>Behandle søknad om fritak fra obligatoriske læringsaktiviteter</td>
<td>Søknad, dokumentasjon, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.10</td>
<td></td>
<td></td>
<td>Behandle søknad om fritak fra obligatoriske læringsaktiviteter - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.11</td>
<td></td>
<td></td>
<td>Behandle korttidsfravær fra obligatorisk undervisning og andre læringsaktiviteter</td>
<td>Søknad</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.12</td>
<td></td>
<td></td>
<td>Behandle sak ved mistanke om fusk og plagiat i tilknytning til læringsaktiviteter</td>
<td>Plagiatkontroll, melding</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.13</td>
<td></td>
<td></td>
<td>Behandle sak ved mistanke om fusk og plagiat i tilknytning til læringsaktiviteter - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.14</td>
<td></td>
<td></td>
<td>Inngå og følge opp avtaler med praksissted</td>
<td>Budsjett, finansiering, avtale, årlig bestilling av praksisplasser, referat, tildeling av midler til utviklingsprosjekter</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.15</td>
<td></td>
<td></td>
<td>Tilrettelegge praksis</td>
<td>Søknad</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.16</td>
<td></td>
<td></td>
<td>Behandle søknad om alternativt praksissted</td>
<td>Søknad</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.17</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra gjentakskvote for praksis</td>
<td>Søknad</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.18</td>
<td></td>
<td></td>
<td>Tildele praksisplass</td>
<td>Søknad, kontrakt/praksisavtale</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.19</td>
<td></td>
<td></td>
<td>Følge opp student i praksis</td>
<td>Taushetserklæring, vaksineattest, politiattest, veiledning, vurderingsrapport, midtveisevaluering, søknad om tilgang til systemer, dokumentasjon av aktive studenter</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.20</td>
<td></td>
<td></td>
<td>Behandle søknad om permisjon fra praksis</td>
<td>Søknad, dialog med praksissted, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.21</td>
<td></td>
<td></td>
<td>Behandle søknad om permisjon fra praksis - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.22</td>
<td></td>
<td></td>
<td>Behandle søknad om økonomisk tilskudd under praksis</td>
<td>Søknad, vedtak, utbetaling, refusjon</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.c.23</td>
<td></td>
<td></td>
<td>Behandle resultat fra praksis</td>
<td>Praksisattest, sensur</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.24</td>
<td></td>
<td></td>
<td>Behandle tvil om ikke bestått praksis</td>
<td>Vurdering, tiltaksplan, referat, tvilsmelding, rettighetsbrev, varsel om fare for stryk, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.25</td>
<td></td>
<td></td>
<td>Behandle tvil om ikke bestått praksis - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.26</td>
<td></td>
<td></td>
<td>Behandle avbrutt/ikke møtt praksis</td>
<td>Dialog med student, dialog med praksissted</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.c.27</td>
<td></td>
<td></td>
<td>Administrere feltarbeid og tokt</td>
<td>Forsikring, risiko- og sårbarhetsanalyse (ROS), egenerklæringsskjema</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.28</td>
<td></td>
<td></td>
<td>Administrere internship</td>
<td>Utlysning, kontrakt</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.c.29</td>
<td></td>
<td></td>
<td>Administrere lab- og verkstedsundervisning/veiledning/øving</td>
<td>Godkjent kurs i helse, miljø og sikkerhet (HMS)</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.c.30</td>
<td></td>
<td></td>
<td>Avklare opphavsrett i henhold til åndsverkloven</td>
<td>Studentarbeid, patent</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.01</td>
<td></td>
<td>Oppfølging av studenter og ph.d.-kandidater</td>
<td>Følge opp studenter ved semesterstart</td>
<td>Oversikt over aktiviteter for studiestart</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.02</td>
<td></td>
<td></td>
<td>Behandle søknad om utsatt studiestart eller fritak fra obligatorisk oppmøte til studiestart</td>
<td>Søknad, innvilgelse, vedtak om inndratt studierett, klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.d.03</td>
<td></td>
<td></td>
<td>Behandle søknad om utsatt studiestart eller fritak fra obligatorisk oppmøte til studiestart - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.04</td>
<td></td>
<td></td>
<td>Tildele stipend/økonomisk støtte til studenter</td>
<td>Utlysning, søknad, tildeling</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.d.05</td>
<td></td>
<td></td>
<td>Behandle refusjoner av og fritak for semesteravgift</td>
<td>Kvittering for betalt semesteravgift ved annen institusjon, kvittering for betalt studieavgift (etter- og videreutdanning), refusjon av beløp til Studentenes og akademikernes internasjonale hjelpefond (SAIH), klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.d.06</td>
<td></td>
<td></td>
<td>Bekrefte studentstatus</td>
<td>Forespørsel, samtykkeerklæring, bekreftelse</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.07</td>
<td></td>
<td></td>
<td>Oppnevne veileder for student</td>
<td>Veiledningsavtale</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.08</td>
<td></td>
<td></td>
<td>Tilrettelegge studiehverdagen</td>
<td>Søknad, dokumentasjon, kartlegging/ vurdering, kontrakt med studiekontakt, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.09</td>
<td></td>
<td></td>
<td>Tilrettelegge studiehverdagen - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.10</td>
<td></td>
<td></td>
<td>Behandle søknad om godkjenning av ekstern utdanning eller realkompetanse</td>
<td>Søknad, dokumentasjon, vurdering (faglig og administrativ), oppdatert utdanningsplan, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.11</td>
<td></td>
<td></td>
<td>Behandle søknad om godkjenning av ekstern utdanning eller realkompetanse - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.12</td>
<td></td>
<td></td>
<td>Vedlikeholde studierett og utdanningsplan</td>
<td>Forhåndsvarsel (tap/ inndragning av studierett, forsinket progresjon), dokumentasjon, veiledning/ progresjonssamtale, søknad om endring av kull/utvidet studierett, søknad om endring av studieprogram, sluttmelding, avslutning ved bortgang, oppdatert utdanningsplan</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.13</td>
<td></td>
<td></td>
<td>Behandle søknad om godkjenning av prosjektoppgave</td>
<td>Prosjektbeskrivelse, oppnevning av veileder, tildeling av økonomisk støtte</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.14</td>
<td></td>
<td></td>
<td>Gi karriere-, gjennomførings- og studieveiledning</td>
<td>Kurstilbud, referat fra veiledning</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.15</td>
<td></td>
<td></td>
<td>Behandle søknad om gyldig forfall</td>
<td>Søknad, klage</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.16</td>
<td></td>
<td></td>
<td>Behandle søknad om gyldig forfall - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.17</td>
<td></td>
<td></td>
<td>Behandle søknad om permisjon</td>
<td>Søknad, dokumentasjon, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.18</td>
<td></td>
<td></td>
<td>Behandle søknad om permisjon - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.19</td>
<td></td>
<td></td>
<td>Behandle søknad om hospitering</td>
<td>Søknad, klage</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.20</td>
<td></td>
<td></td>
<td>Behandle søknad om hospitering - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.21</td>
<td></td>
<td></td>
<td>Behandle politiattest med merknad</td>
<td>Politiattest, uttalelse fra student, uttalelse fra institutt, følgebrev fra praksisadministrasjon, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.22</td>
<td></td>
<td></td>
<td>Behandle politiattest med merknad - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.23</td>
<td></td>
<td></td>
<td>Behandle disiplinærsak</td>
<td>Dokumentasjon, skriftlig advarsel/ forhåndsvarsel, innstilling til institusjonens klagenemnd, klage, oversendelse av klage til Felles klagenemnd, utestengning, tap av eksamensrett, politianmeldelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.24</td>
<td></td>
<td></td>
<td>Behandle skikkethetssak</td>
<td>Tvilsmelding, vurderingssamtale, saksutredning, utvidet veiledning, innstilling til skikkethetsnemnd, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.25</td>
<td></td>
<td></td>
<td>Behandle skikkethetssak - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.26</td>
<td></td>
<td></td>
<td>Behandle skikkethetssak - behandle i Felles klagenemnd</td>
<td>Oversendelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.27</td>
<td></td>
<td></td>
<td>Følge opp studenter etter uønsket hendelse</td>
<td>Større ulykker/terrortrusler/fysisk eller psykisk skade mv., forsikring, informasjon, registrere studentdata</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.28</td>
<td></td>
<td></td>
<td>Følge opp påkrevet vaksinering av studenter</td>
<td>Registrere i nasjonalt vaksineregister</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.29</td>
<td></td>
<td></td>
<td>Søke om lisens, studentlisens eller autorisasjon for helsepersonell</td>
<td>Søknad til Helsedirektoratet</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.30</td>
<td></td>
<td></td>
<td>Informere om studentboligordninger</td>
<td>Avtale med studentsamskipnaden, kunngjøring</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.31</td>
<td></td>
<td></td>
<td>Følge opp innreisende utvekslingsstudenter</td>
<td>Søknad, dokumentasjon, nominasjon, Learning Agreement, stipendutbetaling, endring av Learning Agreement, avslutning av utvekslingsopphold</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.d.32</td>
<td></td>
<td></td>
<td>Følge opp utreisende utvekslingsstudenter</td>
<td>Søknad, nominasjon, forhåndsgodkjenning av emner, opptaksbrev, godkjenning, Grant Agreement, utreiseinformasjon, stipendutbetaling, stipendavtale, Learning Agreement, bekreftet opphold, studentrapport (Mobility Tool), registrering i online linguistic support, tilleggsavtale/samtykke, studentrapport</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.33</td>
<td></td>
<td></td>
<td>Behandle søknad om internship for innreisende og utreisende utvekslingsstudenter</td>
<td>Learning Agreement Internship, stipendavtale, rapport, Confirmation</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.34</td>
<td></td>
<td></td>
<td>Følge opp studenter under Students at risk-ordningen</td>
<td>Dokumentasjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.35</td>
<td></td>
<td></td>
<td>Administrere reservasjon av boliger for utenlandske studenter</td>
<td>Avkreftelse (ikke møtt), antallsoversikt</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.d.36</td>
<td></td>
<td></td>
<td>Følge opp oppholdstillatelser og visum for utenlandske studenter</td>
<td>Dialog med Utlendingsdirektoratet og politiet, progresjonsrapport, søknad om forlenget studieopphold, uttalelse/ vurdering</td>
<td>K</td>
<td>15 år</td>
</tr>
<tr>
<td>C.d.37</td>
<td></td>
<td></td>
<td>Oppnevne veileder for ph.d.-kandidat</td>
<td>Veiledningsavtale, endring av veileder</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.38</td>
<td></td>
<td></td>
<td>Følge opp framdriftsrapporter for ph.d.-kandidat</td>
<td>Framdriftsrapport</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.39</td>
<td></td>
<td></td>
<td>Gjennomføre midtveisevaluering for ph.d.-kandidat</td>
<td>Midtveisevaluering, oppfølgingsbrev</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.d.40</td>
<td></td>
<td></td>
<td>Godkjenne opplæringsdel for ph.d.-kandidat</td>
<td>Søknad, dokumentasjon, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.41</td>
<td></td>
<td></td>
<td>Godkjenne opplæringsdel for ph.d.-kandidat - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.42</td>
<td></td>
<td></td>
<td>Behandle forespørsel om å fortsette på ph.d.-program uten stipendiatstilling/finansiering</td>
<td>Oppsigelse av stipendiatstilling, utløp av ansettelsesperiode</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.d.43</td>
<td></td>
<td></td>
<td>Behandle melding om avslunting av ph.d.-program før oppnådd grad</td>
<td>Utskriving, oppsigelse av stipendiatstilling, utløp av ansettelsesperiode</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.01</td>
<td></td>
<td>Vurdering</td>
<td>Utarbeide eksamensoppgave og sensorveiledning</td>
<td>Eksamensoppgave, sensorveiledning,</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.02</td>
<td></td>
<td></td>
<td>Behandle oppmelding til eksamen/vurdering</td>
<td>Kontroll av gjentaksmulighet, kontroll av forkunnskapskrav, oppmelding</td>
<td>K</td>
<td>2 år</td>
</tr>
<tr>
<td>C.e.03</td>
<td></td>
<td></td>
<td>Behandle oppmelding til eksamen/vurdering for privatist</td>
<td>Søknad</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.e.04</td>
<td></td>
<td></td>
<td>Tilrettelegge ved eksamen/vurdering</td>
<td>Søknad, dokumentasjon, vurdering (faglig og administrativ), tiltak, klage</td>
<td>K</td>
<td>15 år</td>
</tr>
<tr>
<td>C.e.05</td>
<td></td>
<td></td>
<td>Tilrettelegge ved eksamen/vurdering - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.06</td>
<td></td>
<td></td>
<td>Oppnevne sensor</td>
<td>Forespørsel, oppnevning, taushetserklæring, oppnevning av kommisjon, oppnevning av kontrollkommisjon, oppnevning av klagekommisjon</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.07</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra forkunnskapskrav</td>
<td>Søknad, klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.e.08</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra forkunnskapskrav - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.09</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra gjentakskvote</td>
<td>Varsel om sletting av eksamensoppmelding, påminnelse, purring, søknad, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.e.10</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra gjentakskvote - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.11</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra oppmeldingsfrist</td>
<td>Søknad, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.e.12</td>
<td></td>
<td></td>
<td>Behandle søknad om dispensasjon fra oppmeldingsfrist - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.13</td>
<td></td>
<td></td>
<td>Behandle søknad om alternativt vurderingstidspunkt</td>
<td>Søknad, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.e.14</td>
<td></td>
<td></td>
<td>Behandle søknad om alternativt vurderingstidspunkt - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.15</td>
<td></td>
<td></td>
<td>Behandle søknad om utsatt eksamen/vurdering</td>
<td>Søknad, klage</td>
<td>K</td>
<td>10 år</td>
</tr>
<tr>
<td>C.e.16</td>
<td></td>
<td></td>
<td>Behandle søknad om utsatt eksamen/vurdering - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.17</td>
<td></td>
<td></td>
<td>Administrere ekstern eksamensavvikling</td>
<td>Oversikt over eksterne steder, søknad, klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.e.18</td>
<td></td>
<td></td>
<td>Administrere ekstern eksamensavvikling - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.19</td>
<td></td>
<td></td>
<td>Gjennomføre eksamen/vurdering</td>
<td>Kontroll av eksamenskollisjoner, publisering av eksamensdatoer, eksamensplan, oppmøteliste, besvarelse</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.e.20</td>
<td></td>
<td></td>
<td>Gjennomføre eksamen/vurdering for utøvende og skapende studier</td>
<td>Oversikt, forestilling/utstilling mv., program, plan for gjennomføring, avtale om bruk av lokale, annonsering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.21</td>
<td></td>
<td></td>
<td>Administrere begrunnelse og sensur</td>
<td>Sensurprotokoll, endringsprotokoll, vurderingsprotokoll for vitenskapsteori og forskningsmetode, begrunnelse for karakter, kontrollsensur med besvarelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.22</td>
<td></td>
<td></td>
<td>Behandle innlevert master- og bacheloroppgave</td>
<td>Melding om oppgave, kontrakt, konfidensialitetsskjema, avtale om spesialpensum, oppgave, vurdering, publisering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.23</td>
<td></td>
<td></td>
<td>Behandle klage på formelle feil ved eksamen/vurdering</td>
<td>Klage, tilsvar fra eksamensadministrator, tilsvar fra eksaminator, tilsvar fra sensor</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.e.24</td>
<td></td>
<td></td>
<td>Behandle klage på formelle feil ved eksamen/vurdering - behandle i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.e.25</td>
<td></td>
<td></td>
<td>Behandle klage på sensur/eksamenskarakter</td>
<td>Klage, ny sensur, tredjegangs behandling ved karakteravvik, endelig sensur, trekk av klage</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>C.e.26</td>
<td></td>
<td></td>
<td>Behandle mistanke om fusk og plagiat ved eksamen/vurdering</td>
<td>Plagiatkontroll, sensorrapport, rapport fra eksamensvakt, redegjørelse/ uttalelse fra student, referat, orientering om oversendelse til klagenemnd, oversendelse til institusjonens klagenemnd, orientering om at sak frafalles, annullert eksamensresultat, oppdatert RUST-database, vedtak om eksamens- og undervisningsforbud</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.01</td>
<td></td>
<td>Grad og vitnemål</td>
<td>Tildele grad og kvalifikasjon</td>
<td>Malverk, gradfangst, vitnemålsgrunnlag, vitnemål og kvalifikasjon, Diploma Supplement</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.02</td>
<td></td>
<td></td>
<td>Behandle søknad om sluttvurdering (utøvende og skapende studier)</td>
<td>Søknad, utstilling, konsert/forestilling, refleksjonsnotat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.03</td>
<td></td>
<td></td>
<td>Behandle søknad om vurdering av ph.d.-avhandling</td>
<td>Søknad, medforfattererklæring, avhandling</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.04</td>
<td></td>
<td></td>
<td>Behandle errataliste</td>
<td>Errataliste (feilliste, endringsliste)</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.05</td>
<td></td>
<td></td>
<td>Bedømme ph.d.-avhandling</td>
<td>Oppnevning av bedømmelseskomité, habilitetserklæring, innstilling, vedtak om omarbeiding, klausulering, merknad, underkjenning, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.06</td>
<td></td>
<td></td>
<td>Gjennomføre prøveforelesning og disputas</td>
<td>Oppnevning av opponenter, prøveforelesningstema, pressemelding, godkjenning av prøveforelesning, godkjenning av disputas, bekreftelsesbrev, dokumentasjon av oppnådd grad</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.07</td>
<td></td>
<td></td>
<td>Bedømme avhandling/ kunstnerisk utviklingsarbeidsresultat</td>
<td>Oppnevning av bedømmelseskomité og uavhengige sakkyndige, habilitetserklæring, innstilling, vedtak om omarbeiding, klausulering, merknad, underkjenning, klage</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.08</td>
<td></td>
<td></td>
<td>Behandle klage på underkjennelse av ph.d.-avhandling/ kunstnerisk utviklingsarbeidsresultat i klagenemnd</td>
<td>Institusjonens klagenemnd - oversendelse, dokumentasjon, vedtak</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.09</td>
<td></td>
<td></td>
<td>Tildele ph.d.-grad (kreering)</td>
<td>Vitnemål og diplom, doktorgradstildeling, kreering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.10</td>
<td></td>
<td></td>
<td>Behandle søknad om bruk av beskyttet tittel</td>
<td>Søknad</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>C.f.11</td>
<td></td>
<td></td>
<td>Verifisere vitnemål, karakterutskrift og andre utdanningsdokumenter utstedt ved egen institusjon</td>
<td>Henvendelse, verifisering, samtykkeerklæring</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>D.a.01</td>
<td>Formidling</td>
<td>Faglig formidling</td>
<td>Arrangere fagkonferanser/-seminarer</td>
<td>Budsjett, program, grafisk materiell, kontrakt, plan, finansiering, publikasjon, abstract, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.a.02</td>
<td></td>
<td></td>
<td>Samarbeide med eksterne om formidlingsoppdrag</td>
<td>Forespørsel fra ekstern virksomhet, henvendelse til ekstern virksomhet, avtale, formidlingsplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.a.03</td>
<td></td>
<td></td>
<td>Formidle i medier</td>
<td>Intervju, kronikk, foredrag, populærvitenskapelig formidlingsmateriale, samtykke og rettighetshåndtering, publiseringsplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.a.04</td>
<td></td>
<td></td>
<td>Registrere publikasjon</td>
<td>Publikasjonsregistrering i nasjonalt register</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.01</td>
<td></td>
<td>Kommunikasjon og samfunnskontakt</td>
<td>Utarbeide kommunikasjonsplan</td>
<td>Forslag, plan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.02</td>
<td></td>
<td></td>
<td>Holde kontakt med redaksjoner</td>
<td>Intern fagekspertliste, pressekontakt, presseinvitasjon, pressemelding</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.03</td>
<td></td>
<td></td>
<td>Overvåke medieomtale</td>
<td>Statistikk, analyse, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.04</td>
<td></td>
<td></td>
<td>Holde samfunns- og næringslivskontakt</td>
<td>Kontaktregister, handlingsplan, samarbeidsavtale, referat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.05</td>
<td></td>
<td></td>
<td>Samarbeide med kulturaktører</td>
<td>Forespørsel fra ekstern virksomhet, henvendelse til ekstern virksomhet, sponsoravtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.06</td>
<td></td>
<td></td>
<td>Håndtere publikumshenvendelser</td>
<td>Bruk av media, avtale om privat fotografering/filming</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>D.b.07</td>
<td></td>
<td></td>
<td>Utvikle og vedlikeholde grafisk profil og materiell</td>
<td>Avtale, kontrakt, samtykke og rettighetshåndtering, profildokument</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.08</td>
<td></td>
<td></td>
<td>Håndtere profileringsmateriell</td>
<td>Avtale, budsjett, bestillingsrutine</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>D.b.09</td>
<td></td>
<td></td>
<td>Dele ut æresdoktorat</td>
<td>Nominasjon, offentliggjøring, utdeling</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.10</td>
<td></td>
<td></td>
<td>Lage arrangement</td>
<td>Prosjektplan, markedsføringsplan, produksjonsplan, risiko- og sårbarhetsanalyse (ROS), program, grafisk materiell, deltakerliste, avtale, sikkerhetsdokumentasjon, budsjett, invitasjon, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.11</td>
<td></td>
<td></td>
<td>Drive alumnivirksomhet</td>
<td>Alumniregister, kommunikasjon til alumni</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.12</td>
<td></td>
<td></td>
<td>Gjennomføre markedsføringstiltak</td>
<td>Strategi og kanalvalg, målgruppevalg, plan for gjennomføring, markedsføringsmateriell</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.13</td>
<td></td>
<td></td>
<td>Tildele pris</td>
<td>Utlysning, nominasjon, innstilling, utdeling</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.14</td>
<td></td>
<td></td>
<td>Formidle til skoler</td>
<td>Undervisningsopplegg, undervisningmateriell, avtale om besøk, markedsføring av formidlingsopplegg</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.15</td>
<td></td>
<td></td>
<td>Gi kommunikasjonsfaglig veiledning og støtte</td>
<td>Informasjon om kurs, opplæringsopplegg og veiledning for tale/presentasjon/innlegg, mal for kommunikasjonsaktivitet</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>D.b.16</td>
<td></td>
<td></td>
<td>Rekruttere studenter</td>
<td>Aktivitetsplan, budsjett, markedsføringsgrunnlag, materiell, samtykke og rettighetshåndtering, avtale om besøk, avtale om deltakelse på eksternt arrangement, opplegg for studentambassadører</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.17</td>
<td></td>
<td></td>
<td>Markedsføre og selge etter- og videreutdanningstilbud</td>
<td>Strategidokument, handlingsplan, budsjett, salgsplan</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.b.18</td>
<td></td>
<td></td>
<td>Lage semesterstartsarrangement</td>
<td>Invitasjon, samarbeidsavtale, opplæringsopplegg, fadderopplegg, korrespondanse med linjeforeninger</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>D.c.01</td>
<td></td>
<td>Utstillinger og forestillinger</td>
<td>Utarbeide strategi for utstillinger og forestillinger</td>
<td>Strategidokument</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.c.02</td>
<td></td>
<td></td>
<td>Utvikle og produsere utstilling og forestilling</td>
<td>Budsjett, utstillings-/forestillingsproduksjon, markedsføringsplan, konsept, prosjektplan, avtale, gjenstandsutvalg og -innlån, grafisk materiell</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.c.03</td>
<td></td>
<td></td>
<td>Formidle utstilling og forestilling</td>
<td>Avtale om besøk, plan for omvisning, arrangementsplan, besøksstatistikk</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>D.c.04</td>
<td></td>
<td></td>
<td>Leie ut utstilling/objekter</td>
<td>Leieavtale, budsjett, håndteringsplan, tilstandsrapport, vedlikeholdsplan, turnéplan, forsikring, transport, tollbehandling, mottaksrapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.01</td>
<td>Museum og samling</td>
<td>Samlingsforvaltning</td>
<td>Utarbeide samlingsstrategi</td>
<td>Strategi for innsamling og forvaltning, risiko- og sårbarhetsanalyse (ROS), årsplan, referat, evaluering, rapport, samlingsplan, prioritering og beredskapsplan, flytting deaksesjon, kassering, statistikk, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.02</td>
<td></td>
<td></td>
<td>Utarbeide samlingsstrategi for bibliotek</td>
<td>Handlingsplan, plan for bevaring og kassering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.03</td>
<td></td>
<td></td>
<td>Utarbeide og vedlikeholde kvalitetssystem for bibliotek</td>
<td>Rutine, kvalitetspolitikk</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.04</td>
<td></td>
<td></td>
<td>Kuratere samlinger</td>
<td>Inntaksvurdering, katalogisering, vurdering av tilstand/skade, vurdering av kassasjon/avhending</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.05</td>
<td></td>
<td></td>
<td>Motta objekt til samlingene</td>
<td>Funnskjema, bekreftelse på mottak, prosjektavtale, analyse, dokumentasjon, kartavmerking, mottak av innleveringspliktige gjenstander, deponeringsavtale, artsidentifisering og -navngivning, feltinnsamling, aksesjon, kvittering, takkebrev, diplom, finnerlønn</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.06</td>
<td></td>
<td></td>
<td>Administrere flytting av gjenstander over landegrenser</td>
<td>Repatriering, eksporttillatelse, importtillatelse, rådgivning til Tollvesenet</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.07</td>
<td></td>
<td></td>
<td>Vedlikeholde og konservere objekter</td>
<td>Konserveringsrapport, oversikt over objekter, sikrings- og bevaringsdokumentasjon, analyser, klimalogger, forebyggingstiltak, avtale med bokbinder</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.08</td>
<td></td>
<td></td>
<td>Rapportere</td>
<td>Tilstandsrapport for lokaler/magasiner</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.09</td>
<td></td>
<td></td>
<td>Administrere analyser</td>
<td>Behandling og vedtak, vurdering av klausulering, analyse og uttak av prøver, rapport, analyseresultater</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.10</td>
<td></td>
<td></td>
<td>Administrere gave og donasjon</td>
<td>Donasjonsliste, aksesjon, donasjon av utstillings- og undervisningsrekvisita, søknad og tillatelse til donasjon av gjenstander som er ulovlige i Norge, donasjonsavtale</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.11</td>
<td></td>
<td></td>
<td>Forvalte inn- og utlån av gjenstander og samlinger</td>
<td>Innlånsavtale, retur av innlån, søknad om utlån, forsikring, utlånsstatistikk, avtale, skjema, tilstandsrapport, eksport- og importtillatelse</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.12</td>
<td></td>
<td></td>
<td>Forvalte universitetsmuseenes samarbeidstiltak</td>
<td>UniMus, samarbeidsavtale, arkitektur/ oppgradering, handlingsplan, innkalling, referat, kvalitetssystem, kvalitetssikring/ utvikling/ daglig drift av samlingsdatabaser, referat fra fag- og ekspertgruppe</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.13</td>
<td></td>
<td></td>
<td>Gi adgang til gjesteforskere</td>
<td>Adgangsskjema, korrespondanse</td>
<td>K</td>
<td>5 år</td>
</tr>
<tr>
<td>E.a.14</td>
<td></td>
<td></td>
<td>Administrere vedlikehold og konservering</td>
<td>Plan for bevaring av eldre samlinger</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.a.15</td>
<td></td>
<td></td>
<td>Utføre paleontologiske utgravninger</td>
<td>Søknad om tillatelse, tillatelse, budsjett, sponsoravtale, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.01</td>
<td></td>
<td>Kulturminneforvaltning</td>
<td>Koordinere og delta i nasjonale komiteer</td>
<td>Innkalling, referat, drøfting</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.02</td>
<td></td>
<td></td>
<td>Rapportere til departement og direktorat</td>
<td>Bestilling, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.03</td>
<td></td>
<td></td>
<td>Sikre og ivareta kulturminner</td>
<td>Søknad til Riksantikvaren, rapport til Riksantikvaren, innlevering av funn, innspill til sikringsprosjekter</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.04</td>
<td></td>
<td></td>
<td>Behandle areal- og utbyggingssaker og utføre arkeologiske forvaltningsundersøkelser</td>
<td>Uttalelse, planbeskrivelse, kartmateriale, vedtak, prosjektplan, budsjett, referat, søknad om dispensasjon, svar med tilråding, vedtak, klage, bestilling av tiltak, §10-vedtak, aksept av §10-vedtak, kontrakt, arkeologiske registreringer/ arkivering i TOPARK, analyse, svar på analyse,rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.05</td>
<td></td>
<td></td>
<td>Administrere arkeologiske forskningsutgravninger</td>
<td>Søknad, økonomisk støtte, planlegge feltarbeid, resultater/ dokumentasjon, analyse, konservering, rapport, arkeologiske registreringer/ arkivering i TOPARK</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.06</td>
<td></td>
<td></td>
<td>Administrere arkeologiske sikringsutgravninger</td>
<td>Søknad, økonomisk støtte, befaring, fotodokumentasjon, dokumentasjon til bevaringsprogram, analyse, svar på analyse, vedtak, rapport, arkeologiske registreringer/ arkivering i TOPARK, korrespondanse med Riksantikvaren</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.07</td>
<td></td>
<td></td>
<td>Administrere feltundersøkelse</td>
<td>Prosjektplan, dokumentasjon, HMS-dokumentasjon, referat, søknad om finansiering, kontrakt, budsjett, innmåling, proveniens, tillatelse, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.08</td>
<td></td>
<td></td>
<td>Behandle brudd på Lov om kulturminner</td>
<td>Anmeldelse, utarbeide kostnadsoverslag for erstatningssum</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.09</td>
<td></td>
<td></td>
<td>Gi kulturminnefaglige råd</td>
<td>Uttalelse, rådgivning, uforpliktende kostnadsoverslag, uforpliktende kalkyle på utgravning, uforpliktende kostnadskalkyle til tiltakshaver før utgravning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.b.10</td>
<td></td>
<td></td>
<td>Utføre arkeologiske forundersøkelser (kulturminner under vann)</td>
<td>Søknad om tiltak, arealplan, vurdering, befaring, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.c.01</td>
<td></td>
<td>Naturforvaltning</td>
<td>Behandle areal- og utbyggingssaker</td>
<td>Uttalelse, planbeskrivelse, kartmateriale, vedtak, prosjektplan, budsjett, referat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.c.02</td>
<td></td>
<td></td>
<td>Administrere feltundersøkelse</td>
<td>Søknad, kontrakt/avtale, budsjett, prosjektplan, dokumentasjon, rapport, HMS-dokumentasjon, referat</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.c.03</td>
<td></td>
<td></td>
<td>Gi naturområdefaglige råd</td>
<td>Rådgivning</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.c.04</td>
<td></td>
<td></td>
<td>Sikre og ivareta naturområder</td>
<td>Geologisk rådgivning ved byggeprosjekt, faglig rådgivning ved naturkriminalitet, vurdering av politibeslag, søknad om midler, dokumentasjon, statistikk, rapport</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.d.01</td>
<td></td>
<td>Samlingsforvaltning - spesialsamlinger og depotarkiv</td>
<td>Forvalte utlån og utleie av materiale (unika)</td>
<td>Forespørsel/henvendelse, forsikring, oversendelsesbrev, kontrakt/avtale, tillatelse, rådgivning/veiledning, søknad om tilgang til taushetsbelagt eller klausulert materiale, taushetserklæring, samtykkeerklæring,</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.d.02</td>
<td></td>
<td></td>
<td>Forvalte bibliotek- og arkivmateriale</td>
<td>Forespørsel/henvendelse vedr deponering/ donasjon/ avlevering, takkebrev, referansetjenester og veiledning, vurdering av historisk verdi, kvittering, kontrakt for donasjon/ avlevering / deponering, aksesjon, katalog, digitalisering, tilgjengeliggjøring, konservering</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.d.03</td>
<td></td>
<td></td>
<td>Vurdere begjæring om partsinnsyn og innsyn for forskningsformål</td>
<td>Innsynsbegjæring, dokumentasjon, tillatelse fra arkivskaper/-eier, vedtak, klage, kontrakt, taushetserklæring, samtykkeerklæring</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.d.04</td>
<td></td>
<td></td>
<td>Vedlikeholde og kvalitetssikre aktørregister</td>
<td>Føre i aktørregister</td>
<td>B</td>
<td></td>
</tr>
<tr>
<td>E.d.05</td>
<td></td>
<td></td>
<td>Samarbeide med fagmiljø innen bibliotek og arkiv</td>
<td>Invitasjon, deltakelse</td>
<td>B</td>
<td></td>
</tr>
</tbody>
</table>

---
home: true
title: NTNUs Arkivplan
author: DOKU
heroText: NTNUs Arkivplan
shortcuts:
  - header: Studie
    color: cyan
    links:
      - link: "/rutiner/saksomraader/student"
        text: Studentdokumentasjon
      - link: "/rutiner/saksomraader/student"
        text: "↪ Registrering"
      - link: "/rutiner/saksomraader/studie-og-eksamen"
        text: "Studie- og eksamensdokumentasjon"
      - link: "/rutiner/saksomraader/studentombud"
        text: "Studentombud"
    guides:
      - link: "https://innsida.ntnu.no/wiki/-/wiki/Norsk/opprette+saksmappe+i+arkivdel+student+i+ephorte"
        text: Opprette saksmappe
  - header: HR
    color: orange
    links:
      - link: "/rutiner/saksomraader/personal"
        text: Personaldokumentasjon
      - link: "/rutiner/saksomraader/personal"
        text: "↪ Registrering"
      - link: "/rutiner/saksomraader/ansettelsessaker"
        text: "Ansettelsessaker"
    guides:
      - link: "https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Opprette+personalmappe"
        text: Opprette personalmappe
  - header: Forskning
    color: blue
    links:
      - link: "/rutiner/saksomraader/phd"
        text: Ph.d.-dokumentasjon
      - link: "/rutiner/saksomraader/phd"
        text: "↪ Registrering"
      - link: "/rutiner/saksomraader/prosjekter"
        text: Prosjekter
  - header: Organisasjon
    color: pink
    links:
      - link: "/rutiner/saksomraader/styre-raad-og-utvalg/"
        text: Styre, råd- og utvalg
      - link: "/rutiner/saksomraader/rektorvedtak"
        text: Rektorvedtak
      - link: "/rutiner/saksomraader/hoeringer"
        text: Høringer
  - header: Rydding og dokumentfangst
    color: green
    links:
      - link: "/prosjekt/ntnu-sak/rydding-og-dokumentfangst/"
        text: Hovedside
      - link: "https://innsida.ntnu.no/wiki/-/wiki/Norsk/ntnu+sak+-+rydding+og+dokumentfangst"
        text: Om prosjektet
      - link: "/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/ansettelsessaker.pdf"
        text: "↪ Ansettelsessaker"
      - link: "/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/anskaffelsessaker.pdf"
        text: "↪ Anskaffelsessaker"
      - link: "/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/forskningsprosjekt.pdf"
        text: "↪ Forskningsprosjekt"
      - link: "/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/prosjekt.pdf"
        text: "↪ Prosjekt"
      - link: "/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/styre-raad-og-utvalg.pdf"
        text: "↪ Styre, Råd og Utvalg"
  - header: Vitenskapsmuseet
    color: yellow
    links:
      - link: "/rutiner/saksomraader/vitenskapsmuseet"
        text: Dokumentasjon
      - link: "/rutiner/saksomraader/vitenskapsmuseet"
        text: "↪ Registrering"
features:
  - link: "/organisering-og-ansvar/"
  - link: "/regelverk/"
  - link: "/rutiner/"
  - link: "/arkivoversikt/"
  - link: "/prosjekt/"
  - link: "/automatisering/"
---

::: info Trenger du mer hjelp?

Du finner [brukerveiledningene våre på Innsida](https://i.ntnu.no/hjelp-til-bruk-av-elements)

:::

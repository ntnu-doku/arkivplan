---
title: Kategorier
permalink: /kategorier
---

Her er en oversikt over kategorier brukt for å merke innholdet i arkivplanen, og hvilke sider som benytter de.

<MetaList type="category" />

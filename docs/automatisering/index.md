---
date: '2021-01-29T00:00:00'
title: Automatisering
status: Ferdig
category: Oversikt
author: Geir Ekle
---

NTNU er en stor virksomhet med mange prosesser og fagsystemer, og volumene er svært høye på mange fagområder. DOKU har fokus på å automatisere sak- og arkivprosesser.

I arbeidet med automatisering har DOKU tatt i bruk integrasjonsløsninger mellom fagsystem og sak- og arkivsystemet der prosessen er programmert og styrer seg selv. Robotisering (RPA) er teknologi som også er tatt i bruk for å gjøre en prosess automatisk. Robotene utfører regelbaserte og gjentakende oppgaver med høyt volum i brukergrensesnitt som dokumentforvaltere ville ha arbeidet i. I tillegg benyttes felleskomponenter som forvaltes av Digitaliseringsdirektoratet.

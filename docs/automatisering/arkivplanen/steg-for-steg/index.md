---
date: '2021-08-06T00:00:00'
title: Steg for steg
status: Flyttet
category: Referanse
author: Ole Vik
redirect: 'https://gitlab.com/ntnu-doku/arkivplan/-/blob/master/README.md'
---

Automasjonen som gjør mapper og filer om til publiseringsformat bruker Node.js og en rekke kommandoer som kan kjøres manuelt fra kommandolinjen i et terminalvindu. Når systemet bygger strukturen for publisering vil alt dette skje uten menneskelig innblanding, hvis systemet er satt opp i en løsning hvor kommandoene kan kjøres når endringer skjer. Det generelle formatet tar formen `node run /<kommando/>`; kjør bare `node run` for beskrivelser av hver kommando. Alle kommandoer kjøres fra den øverste mappen, hvor package.json befinner seg.

Node.js og Pandoc må være installert og tilgjengelig for å bruke systemet, se siden [Tekniske krav](/arkivplan/automatisering/arkivplanen/tekniske-krav). Status for hver operasjon rapporteres forløpende i terminalvinduet og logges i /logs/task.log.

## Forberedelser

Installer nødvendige bibliotek og rammeverk, gjennom kommandoen `npm install`. Programmet npm – Node Package Manager – følger med Node.js. Dette installerer alle skript oppgitt i package.json, med de definerte versjonene, til /node_modules.

## 1. Innhold

Generer publiseringsformat i samme mappestruktur som mappen /Innhold har, hvor Word-filer omgjøres til Markdown-format, og andre filer kopieres. `node run innhold` gjør følgende:

1.  Finner all filer i /Innhold

2.  Itererer gjennom disse og avgjør

    1.  Hvor publiseringsformat lagres: Et mappenavn en nettleser kjenner igjen

    2.  Hvilken filtype det er: Bare .docx-filer konverteres

    3.  Hva filnavnet er: Dette brukes som tittel hvis den ikke er angitt i dokumentet

3.  Hvis det er en .docx-fil lages en sti under /docs

4.  Hvis filen kan leses hentes metadata og innhold ut, tilpasses og lagres i Markdown-format som index.md under angitt sti

5.  Hvis det er en annen filtype kopieres den til angitt sti

Når prosessen er gjennomført vil alle filer direkte under /Innhold ha en mappe under /docs, av samme navn, men tilpasset nettlesere.

## 2. Indeks

Bygger en indeks av innholdet i /docs for bruk til søking. Lagres som /docs/.vuepress/public/index.json for enkel tilgang internt og eksternt, i JSON-format. Denne filen inneholder en kopi av alt tekstlig innhold og metadata.

## 3. Backup

Lag en sikkerhetskopi av /Innhold, gjennom `node run backup`. Disse lagres i /backup som .zip-filer, gjennom komprimeringsløsning 7-Zip. Filene navngis etter formatet yyyy-MM-dd_HH-mm-ss, for eksempel 2020-12-16_10-49-00.zip. Dette er en type overflødig sikkerhetskopi, da alle mapper og filer allerede har sikkerhetskopier og versjoner via SharePoint.

## 4. Oppvask

Sletter alle genererte filer i /docs, gjennom `node run oppvask`. Dette inkluderer alle mapper og filer foruten index.md, endringslogg.md, kategorier.md, emneknagger.md, package.json, package-lock.json, .vuepress, /node_modules,. Disse unntakene er systemfiler eller automatisk generert innhold, nødvendig for publiseringsløsningen.

## 5. PDF

Lager en PDF-kopi av arkivplanen i /docs/.vuepress/public/utskrift.pdf, samt en PDF-kopi av samlede PDF-vedlegg i /docs/.vuepress/public/vedlegg.pdf, gjennom `node run pdf`.

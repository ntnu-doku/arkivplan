---
date: '2023-11-08T00:00:00'
title: Produksjon av innhold
status: Ferdig
category: Prosess
tags:
  - Nett
author: Ole Vik
---

Dette dokument gir en forenklet forklaring av hvordan innhold produseres for publisering i arkivplanen.

## Stilguide

Innhold formateres som oppgitt under, og sikter å oppfylle følgende mål:

- Innholdet i arkivplanen skal være tydelig strukturert og lett å orientere seg i

- Søkefunksjonalitet for å finne det saksbehandler forbedrer gjenfinnbarhet og treffer både på innhold og metadata

- Tekstlig innhold i sidene skal være forklarende, lett å forstå, og med klart språk

- Arkivplanen skal gi en mest mulig systemuavhengig beskrivelse av prosesser og rutiner knyttet til saksbehandling og arkivering; både nåværende og fremtidige prosesser

- Arkivplanen har og viser til konkrete ressurser og brukerveiledninger for hvordan arbeid utføres i nåværende sak- og arkivsystem ePhorte

- Arkivplanen skal være et verktøy som uttømmende viser hva NTNU har av arkivmateriale

I tillegg søkes følgende retningslinjer etterfulgt:

- Overskrifter er korte og selvforklarende for brødteksten

- Innhold på den enkelte side eller dokument overskrider ikke fire A4-sider

- Kommentarer utrykkes direkte i avsnitt, i sluttnoter eller egne avsnitt. Parentes brukes i hovedsak til forklaring av forkortelser

- Referansemateriale lenkes til fremfor å gjengis

- Unngå vanlige eller faglige forkortelser

- Innhold formateres for å bli oversiktlig i et digitalt grensesnitt

- Lenk ikke til undersider, de lenkes til automatisk

- I lenker, fortell leseren hvor de havner fremfor å bruke uleselige, fulle adresser

Hvert dokument som omgjøres til en side for publisering må inneholde følgende metadata: Publiseringsdato, forfatter, status og kategori. Se beskrivelser av disse lenger ned.

## Maler

[Arkivplan, Mal for innhold.dotx](/arkivplan/vedlegg/automatisering/arkivplanen/produksjon-av-innhold/arkivplan-mal-for-innhold.dotx) er en standardisert mal for produksjon av innhold som egner seg for omgjøring til publiseringsformat. Innhold kan formateres og struktureres på vanlig måte i Word:

- Stiler, som overskrift 2-6, tittel, undertittel og sitat

  - Overskrift 1 brukes ikke, bare tittel

- Formatert skrift som **fet**, *kursiv*, ^(hevet) og _(senket)

- Referanser, både fotnoter[^1] og sluttnoter[^2]

  - Nettsider har generelt sett ikke et konsept for sider, benytt sluttnoter

- Lister, både nummererte og unummererte

- [Lenker](https://www.ntnu.no/adm/doku/) og media

  - Interne lenker, innad i dokumentet

  - Interne lenker, mellom dokumenter

  - Bilder bør være i så stort format/oppløsning som tilgjengelig, og lagres i mappen tilhørende Word-dokumentet hvis mulig

- Tabeller

Alt av det ovennevnte vil inkluderes når innholdet konverteres til publiseringsformatet. For lenker innad i dokumentet, til overskrifter/undertitler, bruk Cross-reference/Kryssreferanse i Word.

Justering av skriftstørrelse, tekst eller innrykk vil ikke komme med, ei heller mer avansert formatering eller innhold. Dette bør heller integreres på annen måte.

## Vedlegg og lenker til de

Vedlegg er selvstendige dokumenter som ikke skal eller kan utrykkes som normale sider i publiseringsløsningen. Disse lagres alltid i en Vedlegg-mappe, under mappen med samme navn som dokumentet. For «Rutiner.docx» blir det «/Innhold/Rutiner/Vedlegg/». Interne lenker vil automatisk tilpasses slik at de peker til riktig vedlegg eller dokument, bruk lenkefunksjonen i Word.

I konverteringen av Word-dokumenter til publiseringsformat så tas ikke vedlegg med. Hvis det skal tas med så må det følge samme mal som normale sider, og legges i en annen undermappe slik at det utrykkes som en normal underside, som alle andre sider i dokumentstrukturen i «/Innhold».

## Klippe og lime inn tekst

Når tekst kopieres fra et sted til et Word-dokument, la alltid Word bruke formateringen vi har satt opp i malen, slik at ingen uforventet formatering kommer med.

1.  Kopier det du skal lime inn

2.  Øverst til venstre i Word, klikk på **ikonet for utklippstavlen**, under Home/Hjem-fanen

3.  Klikk på det mindre **ikonet for utklippstavlen med en pil mot høyre**

Når du holder over dette hinter Word om «**Merge Formatting (M)**», altså at det du limer inn skal formateres med dokumentets stiler.

## Metadata

I tillegg er det viktig å oppdatere feltene i overskriften (Header) og bunnteksten (Footer) i Word. Disse oppdaterer metadata om dokumentet, og består av:

- DATO: Publiseringsdato for nåværende versjon av dokumentet

- FORFATTER: Vedkommende som sist redigerte dokumentet

- ORGANISASJON/«DOKU, NTNU»: Enheten og organisasjonen dokumentet er publisert på vegne av

- STATUS: Bruk kodeordene for å vise hvorvidt dokumentet er klart for publisering

  - Ferdig: Dokumentet kan publiseres slik det er

  - Under arbeid: Dokumentet jobbes aktivt med av forfatteren

  - Utkast: Dokumentet jobbes ikke aktivt med, men inneholder notater

- KATEGORI: Fyll inn én kategoribetegnelse, som grupperer på høyt nivå

  - Organisering: Hvordan NTNU er satt sammen

  - Formelt: Formell informasjon om NTNU

  - Rutiner: Liste over dokumenter som inneholder konkrete rutiner

  - Rutine: En konkret rutine

  - Beslutning: Avgjørelse truffet av styre, utvalg og møte

  - Prosess: En beskrivelse og forklaring av en prosess

  - Referanse: Innhold lenket i hovedsak videre til andre ressurser

  - Regelverk: Lover, overordnede beslutninger, interne bestemmelser og føringer

  - Faglig: Hvordan arkivfaget praktiseres ved NTNU, samt annet fagrelatert

  - Oversikt: Presentasjon av eller samleside for innhold

  - *Bevare: Hva som skal og ikke skal arkiveres*

- EMNEKNAGGER: Fyll inn en kommaseparert liste med kapitaliserte søkeord, som gjør gjenfinnbarhet enkelt

  - DOKU: Om avdelingen

  - Intern: Gjeldene for DOKU

  - Ansvar: Hvem som skal gjøre hva

  - Delegasjon: Hvem som gis ansvar

  - Styring: Føringer for hvordan ting gjøres

  - Generelt: Overfladisk om NTNU

  - Administrativt: Om administrative forhold eller fellesadministrasjonen

  - Dokument: Denne siden er en selvstendig forklaring av noe

  - Vedlegg: Denne siden tilhører ett eller flere dokumenter

  - Automasjon: Beskriver hvordan en prosess automatiseres

  - Nett: Er relatert til nettsider eller nettløsninger

  - Publisering: Offentliggjort informasjon

  - Bestemmelse: En eller flere spesifikke avgjørelser

  - Arkiv: Relatert til arkiver

  - *Bevare: Hva som arkiveres*

  - Beholde: Noe fysisk eller digitalt som ikke skal kastes

  - Kaste: Noe fysisk som ikke skal beholdes

  - Slette: Noe digital som ikke skal beholdes

  - BK-plan: Forkortelse for bevaring- og kassasjons-plan

  - Behandle: Hvordan noe behandles i en sak, flyt eller prosess

  - Krise: Noe uforventet og prekært oppstår

  - Standard: Typisk innhold som bør leses av saksbehandlere

  - Lagre: Importere i sak- og arkivsystemet

  - Laste opp: Importere i sak- og arkivsystemet

  - Arkivere: Importere i sak- og arkivsystemet

  - Laste ned: Eksportere lokal kopi av filen(e)

  - Digitalisering: Overgang fra manuell til effektivisert, digital prosess

  - Begrepsavklaring: Redegjør for faglig terminologi

  - Definisjon: Forklaring av noe, en forklarende tekst

  - Kategorisering: Plassere noe i en kategori

  - Struktur: Hvordan noe er bygget opp

- Mer uvanlige emneknagger:

  - JavaScript: Et programmeringsspråk mye brukt på nett

  - Node: En variant av JavaScript for servere

For eksempel vil ikke dokumentet «/Innhold/Rutiner.docx» trenge kategorien «Rutine», men hver faktiske rutine bør merkes:

- /Innhold/Rutiner/Studiedokumentasjon.docx vil da ha kategorien «Rutiner», og emneknaggen «Registrering»

- /Innhold/Rutiner/Studiedokumentasjon/Eksamen.docx har kategorien «Rutine», og emneknaggene «Rutine, Eksamen, Skjerming, Student»

Fyll i minste fall ut feltene **Category** / **Kategori** og **Keywords** / **Emneknagger**:

- **Title** / **Tittel**-feltet brukes bare hvis tittel på dokumentet skal være annerledes enn overskriften på dokumentet, satt med tittelstilen øverst

- **Subject** / **Tema**-feltet under brukes ikke

- **Author** / **Forfatter**-feltet brukes alltid, for å indikere hvem som sist gjorde endringer

- **Manager** / **Eier**-feltet brukes ikke

- **Category** / **Kategori**-feltet og **Keywords** / **Emneknagger**-feltet fylles inn som angitt over

- **Comments** / **Kommentarer**-feltet brukes ikke

## Informasjonsbokser

Fire typer bokser brukes for å poengtere innhold: Info, bra, advarsel og fare.

::: info Informasjon

Blå boks: Poengtere noe generelt.

:::

::: bra Bra

Poengtere noe bra eller beste praksis.

:::

::: advarsel Advarsel

Gul boks: Advare mot not dårlig eller dårlig praksis.

:::

::: fare Fare

Rød boks: Poengtere feil.

:::

De fungerer som et blikkfang for leseren, for å bryte opp teksten og fremheve særlig viktige momenter. Måten å lage disse i Word er å starte første setningen med tre kolon, etterfulgt av en av typene, og til siste tittel – *::: type Tittel*. Kodeordene for typene skrives med små bokstaver. Etter innholdet avsluttes boksen med tre kolon til på egen linje. Innhold kan inkludere det meste av normal formatering.

[^1]: Fotnote.

[^2]: Sluttnote.

---
date: '2025-01-09T00:00:00'
title: Arkivplanen
status: Ferdig
category: Prosess
tags:
  - Publisering
  - Nett
author: Ole Vik
---

Her beskrives hvordan NTNUs Arkivplan – disse nettsidene – blir laget og publisert. Enkelt sagt omgjøres Word-filer til publiseringsformat, med minimalt av menneskelig innblanding. Dette involverer to prosesser: Konvertering av mapper og filer, og en publiseringsløsning. Valgt publiseringsløsning er per nå [VuePress](https://v1.vuepress.vuejs.org/guide/), men oppsettet kan lett tilpasses særegenheter ved de fleste publiseringsløsninger på nett som benytter moderne standarder.

## Forenklet kravspesifikasjon for publiseringsløsning

I 2020 ble det avgjort at ny løsning for NTNUs arkivplan må være:

- Åpen: Innhold må være tilgjengelig for alle, uten restriksjoner, i et format som ikke låses til løsningen – samt skilles klart fra presentasjon og verktøy

- Fleksibel: Brukbar på tvers av enheter, med minst samme nivå på funksjonalitet som i dag, samt reproduserbart i nye formater

  - Strukturert navigasjon

  - Søk basert på tittel, metadata og innhold

  - Lenker til eksterne ressurser, opplasting av vedlegg – helst inkluderes de i innholdet

- Styrt: Redigering og publisering må ha tilgangsstyring, full historie og versjonering

  - Én kilde til sannhet, fullstendig kontrollert av DOKU

- Anvendelig: Redigering av innhold må ha lav terskel, og kreve minimalt av redaktører

Se [GitLab for en veiledning](https://gitlab.com/ntnu-doku/arkivplan/-/blob/master/README.md) for hvordan systemet fungerer og brukes, også for [tekniske krav og utvikling](https://gitlab.com/ntnu-doku/arkivplan/-/blob/master/CONTRIBUTING.md). Se [Produksjon av innhold](/arkivplan/automatisering/arkivplanen/produksjon-av-innhold) for en gjennomgang av hvordan innhold produseres i Word.

## Ønskede karakteristikker ved publiseringsløsning

I valget av publiseringsløsning ble følgende vektlagt:

- Egnet for teknisk dokumentasjon, anvender populært og bredt støttet rammeverk

- Mappestruktur med filer utrykkes direkte som adresser i nettleseren

- Navigasjonsmeny genereres automatisk, følger mapper, og brødsti er mulig

- Innhold formateres med en godt definert, åpen [standard](https://spec.commonmark.org/current/) som [støtter alle typer tekstformatering](https://www.markdownguide.org/basic-syntax/) på nett. Word-filer kan konverteres automatisk, begge veier

- Åpen kildekode med åpen lisens, som kan publiseres automatisk

- Søkemuligheter internt og eksternt, som kan integreres mot andre tjenester

- Utseende kan tilpasses NTNUs grafiske profil

- Versjonering på alle nivåer: Av arkivplanen totalt, av dokumenter, samt full versjonshistorikk gjennom Git[^1]

- Kan erstattes med tilsvarende systemer med minimalt arbeid

Automatisert publisering av teknisk dokumentasjon er blitt populær i senere år, og blant annet finner man [NOARK5](https://github.com/arkivverket/noark5-standard), Brønnøysundregistrene, Riksrevisjonen, Uninett og andre [på GitHub](https://government.github.com/community/).

Poenget med å publisere arkivplanen på denne måten er at åpenhet ivaretas, i alt fra innhold til system, og løsningen blir mer fleksibel for tilpasninger og integrasjoner enn med klassiske løsninger. Det er også en løsning som krever lite ressurser, hvorav alle tekniske allerede er tilgjengelig. Forholdet mellom innholdet i mapper og filer er direkte gjenkjennelig i resultatet, og redigering og publisering krever lite av redaktører. Innholdet kan også enkelt arkiveres og endringer spores, noe som ellers er krevende.

[^1]: Se «[Why Git for your organization](https://www.atlassian.com/git/tutorials/why-git)» for en gjennomgang.

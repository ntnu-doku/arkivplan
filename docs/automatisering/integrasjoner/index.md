---
date: '2021-01-14T00:00:00'
title: Integrasjoner
status: Ferdig
category: Prosess
tags:
  - Automasjon
  - Digitalisering
author: Geir Ekle
---

## Fagsystem

Mange fagsystemer blir benyttet i saksbehandling. Begrepet fagsystem er brukt om alt som ikke er sak- og arkivsystem. Et fagsystem kan forstås som et støttesystem som er spesialisert for saksbehandlingen i et organ, ofte tilrettelagt for håndtering av et stort antall saker som krever likeartet behandling og har stor grad av repetisjon.

Mange fagsystemer har registreringsfunksjoner for mottatte dokumenter i tillegg til at det kan produseres brev og interne dokumenter. Men fagsystemenes journal- og arkivfunksjonalitet tilfredsstiller sjeldent kravene i NOARK-standarden, og de er ikke godkjent av Riksarkivaren. For å følge arkivloven må dokumentene i disse fagsystemene føres i både fagsystemet og NOARK-systemet. I praksis blir mange dokumenter ikke journalført og arkivert i tråd med lover og forskrifter.

## Fagsystem og integrasjon sak- og arkivsystem

Ved NTNU er det opprettet følgende integrasjoner fra fagsystem til sak- og arkivsystemet:

- Jobbnorge: Overføring av utlysningstekst, offentlig og utvidet søkerliste til ansettelsessaken

- Felles studentsystem (FS): Journalføring og arkivering av mottatt klage på sensur og svar på klage på sensur via StudentWeb, FS og Inspera

- MachForm: Søknad om fjerde- eller femtegangs eksamen

- MachForm: Krav om innsyn i egne opplysninger som NTNU har registrert

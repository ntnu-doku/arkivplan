---
date: '2021-01-14T00:00:00'
title: Fellesløsninger
status: Ferdig
category: Prosess
tags:
  - Automasjon
author: Geir Ekle
---

Her beskrives hvilke tjenester NTNU er koblet mot gjennom moduler i sak- og arkivsystemet.

## Digital postkasse

For å gjøre det enkelt å kommunisere digitalt, er det etablert en sikker digital postkasse for innbyggerne. Innbygger velger selv postkasse fra markedsaktørene Posten og e-Boks for å motta digital post fra NTNU.

## Elektronisk Digital Forsendelse

Elektronisk Digital Forsendelse er egnet for å sende taushetsbelagte opplysninger og annen beskyttelsesverdig informasjon. Digital post som sendes gjennom denne løsningen vil være kryptert frem til postkassen, og innbygger logger seg inn via ID-porten for tilgang. Utskrift og forsendelse som brevpost er en del av tjenesten, slik at NTNU kan ekspedere både digital post og papirpost til innbyggerne fra sak- og arkivsystemet.

## eFormidling

eFormidling – elektronisk formidling – gjør det mulig å sende post elektronisk til virksomheter. Virksomheter vil motta posten i sin valgte kanal for elektronisk mottak. Det betyr at statlige virksomheter som bruker eFormidling vil få post direkte i sitt sak- og arkivsystem. Det samme gjelder kommuner som benytter tjenesten SvarInn hos kommunesektorens organisasjon og utviklingspartner (KS). Øvrige virksomheter mottar sin post i meldingsboksen i Altinn.

## eSignering

eSignering muliggjør elektronisk signering av dokumenter, og unngår da manuell håndtering ved utlevering og innhenting av dokumenter som skal signeres. Ved elektronisk signering av dokumenter gjennomføres selve signeringen med elektronisk ID i Postens signeringsportal. Det signerte dokumentet kan ikke endres i etterkant, og kommer tilbake direkte i sak- og arkivsystemet etter at mottaker har fullført prosessen. Tjenesten støtter både enkelt- og multisignatur for signeringsoppdrag.

## Enhetsregisteret

I Enhetsregisteret finnes samtlige norske foretak, statlige som private, og dette er tilgjengelig i sak- og arkivsystemet. Alle er identifiser med et nisifret organisasjonsnummer. Enhetsregisteret benyttes blant annet for å finne enheter og organisasjonsnummer for elektronisk ekspedering til virksomheter.

## Kontakt- og reservasjonsregisteret

Kontakt og reservasjonsregisteret (KRR) inneholder blant annet mobilnummer og e-post til innbyggerne i Norge og blir benyttet for å varsle mottaker om digital post fra NTNU i digital postkasse. Registeret viser også hvem som har reservert seg mot digital kommunikasjon slik at forsendelser til mottaker vil skje via brevpost.

## Folkeregisteret

Oppslag i Folkeregisteret gjøres for å identifisere riktig person med fødselsnummer for å ekspedere forsendelser digitalt til mottaker, via digital postkasse, eller som brevpost, via postens utskriftstjeneste.

## ID-porten

ID-porten benyttes av brukere for å logge på tjenester og autentisere seg. ID-porten benyttes blant annet ved innsynsbegjæring i egne opplysninger som NTNU har registrert. Skjema om innsynsbegjæring der avsender har identifisert seg via ID-porten blir mottatt og registrert automatisk i sak- og arkivsystemet.

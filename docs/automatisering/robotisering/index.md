---
date: '2021-01-14T00:00:00'
title: Robotisering
status: Ferdig
category: Prosess
tags:
  - Automasjon
author: Geir Ekle
---

## Roboter

Robotteknologi (RPA, fra «Robotic Process Automation») er tatt i bruk av DOKU og er en del av NTNUs Digitaliseringsprogram. Mange av prosessene som nå utføres helt eller delvis av en robot har ført til økt dokumentfangst og sikret standardisering. Roboten er enkelt forklart en programvare som i prinsippet kan utføre alle oppgaver og handlinger som et menneske kan utføre på en datamaskin. Oppgavene er i hovedsak regelstyrte og foregår digitalt.

NTNU benytter RPA-programvaren [Blue Prism](https://www.blueprism.com/) for å utvikle automatiske prosesser ved hjelp av robot. Følgende fem roboter arbeider med forskjellige prosesser i DOKU: Dokubert, Dokuline, Roberta, Omega og Optimus Prime.

Robotene utfører følgende oppgaver:

- Journalføring og arkivering av mottatte skjema fra MachForm som gjelder begrunnelse og klage på sensur

- Journalføring og arkivering av vitnemål

- Journalføring og arkivering av vitnemål fra avsluttet database hos tidligere SVT-fakultetet

- Journalføring av ferdigstilte og ekspederte dokumenter

- Publisering av offentlig journal på NTNUs innsynsløsning eInnsyn

- Journalføring og arkivering i samlesaker

DOKU arbeider med og har under utvikling følgende prosesser som roboter skal utføre:

- Konvertering av arkiverte Excel-regneark til lesbart PDF/A arkivformat

- Journalføring og arkivering av Godkjenning av ekstern utdanning

- Journalføring og arkivering av studieplaner i samlesaker

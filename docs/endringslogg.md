---
title: Endringslogg
permalink: /endringslogg
---

Alle nevneverdige endringer i arkivplanen vil oppføres her.

Formatet er basert på [Lag en Endringslogg](https://olevik.github.io/keep-a-changelog/), og følger [Datobasert versjonering](https://calver.org/) for innhold med formen `YYYY-0M-0D`. Innholdsmessige oppdateringer er derfor gjeldene fra datoen de er publisert. Dette gjelder kun for meningsbærende endringer i det enkelte dokument.

## 2025-01-10

### Lagt til

- Fylte ut Prosjekt / NTNU Sak

### Endret

- Erstattet gamle lenker som feilet

## 2025-01-06

### Lagt til

- Endringer i navn og organisering i Organisering og ansvar / Historikk

## 2024-11-11

### Endret

- Frister for begrunnelse for og klage på sensur i Rutiner / Saksområder / Studie og eksamen

## 2024-09-20

### Lagt til

- Oppdatering av nye registreringsrutiner for studieområdet: Studieforløp

## 2024-08-06

### Lagt til

- Oppdatering av nye registreringsrutiner for studieområdet: Tilrettelegging

## 2023-11-08

### Lagt til

- Oppdatering av nye registreringsrutiner for studieområdet: Eksamen, fusk, master, opptak, politiattest, praksis, studieforløp, tilrettelegging
- Oppdaterte forklaring av vedlegg i Automatisering / Arkivplanen / Produksjon av innhold

## 2023-09-16

### Lagt til

- Side for Bevarings- og kassasjonsplan for UH-sektoren under / Regelverk / Bevaring og kassasjon
- Kontekstuell forklaring av ny plan i / Regelverk / Bevaring og kassasjon, inkludert original PDF og lenke til UiBs navigerbare utgave

## 2023-03-22

### Lagt til

- Avsnitt om digitale vitnemål i / Rutiner / Saksområder / Studie- og eksamensdokumentasjon

## 2022-12-05

### Endret

- Organisasjonskart i / Organisering og ansvar / Organisering, samt lenke til NTNU.no

## 2022-03-15

### Lagt til

- Registreringsrutiner for Vitenskapsmuseet (VM)

## 2022-03-07

### Endret

- Studieforløp oppdatert til v2.2.0, med tillegg for CMEDFORSK

## 2022-01-27

### Lagt til

- Arkivnøkkel og kassasjonsplan for de statlige høgskolene 01.07.1994, revidert oktober 1996 og mai 2001, med kassasjonsplan mai 2001 - Modulink og ePhorte (vedlegg) på Arkivoversikt/ Arkivnøkler
- Arkivnøkkel for universiteter og høgskoler 2014 - Public360 (vedlegg) på Arkivoversikt/ Arkivnøkler

## 2021-11-19

### Lagt til

- Rutine for varsling lagt til Rutiner / Registrering

### Endret

- I Organisering og ansvar / Ansvar og fullmakter er setningen "_Registrere direkte mottatte saksdokumenter selv for journalføring og arkivering eller oversende til DOKU_" endret til "_Registrere direkte mottatte saksdokumenter selv for journalføring og arkivering_"

## 2021-11-11

### Lagt til

- Forslag til rutiner for NTNU Sak Rydding og dokumentfangst

## 2021-09-28

### Endret

- Ny rutine for Phd-dokumentasjon

## 2021-09-20

### Endret

- Slått sammen Rutiner / Saksområder og Rutiner / Registrering til enkeltsider for saksområder
- Lenket til relaterte brukerveiledninger
- Om Regionalt samarbeidsorgan Midt-Norge i Organisering og ansvar / Historikk

### Fikset

- Lenkekaos i Organisering og ansvar / Organisering

## 2021-08-12

### Endret

- Versjon 1.4 av Standardtitler - Administrasjon arbeidsforhold
- Versjon 1.4 av Standardtitler - Arbeidsavtaler - Forlengelser - Opphør
- Versjon 1.4 av Standardtitler - Opprykk etter kompetanse
- Versjon 1.4 av Standardtitler - Sykefravær - HMS - IA

## 2021-08-10

### Lagt til

- Sjekkliste for saksbehandlere

### Endret

- Flyttet Avvik øverst i Rutiner / Beredskap og sikkerhet
- Flyttet teknisk dokumentasjon om Arkivplanen til [GitLab](https://gitlab.com/ntnu-doku/arkivplan/)

### Fikset

- Interne og eksterne lenker

## 2021-04-13

### Lagt til

- Seksjon om avvik i Elektroniske arkivserier og –perioder

## 2021-03-01

### Lagt til

- Første publisering av ny utgave av NTNUs arkivplan

const { path } = require("@vuepress/shared-utils");

module.exports = {
  name: "childTOC",
  enhanceAppFiles: path.resolve(__dirname, "enhanceAppFile.js"),
};

import { merge, pickBy, identity } from "lodash-es";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Europe/Oslo");

/**
 * Create Schema.org-definition
 * @param {String} type Valid Schema.org-type, @see https://schema.org/docs/full.html
 * @param {Object} data Object of data to merge, will only validate dates, and ignore false properties
 * @returns Schema.org-object of given type
 */
export const Schema = (type = "", data = {}) => {
  if (data.hasOwnProperty("date")) data.date = dayjs(data.date).toISOString();
  if (data.hasOwnProperty("datePublished"))
    data.datePublished = dayjs(data.datePublished).toISOString();
  if (data.hasOwnProperty("dateModified"))
    data.dateModified = dayjs(data.dateModified).toISOString();
  return merge(
    {
      "@context": "https://schema.org/",
      "@type": type,
    },
    pickBy(data, identity)
  );
};

/**
 * Create website-definition for JSON-LD
 * @param {Object} data Object with website-details
 * @param {String} data.url URL
 * @param {String} data.name Name
 * @param {String} data.description Description
 * @param {String} data.inLanguage Language in bcp47-format
 * @param {Object} data.publisher Schema.org-object of type Author or Organization
 * @param {Object} data.potentialAction Schema.org-object of type PotentialAction
 * @returns Schema.org-object of type WebSite
 */
export const WebSite = (data = {}) => {
  return Schema("WebSite", data);
};

/**
 * Create webpage-definition for JSON-LD
 * @param {Object} data Object with webpage-details
 * @param {String} data.url URL
 * @param {String} data.name Name
 * @param {String} data.description Description
 * @param {String} data.inLanguage Language in bcp47-format
 * @param {String} data.datePublished ISO-date
 * @param {String} data.dateModified ISO-date
 * @param {Object} data.author Schema.org-object of type Author or Organization
 * @param {Object} data.primaryImageOfPage Schema.org-object of type PrimaryImageOfPage
 * @returns Schema.org-object of type WebPage
 */
export const WebPage = (data) => {
  return Schema("WebPage", data);
};

/**
 * Create image-definition for JSON-LD
 * @param {Object} data Object with image-details
 * @param {String} data.url URL
 * @param {String} data.caption Caption
 * @param {Number} data.width Width
 * @param {Number} data.height Height
 * @param {String} data.inLanguage Language in bcp47-format
 * @returns Schema.org-object of type Image
 */
export const Image = (data) => {
  return Schema("ImageObject", data);
};

/**
 * Create search-indicator for JSON-LD
 * @param {String} param Search-parameter name
 * @returns Schema.org-object of type SearchAction
 */
export const Search = (param) => {
  return Schema("SearchAction", {
    target: `?${param}={search_term_string}`,
    query: "required",
  });
};

/**
 * Create person-definition for JSON-LD
 * @param {Object} data Object with person-details
 * @param {String} data.name Name
 * @param {String} data.description Description
 * @param {String} data.url URL
 * @param {Object} data.image Schema.org-object of type ImageObject
 * @returns Schema.org-object of type Person
 */
export const Person = (data) => {
  return Schema("Person", data);
};

/**
 * Create organization-definition for JSON-LD
 * @param {Object} data Object with organization-details
 * @param {String} data.name Name
 * @param {String} data.description Description
 * @param {String} data.url URL
 * @param {Object} data.image Schema.org-object of type ImageObject
 * @returns Schema.org-object of type Organization
 */
export const Organization = (data) => {
  return Schema("Organization", data);
};

/**
 * Create breadcrumb-hierarchy for JSON-LD
 * @param {String} base FQDN with protocol used as canonical URL base
 * @param {Array.<{path: String, title: String}>} paths Array of objects with Path and Name
 * @returns Schema.org-object of type BreadCrumbList
 */
export const BreadCrumbList = (base, [...paths]) => {
  let elements = [];
  for (let i = 0; i < paths.length; i++) {
    elements.push({
      "@type": "ListItem",
      position: i + 1,
      item: {
        "@id": base + paths[i].path,
        name: paths[i].title,
      },
    });
  }
  return Schema("BreadcrumbList", { itemListElement: elements });
};

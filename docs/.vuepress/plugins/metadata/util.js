const striptags = require("striptags");
const stripmd = require("remove-markdown");
const matter = require("gray-matter");

/**
 * Removes duplicate objects from an Array of JavaScript objects
 * @param {Array} arr Array of Objects
 * @param {Array} keyProps Array of keys to determine uniqueness
 */
module.exports.getUniqueArray = (arr, keyProps) => {
  return Object.values(
    arr.reduce((uniqueMap, entry) => {
      const key = keyProps.map((k) => entry[k]).join("|");
      if (!(key in uniqueMap)) uniqueMap[key] = entry;
      return uniqueMap;
    }, {})
  );
};

/**
 * Test whether Page is an article
 * @param {String} path Page path
 * @returns {Boolean} True if Page is an article
 */
module.exports.isArticle = (themeConfig, path) => {
  if (
    [
      "/",
      "/lenker",
      "/endringslogg",
      themeConfig.categoriesPage,
      themeConfig.tagsPage,
    ].includes(path)
  ) {
    return false;
  }
  return true;
};

/**
 * Returns the meme type of an image, based on the extension
 * @param {String} img Image path
 */
module.exports.imageMimeType = (img) => {
  if (!img) {
    return null;
  }
  const regex = /\.([0-9a-z]+)(?:[\?#]|$)/i;
  if (
    Array.isArray(img.match(regex)) &&
    ["png", "jpg", "jpeg", "gif"].some((ext) => img.match(regex)[1] === ext)
  ) {
    return "image/" + img.match(regex)[1];
  } else {
    return null;
  }
};

/**
 * Convert slugified string into title
 * @param {String} slug
 * @returns Human-readable title
 * @see https://stackoverflow.com/a/8980868
 */
module.exports.titleize = (slug) => {
  let words = slug.split("-");
  return words
    .map(function (word) {
      return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
    })
    .join(" ");
};

module.exports.breadcrumbs = (page, pages) => {
  const parts = page.path.split("/");
  if (!parts[parts.length - 1].length) {
    parts.pop();
  }
  let link = "";
  let crumbs = parts.map((slug) => {
    link += slug;
    const pageObj = pages.find(
      (el) => el.path === link || el.path === link + "/"
    );
    link += "/";
    if (pageObj) {
      return {
        path: pageObj.path,
        title: pageObj.title || pageObj.frontmatter.breadcrumb,
      };
    }
  });
  crumbs = crumbs.filter(Boolean);
  // crumbs.push({ path: page.path, title: page.title });
  return crumbs;
};

/**
 * Log to console.error
 * @param {String} source Source of error
 * @param {String} message Error-message
 */
module.exports.logError = (source, message) => {
  console.error("ERROR", source, message);
};

/**
 * Create JSON-LD-string
 * @param {Array} nodes Schema.org-definitions as an array of objects
 * @returns JSON-LD-string encapsulated in application/ld+json HTML script-tag
 */
module.exports.renderJSONLD = (nodes = []) => {
  if (!Array.isArray(nodes) || nodes.length < 1) {
    util.logError(
      "render()",
      "nodes-parameter must be passed an array of valid nodes"
    );
    throw new Error("render(nodes: []) must be passed an array of valid nodes");
  }
  try {
    return `<script type="application/ld+json">${JSON.stringify(
      nodes
    )}</script>`;
  } catch (error) {
    util.logError("render()", "could not stringify JSON");
    throw new Error(error);
  }
};

// const striptags = require("striptags");
// const stripmd = require("remove-markdown");
// const matter = require("gray-matter");

/**
 * Create metadata-description from Page content
 * @param {Object} config Object with options
 * @param {String} config.content Unprocessed Markdown-content
 * @param {Number} config.limit Optional, limit by char or by word, default `200` by `char`, `45` by `word`
 * @param {String} config.endWith Optional, affix
 * @param {String} config.by Optional, mode
 * @returns Page description-string
 * @see https://github.com/sulfureux/description/blob/v0.0.4/index.js
 */
module.exports.metaDescription = ({
  content,
  limit = null,
  endWith = "...",
  by = "char",
}) => {
  content = matter(content).content;
  let origin = striptags(stripmd(content));
  let result = "";
  let count = origin.length;
  if (by === "word") {
    if (!limit) limit = 45;
    let contentArr = origin.split(" ");
    count = contentArr.length;
    if (limit - count >= 0) return origin;
    result = contentArr.slice(0, limit).join(" ");
  } else {
    if (!limit) limit = 200;
    if (limit - count >= 0) return origin;
    let originLimit = limit;
    let i = limit;
    while (i--) {
      if (origin.charAt(i) === " ") {
        limit = i;
        break;
      }
    }
    if (limit < originLimit * 0.7) {
      limit = originLimit;
    }
    result = origin.substring(0, limit);
  }
  return result + " " + endWith;
};

const util = require("./util");
const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const timezone = require("dayjs/plugin/timezone");
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Europe/Oslo");

module.exports = (options = {}, context) => ({
  extendPageData($page) {
    const { frontmatter, path } = $page;
    const { pages } = context.getSiteData ? context.getSiteData() : context;
    if (!$page.title) {
      $page.title = util.titleize($page.slug);
    }
    let description = frontmatter.description || context.siteConfig.description;
    let pageContent = null;
    if (
      $page.hasOwnProperty("_content") &&
      typeof $page._content === "string" &&
      $page._content.length > 10
    ) {
      pageContent = $page._content.replaceAll(/:::[\s\S]*:::/gm, "");
    }
    if (pageContent) {
      description = util.metaDescription({
        content: pageContent,
        endWith: "",
        limit: 155,
        by: "char",
      });
    }
    description = description.trim();
    const metadata = {
      title: `${frontmatter.title} | ${context.siteConfig.title}`,
      description: frontmatter.description
        ? frontmatter.description
        : description,
      url:
        frontmatter.canonicalUrl && typeof frontmatter.canonicalUrl === "string"
          ? frontmatter.canonicalUrl.startsWith("http")
            ? frontmatter.canonicalUrl
            : context.siteConfig.themeConfig.domain + $page.path
          : context.siteConfig.themeConfig.domain + $page.path,
      image:
        frontmatter.image && typeof frontmatter.image === "string"
          ? frontmatter.image.startsWith("http")
            ? frontmatter.image
            : context.siteConfig.themeConfig.domain + frontmatter.image
          : null,
      type: util.isArticle(context.siteConfig.themeConfig, path)
        ? "article"
        : "website",
      siteName: context.siteConfig.title || null,
      siteLogo:
        context.siteConfig.themeConfig.domain +
        context.siteConfig.themeConfig.defaultImage,
      published: frontmatter.date
        ? dayjs(frontmatter.date).toISOString()
        : $page.lastUpdated
        ? dayjs($page.lastUpdated).toISOString()
        : null,
      modified: $page.lastUpdated
        ? dayjs($page.lastUpdated).toISOString()
        : null,
      author:
        context.siteConfig.themeConfig.author.name ||
        context.siteConfig.themeConfig.author ||
        null,
    };
    if (!metadata.image) {
      metadata.image = `${context.siteConfig.themeConfig.domain}/assets/img/social${$page.path}social.jpg`;
    }

    let articleTags = [];
    if (util.isArticle(context.siteConfig.themeConfig, path)) {
      articleTags.push(
        {
          property: "article:published_time",
          content: metadata.published,
        },
        {
          property: "article:modified_time",
          content: metadata.modified,
        },
        {
          property: "article:section",
          content: frontmatter.category
            ? frontmatter.category.replace(/(?:^|\s)\S/g, (a) =>
                a.toUpperCase()
              )
            : null,
        },
        {
          property: "article:author",
          content:
            util.isArticle(context.siteConfig.themeConfig, path) &&
            metadata.author
              ? metadata.author
              : null,
        }
      );
      if (frontmatter.tags && frontmatter.tags.length) {
        frontmatter.tags.forEach((tag, i) =>
          articleTags.push({
            property: "article:tag",
            content: tag,
          })
        );
      }
    }

    let headMeta = [
      { name: "description", content: metadata.description },
      {
        name: "keywords",
        content:
          frontmatter.tags && frontmatter.tags.length
            ? frontmatter.tags.join(", ")
            : null,
      },
      { itemprop: "name", content: metadata.title },
      { itemprop: "description", content: metadata.description },
      {
        itemprop: "image",
        content: metadata.image ? metadata.image : null,
      },
      { property: "og:url", content: metadata.url },
      { property: "og:type", content: metadata.type },
      { property: "og:title", content: metadata.title },
      {
        property: "og:image",
        content: metadata.image ? metadata.image : null,
      },
      {
        property: "og:image:type",
        content:
          metadata.image && util.imageMimeType(metadata.image)
            ? util.imageMimeType(metadata.image)
            : null,
      },
      {
        property: "og:image:alt",
        content: metadata.image ? metadata.title : null,
      },
      { property: "og:description", content: metadata.description },
      { property: "og:updated_time", content: metadata.modified },
      {
        property: "og:locale",
        content: context.siteConfig.themeConfig.language_icu,
      },
      ...articleTags,
      {
        property: "twitter:domain",
        content: context.siteConfig.themeConfig.domain,
      },
      { property: "twitter:url", content: metadata.url },
      { property: "twitter:title", content: metadata.title },
      { property: "twitter:description", content: metadata.description },
      {
        property: "twitter:image",
        content: metadata.image ? metadata.image : null,
      },
      { property: "twitter:image:alt", content: metadata.title },
    ];
    headMeta = headMeta.filter((meta) => meta.content && meta.content !== "");
    headMeta = [...(frontmatter.meta || []), ...headMeta];
    headMeta = util.getUniqueArray(headMeta, [
      "name",
      "content",
      "itemprop",
      "property",
    ]);
    frontmatter.meta = headMeta;
  },
});

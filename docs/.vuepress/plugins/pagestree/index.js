const {
  createTree,
  menuSortandTrim,
  menuReinsertTrailingSlash,
  cleanDeep,
} = require("@lib/utilities");

module.exports = (options, context = {}) => ({
  name: "pagesTree",
  async ready() {
    const { pages, themeConfig } = context.getSiteData
      ? context.getSiteData()
      : context;
    const items = [];
    for (let i = 0; i < pages.length; i++) {
      items.push({ title: pages[i].title, path: pages[i].path });
    }
    themeConfig.pagesTree = items;
    let paths = [];
    for (let i = 0; i < pages.length; i++) {
      if (pages[i].path !== "/") {
        paths.push({ title: pages[i].title, path: pages[i].path });
      }
    }
    paths = menuSortandTrim(paths);
    let tree = cleanDeep(createTree(paths));
    menuReinsertTrailingSlash(tree);
    themeConfig.pagesTree = tree;
  },
});

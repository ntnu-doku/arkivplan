const {
  createTree,
  menuSortandTrim,
  menuReinsertTrailingSlash,
  cleanDeep,
} = require("@lib/utilities");

module.exports = (options, context = {}) => ({
  name: "autoMenu",
  async ready() {
    const { pages, themeConfig } = context.getSiteData
      ? context.getSiteData()
      : context;
    const items = [];
    for (let i = 0; i < pages.length; i++) {
      items.push({ text: pages[i].title, link: pages[i].path });
    }
    let paths = [];
    for (let i = 0; i < pages.length; i++) {
      if (
        pages[i].frontmatter.hasOwnProperty("menu") &&
        pages[i].frontmatter.menu == false
      ) {
        continue;
      }
      if (
        pages[i].path !== "/" &&
        pages[i].hasOwnProperty("frontmatter") &&
        pages[i].frontmatter.hasOwnProperty("status")
      ) {
        paths.push({ text: pages[i].title, link: pages[i].path });
      }
    }
    paths = menuSortandTrim(paths);
    let tree = cleanDeep(createTree(paths));
    menuReinsertTrailingSlash(tree);
    const menu = [
      items.find((obj) => {
        return obj.link === "/";
      }),
    ];
    for (let i = 0; i < tree.length; i++) {
      if (tree[i].hasOwnProperty("children")) {
        delete tree[i].children;
      }
      menu.push(tree[i]);
    }
    themeConfig.nav = [
      {
        text: "Innhold",
        ariaLabel: "Snarveier",
        items: menu,
        items: [
          { items: menu },
          {
            items: [
              { text: "Kategorier", link: "/kategorier" },
              { text: "Emneknagger", link: "/emneknagger" },
              { text: "Endringslogg", link: "/endringslogg" },
              { text: "Utskrift", link: "/print" },
              { text: "Lenker", link: "/lenker" },
            ],
          },
          {
            items: [{ text: "GitLab", link: themeConfig.repo }],
          },
        ],
      },
    ];
  },
});

const {
  createTree,
  menuSortandTrim,
  menuReinsertTrailingSlash,
  cleanDeep,
} = require("@lib/utilities");

module.exports = (options, context = {}) => ({
  name: "autoSidebar",
  async ready() {
    const { pages, themeConfig } = context.getSiteData
      ? context.getSiteData()
      : context;
    let paths = [];
    for (let i = 0; i < pages.length; i++) {
      if (
        pages[i].frontmatter.hasOwnProperty("sidebar") &&
        pages[i].frontmatter.sidebar == false
      ) {
        continue;
      }
      if (
        pages[i].path !== "/" &&
        pages[i].frontmatter.hasOwnProperty("status") &&
        !pages[i].frontmatter.hasOwnProperty("redirect")
      ) {
        paths.push({
          title: pages[i].title,
          path: pages[i].path,
          key: pages[i].key,
          collapsable: true,
        });
      }
    }
    paths = menuSortandTrim(paths);
    let tree = cleanDeep(createTree(paths));
    menuReinsertTrailingSlash(tree);
    themeConfig.sidebar = tree;
  },
});

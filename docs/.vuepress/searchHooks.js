export default {
  async processSuggestions(suggestions, queryString, queryTerms) {
    if (queryString) {
      suggestions.push({
        path: "https://innsida.ntnu.no/sok?category=innsidaweb&query=",
        slug: queryString,
        title: "Søk på Innsida",
        contentStr: '"' + queryString + '"',
        external: true,
      });
      suggestions.push({
        path: "https://www.ntnu.no/sok?category=all&sortby=magic&query=",
        slug: queryString,
        title: "Søk på NTNU.no",
        contentStr: '"' + queryString + '"',
        external: true,
      });
    }
    return suggestions;
  },

  async onGoToSuggestion(index, suggestion, queryString, queryTerms) {
    return true;
  },
};

require("dotenv").config();
require("module-alias/register");
const path = require("path"),
  webpack = require("webpack"),
  autoMenu = require("./plugins/automenu"),
  autoSidebar = require("./plugins/autosidebar"),
  pagesTree = require("./plugins/pagestree"),
  readingTime = require("./plugins/reading-time"),
  childTOC = require("./plugins/child-toc"),
  metadata = require("./plugins/metadata");

module.exports = {
  base: "/arkivplan/",
  dest: "public",
  head: [
    [
      "link",
      {
        rel: "icon",
        href: "/assets/favicon/favicon-96x96.png",
        type: "image/png",
        sizes: "96x96",
      },
    ],
    [
      "link",
      {
        rel: "icon",
        href: "/assets/favicon/favicon.svg",
        type: "image/svg+xml",
      },
    ],
    ["link", { rel: "shortcut icon", href: "/assets/favicon/favicon.ico" }],
    [
      "link",
      {
        rel: "apple-touch-icon",
        href: "/assets/favicon/apple-touch-icon.png",
        sizes: "180x180",
      },
    ],
    ["link", { rel: "manifest", href: "/assets/site.webmanifest" }],
    ["meta", { name: "apple-mobile-web-app-title", content: "NTNU arkivplan" }],
    [
      "meta",
      { name: "viewport", content: "width=device-width, initial-scale=1" },
    ],
  ],
  title: "NTNUs arkivplan",
  url: "https://ntnu.no/arkivplan",
  description:
    "NTNUs arkivplan har en gjennomgang av regelverk, bestemmelser og rutiner for informasjonssikkerhet, personvern og rettigheter for saksbehandling og arkiv.",
  longDescription:
    "I NTNUs arkivplan finner du en helhetlig gjennomgang av regelverk, bestemmelser og rutiner som er viktig for å ivareta god saksbehandling, arkivering, informasjonssikkerhet, personvern og innsynsrettigheter.",
  markdown: {
    lineNumbers: true,
    pageSuffix: "index.html",
    extendMarkdown: (md) => {
      md.use(require("markdown-it-footnote"));
    },
  },
  additionalPages: [
    {
      path: "/print/",
      frontmatter: {
        layout: "All",
        pageClass: "A4",
        title: "NTNUs Arkivplan - Utskriftsformat",
      },
    },
  ],
  plugins: [
    autoMenu,
    autoSidebar,
    pagesTree,
    readingTime,
    childTOC,
    metadata,
    "vuepress-plugin-typescript",
    "vuepress-plugin-nprogress",
    "reading-progress",
    [
      "sitemap",
      {
        hostname: "https://ntnu.no/arkivplan",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "info",
        defaultTitle: "INFO",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "bra",
        defaultTitle: "BRA",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "advarsel",
        defaultTitle: "ADVARSEL",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "fare",
        defaultTitle: "FARE",
      },
    ],
  ],
  locales: {
    "/": {
      lang: "nb",
    },
  },
  themeConfig: {
    logo: "/assets/img/logo_ntnu.svg",
    defaultImage: "/assets/img/bakgrunn.jpg",
    title: "Arkivplan",
    footer: "NTNU © 2021-",
    home: "https://www.ntnu.no/adm/doku/",
    domain: "https://ntnu.no/arkivplan",
    domainAlt: "NTNU.no/arkivplan",
    repo: "https://gitlab.com/ntnu-doku/arkivplan/",
    author: {
      name: "NTNU",
      company: "Norges teknisk-naturvitenskapelige universitet",
      url: "https://www.ntnu.no/",
      image: {
        url: "/assets/img/ntnu.png",
        width: 323,
        height: 323,
        caption: "NTNU Logo",
      },
    },
    docsBranch: "master",
    docsDir: "docs",
    editLinks: true,
    nextLinks: true,
    prevLinks: true,
    categoriesPage: "/kategorier",
    tagsPage: "/emneknagger",
    language: "no",
    language_bcp47: "nb-NO",
    language_icu: "nb_NO",
    locale: {
      doku: "DOKU",
      home: "Hjem",
      author: "Forfatter",
      date: "Dato",
      category: "Kategori",
      tags: "Emneknagger",
      created: "Opprettet",
      updated: "Sist oppdatert",
      updatedBy: "Endret av",
      title: "Tittel",
      link: "Lenke",
      caseOfficers: "Saksbehandlere",
      tableOfContents: "Innhold",
      timeToRead: "Lesetid",
      editTitle: "Er det feil i innholdet, eller noe som ikke fungerer?",
      editLink:
        'Hjelp oss å forbedre denne siden, <a href="https://www.ntnu.no/adm/auv/ansatte-og-tjenester" target="_blank" rel="noopener noreferrer">kontakt DOKU</a>!',
      search: "Søk",
      childPages: "Undersider",
      notFound: "Fant intet innhold på denne siden",
      moved: "Innholdet er flyttet til",
    },
    smoothScroll: false,
    activeHeaderLinks: false,
  },
  configureWebpack: (config) => {
    const env =
      process.env.NODE_ENV === "production"
        ? { ...process.env }
        : { ...process.env.development };
    return {
      resolve: {
        alias: {
          "@plugins": path.resolve(__dirname, "plugins"),
        },
      },
      plugins: [new webpack.EnvironmentPlugin(env)],
    };
  },
  chainWebpack: (config) => {
    config.module
      .rule("files")
      .test(/\.(pdf|zip|ait|log|txt)$/)
      .use("file-loader")
      .loader("file-loader")
      .options({
        name: `[path][name].[ext]`,
        limit: 10000,
        esModule: false,
      });
    config.module.rule("images").use("url-loader").options({
      limit: 10000,
      esModule: false,
    });
  },
  slugify: function (target) {
    const slugify = require("slugify");
    return slugify(target, {
      locale: "nb",
      lower: true,
      remove: /[^\w\s$*_+~.()'"!\-:@/]/g,
    });
  },
};

module.exports = (options, ctx) => {
  const { themeConfig, siteConfig } = ctx;
  const enableSmoothScroll = themeConfig.smoothScroll === true;

  return {
    plugins: [
      ["@vuepress/active-header-links", options.activeHeaderLinks],
      "@vuepress/search",
      "@vuepress/plugin-nprogress",
      ["smooth-scroll", enableSmoothScroll],
    ],
  };
};

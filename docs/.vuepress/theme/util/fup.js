/**
 * @see https://github.com/theamith/json-to-html-table
 */
export function jsonToHTMLTable(parsedJson) {
  var tableHeaders = new Array();
  for (var i = 0; i < parsedJson.length; i++) {
    for (var j = 0; j < Object.keys(parsedJson[i]).length; j++) {
      if (tableHeaders.indexOf(Object.keys(parsedJson[i])[j]) == -1)
        tableHeaders.push(Object.keys(parsedJson[i])[j]);
    }
  }
  var headersHtml = "<tr>";
  for (var k = 0; k < tableHeaders.length; k++) {
    headersHtml += '<th scope="col">' + tableHeaders[k] + "</th>";
  }
  headersHtml += "</tr>";
  var rows = "";
  for (var l = 0; l < parsedJson.length; l++) {
    rows += "<tr>";
    for (var m = 0; m < tableHeaders.length; m++) {
      if (typeof parsedJson[l][tableHeaders[m]] == "undefined")
        rows += "<td></td>";
      else rows += "<td>" + parsedJson[l][tableHeaders[m]] + "</td>";
    }
    rows += "</tr>";
  }
  return `<table><thead>${headersHtml}</thead><tbody>${rows}</tbody</table>`;
}

export function getFUPTable() {
  return Array.from(document.querySelectorAll("table")).find(
    (el) =>
      el.querySelectorAll("thead tr th")[0].innerText === "Kode" &&
      el.querySelectorAll("thead tr th")[1].innerText === "Funksjon" &&
      el.querySelectorAll("thead tr th")[2].innerText === "Underfunksjon" &&
      el.querySelectorAll("thead tr th")[3].innerText === "Prosess"
  );
}

/**
 * @version 2.0.3
 * @see https://github.com/acagastya/jsonify-my-table
 */
function jsonifyHTMLTableElement(tableElement) {
  const result = {};
  result["error"] = false;
  // 1. Does table have rows?
  if (tableElement.rows.length < 1) {
    result["error"] = "There are no rows in the table.";
    return result;
  }

  // 2. Extract headers.
  const headers = [...tableElement.rows[0].cells].map((cell) => {
    if (cell.childElementCount == 0) return cell.innerText;
    if (cell.childElementCount != 1) {
      result["error"] = "Can't extract headers.";
      return undefined;
    }
    while (cell.childElementCount == 1) cell = cell.children[0];
    return cell.innerText;
  });

  // 3. Are headers unique?
  if (headers.length > new Set(headers).size) {
    result["error"] = "Headers repeat";
    return result;
  }

  // 4. Prepare the result.
  const res = [...tableElement.rows].map((row, index) => {
    if (index == 0) return; // This is header
    const cells = [...row.cells].map((cell) => {
      if (cell.childElementCount == 0) return cell.innerText;
      if (cell.childElementCount != 1) {
        result["error"] = "Can't extract information.";
        return undefined;
      }
      while (cell.childElementCount == 1) cell = cell.children[0];
      return cell.innerText;
    });
    return headers.reduce(
      (acc, cur, index) => (acc = { ...acc, [cur.valueOf()]: cells[index] }),
      {}
    );
  });

  res.shift(); // Remove header
  result["res"] = res;
  result["headers"] = headers;
  return result;
}

export function adaptFUPTable() {
  let table = getFUPTable();
  if (!table) return;
  let json = jsonifyHTMLTableElement(table).res;
  json = json.map((item) => {
    return {
      Kode: item["Kode"],
      Funksjon: item["Funksjon"],
      Underfunksjon: item["Underfunksjon"],
      Prosess: item["Prosess"],
      Stikkord: item["Stikkord"],
      "Bevaring / Kassasjon": item["B/K"],
      "Frist for kassasjon": item["Frist for kassasjon"],
    };
  });
  for (let i = 0; i < json.length; i++) {
    if (json[i].Funksjon == "") {
      json[i].Funksjon = json[i - 1].Funksjon;
    }
    if (json[i].Underfunksjon == "") {
      json[i].Underfunksjon = json[i - 1].Underfunksjon;
    }
    if (json[i]["Bevaring / Kassasjon"] == "B") {
      json[i]["Bevaring / Kassasjon"] = "Bevar";
    }
    if (json[i]["Bevaring / Kassasjon"] == "K") {
      json[i]["Bevaring / Kassasjon"] = "Kasser";
    }
  }
  table.innerHTML = jsonToHTMLTable(json);
  table = getFUPTable();
  let headerCells = table.querySelectorAll("thead th");
  headerCells.forEach((_headerCell, i) => {
    _headerCell.setAttribute("scope", "col");
    table
      .querySelectorAll(`tbody tr td:nth-child(${i + 1})`)
      .forEach((cell) => {
        cell.dataset.label = headerCells[i].innerText.trim();
      });
  });
  table.querySelectorAll(`tbody tr td:first-child`).forEach((cell) => {
    cell.setAttribute("scope", "row");
  });
  const p = document.createElement("p");
  p.innerHTML = `<a href="/arkivplan/assets/fup.html">Større versjon</a>`;
  let tableParent = getFUPTable().parentNode;
  tableParent.insertBefore(p, table);
}

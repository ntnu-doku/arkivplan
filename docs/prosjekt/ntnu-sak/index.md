---
date: '2025-01-08T00:00:00'
title: NTNU Sak
status: Ferdig
category: Oversikt
tags:
  - Digitalisering
  - Arkiv
  - Dokument
author: Ole Vik
---

[NTNU Sak](https://i.ntnu.no/wiki/-/wiki/Norsk/NTNU+Sak+-+innf%C3%B8ring) var fra våren 2021 til slutten av 2024 et prosjekt for innføring av standard fellestjenester for saksbehandling ved NTNU, med en ny saksbehandlingsløsning levert av det nasjonale prosjektet UH Sak. I løpet av prosjektet har ambisjoner om å heve kvaliteten i saksbehandlingen og forbedre, fornye og forenkle prosesser og tjenester kommet til.

Som ledd i prosjektets arbeid ble det satt i verk en gjennomgang og forbedring av administrative arbeidsprosesser som ble [implementert ved NTNU](https://i.ntnu.no/wiki/-/wiki/Norsk/Prosesser+ved+NTNU). Mange ansatte var med i arbeidsgrupper som skulle standardisere og øke kvaliteten i saksbehandlingen. Dette arbeidet fortsetter ved NTNU gjennom [prosessorganiseringen](https://i.ntnu.no/wiki/-/wiki/Norsk/Prosessorganisering+-+roller+og+ansvar).

Det var usikkert om løsningen ville dekke behovene til saksbehandling og bevaring av dokumentasjon godt nok. Mye teknisk og funksjonsmessig utvikling gjensto. Løsningen skulle etter planen erstatte sak- og arkivsystemet ePhorte. I tillegg skulle NTNU få en ny plattform for saksbehandling, med større utviklingsmuligheter. Sektoravtalen med leverandøren Sopra Steria ble avsluttet i juni 2024 etter at eierne i UH Sak vurderte risikoen med innføringen som for høy. Se [pressemelding fra Sikt](https://web.archive.org/web/20240626123303/https:/sikt.no/tiltak/uh-sak/avbestiller-uh-sak), omtale i [Universitetsavisa](https://web.archive.org/web/20240626123431/https:/www.universitetsavisa.no/bjorn-haugstad-ntnu-roar-tobro/dropper-prosjekt-til-over-100-millioner-kroner/410601) og [Khrono](https://web.archive.org/web/20240626123607/https:/www.khrono.no/avsluttet-samarbeidet-med-leverandor-av-nytt-system-for-saksbehandling/885106) 26.06.2024.

## Arkivert fra prosjektet

Det meste av materiale fra og informasjon om prosjektet er ryddet og arkivert, og tilgjengelig gjennom følgende saksnummer og utvalgte journalposter. De er samlet under ordningsprinsippet «Prosjekt» med verdien «NTNU Sak - Innføringsprosjekt for ny saksbehandlingsløsning og arkivkjerne» i sak- og arkivsystemet [Elements](https://i.ntnu.no/wiki/-/wiki/Norsk/Elements+-+sak-+og+arkivsystem).

### Prosjektledelse/-kontor

2022/7483 Prosjekt - NTNU Sak - Styrende dokumenter

- Gjennomføring av prosjektet NTNU Sak

- Utredninger og ambisjoner for NTNU Sak

2022/7489 Prosjekt - NTNU Sak - Styringsgruppemøter

### Delprosjekter/arbeidspakker

2024/97048 Prosjekt - NTNU Sak Innføring

- Prosjektformål NTNU Sak Innføring

- Innførings- og beredskapsplaner for NTNU Sak Innføring

#### Dialog

- Modell for innføring av NTNU Sak

- Prosessorganisering ved NTNU gjennom NTNU Sak

- NTNU Saks involvering av organisasjonen med fagspesialister og prosessrådgivere

#### Kommunikasjon

- Kommunikasjonsarbeid i NTNU Sak

- Innsikt og endringshistorier for brukere av NTNU Sak

- Grafisk profil, grafikk og personas for NTNU Sak

2023/33377 Prosjekt - NTNU Sak Innføring - Risikovurderinger fra organisasjonsenhetene

2024/96487 Prosjekt - NTNU Sak Brukerstøtte

- Prosjektformål NTNU Sak Brukerstøtte

- Brukerstøtte for saksbehandling med etablerte og nye brukerstøttegrupper ved NTNU

- Arbeid med fornyet brukerstøttetilbud for saksbehandling ved NTNU

- Opplæring i brukerstøtte fra NTNU Sak

- Støttemateriale og utvikling av det for NTNU Sak

- Beredskap under innføring av sak- og arkivsystemet Elements

2024/97111 Prosjekt - NTNU Sak Opplæring

- Prosjektformål NTNU Sak Opplæring

- Generell opplæring i offentlig forvaltning og saksbehandling

- Metode for utvikling av læringsressurser NTNU Sak og Elements

- Arbeid med opplæring ved innføring av Elements

2024/98217 Prosjekt - NTNU Sak Forvaltning og løsning

- Prosjektformål NTNU Sak Forvaltning og løsning

- Etablering av forvaltningsorganisasjon for Drift, Forvaltning og Videreutvikling av NTNU Sak

- Beredskap, driftsetting og overlevering til forvaltningsorganisasjon og linje fra NTNU Sak

- Etablering og verifikasjon av teknisk løsning for NTNU Sak

- Sikkerhetsvurderinger for NTNU Sak

2024/14173 Prosjekt - NTNU Sak Konfigurasjon og innholdsproduksjon - Tilgangsstyring

- Presentasjon til prosessansvarlige - Tilgangsbegrensede prosess i NTNU sak

- Dekningsgrad av saksprosesser i arbeidet med tilgangsstyring

- Brukerhistorier innen tilgangsstyring

- Brukerbehov innen tilgangsstyring

- Detaljert beskrivelse - ROLF roller

2024/14788 Prosjekt - NTNU Sak Konfigurasjon og innholdsproduksjon - Arbeidspakke produksjon av innhold

- Metodikk for utvikling av skjema til bruk i saksbehandling på NTNU

- Prosessdesignerens guide - Innholdsfabrikken

- Prioriteringskriterier for innholdsproduksjon

- Oversikt over skjema og maler ved NTNU

2024/14000 Prosjekt - NTNU Sak Konfigurasjon og innholdsproduksjon - Arbeidspakke funksjonell konfigurasjon

- Overlevering av konfigurasjonsarbeid

- Presentasjon for prosessansvarlige - Tema for konfigurasjon ved NTNU

2024/14206 Prosjekt - NTNU Sak Konfigurasjon og innholdsproduksjon - Presentasjoner

2023/15738 Prosjekt - NTNU Sak - Tjenesteutviklingsteamet

- Prosessmandat - mal

- Roller - prosessarbeid

- Metode for digitalisering av tjenester

- Modell for tjeneste og prosess - Teaterscenen

- Personas studenter

2022/56507 NTNU sak prosjektnummer 82505400 - Rydding og dokumentfangst - prosjektdokumentasjon

- Sluttrapport

- Anbefalte prioriterte områder - dokumentfangst

2022/27656 Prosjekt - NTNU Sak - Testmiljø og testdata

### Relaterte

2023/38982 Kvalitet i saksbehandling - prosesseiers forvaltningsansvar

- Prosesseiers arbeid med ny saksbehandlingsløsning og behandling av personopplysninger - igangsetting av arbeid

- Prosesseiers og -ansvarliges oppdrag med å identifisere lokalt brukerstøtteapparat og forvalte informasjon

2024/23973 Status for prosesseieres og -ansvarliges oppdrag med å identifisere lokalt brukerstøtteapparat og forvalte informasjon

---
date: '2025-01-08T00:00:00'
title: Rydding og dokumentfangst
status: Ferdig
category: Rutiner
tags:
  - Administrativt
  - Ansvar
author: Ole Vik
---

Rydding og dokumentfangst er et delprosjekt under [NTNU Sak](https://i.ntnu.no/wiki/-/wiki/Norsk/NTNU+Sak+-+innf%C3%B8ring), og her finner du gjeldene forslag til rutiner som er relevant for deg som saksbehandler. Se [prosjektets side på Innsida](https://i.ntnu.no/wiki/-/wiki/Norsk/NTNU+Sak+Rydding+og+dokumentfangst) for mer informasjon.

::: info Registreringsrutiner

[Ansettelsessaker](/arkivplan/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/ansettelsessaker.pdf)

[Anskaffelsessaker](/arkivplan/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/anskaffelsessaker.pdf)

[Forskningsprosjekt](/arkivplan/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/forskningsprosjekt.pdf)

[Prosjekt](/arkivplan/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/prosjekt.pdf)

[Styre, Råd og Utvalg](/arkivplan/vedlegg/prosjekt/ntnu-sak/rydding-og-dokumentfangst/styre-raad-og-utvalg.pdf)

:::

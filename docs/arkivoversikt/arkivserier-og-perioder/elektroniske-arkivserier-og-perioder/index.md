---
date: '2021-04-13T00:00:00'
title: Elektroniske arkivserier og -perioder
status: Ferdig
category: Prosess
tags:
  - Dokument
author: Geir Ekle
---

## Saksarkiv for NTNU

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Inn- og utgående saksdokumenter, interne notater med og uten oppfølging, saksfremlegg til styrer og faste råd og utvalg

Tilgang for: Alle registrerte brukere i henhold til autorisasjon og roller

Ordning: Arkivdel EMNE3 / Arkivnøkkel for NTNU

Periode: Start januar 2017

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei

Antall år til kassasjon:

Type: Original

Kassasjonshjemmel: Kassasjon i henhold til felles kassasjonsregler for statsforvaltningen og vedtak fra Riksarkivaren. Rundskriv NOARK-systemer fra Riksarkivaren

Merknader:

Produsert i elektronisk system: ePhorte

Godkjent av:

Dato:

Informasjonssikkerhet:

## Bygningsarkiv

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Drift, vedlikehold, ombygging og annet som gjelder NTNUs bygninger.

Anbud og kontrakter

Tilgang for: Arkivmedarbeidere og saksbehandlere i henhold til roller og autorisasjon

Ordning: Bygningsnummer (LYDIA) / NTNUs arkivnøkkel 034

Periode: 01.12.2004

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei

Antall år til kassasjon:

Type: Original

Kassasjonshjemmel:

Merknader: Bankgarantier oppbevares i tillegg på papir i låst safe/skap

Produsert i elektronisk system:

Godkjent av:

Dato:

Informasjonssikkerhet:

## Personalmapper

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Sykefravær, HMS, IA

Administrasjon arbeidsforhold

Arbeidsavtaler, forlengelser opphør

Opprykk etter kompetanse

Tjenstlig tilrettevisning/advarsel

Tilgang for: Arkivmedarbeidere og saksbehandlere i henhold til roller og autorisasjon

Ordning: Arkivdel PERS, Personalarkiv/Ansattnummer 6 siffer

Periode: Start juli 2012

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei

Antall år til kassasjon:

Type: Original

Kassasjonshjemmel:

Merknader:

Produsert i elektronisk system: ePhorte

Godkjent av: Arkivleder

Dato:

Informasjonssikkerhet:

## Studentdokumentasjon

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Eksamen, fusk, master, opptak, politiattest, praksis, skikkethet, studieforløp, tilrettelegging

Tilgang for: Arkivmedarbeidere og saksbehandlere i henhold til roller og autorisasjon

Ordning: Fødselsnummer

Periode: Desember 2016

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei

Antall år til kassasjon

Type: Original

Kassasjonshjemmel:

Merknader:

Produsert i elektronisk system:

Godkjent av:

Dato:

Informasjonssikkerhet:

## Styret NTNU

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Innkallinger, saksframlegg, vedtak, protokoller

Tilgang for: Arkivmedarbeidere og saksbehandlere i henhold til roller og autorisasjon

Ordning: Arkivdel Styret / Arkivnøkkel for NTNU

Periode: Januar 1996

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei

Antall år til kassasjon:

Type: Original

Kassasjonshjemmel:

Merknader: Vedlegg til saksframlegg fra 01.01.1996 til 31.12.2008 som papirarkiv, fjernarkiv Dora

Produsert i elektronisk system: ePhorte

Godkjent av:

Dato: 29.02.2012

Informasjonssikkerhet:

## Topografisk arkiv

Daglig ansvar: Daglig ansvar: Institutt for arkeologi og kulturhistorie (VM)

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Topografisk arkiv er NTNU Vitenskapsmuseets arkiv for arkeologisk og kulturhistorisk dokumentasjon fra museumsdistriktet. Dette omfatter rapporter, analyser, innberetninger, kart, tegninger, korrespondanse og andre primærkilder som gjengir, beskriver, analyserer eller utforsker konkrete funn og fornminner i museumsdistriktet. Dokumentasjonen skal være knyttet til arkeologi og kulturhistorie innen museumsdistriktet. Arkivet skal inneholde primærkilder for dokumentasjon, og tilvekst omfatter fra 01.01.2017 ikke dokumenter fra saksbehandling innenfor museets forvaltningsrelaterte oppgaver etter kulturminneloven eller vitenskapelige publikasjoner. Arkivet er ordnet etter stedsinformasjon, primært etter den offentlige matrikkelen (fylke, kommune, gårds- og bruksnummer).

Tilgang for: Arkivmedarbeidere og saksbehandlere i henhold til roller og autorisasjon

Ordning: Topografisk etter fylke, kommune, matrikkelgård, adresser og/eller lokaliteter

Periode: April 2005

Oppbevaringsmedium: Elektronisk

Fysisk plassering: Gunnerushuset

Overføring: Nei

Kassasjon:

Antall år til kassasjon:

Type:

Kassasjonshjemmel:

Merknader:

Produsert i elektronisk system: ePhorte

Godkjent av:

Dato: 29.02.2012

Informasjonssikkerhet:

## Vitnemål

Arkivserie: Vitnemål

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: NTNU

Innhold: Kopi av utstedte og underskrevet vitnemål

Tilgang for: Arkivmedarbeidere og saksbehandlere i henhold til roller og autorisasjon

Ordning: Arkivdel Vitnemål - Arkivnøkkel NTNU

Periode: Mars 2018

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei. Bevares

Antall år til kassasjon

Type:

Kassasjonshjemmel:

Merknader:

Produsert i elektronisk system: ePhorte

Godkjent av:

Dato: 29.02.2012

Informasjonssikkerhet:

## Avvik

Daglig ansvar: Arkivleder

Arkiv: NTNU

Journalførende enhet: Avvik

Innhold: Avviksmeldingene kan omhandle skader på mennesker, miljø, bygg eller materiell, farlige situasjoner eller nesten-ulykker, vold og trusler, brudd på rutiner og prosedyrer / lover og forskrifter og forslag til forbedringer knyttet til helse, miljø og sikkerhet ved NTNU.

Tilgang for: Brukere i henhold til autorisasjon og roller

Ordning: Arkivdel Avvik / Arkivnøkkel for NTNU

Periode: 16.01.2012 – 19.04.2017

Oppbevaringsmedium: Elektronisk

Fysisk plassering:

Overføring:

Kassasjon: Nei. Bevares

Antall år til kassasjon:

Type: Original

Kassasjonshjemmel:

Merknader:

Produsert i elektronisk system: Elektronisk system for håndtering av HMS-avvik, Risk Manager. Overført for arkivering i sak- og arkivsystemet.

Godkjent av:

Dato: 13.04.2021

Informasjonssikkerhet:

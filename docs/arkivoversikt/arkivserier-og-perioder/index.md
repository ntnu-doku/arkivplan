---
date: '2021-07-30T00:00:00'
title: Arkivserier og -perioder
status: Utkast
category: Organisering
tags:
  - Dokument
author: Geir Ekle
---

Arkivserie er en del av et arkiv som er ordnet etter ett og samme ordningsprinsipp.  
Ordningsprinsipp består av to hovedelementer: Inndelingsprinsipp og rekkeordningssystem. Ordningsprinsippene er vanligvis beskrevet i en arkivnøkkel.

## Arkivperioder

En arkivperiode er tidsperioden for inndeling av arkiv. I perioden 1996 til 2004 hadde alle nivåer ved NTNU egne arkiv; hovedarkivet (sentral ledelse og administrasjon), fakultetene, instituttene, Vitenskapsmuseet (VM) og Universitetsbiblioteket i Trondheim (UBiT).

Hovedarkivet, VM, UBiT og arkivene på fakultetsnivå benyttet Symfoni Sak og Arkiv til elektroniske journaler mens instituttene førte manuelle journaler.

Elektronisk journal og elektronisk saksbehandling og arkivering av saksdokumenter ble innført ved sentral ledelse og administrasjon i desember 2004, og ved fakultetene i løpet av våren 2005.

Ved fusjonen med høgskolene i Sør-Trøndelag, Ålesund og Gjøvik fra 2016 (mellomåret) ble høgskolenes administrative inndeling registrert og tilknyttet NTNUs arkivdel for sakarkivet, men med egne journalførende enheter.

Ny arkivperiode og administrativ struktur ble etablert i sak- og arkivsystemet fra 2017 med ny administrativ organisering etter fusjonen.

## Elektroniske arkivserier og -perioder

Se siden [Elektroniske arkivserier og -perioder](/arkivplan/arkivoversikt/arkivserier-og-perioder/elektroniske-arkivserier-og-perioder) for nærmere beskrivelse og oversikt.

## Avsluttet arkiv

NTH administrasjonen 1910-1995 – oppbevart i fjernarkiv, se [Arkivoversikt](/arkivplan/vedlegg/arkivoversikt/arkivserier-og-perioder/arkivoversikt-nth-administrasjonen-1910-1995.pdf) (vedlegg)

UNIT administrasjonen 1968-1995 – oppbevart i fjernarkiv

NLHT administrasjonen 1922-1984 – oppbevart i fjernarkiv

AVH administrasjonen 1984-1995 – oppbevart i fjernarkiv

NTNU felles administrasjon og ledelse 1996-2004 – oppbevart i fjernarkiv

NTNU 2004-2013 – elektronisk arkiv

---
date: '2021-01-26T00:00:00'
title: Elektroniske systemer
status: Ferdig
category: Referanse
tags:
  - Administrativt
author: Geir Ekle
---

NTNU har et felles sak- og arkivsystem, ePhorte, for saksbehandling i hele organisasjonen.  
For annen saksbehandling benytter NTNU en rekke elektroniske fagsystemer og databaser til å holde orden på informasjon om NTNUs virksomhet, forvaltning og tjeneste, med mer.

En oppdatert liste over NTNUs systemer finnes på [Innsida.NTNU.no/systemer](https://innsida.ntnu.no/systemer).

Oversikt over elektroniske system bør inneholde følgende opplysninger så langt det er mulig:

**System  
**Systemnavn  
Tatt i bruk dato  
Avsluttet dato  
  
**Administrative data  
**Arkivdeler  
Innhold  
Lisensinnehaver  
Brukergruppe  
Systemansvarlig  
Driftsansvarlig  
Relasjoner til andre system  
Leverandør/kontaktperson  
Merknader  
  
**Tekniske data**  
Databaseplattform  
Produksjonsformat  
Arkivformat  
Lokalisering/plassering  
Systemdokumentasjon (vedlegg)  
Brukerdokumentasjon (vedlegg)  
  
**Avlevering**  
Kassasjon  
Hjemmel for kassasjon  
Tabelluttrekk  
Overføring  
Metadata  
Merknader  
Godkjent av  
Godkjent dato  
Infosikkerhet

---
date: '2020-12-04T00:00:00'
title: Fjernarkiv
status: Ferdig
category: Faglig
tags:
  - Bevare
  - Beholde
author: Geir Ekle
---

Krav til arkivlokaler er hjemlet i arkivforskriftens § 7 og Riksarkivarens forskrift kapittel 2. Lokaler hvor man oppbevarer arkiv over lengre tid er å betrakte som arkivlokaler.

Fjernarkiv er arkivlokaler hvor arkivmateriale som er skilt ut fra dagligarkiv blir oppbevart fordi det ikke lenger er en del av saksbehandlingen eller ikke lenger i daglig bruk. Materialet skal oppbevares i lokaler godkjent til dette formålet, og lokalene skal gi arkivmaterialet vern mot vann, fukt, brann, klimapåvirkning, innbrudd og annet skadeverk.

## Historiske arkiv

De historiske arkivene er en viktig del av NTNUs interne hukommelse og har betydelig kulturell og forskningsmessig verdi som NTNU forvalter inntil de avleveres til Arkivverket.

NTNUs historiske arkiver befinner seg i NTNUs fjernarkiv som er lokalisert ved Arkivsenteret på Dora i Trondheim. Samlingen består av blant annet sakarkiver, personalmapper, studieplaner, studentlister, avhandlinger og vitnemål fra tidligere universitet og høgskoler som NTH, NLHT, AVH, HiST og NTNU. Tegninger av bygninger og campus helt tilbake til planleggingen av NTH på starten av 1900-tallet er også en del av det historiske materialet.

Historiske arkiv fra Høgskolen i Gjøvik og Høgskolen i Ålesund er oppbevart på NTNUs campuser i henholdsvis Gjøvik og Ålesund.

## Tilgjengelighet

Alle kan søke om innsyn i NTNUs historiske arkiver, som f.eks. forskere, studenter, journalister eller andre interesserte. Hvert innsyn blir behandlet i henhold til gjeldende lovverk.

Kontakt <postmottak@ntnu.no> og angi så nøyaktig som mulig hva du er ute etter.  
Er dokumentene offentlige kan de lånes ut for gjennomsyn på lesesalen på Arkivsenteret, Dora.

For informasjon om kopi av vitnemål se [informasjon på Innsida](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Vitnem%C3%A5l).

## Materiale til fjernarkiv

Fellesadministrasjonen, fakultetene, instituttene og Vitenskapsmuseet kan selv organisere ordning og avtale avlevering av arkivmateriale direkte til Arkivverket, eller etter avtale med DOKU sende arkivmaterialet til fjernarkiv på Dora for oppbevaring.

Ta kontakt med DOKU dersom det er arkivmateriale og dokumentasjon som skal overføres til fjernarkiv for oppbevaring. DOKU kommer gjerne på befaring for å gi råd før overføring av arkivmateriale dersom det er ønskelig.

::: info Sending av arkivmateriale til fjernarkiv

Før arkivmateriale sendes til fjernarkiv for oppbevaring skal følgende kriterier være oppfylt:

- Materialet skal pakkes i arkivbokser som nummereres og merkes med arkivkode og ytterår, med blyant

  - A4 med 9,5 cm dybde (Staples varenummer 842006)

- Det skal lages en liste over arkivmaterialet

  - Hver arkivboks føres opp fortløpende, og man skal også føre opp innholdet i boksene, arkivkoder og tidsperiodens ytterår

  - Samme informasjon skal stå utenpå boksen

  - Institutt- og fakultetsnavn skal stå øverst på lista

- Plastomslag, strikk og lignende skal fjernes før arkivmaterialet legges i bokser

- Forskjellige arkivserier skal ikke slås sammen, men pakkes slik de opprinnelig ble dannet, og alt materiale fra en tidsperiode skal sendes samlet

  - Eventuelle arkivnøkler som er benyttet skal følge med

Eksempel på merking på arkivboks:

Fakultet for … eller Institutt for …  
Hva slags materiale eller Arkivkode  
1973 – 1975  
Boks 1

Liste over arkivmaterialet:

Fakultet for … eller Institutt for …  
1973 – 1975  
Da 1 (se vedlegget “Ordningsplan” lenger ned på denne siden)

:::

### Eksamensbesvarelser

::: info Sending av eksamensbesvarelser til fjernarkiv

Før eksamensbesvarelser kan sendes fjernarkiv for oppbevaring må følgende være klargjort:

- Besvarelsene tas ut av konvoluttene, sorteres på emnekode i stigende rekkefølge etter kandidatnummer og legges i arkivbokser.

  - Eventuelle plastlommer fjernes. Boksene merkes med emnekode og kandidatnummer fra – til på fremsiden, kortsiden av esken, ikke på lokket.

- Boksene stables i transportbur – ta kontakt med Transportsentralen for dette – eller i pappesker, flyttekasser som settes på paller. Ikke mer enn to kasser i høyden.

  - Boksene må merkes godt med innhold, avsender og mottaker.

- Selve oppgaveteksten tas vare på og legges for seg i egne arkivbokser, godt merket.

:::

### Transport til fjernarkiv

Transport av materialet til fjernarkiv på Dora bestilles etter at det er gjort avtale med fagmiljøet på Dora. Transport bestilles via Transportsentralen på NTNU.

## Ordningsplan

[Ordningsplan for arkivene etter fakulteter og institutter](/arkivplan/vedlegg/arkivoversikt/fjernarkiv/ordningsplan_for_arkivene_etter_fakulteter_og_institutter.pdf) (vedlegg)

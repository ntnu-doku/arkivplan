---
date: '2021-01-29T00:00:00'
title: Arkivoversikt
status: Ferdig
category: Oversikt
author: Geir Ekle
---

Arkiv har flere betydninger. Det kan bety dokumenter som blir til, produseres eller mottas i en virksomhet, og samles som et resultat av dette. Arkiv kan også forstås som oppbevaringssted av dokumenter og enheter som utfører arkivrelaterte oppgaver; arkivtjeneste og dokumentasjonsforvaltning.

NTNUs arkiver dokumenterer beslutninger, handlinger og minner. De er informasjonskilder som understøtter pålitelige og transparente administrative prosesser. Arkivoversikten viser hvilke arkiv som benyttes i det daglige, hvilke som er deponert og avlevert til Arkivverket og hvilke som er oppbevart i fjernarkivet. I tillegg finnes tidligere og nåværende arkivnøkler i oversikten. En arkivnøkkel er inngangen til arkivene og sier noe om hvordan arkivene er ordnet og inndelt.

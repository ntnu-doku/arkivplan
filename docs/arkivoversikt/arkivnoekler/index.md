---
date: '2022-01-27T00:00:00'
title: Arkivnøkler
status: Ferdig
category: Organisering
tags:
  - Definisjon
  - Kategorisering
author: Geir Ekle
---

En arkivnøkkel er et system for ordning av arkiv basert på ett eller flere ordningsprinsipp. Arkivnøkler viser inndelingsprinsipp og rekkeordningssystem for arkivet, og gir en systematisk oversikt over arkivkodene eller verdiene som benyttes i rekkeordningen. Arkivnøkler brukes for å gruppere en rekke dokumenter, som hører sammen enten basert på funksjon eller emne.

Arkivforskriften § 2-3 sier at en virksomhet normalt skal ha et klassifikasjonssystem – en arkivnøkkel – som omfatter alle saksområder. NTNU trenger ikke godkjenning av arkivnøkkelen, men den skal registreres hos Arkivverket. Å benytte en arkivkode eller en verdi, og merke tilhørende dokumenter med denne koden eller verdien, kalles å klassere.

## Arkivnøkler og bruksperioder

1922-1964 Arkivnøkkel for Norges tekniske høgskole 01.01.22-31.12.64

1965-1966 samme som over

1967-1974 Arkivnøkkel for NTH, del 2 av Arkivhåndbok for NTH 01.11.67-17.12.75

Tillegg: 01.11.67, del 3 av Arkivhåndbok 1967

Tillegg: Juli 1974, nytt opplag med rettelser og tillegg

1970-1981 Arkivnøkkel for Universitetet i Trondheim 01.08.1970-31.12.81 Utbyggingssekretariatet

1975-1981 Arkivnøkkel for NTH, ny utgave med noen få rettelser 18.12.75-31.12.81

1982-1994 Arkivnøkkel for Norges tekniske høgskole 01.01.82-31.08.94

Tillegg: 1984, del 3 av Arkivhåndbok 1982

1994-1996 Arkivnøkkel for UNIT, også benyttet av NTH, 01.09.94-ca. 30.11.96

1996-2004 Arkivnøkkel for NTNU ca. 01.12.96-07.12.04

2004- og videre Arkivnøkkel for NTNU 08.12.04- og videre

::: info Gjeldene arkivnøkkel

Se vedlegget [Arkivnøkkel for NTNU - 2004](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnokkelntnu2004.pdf).

:::

[Fellesnøkkelen](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/fellesnoekkelen-1999.pdf) (vedlegg)

[Arkivnøkkel for NTH 1922 - 1967](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnokkelnth19221967.pdf) (vedlegg)

[Arkivnøkkel for NTH 1967 - 1982](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnokkelnth19671982.pdf) (vedlegg)

[Arkivnøkkel for NTH 1982 - 1994](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnokkelnth19821994.pdf) (vedlegg)

[Arkivnøkkel for NTNU - 2004](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnokkelntnu2004.pdf) (vedlegg)

[Arkivnøkkel for UNIT - Utbyggingssekretariatet 1970 - 1982](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnokkelunitutbyggingssekr19701982.pdf) (vedlegg)

[Arkivnøkkel og kassasjonsplan for de statlige høgskolene 01.07.1994, revidert oktober 1996 og mai 2001, med kassasjonsplan mai 2001 - Modulink og ePhorte](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnoekkel-og-kassasjonsplan-for-de-statlige-hoegskolene-modulink-og-ephorte.pdf) (vedlegg)

[Arkivnøkkel for universiteter og høgskoler 2014 - Public360](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/arkivnoekkel-for-statlige-universitet-og-hoegskoler-public360.pdf) (vedlegg)

[Tidligere rutiner: Generelle rutiner for saksbehandling og arkiv ved innføring av elektronisk sak- og arkivsystem ved NTNU](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/tidligere-rutiner-ved-innfoering-sak-og-arkiv.pdf) (vedlegg)

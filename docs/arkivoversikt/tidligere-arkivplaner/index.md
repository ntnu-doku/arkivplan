---
date: '2021-02-02T00:00:00'
title: Tidligere arkivplaner
status: Ferdig
category: Referanse
author: Geir Ekle
---

Arkivplanen er en samlet oversikt over arkiv og arkivfunksjoner. Ifølge arkivforskriften § 4 skal arkivplanen vise hva arkivet omfatter og hvordan det er organisert. Dessuten skal den vise hvilke instrukser, regler og planer som gjelder for arkivarbeidet.

Arkivplanen og eventuelle vedlegg skal bevares. Ved vesentlige endringer skal tidligere versjoner av arkivplan arkiveres. Arkivplanen bør som regel «fryses» og arkiveres i sak- og arkivsystemet minst en gang i året.

Hvis arkivplanen lages i et elektronisk system eller en nettløsning, må det være mulig å eksportere den til et arkivformat for arkivering.

NTNUs arkivplaner er arkivert i sak- og arkivsystemet.

[NTNUs arkivplan fra november 2020](/arkivplan/vedlegg/arkivoversikt/tidligere-arkivplaner/ntnu_arkivplan_pr_november_2020.pdf) (vedlegg)

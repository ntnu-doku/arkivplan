---
date: '2021-01-27T00:00:00'
title: Deponert arkiv
status: Ferdig
category: Formelt
tags:
  - Arkiv
  - Bevare
  - Dokument
author: Geir Ekle
---

Å deponere et arkiv betyr å overføre arkivmaterialet til et depot uten at råderetten over det overføres. NTNU deponerer arkiv til Arkivverket. Arkivforskriften § 13 og Riksarkivarens forskrift kapittel 6 og 8 hjemler krav om deponering og deponeringsprosesser.

Testing av arkivuttrekk gjøres av Arkivverket etter kravene som er fastsatt i forskriftsbestemmelsene om elektronisk arkivmateriale som avleveres eller overføres som depositum til Arkivverket, og de spesifikke kravene fastsatt i Noark 4-standarden. Testing utføres av eget testverktøy.

## UNIT, NTNU

Journalbaser uten dokumenter, 1995-2005: Deponering testet og godkjent i perioden 2013-2014.

<table>
<thead>
<tr>
<th><strong>Organisasjon</strong></th>
<th><strong>Enhet</strong></th>
<th><strong>Tidsperiode</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Universitetet i Trondheim</td>
<td>Norges tekniske høgskole Administrasjonen</td>
<td>01.01.1995-31.12.1995</td>
</tr>
<tr>
<td>Universitetet i Trondheim</td>
<td>Den allmennvitenskapelige høgskolen Administrasjonen</td>
<td>01.01.1995-31.12.1995</td>
</tr>
<tr>
<td>Universitetet i Trondheim</td>
<td>Universitets-administrasjonen</td>
<td>01.01.1995-31.12.1995</td>
</tr>
<tr>
<td>NTNU</td>
<td>Administrasjonen</td>
<td>01.01.1996-31.12.2004</td>
</tr>
<tr>
<td>Universitetet i Trondheim</td>
<td>Det medisinske fakultet</td>
<td>01.01.1995-31.12.1995</td>
</tr>
<tr>
<td>NTNU</td>
<td>Det medisinske fakultet</td>
<td>01.01.1996-14.07.2005</td>
</tr>
<tr>
<td>Universitetet i Trondheim</td>
<td>Vitenskapsmuseet</td>
<td>01.01.1995-31.12.1995</td>
</tr>
<tr>
<td>NTNU</td>
<td>Vitenskapsmuseet</td>
<td>01.01.1996-31.03.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Universitetsbiblioteket</td>
<td>01.01.1997-19.04.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Artsdatabanken</td>
<td>01.01.1997-15.04.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for geofag og petroleumsteknologi</td>
<td>01.01.1997-31.12.2001</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for ingeniørvitenskap og teknologi</td>
<td>01.01.2002-30.04.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for fysikk, informatikk og matematikk</td>
<td>01.01.1997-31.12.2001</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for kjemi og biologi</td>
<td>01.01.1997-09.05.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for marin teknikk</td>
<td>01.01.1997-30.04.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for maskinteknikk</td>
<td>01.01.1997-31.12.2001</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for bygg- og miljøteknikk</td>
<td>01.01.1997-31.12.2001</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for elektronikk og telekommunikasjon</td>
<td>01.01.1997-31.12.2001</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for informasjonsteknologi, matematikk og elektronikk</td>
<td>01.01.2002-30.04.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Fakultet for samfunnsvitenskap og teknologiledelse</td>
<td>01.05.1996-22.04.2005</td>
</tr>
<tr>
<td>NTNU</td>
<td>Det historisk-filosofisk fakultet</td>
<td>01.01.1996-22.04.2005</td>
</tr>
</tbody>
</table>

Journalbaser med dokumenter fra NTNU, 05.11.2004-16.04.2013: Deponering testet og godkjent 06.01.2016.

Journal og sakarkiv med elektroniske dokumenter med til sammen 10 arkivdeler fordelt på fellesadministrasjonen, universitetsbiblioteket, Vitenskapsmuseet og 7 fakulteter; SVT, HF, IVT, NT, IME, AB, DMF. Uttrekk fra system ePhorte 5.

::: info Noark-base

Fra og med ePhorte 4 (v2.0.25) brukes en Noark 5-base. NTNU oppgraderte til ePhorte 4 (v2.0.28) i mai 2013, og etter at periodiseringen av saksarkivet var avsluttet. Ved oppgraderingen gikk NTNU over til en Noark 5-støttet base. NTNU benyttet ePhorte 5 (v5.1.3) ved uttrekkstidspunktet. Derfor er data som er periodisert i systemet produsert i en Noark 4-løsning, men overført til i Noark 5-løsning. Uttrekket er i samsvar med Riksarkivarens forskrift kap. VIII og Noark 5-standarden.

:::

## Høgskolen i Sør-Trøndelag

Journalbase med dokumenter, 01.01.2006-30.04.2014: Deponering testet og godkjent 22.03.2018.

Journalbase med dokumenter, 15.02.2014-31.12.2015: Deponering testet og godkjent 01.03.2018.

## Høgskolen i Ålesund

Journalbase med dokumenter, 01.04.2006-15.04.2015: Deponering testet og godkjent 10.01.2017.

Journalbase med dokumenter, 31.03.2014-31.12.2015: Deponering testet og godkjent 05.02.2018.

## Høgskolen i Gjøvik

Journalbase med dokumenter, 01.02.2005-30.05.2014: Deponering testet og godkjent 05.02.2018.

Journalbase med dokumenter, 28.05.2014-31.12.2015: Deponering testet og godkjent 12.03.2018.

---
date: '2021-02-05T00:00:00'
title: Avlevert arkiv
status: Ferdig
category: Faglig
author: Geir Ekle
---

Avlevering betyr å overføre eldre og avsluttet arkiv til et arkivdepot. Et arkivdepot er en enhet hvor bevaringsverdig arkivmateriale oppbevares permanent. Arkivdepotet sikrer lagring av eldre arkivmateriale og legger forholdene til rette for betjening av brukere som er interessert i materialet. NTNU avleverer eldre og avsluttet arkiv til Arkivverket.

Hjemmel for avlevering og arkivdepot finnes i arkivforskriften § 18 og Riksarkivarens forskrift kapittel 5 og 6.

## Avleverte arkiver

### Bergavdelingen

NTH Bergavdelingen 1907-1989

NTH Geologisk institutt 1905-1985

NTH Institutt for gruvedrift 1914-1993

NTH Metallurgisk institutt 1951-1988

NTH Oppredningslaboratoriet 1910-1988

### Bygningsingeniøravdelingen

NTH/NTNU Institutt for veg og jernbanebygging 1935-1999

NTH/NTNU Institutt for samferdelsteknikk 1966-1999

NTH/Fakultet for bygningsingeniørfag 1911-1996

### Kjemiavdelingen

NTH Kjemiavdelingen/Fakultet for kjemi 1910-1990

NTH/UNIT Institutt for marin biokjemi/teknisk biokjemi 1970-1986

NTH Institutt for teknisk biokjemi 1955-1988

NTH Institutt for fysikalsk kjemi 1953-1955

NTH Institutt for bioteknologi 1986-1996

NTH Institutt for kjemiteknikk 1937-1986

NTH Institutt for silikat og høytemperaturkjemi 1912-1989

NTH Institutt for uorganisk kjemi 1910-1990

NTH Institutt for industriell kjemi 1912-1980

NTH Institutt for organisk kjemi 1931-1996

### Maskinavdelingen

NTH/NTNU Maskinavdelingen/Fakultet for maskinteknikk 1911-2002

NTH Institutt for forbrenningsmotorer og marint maskineri/Institutt for marint maskineri 1934-1996 (Maskinavdelingen/Skipstekniskavdeling)

### Almenavdelingen

NTH Almenavdelingen/fakultet for kjemi 1928-1996

NTH Institutt for fysikk 1910-1988

NTH Institutt for teoretisk fysikk-1953-1992

NTH Institutt for teknisk fysikk 1946-1988

NTH Institutt for biofysikk 1960-1993

NTH Institutt for allmenn fysikk 1961-1986

NTH Institutt for røntgenteknikk 1960-1987

NTH Institutt for eksperimentalfysikk 1962 -1989

NTH Matematisk institutt 1972-1985

### Elektroteknisk avdeling

NTH Fakultet for elektro og datateknikk 1903-1995

NTH Institutt for telematikk og datateknikk 1932-1985

### Avdeling for realfag

AVH/NTNU Kjemisk institutt 1961-2001

NTH Fakultet for fysikk og matematikk 1960 -1997

NTH Institutt for anvendt optikk 1961 -1990

NTH Materialprøvingsanstalten 1911-1989

NTH Skipsteknisk avdeling/Marinteknisk avdeling/Fakultet for marin teknikk 1952-1995

Brandtzæg, Anton, professor ved NTH 1932-1952

Vogt, T., professor ved NTH 1869-1982

NTH Granskningskomiteen 1940-1959

NTH Skipsmodelltanken v/professor H. Faanes 1922-1945

NTNU Nettverk for kunst og estetikk 1996 – 2000

NTNU Vitenskapsmuseet – eldre saksarkiv (se sak 2018/12680)

NTNU Fakultet for samfunnsvitenskap og teknologiledelse (SVT) – eldre materiale

Fakultetsadministrasjonen, Geografisk institutt, Institutt for industriell økonomi og teknologiledelse, Institutt for samfunnsøkonomi, Sosialantropologisk institutt, Institutt for sosialt arbeid og helsevitenskap, Pedagogisk institutt og Institutt for sosiologi og statsvitenskap (se sak 2017/8793).

NTNU Det humanistiske fakultet (HF) – eldre materiale

Filosofisk institutt, Religionsvitenskapelig institutt, Ex.phil-senteret, Arkeologisk og religionsvitenskapelig institutt, Institutt for historiske studier (se sak 2017/20948 og 2014/22813).

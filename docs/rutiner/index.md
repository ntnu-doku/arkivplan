---
date: '2021-01-29T00:00:00'
title: Rutiner
status: Ferdig
category: Oversikt
author: Geir Ekle
---

Det er utarbeidet skriftlige rutinebeskrivelser og brukerdokumentasjon for å ivareta arbeidsrutiner i tilknytning til elektronisk saksbehandling og arkivering, samt systemadministrasjon.

Rutineoversikten inneholder blant annet systemuavhengige definisjoner og beskrivelser og har fokus på saksgang og arbeidsflyt. Rutinene skal i all hovedsak ivareta krav til saksbehandling og arkivering, sikkerhet i behandling av opplysninger, samt forsvarlige og effektive arbeidsprosesser.

I tillegg er det utarbeidet tilpasset brukerdokumentasjon for NTNUs sak- og arkivsystem. Dokumentasjonen inneholder beskrivelser av systemets funksjoner og virkemåte med forklaring til skjermbilder og funksjonsmenyer med mer.

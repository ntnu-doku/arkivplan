---
date: '2021-09-20T00:00:00'
title: Registrering
status: Ferdig
category: Rutiner
tags:
  - Standard
author: Ole Vik
---

## Studiedokumentasjon

[Eksamen](/arkivplan/vedlegg/rutiner/registrering/eksamen-v2.1.pdf) (vedlegg)

[Fusk](/arkivplan/vedlegg/rutiner/registrering/fusk-v2.1.0.pdf) (vedlegg)

[Master](/arkivplan/vedlegg/rutiner/registrering/master-v2.0.1.pdf) (vedlegg)

[Opptak](/arkivplan/vedlegg/rutiner/registrering/opptak-v2.0.1.pdf) (vedlegg)

[Politiattest](/arkivplan/vedlegg/rutiner/registrering/politiattest-v2.0.1.pdf) (vedlegg)

[Praksis](/arkivplan/vedlegg/rutiner/registrering/praksis-v2.1.1.pdf) (vedlegg)

[Skikkethet](/arkivplan/vedlegg/rutiner/registrering/skikkethet-v2.0.pdf) (vedlegg)

[Studieforløp](/arkivplan/vedlegg/rutiner/registrering/studieforloep-v2.3.0.pdf) (vedlegg)

[Tilrettelegging](/arkivplan/vedlegg/rutiner/registrering/tilrettelegging-v2.0.2.pdf) (vedlegg)

## Ph.d.-dokumentasjon

[Ph.d-dokumentasjon](/arkivplan/vedlegg/rutiner/registrering/phd.pdf) (vedlegg)

## Personaldokumentasjon

[Administrasjon arbeidsforhold](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-administrasjon-arbeidsforhold.pdf) (vedlegg)

[Arbeidsavtaler - forlengelser – opphør](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-arbeidsavtaler-forlengelser-opphoer.pdf) (vedlegg)

[Sykefravær - HMS - IA](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-sykefravaer-hms-ia.pdf) (vedlegg)

[Opprykk etter kompetanse](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-opprykk-etter-kompetanse.pdf) (vedlegg)

::: info Unntak

Personalsak vedrørende disiplinærreaksjoner, suspensjon, avskjed og konflikter, registreres som egen tematisk sak i personalarkivet. Tittel på tematisk sak tilpasses etter disiplinærforholdet. **Sakene skjermes og unntas offentlighet i sin helhet med tilgangskode U.** Kun saksbehandler som eier saken har da tilgang. For å gi flere lesetilgang til saken opprettes ad hoc tilgangsgruppe.

:::

[Opprette personalmappe](/arkivplan/vedlegg/rutiner/registrering/opprette-personalmappe.pdf) (vedlegg)

[Tilgangskoder og skjerming av informasjon og dokumenter](/arkivplan/vedlegg/rutiner/registrering/veiledning-tilgangsstyring-personalmapper.pdf) (vedlegg)

## Fravær- og refusjonssenteret

Fra oktober 2012 har NTNU et sentralt Fravær- og refusjonssenter ved HR (HR-FRS). Senteret har som oppgave å sørge for oppfølging mot NAV og sikre at NTNU får korrekt grunnlag for refusjonsutbetaling fra NAV.

Saker og dokumenter som skal følges opp av senteret er:

- Foreldrepermisjon og vedtak om foreldrepenger

- Vedtak om pleiepenger og opplæringspenger

- Vedtak om fritak i arbeidsgiverperioden, kronisk syk eller svangerskapsrelatert

Det vil også omfatte vedtak om avslag på disse sakene.

DOKU registrerer aktuelt dokument som inngående ufordelt brev til HR-FRS, med kopi til fakultet, institutt eller enhet.

[Personaldokumentasjon i tilknytning til HR-FRS](/arkivplan/vedlegg/rutiner/registrering/hr-frs.pdf) (vedlegg)

Følgende dokumenter blir ikke registrert i sak- og arkivsystemet:

- Egenmelding: Ansatte registrerer fravær i Paga

- Sykemeldinger: Betraktes som regnskapsbilag. Oppbevares i henhold til regnskapsloven hos HR-FRS

- Det skilles ikke på sykemelding del C og D, begge deler oppbevares like lenge hos HR-FRS

Se [Innsida.NTNU.no/Sykdom](https://innsida.ntnu.no/sykdom) for mer informasjon.

## Rektorvedtak

[Rektorvedtak](/arkivplan/vedlegg/rutiner/registrering/rektorvedtak.pdf) (vedlegg)

[Søke etter og lese rektovedtak](/arkivplan/vedlegg/rutiner/registrering/rutine-for-aa-soeke-etter-og-lese-rektovedtak.pdf) (vedlegg)

## Ansettelse

Under arbeid 2021.

## Varslingsrutine

[Varsling](/arkivplan/vedlegg/rutiner/registrering/varsling-1.2.pdf) (vedlegg)

## Anskaffelse

Under arbeid 2021.

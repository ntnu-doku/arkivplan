---
date: '2021-02-05T00:00:00'
title: 'Skygge- eller kopiarkiv '
status: Ferdig
category: Rutine
author: Geir Ekle
---

Det er DOKU som er ansvarlig for å håndtere papirbasert originalt arkivmateriale. Du som saksbehandler skal kun unntaksvis oppbevare originalt arkivmateriale på kontoret eller arbeidsplassen.

Dersom du har behov for fysiske arbeidskopier, skal disse fortrinnsvis skrives ut fra sak- og arkivsystemet. Du er selv ansvarlig for at slike kopier blir forsvarlig oppbevart og senere makulert så fort behovet for å oppbevare disse lokalt ikke lenger er til stede.

Systematiserte skygge- eller kopiarkiv hos deg som saksbehandler eller ved enhetene må unngås. Kopiarkiv er uheldig fordi det øker risikoen for at journalføringspliktig og eller arkivverdig dokumentasjon ikke når fram til DOKU, samt at det fort kan oppstå tvil om hvorvidt et kopiarkiv i praksis kan inneholde originalt arkivmateriale. Å gjennomgå et kopiarkiv i ettertid for å lokalisere eventuell arkivverdig dokumentasjon er omfattende og ressurskrevende.

Den samme problematikken er til stede der det opprettes filstrukturer hvor man lagrer saksdokumenter i elektronisk versjon. Det er viktig at du er bevisst på at all saksdokumentasjon må vurderes for journalføring og arkivering i sak- og arkivsystemet.

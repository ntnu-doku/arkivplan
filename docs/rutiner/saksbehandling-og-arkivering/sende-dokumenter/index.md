---
date: '2021-02-05T00:00:00'
title: Sende dokumenter
status: Ferdig
category: Rutine
author: Geir Ekle
---

## E-post

Du kan sende utgående brev som e-post – med eller uten vedlegg – fra sak- og arkivsystemet eller fra e-postklienten Outlook der du også kan journalføre og arkivere før sending. Dersom  
e-postkorrespondanse er ført utenfor sak- og arkivsystemet, er det viktig at denne registreres dersom det i ettertid blir åpenbart at korrespondansen er viktig som dokumentasjon og kan betraktes som et saksdokument.

Sending av utgående brev via e-post – enten som en erstatning for eller som et tillegg til brevpost –kan gjøres gitt at NTNUs retningslinjer for bruk av e-post følges.

Les mer om bruk av og sikker e-post på [Innsida.NTNU.no/Sikker e-Post](https://innsida.ntnu.no/wiki/-/wiki/Norsk/sikker+e-post).

::: fare Du skal IKKE

Sende vedtak som er skjermet med hjemmel på e-post.

:::

::: bra Du KAN

Unntaksvis benytte e-post dersom mottaker av ulike årsaker har et særlig behov for å motta informasjon eller dokumenter via e-post.

:::

::: info Merk

Enkeltvedtak skal ikke bare sendes på e-post; hvis det har betydning for mottakerens rettsstilling må det også ha en fysisk kopi.

Det er viktig at du har et bevisst forhold til dette, og det forutsettes at det dreier seg om opplysninger eller dokumenter som ikke er underlagt taushetsplikt, at de kun omhandler mottakers forhold, at mottaker informeres om risikoen ved bruk av e-postkommunikasjon, og samtykker til dette.

:::

eForvaltningsforskriften § 8 har bestemmelser om hvordan enkeltvedtak skal gjøres tilgjengelig for mottaker: «Innholdet i enkeltvedtaket skal gjøres tilgjengelig i egnet informasjonssystem. Dersom parten ber om det, det ikke er hensiktsmessig å kommunisere digitalt med parten via egnet informasjonssystem, det er forsvarlig og annet regelverk ikke er til hinder for dette, kan vedtaket likevel sendes til en elektronisk adresse parten oppgir».

## Sikker elektronisk sending

Du kan sende utgående brev på en sikker måte til privatpersoner via sak- og arkivsystemets modul «EDF - sikker digital post». Forsendelsen går til mottakers digitale postkasse hos Digipost eller eBoks. Dersom mottaker ikke har digital postkasse eller har reservert seg mot digital kommunikasjon, sendes dokumentet automatisk til sentral utskriftstjeneste (Posten) og går som brevpost til mottaker.

Du kan også sende utgående brev på en sikker måte til virksomheter, private og offentlige, via sak- og arkivsystemets modul eFormidling. Forsendelsen går til mottakers postboks i Altinn eller direkte inn til virksomhetens sak- og arkivsystem.

Tjenestene tilbys som følge av den nasjonale satsingen hvor digital kommunikasjon skal være hovedregelen i offentlig forvaltning.

## Brevpost

Du kan sende utgående brev i posten. Det forutsetter at du skriver ut dokumentene lokalt og legger i konvolutt for utsendelse som brevpost. NTNUs posttjeneste henter og bringer post til alle universitetets bygninger i Trondheim. Gjøvik og Ålesund har andre ordninger.

Les mer om NTNUs posttjeneste på [Innsida.NTNU.no/post](https://innsida.ntnu.no/wiki/-/wiki/Norsk/post).

---
date: '2021-02-05T00:00:00'
title: Taushetsplikt
status: Ferdig
category: Rutine
author: Geir Ekle
---

Som ansatt ved NTNU er du underlagt forvaltningsloven, som i § 13 slår fast at «enhver som utfører tjeneste eller arbeid for et forvaltningsorgan, plikter å hindre at andre får adgang til eller kjennskap til det han i forbindelse med tjenesten eller arbeidet får vite om:

- Noens personlige forhold, eller

- Tekniske innretninger og fremgangsmåter samt drifts- eller forretningsforhold som det vil være av konkurransemessig betydning å hemmeligholde av hensyn til den opplysningen angår. (…)»

Som personlige forhold regnes ikke fødested, fødselsdato, personnummer, statsborgerskap, sivilstand, yrke, bopel og arbeidssted, med mindre slike opplysninger røper et klientforhold eller andre forhold som må anses som personlige.

Som personlige forhold kan regnes fysisk og psykisk helse, en persons karakter, følelsesliv og holdninger til politiske og religiøse spørsmål, familiære forhold, samt økonomiske forhold.

Denne taushetsplikten gjelder selv om man ikke har undertegnet en særskilt taushetserklæring, og gjelder også etter at man har sluttet i jobben. Taushetsplikten faller normalt bort etter 60 år dersom ikke særlovgivning fastsetter forlenget taushetsplikt.

Taushetsplikten er imidlertid forenklet sett ikke til hinder for at opplysninger kan gjøres kjent etter samtykke, til part, som anonymisert statistikk, for bruk til det aktuelle formål, til andre tjenestemenn innenfor organet som har et tjenstlig behov – for eksempel saksbehandlere og dokumentasjonsforvaltning, der man har lovhjemmel for å utlevere til andre forvaltningsorgan eller for bruk i forskning.

---
date: '2021-02-05T00:00:00'
title: Personopplysninger
status: Ferdig
category: Rutine
author: Geir Ekle
---

Opplysninger – som metadata eller som en del av et dokument – i et sak- og arkivsystem eller fagsystem faller ofte inn under minst én av tre kategorier: Personopplysninger, sensitive personopplysninger og eller taushetsbelagte opplysninger. En personopplysning kan være en ren personopplysning, som navn eller adresse.

En personopplysning kan være sensitiv, som en persons tilknytning til en fagforening eller seksuelle legning. En personopplysning kan dessuten være taushetsbelagt, som en persons sykdomsdiagnose eller klientforhold med fengselsvesenet. I en særstilling kommer personnummer, som hverken er definert som en sensitiv eller taushetsbelagt opplysning, men som det er gitt særlige regler for bruken av.

Det er primært personopplysningsloven og offentlighetsloven med forskrifter som regulerer bruk og behandling av personopplysninger og sensitive personopplysninger, mens det primært er forvaltningsloven og offentlighetsloven med forskrifter som regulerer bruk og behandling av taushetsbelagte opplysninger.

Offentlighetslovens hjemmel om taushetsplikt kan ikke benyttes alene, den må følges av en henvisning til enten forvaltningsloven eller annen særlovgivning – for eksempel helsepersonelloven, barnevernsloven eller andre. Det finnes også enkelte andre lover som i seg selv pålegger taushetsplikt, som sikkerhetsloven og beskyttelsesinstruksen.

Les mer om personopplysninger og personvern på [NTNU.no/personvern](https://www.ntnu.no/personvern) og [Datatilsynet.no](https://www.datatilsynet.no/).

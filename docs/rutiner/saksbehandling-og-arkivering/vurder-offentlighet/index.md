---
date: '2021-02-05T00:00:00'
title: Vurder offentlighet
status: Ferdig
category: Rutine
author: Geir Ekle
---

NTNUs saksdokumenter offentlige så langt det ikke er gjort unntak i lov eller i medhold av lov. Du må gjøre deg kjent med offentlighetsloven og tilhørende forskrift og hvordan regelverket skal praktiseres.

Du som saksbehandler er ansvarlig for at eventuell tilgangskode og hjemmel blir registrert i tilknytning til dokumentet. Offentlighetsvurderingen kan gjøres sammen med leder. DOKU kan bistå i vurderingen. NTNU praktiserer meroffentlighet, jamfør offentlighetsloven § 11. Det vil si at når noen ber om innsyn i et dokument, skal det vurderes om dokumentets innhold bør kunne gjøres kjent helt eller delvis, selv om det etter offentlighetsloven kan eller skal unntas fra offentlighet.

Ved intern korrespondanse mellom enheter ved NTNU benyttes som hovedregel N-notater. Dokumenter som blir utvekslet mellom fakulteter eller sendt fra administrasjonen til et kollegialt organ kan ikke unntas offentlighet etter offentlighetsloven § 14 om organinterne dokumenter, jamfør forskrift til offentlighetsloven § 8.

Tilgangskoder og hjemmel styrer hvilke interne brukere av sak- og arkivsystemet som får lese de elektroniske dokumentene. Følgende tilgangskoder benyttes:

- F: Forskning og fagsaker

- FS: Studiesaker – herunder karakteropplysninger

- H: Helse, miljø og sikkerhet

- K: Kompendier

- L: Ledelse

- PM: Personalmappe - saksnivå

- P: Personalsaker

- PX: Personalsaker, særlig sensitive personopplysninger

- S: Studentsaker

- SX: Student- og ph.d-saker, særlig sensitive personopplysninger

- SO: Studentombudets saker

- U: Unntatt offentlighet

- Ø: Økonomisaker

- XX: Midlertidig sperret – i påvente av offentlighetsvurdering

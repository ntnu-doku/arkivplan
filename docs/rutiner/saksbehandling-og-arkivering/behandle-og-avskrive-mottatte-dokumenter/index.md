---
date: '2021-01-18T00:00:00'
title: Behandle og avskrive mottatte dokumenter
status: Ferdig
category: Rutine
tags:
  - Behandle
author: Geir Ekle
---

Du som saksbehandler mottar inngående (I) og interne (N) saksdokumenter som journalposter via det forhåndsdefinerte søket *Ubesvart post*.

::: advarsel Restanseflagg

Du vil se et rødt restanseflagg på journalposter som du har hovedansvaret for å behandle. Journalpostene i dette søket utgjør din restanseliste.

:::

::: info Kontrollering

Du må kontrollere at alle dokumenter i en inngående journalpost du har behandlingsansvaret for, er lesbare og tilsynelatende komplette.

:::

Før du som saksbehandler avskriver en restanse direkte skal du forsikre seg om at dette er en henvendelse du skal ha behandlingsansvaret for. Dersom du skal gi skriftlig svar på en restanse skal du som hovedregel bruke funksjonen besvar/svar – med utgående post, enkel e-post eller notat.

::: info Husk!

- Avskrivingsinformasjon – å knytte henvendelse og eventuelt svar sammen – er et lovpålagt krav jamfør Arkivforskriften § 10.

- Dersom du får fordelt en journalpost som du ikke har ansvar for, informer så raskt som mulig leder/postfordeler – eventuelt DOKU – om at journalposten må omfordeles

:::

::: fare Men!

Du skal da *ikke* avskrive restansen, for avskrevne restanser gjenopprettes ikke nødvendigvis for ny saksbehandler ved omfordeling.

:::

Dersom du ikke kan gi et endelig svar/vedtak innen rimelig tid, og senest innen én måned, skal du gi et midlertidig svar jamfør forvaltningsloven § 11a. Midlertidige svar skal også registreres i sak- og arkivsystemet – avskrivingsmåte endres fra «BU - Besvart med utgående brev» til «/*/*/* - Midlertidig svar sendt».

[Kontakt DOKU](https://www.ntnu.no/adm/auv/ansatte-og-tjenester) for mer informasjon om du er usikker.

X-notater og kopier ligger som egne forhåndsdefinerte søk, og du må regelmessig sjekke disse søkene for ny informasjon.

Er det gitt et muntlig svar, bør dette noteres i merknadsfeltet ved avskriving. Eventuelt må du vurdere om det muntlige svaret var såpass viktig for å forstå saken at det skal nedtegnes og arkiveres i henhold til skriftlighetsprinsippet i forvaltningsloven § 11 d.

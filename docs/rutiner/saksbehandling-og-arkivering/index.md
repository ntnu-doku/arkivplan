---
date: '2021-02-04T00:00:00'
title: Saksbehandling og arkivering
status: Ferdig
category: Rutiner
tags:
  - Behandle
  - Arkivere
  - Ansvar
author: Geir Ekle
---

De alminnelige reglene om offentlig saksbehandling består av veiledningsplikt, plikt til å avgjøre saken uten ugrunnet opphold og taushetsplikt, jamfør forvaltningsloven kapittel 3.

Sak- og arkivsystemet skal understøtte NTNUs saksbehandlingsprosesser og dokumentere virksomhetens prosesser og dokumenter.

Dokumenter regnes som saksdokumenter for NTNU, dersom de er gjenstand for saksbehandling eller har verdi som dokumentasjon, jamfør arkivforskriften § 2-6.  
Saksdokumenter journalføres og arkiveres.

Følgende spørsmål kan hjelpe deg med å avklare hva som regnes som saksdokumenter. Er svaret ja på et eller flere av disse spørsmålene, journalfør og arkiver dokument i sak- og arkivsystemet.

1.  Er dokumentet sendt oss for at vi skal fatte vedtak eller beslutning i en sak?

2.  Er dokumentet viktig for å forstå beslutningsgrunnlaget i en sak?

3.  Inneholder dokumentet opplysninger som dokumenterer handlinger utført av NTNU?

4.  Inneholder dokumentet informasjon som kan gjenbrukes i andre saker?

5.  Er dokumentet viktig for at NTNU kan dokumentere sine vedtak og tiltak i ettertid?

6.  Er dokumentet viktig med tanke på at offentligheten skal kunne se viktig dokumentasjon om NTNUs planer og virksomhet?

7.  Er dokumentet viktig for at forvaltningens historie kan skrives i fremtiden?

8.  Ivaretar det rettsikkerheten for studenter og ansatte?

Se også [Funksjonsanalyse – prosesser, aktiviteter, transaksjoner, saksdokumenter](/arkivplan/vedlegg/rutiner/saksdokumenter/funksjonsanalyse.pdf) (vedlegg).

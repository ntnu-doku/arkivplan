---
date: '2021-02-05T00:00:00'
title: Intern korrespondanse
status: Ferdig
category: Rutine
author: Geir Ekle
---

Intern korrespondanse er korrespondanse mellom NTNUs administrative enheter, eller innad i den enkelte enhet. For den interne korrespondansen benyttes interne notat.

## N-notat

Bruk N-notat for dokumenter hvor mottaker skal tildeles oppfølgingsansvar, eller hvor det er viktig at mottaker blir gjort oppmerksom på og aktivt må ta stilling til at dokumentet er mottatt. Mottak utløser krav om oppfølging – restanse – og avskriving.

Avskriving betyr å svare på henvendelsen med et nytt dokument eller å ta henvendelsen for eksempel til orientering eller etterretning.

## X-notat

Bruk X-notat for dokumenter som kun er til informasjon for mottaker, eller for dokumenter som skal journalføres og arkiveres uten mottaker. Mottak utløser ikke krav om oppfølging – restanse – og avskriving. Denne dokumenttypen brukes typisk for dokumenter som møteinnkallinger, møtereferater, saksnotat som inngår i møtebøker til styrer, råd, utvalg og lignende.

Ved bruk av X-notat får avsender ingen bekreftelse på at mottaker har lest notatet, slik man får ved bruk av N-notat.

## S-saksframlegg

Bruk S-saksframlegg for å opprette saksframlegg til styre, råd eller utvalg som benytter møte- og utvalgsmodulen i sak- og arkivsystemet. Saksbehandler sender saksframlegg til utvalgssekretæren i det aktuelle styre, råd eller utvalg.

::: info Y-dokument

Merk at dokumenttypen Y-dokument – mappedokument – ikke skal benyttes.

:::

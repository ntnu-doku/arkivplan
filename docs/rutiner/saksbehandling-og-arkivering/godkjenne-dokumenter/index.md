---
date: '2021-02-05T00:00:00'
title: Godkjenne dokumenter
status: Ferdig
category: Rutine
author: Geir Ekle
---

I stedet for fysisk signering av dokumenter kan du bruke elektronisk godkjenning av dokumenter før sending. Elektronisk godkjenning er en egen arbeidsflyt som er satt opp i sak- og arkivsystemet.

Det er tre nivåer for elektronisk godkjenning:

**Selvgodkjenning**  
Du sender dokumenter til mottaker(e) uten å ha fått dokumentet godkjent av andre.

**Enkel godkjenning**  
Én leder godkjenner dokumentet før du sender til mottakere(e).

**Sekvensiell godkjenning**  
To eller flere, hvorav minst én leder, godkjenner dokumentet før du sender til mottaker(e).

Når et utgående brev eller internt notat blir elektronisk godkjent, loggfører systemet at dokumentet er godkjent av de(n) som tidligere normalt sett ville ha signert og parafert – medsignert – fysisk. Hvem som står som avsender på notater og utgående brev varierer fra sakstype til sakstype, og du som saksbehandler skal ha oversikt over hva som gjelder for de enkelte sakstypene. Det er dokumentets innhold og dine fullmakter som saksbehandler som avgjør på hvilket nivå dokumentet skal godkjennes.

Utgående dokumenter og interne notater skal som hovedregel produseres direkte i sak- arkivsystemet. Det skal ikke være nødvendig å skanne inn utgående signerte brev til sak- arkivsystemet.

Standardteksten «I samsvar med fullmakt er dette dokumentet godkjent elektronisk og har derfor ingen fysisk signatur» er tilknyttet utgående brevmal for bruk ved elektronisk godkjenning. Du som saksbehandler er ansvarlig for å sende et brev eller notat på godkjenningsrunde til de(n) som skal signere og eventuelt parafere. Når brevet eller notatet er godkjent, skal du fullføre godkjenningsrunden, ferdigstille og sende utgående dokument. Interne notater sendes automatisk til mottaker(e) ved ferdigstillelse. Det samme gjelder for interne kopimottakere på utgående dokument.

På enkelte utgående brev kan du velge å påføre en signatur. Dette kan gjelde dokumenter som omhandler tilbud om ansettelse, fratreden/oppsigelse, eller tildeling av ph.d.-grad, bekreftelser, og dokumenter som sendes utenlandske læresteder som ikke aksepterer brev uten fysisk signatur. I de tilfeller der du ønsker å påføre utgående brev en fysisk signatur kan standardteksten om elektronisk godkjenning i brevmalen fjernes. Merk at dokumentet likevel skal sendes på godkjenningsrunde.

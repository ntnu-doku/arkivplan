---
date: '2021-02-05T00:00:00'
title: Ferdigstille dokumenter
status: Ferdig
category: Rutine
author: Geir Ekle
---

Reserverte journalposter er dokumenter du har opprettet og fortsatt arbeider med, som har status R (Reservert). Disse journalpostene finner du i søket Under arbeid.

Du har ansvar for at en journalpost med dokumenter endrer status fra R til F (Ferdigstilt), og at du sender dokumentene til mottaker(e).

::: fare OBS!

Ikke sett uferdige dokumenter i status F, da disse verken kan eller skal endres på etter dette.

:::

::: info Sjekkliste for ferdigstilling

Når du ferdigstiller så sjekker du at:

- Mottakers navn og adresse er identisk på brev og i journalpost

- Alle vedlegg følger brevet

- Dokumentet lar seg åpne og er lesbart

- Dokumentet inneholder den teksten det er ment å inneholde

- Spor etter redigering i dokumentet – synlige og usynlige – er fjernet

- At godkjenningsrunde er gjennomført der dette er aktuelt

:::

DOKU kontrollerer ferdigstilt journalpost og journalfører til status J (Journalført) for konvertering av dokumenter til godkjent arkivformat og endelig arkivering. Dette inkluderer låsing av metadata og dokument for endringer.

N- og X-notater, saksframlegg, samt eventuelle kopier til interne mottakere, blir automatisk sendt via sak- og arkivsystemet ved ferdigstillelse. Du behøver ikke skrive ut interne notater for å sende disse i papir til mottaker(e).

---
date: '2020-12-08T00:00:00'
title: Formkrav ved klagesaker
status: Ferdig
category: Rutine
author: Geir Ekle
---

Forvaltningsloven § 32 har bestemmelser om klagens adressat, form og innhold. En erklæring om klage skal «(…) b) være undertegnet av klageren eller hans fullmektig eller være autentisert som fastsatt i forskrift, eller i medhold av forskrift, jamfør § 15 a (…)».

E-post har ingen sikker autentisering, og i klagesaker er det altså et skjerpet krav til autentisering - dermed er en tradisjonell e-post ikke akseptert. En signert klage som er skannet inn og sendt på e-post som PDF vil imidlertid være å betrakte som godkjent.

Dette innebærer:

- At det ved gruppeklager ikke er tilstrekkelig at hver enkelt sender en e-post - det er et krav om ett eller ev. flere dokumenter som er underskrevet av alle – og signaturen kan ikke stå fritt, det må tydelig framgå at det er klagen som signeres

- At fristen anses å være overholdt hvis en har sendt inn klage innen fristen selv om vedkommende brukte e-post, men at klagen avvises dersom signaturen(e) ikke er kommet inn innen en gitt frist

Klager skal også varsles om signaturkravet, samt om hvilke andre formaliteter som gjelder. Det er for øvrig i utgangspunktet klagers ansvar å sørge for at klagen blir fremsatt på en slik måte at kravene til forvaltningsloven er oppfylt. En klager som ønsker å klage må da sørge for å ta kontakt med eventuelt andre klagere ved gruppeklage for å høre om de er av samme oppfatning. Det er altså ikke du som saksbehandler som skal gjøre dette, men du har veiledningsplikt.

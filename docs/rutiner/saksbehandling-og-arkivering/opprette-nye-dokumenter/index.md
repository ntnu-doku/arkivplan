---
date: '2021-02-05T00:00:00'
title: Opprette nye dokumenter
status: Ferdig
category: Rutine
author: Geir Ekle
---

Dersom du skal begynne på en henvendelse oppretter du en ny journalpost. Om du velger ny utgående, ny utgående e-post, N-notat, X-notat eller S-saksframlegg, avhenger av hvem henvendelsen er rettet til. Journalposter som er utgående benyttes for eksterne henvendelser, mens notat og saksframlegg benyttes for interne henvendelser.

Notater, saksframlegg og utgående brev skal du som hovedregel produsere direkte i sak- og arkivsystemet slik at riktig mal og metadata – blant annet saksnummer – påføres hoveddokumentet. Du velger selv om du vil skrive innholdet direkte inn i malen som du henter fra systemet, eller om du vil lage et utkast utenfor systemet og deretter klippe ut og lime tekst inn i dokument tilknyttet en journalpost i sak- og arkivsystemet.

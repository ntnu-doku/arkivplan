---
date: '2021-02-05T00:00:00'
title: 'Skriveregler, standardtitler og maler'
status: Ferdig
category: Rutine
author: Geir Ekle
---

Sakstitler, journalpostinnhold og dokumenttittel skal inneholde sentrale og meningsbærende beskrivelser slik at skal være mulig å søke opp akkurat denne saken, journalpost eller dokument. Av søke- og statistikkhensyn skal språket være standardisert på bokmål eller nynorsk[^1], men typiske faguttrykk og titler på originalspråket, for eksempel engelsk, kan skrives i tillegg. På mange ensartede saks- og dokumenttyper er det utarbeidet standardtitler.

::: info Merk

Du skal i svært liten grad benytte forkortelser og tegnsetting, da dette kan vanskeliggjøre søk, statistikk og rapporter. Som hovedregel benyttes kun [tankestrek](https://www.sprakradet.no/sprakhjelp/Skriveregler/tegn/Tankestrek/) som tegnsetting, og det er normalt sett kun godt innarbeidede forkortelser som skal benyttes, som NTNU eller enhetsforkortelser ved NTNU.

:::

Der det finnes standard dokumentmaler med predefinert innhold i sak- og arkivsystemet, skal disse i mest mulig grad benyttes. Dette gjelder blant annet brevmaler – med eller uten tekst for elektronisk godkjenning, notater, arbeidsavtaler og klagebehandling ved sensur.

[^1]: Se [Generelle rutiner for saksbehandling og arkiv ved innføring av elektronisk sak- og arkivsystem ePhorte ved NTNU](/arkivplan/vedlegg/arkivoversikt/arkivnoekler/tidligere-rutiner-ved-innfoering-sak-og-arkiv.pdf). Spesifikt, fra side syv: «Av søkehensyn skal språket skal være standardisert på bokmål, men typiske faguttrykk og titler på engelsk(eller andre språk) kan skrives i tillegg. Vær bevisst i bruken av forkortelser.»

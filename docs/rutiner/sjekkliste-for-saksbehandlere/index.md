---
date: '2021-08-05T00:00:00'
title: Sjekkliste for saksbehandlere
status: Ferdig
category: Rutine
tags:
  - Dokument
  - Ansvar
  - Standard
author: Ole Vik
---

## Daglig

- [Logge inn i ePhorte](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+P%C3%A5logging)

- [Se etter nye saker eller dokumenter fordelt til deg](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+S%C3%B8kemuligheter)

- Registrere dokumenter du har mottatt direkte, for eksempel [via epost](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Eksportere+og+arkivere+e-post+i+ePhorte+Outlook) eller [via import](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ephorte+-+importere+filer+fra+disk)

- [Avskrive restanser](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Besvare+og+avskrive+restanse)

- [Ferdigstille dokumenter: Sette status til F - Ferdigstilt](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Ferdigstille+journalpost)

## Ukentlig

- Påse at det ikke ligger uferdig arbeid ifra uken i ePhorte

- [Ferdigstille](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Ferdigstille+journalpost)

- [Avskrive restanser](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Besvare+og+avskrive+restanse)

##  Ved registrering

- Kontroller at all informasjon er korrekt og komplett

- Sjekk at dokumentet lar seg åpne og at innholdet er leselig

- [Vurder om dokumentet og/eller tittel på journalpost skal skjermes eller være åpen for offentligheten](/arkivplan/rutiner/saksbehandling-og-arkivering/vurder-offentlighet)

- [Ved skjerming, påfør rett hjemmel og eventuelt skjerme rett mengde tekst i journalposttittel/sakstittel](/arkivplan/rutiner/registrering)

## Ved avslutning av arbeidsforhold

- Samme som ukentlig

- Ikke la [journalposter](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Ferdigstille+journalpost)/[dokumenter](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Ferdigstille+notat+eller+brev) bli liggende under arbeid

- [Avskriv alle restanser](https://innsida.ntnu.no/wiki/-/wiki/Norsk/ePhorte+-+Besvare+og+avskrive+restanse)

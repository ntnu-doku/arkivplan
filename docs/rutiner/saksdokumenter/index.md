---
date: '2021-02-04T00:00:00'
title: Saksdokumenter
status: Ferdig
category: Faglig
tags:
  - Begrepsavklaring
  - Definisjon
author: Geir Ekle
---

## Dokumentfangst av saksdokumenter

NTNU er lovpålagt å journalføre og arkivere sine saksdokumenter. For å kunne oppfylle dette kravet må DOKU bedrive aktiv dokumentfangst. En forutsetning for dette er å identifisere områder der arkivverdige dokumentasjon skapes. DOKU er avhengig av hjelp fra hele organisasjonen for å gjøre dette arbeidet, og den enkelte medarbeider hos DOKU må ha god kjennskap til organisasjonsstrukturen, virksomheten, ansvarsfordelingen og dokumentflyten.

## Identifikasjon av saksdokumenter

NTNU har i samarbeid med andre institusjoner i universitets- og høyskolesektoren (UH) gjennomført en funksjonsanalyse som beskriver virksomhetens oppgaver som skaper saksdokumenter. I funksjonsanalysen fra 2020/21 er hovedfunksjoner med underfunksjoner identifisert. En funksjon blir igjen delt opp i aktiviteter. Aktiviteten deles opp i transaksjoner, og det er selve transaksjonene som skaper saks- og arkivdokumenter.

[Funksjonsanalyse – prosesser, aktiviteter, transaksjoner, saksdokumenter](/arkivplan/vedlegg/rutiner/saksdokumenter/funksjonsanalyse.pdf) (vedlegg).

## Organisering av saksdokumenter

### Saksmappe

En saksmappe er en overordnet betegnelse for en eller flere journalposter med dokumenter innenfor samme tema. Saksmappen er registrert med opplysninger (metadata) som er felles for alle journalpostene med dokumenter i saksmappen. En sak kan ferdigstilles og avsluttes når alle journalpostene er behandlet og man ikke forventer at det vil komme nye tilførsler av dokumenter. Saksbehandler har anledning til å merke en sak som status ferdig, men det er DOKU som foretar selve avsluttingen – status avsluttet – etter gjeldende rutiner.

### Journalpost

En journalpost inneholder alltid et hoveddokument. I tillegg kan den også inneholde ett eller flere vedlegg til hoveddokumentet. I journalposten registreres opplysninger (metadata) for å forstå dokumentets innhold og opplysninger som er nødvendige for å knytte journalposten til den overordnede saksmappen, for eksempel journalpostnummer.

### Dokument

Et dokument er en avgrenset informasjonsmengde som kan være lagret fysisk eller digitalt, typisk på papir, i et elektronisk system eller andre medier som brev, notater, regneark, SMS, MMS, foto eller video. I sak- og arkivsystemet vil et hvert dokument alltid være tilknyttet minst én journalpost, enten som et hoveddokument eller som et vedlegg.

Det finnes fem hoveddokumenttyper i sak - og arkivsystemet, som alle kan ha tilknyttede vedlegg:

- I - Inngående

- U - Utgående

- N - Internt notat med restanse

- X - Internt notat uten restanse

- S - Saksframlegg

## Arkivrutiner for å sikre dokumentfangst og kvalitetssikring

Kvaliteten på NTNUs arkiv- og dokumentasjonsforvaltning er direkte koblet til kvaliteten på rutinene og DOKUs muligheter for å utføre utviklingsarbeid. Arkivrutiner er ikke statiske. Ved å revidere og oppdatere eksisterende rutiner jevnlig, og utvikle nye rutiner ved behov, bidrar DOKU til at NTNU har en effektiv og forskriftsmessig arkiv- og dokumentasjonsforvaltning. Det er viktig at arkivrutiner utvikles i forkant av organisatoriske endringer, systemutviklinger og virksomhetsutvidelser, og at DOKU involveres tidlig i prosessen.

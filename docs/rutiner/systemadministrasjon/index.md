---
date: '2021-02-05T00:00:00'
title: Systemadministrasjon
status: Ferdig
category: Rutiner
tags:
  - Struktur
author: Geir E. W. Ekle
---

NTNU har et fullelektronisk sak- og arkivsystem. Systemet tar imot, produserer og arkiverer sak- og arkivdokumenter elektronisk. Arkivloven, arkivforskriften og Riksarkivarens forskrift hjemler bruk av arkivsystem for elektroniske sak- og arkivdokumenter. DOKU er systemansvarlig.

Ansvarsforhold og rutiner som nærmere beskrevet nedenfor er med bakgrunn i kravene i Riksarkivarens forskrift kapittel 3, § 3-2.

## Konfigurasjon og oppsett

Konfigurasjonsdokumentet for sak- og arkivsystemet inneholder registreringene for systemadministratormodulen. Det er viktig å holde dette adskilt fra endringer i konfigurasjonsfilen og andre endringer i servere og databaser. Endringer i systemadministratormodulen vil kunne påvirke brukervennligheten. Endringer skal av den grunn testes i testmiljøet før endringer utføres i produksjon.

Konfigurasjonsdokumentet er registrert i sak 2011/16396.

## Brukertilgang

Ledere melder inn ansattes behov for brukertilgang til DOKU. Ansatt får tildelt brukeridentitet med en rolle som er tilknyttet en administrativ enhet. Det er utarbeidet eget meldeskjema som sendes til DOKU. Rolle og rettigheter gis på bakgrunn av opplysninger i mottatt meldeskjema.

Brukertilgang gis for den perioden den ansatte har arbeidsoppgaver som gir rett til tilgangen. Opphør av behovet må meldes til systemansvarlig. Dersom det skjer endringer etter at tildelt rolle og rettigheter er utført, må dette meldes for korrigering av rolle eller tilganger. Tildelt brukeridentitet er personlig, og passord skal aldri overdras til andre – heller ikke IT-personell og brukerstøtte, i samsvar med NTNUs IT-reglement.

DOKU er ansvarlig for å opprette, autorisere og avslutte brukere etter anvisning fra ansvarlig innmelder. Systemansvarlig har ansvar for at dette blir vedlikeholdt kontinuerlig, og skal ikke gjøre endringer i brukerrettighetene uten skriftlig beskjed. Vedlikehold av brukerrettigheter er grunnleggende for sikkerheten i sak- og arkivsystemet.

## Rettigheter

Tilgangskoder i kombinasjon med lovhjemmel benyttes for å skjerme tekst og informasjon fra å komme på offentlig journal, og til å begrense hva brukere får lov til å lese og søke etter internt i sak- og arkivsystemet. Brukere får automatisk tildelt et standard sett med tilgangskoder. Tilgangskoder utover standard tildeling er bestemt ut fra innmeldt behov.

Når bruker autoriseres med tilgangskoder for egne saksmapper, journalposter og dokumenter:

- Bruker får leserettigheter til de saksmapper og de journalposter og dokumenter som hen er ansvarlig for eller intern mottaker av. Bruker vil også få leserettigheter til journalposten og dokumentet hvis vedkommende er mottaker av en oppgave som er tilknyttet journalposten og dokumentet.

Når bruker autoriseres med tilgangskoder for administrativ enhet:

- Bruker vil få leserettigheter til de saksmapper og journalposter og dokumenter der saksansvarlig eller saksbehandler tilhører den enheten autorisasjonen er gitt, inkludert underliggende enheter.

[Rettigheter basert på rolle](/arkivplan/vedlegg/rutiner/systemadministrasjon/rettigheter-basert-paa-rolle.pdf) (vedlegg)

## Tilgangskoder

- DX: FoU – særlig sensitive personopplysninger

- F: Forskning, Fagsaker

- FS: Studiesaker

- H: Helse, miljø og sikkerhet

- K: Kompendier

- L: Ledelse

- MU: Møte- og utvalgsdokument

- PM: Personalmappe - saksmappenivå

- P: Personalsaker

- PX: Personalsaker, særlig sensitive personopplysninger

- S: Student- og ph.d.-saker

- SB: Sikkerhet og beredskap

- SO: Unntatt offentlighet studentombudet

- SX: Student- og ph.d-saker, særlig sensitive personopplysninger

- U: Unntatt offentlighet

- Ø: Økonomisaker

- XX: Midlertidig sperret (i påvente av offentlighetsvurdering)

## Konvertering til godkjent filformat

I sak- og arkivsystemet skal alle elektroniske dokumenter lagres i godkjente filformater. Filformater har blant annet betydning for senere avlevering og deponering til Arkivverket. Godkjente filformater er til enhver tid beskrevet i Riksarkivarens forskrift kapittel 5, del IV, § 5-17.

Sak- og arkivdokumenter omgjøres av en egen PDF-konverter tilknyttet sak- og arkivsystemet til filformatet PDF/A. Konverteringen skjer når journalposten settes til status J – journalført – av DOKU. PDF-konverteren benyttes til å konvertere filformat som brukes til produksjon av tekstdokumenter, regneark, lysbilder og e-post som DOC, DOCX, ODT, XLS, XLSX, PPT, PPTX, HTML, MSG. Dokumenter som er tilknyttet og arkivert i arkivformat blir ikke konvertert.

Kontroll av om konverteringen har vært vellykket blir fanget opp ved kvalitetssikringen utført av DOKU ved journalføring, offentlig journal og rapport om dokumenter journalført – status J og dokumentformat.

## Autentisering og signering

For autentisering og signering av dokumenter se siden [Saksbehandling og arkivering](/arkivplan/rutiner/saksbehandling-og-arkivering), under Godkjenne dokument. Digital signatur benyttes ikke i sak- og arkivsystemet. Digitalt signerte dokumenter mottas fra tjenesten eSignering via integrasjonsoppsettet.

## Kvalitetssikring av registrering og arkivering

Saksbehandler som skriver og ferdigstiller dokumenter har ansvar for at dette blir registrert på en korrekt måte, se siden [Saksbehandling og arkivering](/arkivplan/rutiner/saksbehandling-og-arkivering). DOKU har ansvaret for å kvalitetssikre alle registreringer som blir gjort i journalen.

DOKU er ansvarlig for at inngående saksdokumenter på papir skannes. Programmet PixEdit benyttes av DOKU som skanneprogram, og programmet utfører automatisk etterbehandling av skannede dokumenter i henhold til konfigurasjon. Følgende kontrolleres etter utført skanning:

- At alle sider er kommet med på hvert dokument

- At skanningen er god nok kvalitetsmessig

## Teknisk drift og vedlikehold

Ansvaret for teknisk drift og vedlikehold av systemet ligger hos IT-drift. Det er inngått egen avtale mellom IT-drift, IT-forvaltning og DOKU om drift og forvaltning av sak- arkivsystemet. I henhold til avtale skal IT-enhetene blant annet:

- Ha rutiner for sikkerhetskopi av data

- Gjennomføre nødvendig sikkerhetskopiering

- Godkjente arkivformater benyttes i arkiveringen – konvertering til arkivformat og løsningen for dette

- Tilstrekkelige lagringsmedium skal benyttes

## Mottak og sending

NTNU har et sentralt e-postmottak. DOKU åpner mottatte e-poster og har ansvaret for å journalføre arkivverdige saksdokumenter. Når saksbehandler mottar e-post direkte – utenom sentralt e-postmottak – må saksbehandler vurdere om e-posten er arkivverdig og skal journalføres. Dersom e-posten er arkivverdig, skal e-posten enten journalføres av saksbehandler selv eller videresendes til DOKU for journalføring.

Papirpost til NTNU leveres til DOKU av intern posttjeneste på campus Trondheim, ISS Facility Services AS på campus Gjøvik og Posten på campus Ålesund.

Mottak av dokumenter skjer også via systemintegrasjoner og robotteknologi til sak- arkivsystemet, samt via Altinn.

Se siden [Integrasjoner](/arkivplan/automatisering/integrasjoner) for mer informasjon om automatisering, samt [Saksbehandling og arkivering](/arkivplan/rutiner/saksbehandling-og-arkivering) for ekspedering av dokumenter.

## Fordeling av dokumenter

Som hovedregel fordeler DOKU innkommende dokumenter til administrativ enhet. Leder ved administrativ eller den som har fått denne oppgaven delegert, skal sørge for at inngående dokumenter og interne notater fortløpende blir fordelt til saksbehandler. Se siden [Ansvar og fullmakter](/arkivplan/organisering-og-ansvar/ansvar-og-fullmakter) for informasjon om leders arkivansvar.

## Rettelser i journalen

Uriktige journal- og arkivopplysninger skal rettes opp snarest mulig. Saksbehandler kan rette eventuelle feil der hen er saksansvarlig. Oppdages det feil som ikke lar seg rette ut fra tilganger, rettigheter eller status, skal DOKU kontaktes. DOKU kan bistå med å rette feil i registrerte opplysninger i saker og journalposter.

Se siden [Saksbehandling og arkivering](/arkivplan/rutiner/saksbehandling-og-arkivering) for mer periodiske rutiner.

## Avskrivning og ferdigstilling

Saksbehandler har ansvaret for å avskrive alle dokumenter i en sak. Enten svaret er via brev, e-post, telefon eller at dokumentet tas til etterretning. Se siden [Saksbehandling og arkivering](/arkivplan/rutiner/saksbehandling-og-arkivering) for mer om behandling og avskriving av mottatt dokument. DOKU sørger for oppfølging og kvalitetssikring av avskrivninger.

## Offentlighet

Se siden [Saksbehandling og arkivering](/arkivplan/rutiner/saksbehandling-og-arkivering) for mer om vurdering av offentlighet.

## Driftsproblemer

DOKU har utarbeidet varslingsrutiner til IT-drift og brukere hvis sak- arkivsystemet er ute av drift eller har driftsforstyrrelser. Varslingsrutinene er publisert som eget dokument, Driftsstans og eller tekniske problemer, på NTNUs Intranettsider for DOKU.

Hvis sak- arkivsystemet er ute av drift utsettes journalføring til systemet er tilgjengelig igjen.  
Skannemodulen (PixEdit-løsningen) vil i de fleste tilfeller være tilgjengelig. Dokumenter kan skannes og formidles som elektroniske kopier til saksbehandlere. E-post til postmottak kan videreformidles til saksbehandlere. Dokumenter som produseres i tekstbehandlersystem må lagres lokalt inntil systemet er i drift for videre journalføring og arkivering.

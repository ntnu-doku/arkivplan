---
date: '2021-07-30T00:00:00'
title: Beredskap og sikkerhet i tilknytning til arkiv
status: Ferdig
category: Regelverk
tags:
  - Rutine
  - Krise
author: Geir Ekle
---

## Avvik

Avvik meldes i avvikssystemet Risk Manager.

Les mer om [Melde HMS-avvik](https://innsida.ntnu.no/wiki/-/wiki/Norsk/melde+hms-avvik) og [Melde avvik om informasjonssikkerhet](https://innsida.ntnu.no/wiki/-/wiki/Norsk/informasjonssikkerhet+-+avvik) på NTNUs nettsider.

## Elektronisk arkiv

Det er inngått egen driftsavtale mellom IT-drift og IT-forvaltning og DOKU for drift av sak- og arkivsystemet. IT-drift har ansvar for å stille med nødvendig lagringskapasitet. Det tas sikkerhetskopier av elektronisk arkiv og back-up av basen i samsvar med gjeldende tjenestebeskrivelse.

DOKU har utarbeidet varslingsrutiner til IT-drift og brukere hvis sak- og arkivsystemet er ute av drift eller har driftsforstyrrelser. Varslingsrutinene er publisert som eget dokument, Driftsstans og eller tekniske problemer, på NTNUs Intranettsider for DOKU.

Hvis sak- og arkivsystemet er ute av drift utsettes journalføring til systemet er tilgjengelig igjen.  
Skannemodulen (PixEdit-løsningen) vil i de fleste tilfeller være tilgjengelig. Dokumenter kan skannes og formidles som elektroniske kopier til saksbehandlere. E-post til postmottak kan formidles som kopi til saksbehandlere. Dokumenter som produseres i tekstbehandlersystem må lagres lokalt inntil sak- arkivsystemet er i drift for videre journalføring og arkivering.

## Papirbasert arkiv

Uforutsette skader på papirbasert arkivmateriale skal forebygges gjennom arkivforskriftens krav til godkjente arkivlokaler.

Dersom det oppstår vannskader på papir, bør følgende tiltak utføres:

- Papirbasert arkivmateriale som er utsatt for mindre vannskader lufttørkes. Protokoller o.l bør åpnes eller legges med trekkpapir mellom sidene. Kaldluftvifter bør anvendes for utlufting, ikke varme. Varme øker nedbrytningshastigheten i papiret og fremmer soppdannelser.

- Papirbasert arkivmateriale som er utsatt for større vannskader legges i tette plastposer/-sekker og fryses ned snarest mulig. Dette gjøres for å stanse soppvekst og annen nedbrytning av papirmaterialet. Arkivmaterialet sendes i nedfrosset tilstand til spesialfirma for frysetørking.

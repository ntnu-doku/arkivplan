---
date: '2021-01-29T00:00:00'
title: Dokumentasjonsforvaltning
status: Ferdig
category: Rutiner
tags:
  - Intern
  - Publisering
author: Geir Ekle
---

## Postmottak

DOKU forvalter en stor mengde informasjon og post hver dag. Dette inkluderer registrering av arkivverdig post, omadressering av henvendelser som skal besvares og sletting av ikke-arkivverdig materiale. Det vil si DOKU sorterer ut post som hverken er gjenstand for saksbehandling eller har verdi som dokumentasjon, jevnfør arkivforskriften § 14.

Kanalene for mottak av informasjon er mange; inngående papirpost, e-post og post fra andre elektroniske mottak som Altinn og eFormidling.

::: info Omadressering

Med omadressering menes det at e-post som sendes til et av NTNUs postmottak sendes på nytt til en annen mottaker, men med original avsender. På den måten kan ny mottaker svare avsender direkte uten å måtte gå via postmottak og DOKU.

:::

## Skanning

DOKU er ansvarlig for at inngående saksdokumenter på papir skannes. Hoveddokumenter og vedlegg skannes som hovedregel hver for seg, men kan også skannes som en fil.

Dato for skanning vil som regel være den samme dato som dokumentet ble mottatt og journalført, men i perioder kan det være slik at det ikke er samsvar mellom datoene. Dersom det er relevant - f.eks. ved frister eller større avvik mellom dokumentdato og skannedato og journalføringsdato, kan feltet “mottatt dato” benyttes i journalposten.

DOKU oppbevarer papiroriginalene i minimum tre måneder fra skannedato før de makuleres. Papiroriginaler skal kun unntaksvis overleveres til saksbehandler.

Makulering skal gjennomføres på en betryggende måte ved bruk av makuleringsmaskin, eller ved å anskaffe makuleringsbeholdere med hengelås fra NTNUs vaktmestertjenester. Beholderen med innhold går da direkte til anlegg for makulering.

::: advarsel Unntak for makulering  
Unntak gjelder for følgende dokumenter som skal oppbevares på papir, samt bevares dersom det ikke foreligger kassasjonsvedtak:

- Kontrakter med utenlandske parter

- Obligasjoner, tinglyste dokumenter, skjøter, garantier

*Videre rutine for oppbevaring av papirdokumenter er under utarbeidelse.*

:::

## Journalføring

NTNU har plikt til å journalføre alle inn- og utgående saksdokumenter som er gjenstand for saksbehandling og har verdi som dokumentasjon, jamfør arkivforskriften § 9. I tillegg til å ha et arkivmessig formål er journalføringsplikten også knyttet opp mot offentlighetsloven. Plikten til å journalføre er ment å bidra til at offentlig virksomhet er åpen og gjennomsiktig, styrke rettssikkerheten og tilliten til det offentlige, og allmennhetens kontroll.

Et saksdokument er dokument som er kommet inn til eller framlagt NTNU, eller dokument som NTNU selv har opprettet – i betydning sendt fra seg eller ferdigstilt – og som gjelder ansvarsområdet eller virksomheten til NTNU, jamfør bestemmelsene i offentlighetsloven § 4. Ved NTNU er det besluttet at også organinterne saksdokumenter skal journalføres på lik linje med inn- og utgående saksdokumenter, jamfør bestemmelsene om journalføring av interne dokument i arkivforskriften § 9.

Når saksdokumenter blir journalført i sak- og arkivsystemet blir saksdokumentene tilknyttet til saksmappe og journalpost med tilknyttede metadata for identifisering og gjenfinning.

Journalføring innebærer følgende:

- At journalposten er tilknyttet riktig sak

- At korrekt dokumenttype er benyttet, og endre dokumenttype dersom dette ikke er tilfelle

- At dokumentet lar seg åpne, har et innhold og er lesbart

- At det er samsvar mellom opplysninger og dato i dokument og registrerte metadata, og at ev mottatt dato benyttes dersom sprik mellom journaldato og mottatt dato

- At journalposttittel er beskrivende, uten skrivefeil og i henhold til skriveregler og standardtitler

- Om tilgangskode og hjemmel er nødvendig og korrekt (på hoveddokument og eller vedlegg)

- Om hele eller deler av journalposttittel og eller avsender, mottaker skal eller bør skjermes

- At alle nødvendige vedlegg følger med journalposten, eventuelt ligger på saken

## Offentlig postjournal

Alle saksdokumenter som kommer inn til eller blir framlagt for NTNU, eller dokumenter som NTNU selv har opprettet, er offentlige og åpne for innsyn dersom ikke annet følger av lov eller forskrift.

NTNUs offentlige postjournal er tilgjengelig på NTNUs nettsider og gir bl.a. opplysninger om dokumentets tittel og dato, sakstilknytning og eventuell hjemmel for unntak fra offentlighet. Dokumentene er ikke publisert, men det kan bes om innsyn i disse.

DOKU kvalitetssikrer den offentlige postjournalen før tilgjengeliggjøring og publisering på NTNUs nettsider.

## Restanseoppfølging og ferdigstillelse

En restanseliste er en rapport over journalposter som ikke er ferdigbehandlet.

Restanseoppfølging er viktig både for den enkelte saksbehandler og for NTNU som helhet. Restanselisten gir saksbehandleren oversikt over hvilke saker hen har fått fordelt og jobber med. Den er også viktig for at ledelsen skal kunne danne seg et bilde av omfanget av saksbehandlingen ved egen enhet. Ved aktivt å ta stilling til sine restanser vil man også bedre kunne sikre at henvendelser besvares innenfor den frist som settes for svar og midlertidig svar i forvaltningsloven § 11a.

Det er saksbehandler som har ansvaret for å endre status på journalpost fra reservert (status R) til ferdigstilt (status F). DOKU bør følge opp slik at journalposter ferdigstilles, for eksempel ved å ta ut oversikter over enhetenes reserverte journalposter i forbindelse med restanseoppfølgingen, eller ved å sende ut påminnelser til saksbehandlerne om å gå igjennom og ha et bevisst forhold til alle journalposter som ligger i søket “under arbeid”.

## Avslutte saker

DOKU avslutter saker (status A). Avslutning av en sak innebærer at det ikke er anledning til å registrere noen nye journalposter med dokumenter i saken. En avsluttet sak kan åpnes igjen av DOKU dersom det er behov for det, forutsatt at den aktuelle saken ikke ligger i en avsluttet arkivdel.

Ulike sakstyper avsluttes på ulike tidspunkt. Sakstyper med objekt som klassering, for eksempel personalmapper og studentmapper, kan avsluttes når tilknytningsforholdet har opphørt - avsluttet arbeidsforhold eller avsluttet studiet. Sakstyper med emne som klassering kan avsluttes når saken er ferdigbehandlet.

Ved saksavslutning sjekkes det:

- At saken er fordelt til administrativ enhet og har en saksansvarlig

- At saken er klassert med arkivkode

- Om tilgangskode og hjemmel er nødvendig og at det er korrekt

- At sakstittel er beskrivende, uten skrivefeil og i henhold til eventuelle skriveregler

- Om hele eller deler av sakstittel skal eller bør skjermes

- At alle journalposter er journalført, fordelt og avskrevet

## Kvalitetssikring

I tillegg til den kontinuerlige kvalitetssikringen som foregår under postmottak, skanning, registrering og journalføring, sjekker DOKU også om det forekommer:

- Saker uten journalposter

- Journalposter uten tilknyttet dokument

- Vedlegg uten tilknyttet dokument

- Ikke offentlighetsvurderte journalposter (status XX)

- Reserverte og saker uten klassering – primært saker opprettet av saksbehandler

- Saker markert som Ferdig fra saksbehandler

- Duplikater av journalpost – fortrinnsvis reserverte

## Periodisering, bortsetting og deponering, samt avlevering

Arkiv skal periodedeles, jamfør arkivforskriften § 13 og Riksarkivarens forskrift kapittel 4.

For sakarkivet bør periodene være på 4-5 år og følge kalenderåret.

DOKU har ansvar for periodisering av sak- og arkivsystemet og at elektronisk kopi av avsluttet periode blir deponert hos Arkivverket. Fysisk arkivmateriale som enda ikke er gammelt nok til å avleveres til Arkivverket, men som ikke lenger er i aktiv bruk, kalles for et *bortsettingsarkiv*.

Fakultetene og enhetene kan oppbevare sine bortsettingsarkiv i fjernarkivet på Arkivsenteret, Dora. En forutsetning for slik oppbevaring er at arkivmaterialet grovsorteres i henhold til retningslinjene for oppbevaring av arkivmateriale i fjernarkivet Dora. Se dokumentet [Fjernarkiv](/arkivplan/arkivoversikt/fjernarkiv).

Det må være inngått en avleveringsavtale mellom DOKU og Arkivverket før en avlevering av arkivmateriale kan skje. Arkivmateriale som er ryddet, ordnet og listeført, blir registrert i arkivregistreringssystemet Asta av DOKU før arkivmaterialet avleveres til Arkivverket.

## Bevaring og kassasjon

Se dokumentet [Bevaring og kassasjon](/arkivplan/regelverk/bevaring-og-kassasjon).

## Oppdatering av arkivplan

Arkivplanen skal holdes oppdatert og er et verktøy for å dokumentere NTNUs rutiner, regler og planer for arkivarbeidet.

Ved oppdatering av arkivplanen skal endringer loggføres, så fremt det ikke kun er snakk om retting av skrivefeil, åpenbart feilaktig informasjon som ikke har hatt betydning for NTNUs praktisering og forståelse av dokumentasjons- og arkivforvaltningen, eller utseendemessige korrigeringer.

Fjorårets versjon av arkivplan arkiveres i sak- og arkivsystemet. Det samme gjelder dersom det skulle skje store organisatoriske eller arkivmessige endringer.

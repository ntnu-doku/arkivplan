---
date: '2020-12-17T00:00:00'
title: Veiledninger og brukerdokumentasjon
status: Ferdig
category: Referanse
tags:
  - Nett
author: Ole Vik
---

## Brukerveiledninger

Alle interne brukerveiledninger for sak- og arkivsystemet er laget av DOKU og tilgjengelig på [Innsida.NTNU.no/Brukerveiledninger+for+ePhorte](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Brukerveiledninger+for+ePhorte).

Brukerveiledningene dekker typiske prosedyrer for hvordan arbeid utføres i sak- og arkivsystemet, ofte på en mer pedagogisk måte enn systemets egen dokumentasjon.

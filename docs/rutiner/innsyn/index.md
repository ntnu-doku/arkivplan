---
date: '2021-01-08T00:00:00'
title: Innsyn
status: Ferdig
category: Regelverk
tags:
  - Behandle
author: Geir Ekle
---

## Offentlighetens innsyn

Det er flere lover som styrer offentlighetens tilgang til arkivmaterialet:

- Offentlighetslovens krav om innsyn i offentlige saksdokumenter

- [Forvaltningslovens](https://lovdata.no/lov/1967-02-10) regler om partsinnsyn

- Personopplysningslovens bestemmelser og GDPR-forordningen om innsyn i personopplysninger

- De ulike særlovenes innsynsbestemmelser m.m.

Samtidig må vi ta hensyn til prinsippet om meroffentlighet og ulike avgraderingsregler. Riksarkivaren gir eksempelvis bestemmelser om når avsluttede arkiver generelt kan stilles til disposisjon for allmennheten (normalt 60 år). Disse bestemmelsene om innsynsrettigheter må vurderes opp mot hensynet til personvernet og unntak fra offentlighet som finnes i offentlighetsloven, sikkerhetsloven og i særlover.

Viktige punkter fra offentlighetsloven:

- Formålet med loven er blant annet å legge til rette for en åpen og gjennomsiktig forvaltning

- Utgangspunktet er at alle saksdokumenter i offentlig forvaltning er offentlige og åpne for innsyn

- Det finnes flere unntak fra hovedregelen om fullt innsyn:

  - §13: Må unntas (taushetsbelagte opplysninger)

  - §14-26: Kan unntas

- Alle kan be om innsyn i dokumenter

- Krav om innsyn skal behandles uten ugrunnet opphold. Sivilombudsmannen anbefaler svar i løpet av 1-3 dager

- Dersom det ikke blir gitt et svar innen 5 dager regnes det automatisk som et avslag på innsyn

- Avslag på innsyn kan påklages (§32)

[Les mer og innsyn i dokumenter på NTNUs nettsider.](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Innsyn+i+offentlige+dokument)

## Offentlig postjournal

NTNU er forpliktet til å føre journal. Denne plikten er hjemlet i offentlighetsloven § 10 som sier at organet skal føre journal etter reglene i arkivloven med forskrifter. NTNU publiserer sin offentlige postjournal på NTNUs nettsider via modulen eInnsyn som er tilknyttet sak- arkivsystemet. Den offentlige postjournalen inneholder informasjon om saker og dokumenter for alle nivå på NTNU - inngående og utgående dokumenter, interne notater og saksframlegg. Publiseringen skjer fortløpende etter kvalitetssikring utført av DOKU, men med inntil sju dagers forsinkelse. Offentlig postjournal er tilgjengelig for tre måneder tilbake i tid. De elektroniske dokumentene er ikke tilgjengelige, men disse kan gis ut ved innsynsforespørsel.

[Les mer om NTNUs offentlige postjournal og eInnsyn på NTNUs nettsider.](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Offentlig+postjournal)

## Partsinnsyn

Forvaltningsloven [§ 2 bokstav e](https://lovdata.no/lov/1967-02-10/§2) definerer part som person som en avgjørelse retter seg mot eller som saken ellers direkte gjelder. En person i denne sammenheng kan være både privatpersoner, bedrifter og andre organisasjoner eller sammenslutninger.

Etter forvaltningsloven § 18 er hovedregelen at partene har rett til innsyn i sakens dokumenter. Partsinnsyn er mer omfattende enn den retten allmennheten har rett til innsyn etter offentlighetsloven da partene også vil ha innsyn i en del dokumenter som er underlagt taushetsplikt. Forvaltningsloven [§ 13 b](https://lovdata.no/lov/1967-02-10/§13b) sier at taushetsplikt ikke er til hinder for parters innsynsrett.

Forvaltningslovens §§ 18 og 19 har nærmere bestemmelser om unntak for partsinnsyn, mens § 20 har bestemmelser om formene for dokumentinnsyn.

Et avslag på et krav om å få dokumentinnsyn kan påklages etter forvaltningslovens § 21.

Merinnsyn skal alltid praktiseres og er også understreket i forvaltningslovens forarbeider.

## Innsyn egne opplysninger

Retten til innsyn i egne opplysninger er regulert i personopplysningsloven og personvernforordningen GDPR. Dersom NTNU har lagret opplysninger om deg som person har du rett til å vite følgende:

- Hvilke personopplysninger og kategori av personopplysninger blir lagret

- Hva er formålet med behandlingen

- Hvor kommer opplysningene fra

- Hvilke kategorier av mottakere blir de utlevert til og blir de utlevert utenfor EU/EØS

- Hvor lenge blir personopplysninger lagret

[Les mer om personopplysninger og innsyn i egne opplysninger på NTNUs nettsider.](https://www.ntnu.no/personopplysninger/)

## Historisk arkiv

De historiske arkivene er en viktig del av NTNUs interne hukommelse og har betydelig kulturell og forskningsmessig verdi som NTNU forvalter inntil de overføres og avleveres til Arkivverket.

NTNUs historiske arkiver befinner seg i NTNUs fjernarkiv som er lokalisert ved Arkivsenteret på Dora i Trondheim. Samlingen består av blant annet sakarkiver, personalmapper, studieplaner, studentlister, avhandlinger og vitnemål fra tidligere universitet og høgskoler som NTH, NLHT, AVH, HiST og NTNU. Tegninger av bygninger og campus helt tilbake til planleggingen av NTH på starten av 1900-tallet er også en del av det historiske materialet.

Historiske arkiv fra Høgskolen i Gjøvik og Høgskolen i Ålesund er oppbevart på campus i henholdsvis Gjøvik og Ålesund.

## Saksbehandle innsyn

Som saksbehandler kan du motta krav om innsyn. Krav skal behandles uten ugrunnet opphold, og normalt innen 1-3 virkedager. Dersom det ikke blir gitt svar innen fem virkedager er det å anse som et avslag som kan påklages. Dersom det ikke er mulig å svare innen fristen, for eksempel at innsynet gjelder mange dokumenter, bør du sende et foreløpig svar.

Når du behandler innsyn må du vurdere om det er behov for å unnta (kan eller skal), er det hjemmel for å unnta, og om unntak er nødvendig – meroffentlighet.

Etter vurdering kan du enten gi innsyn eller avslå innsyn. Avslag må gjøres skriftlig og du må vise til lovbestemmelsen som gir grunnlag for avslaget.

Som saksbehandler kan du få krav om begrunnelse for avslaget. Et slikt krav må være mottatt innen tre uker etter at avslaget ble gitt. En begrunnelse for avslaget må være gitt innen ti dager og bør inneholde hovedhensynene som ble lagt til grunn for avslaget.

Som saksbehandler kan du motta klage på avslaget. En klage må være mottatt innen tre uker etter at avslaget ble gitt. Kravet om rask saksbehandling gjelder også for klagesaker. Du må vurdere innsynskravet på nytt og dersom det ikke er grunnlag for å nekte innsyn, kan du omgjøre opprinnelig vedtak og gi innsyn. Dersom innsyn fortsatt ikke kan gis, skal saken sendes videre til Felles klagenemnd, Unit, som fra 01.06.2015 er klageinstans for utdanningsinstitusjoners avgjørelser etter offentlighetsloven.

Felles klagenemnd kan endre vedtaket om avslag og gi helt eller delvis innsyn, eller opprettholde avslaget om innsyn.

Ved klage på vedtak om innsyn i egne personopplysninger er Datatilsynet rette klageinstans.

## Be om innsyn

Alle kan be om innsyn i NTNUs arkivmateriale.

Innsyn etter offentlighetsloven kan bestilles fra NTNUs offentlige postjournal fra eInnsyn eller å sende innsynsforespørsel til <postmottak@ntnu.no> med beskrivelse av hva det ønskes innsyn i.

Krav om partsinnsyn sendes til <postmottak@ntnu.no>

Krav om innsyn i egne opplysninger bestilles via eget nettskjema på NTNUs nettsider – [NTNU personopplysninger.](https://www.ntnu.no/personopplysninger/#/)

## DOKUs behandling av innsyn

DOKU, som har ansvaret for <postmottak@ntnu.no> har en fast rutine på hvordan innsynsbegjæringer behandles.

Innsynsbegjæringer som går på offentlig journal svares ut av DOKU fortløpende.

Innsynsbegjæringer som går på innsyn i fysisk eller digitalt materiale sendes til rett enhet som saksbehandler innsynet. DOKU registrerer innsynsbegjæringen og videreformidler internt.

Innsynsbegjæringer som går på egne personopplysninger (som har blitt fylt ut via [denne lenken](https://www.ntnu.no/personopplysninger/#/)) dukker opp i sak- og arkivsystemet og blir behandlet av DOKU.

DOKUs rutine for behandling av innsyn (vedlegg).

---
date: '2023-03-22T00:00:00'
title: Studie- og eksamensdokumentasjon
status: Ferdig
category: Rutine
author: Geir Ekle
---

## Studiehåndbøker og emnebeskrivelser

Det har vært en gradvis overgang fra trykte til digitale studiehåndbøker. DOKU har trykte eksemplarer i fjernarkivet på Dora. Digitale utgaver av studiehåndbøkene finnes på [NTNU.no/studier/studiehandbok](http://www.ntnu.no/studier/studiehandbok).

Studiehåndbøker finnes innenfor følgende områder:

- Arkitektur- og design

- Humanistiske fag

- Ingeniørfag (bachelor)

- Master i teknologi (sivilingeniør)

- Medisin og helsefag

- Realfag og informatikk

- Samfunns- og utdanningsvitenskapelige fag

- Økonomi

- Doktorgradsstudiet - Ph.d.-studiet

Emnebeskrivelser inngår ikke som en del av studiehåndbøkene. Emnebeskrivelsene finnes publisert på [NTNU.no/studier/emner](http://www.ntnu.no/studier/emner). Emnebeskrivelsene fra studieårene 2008-2009 er arkivert i sak- og arkivsystemet.

## Pensumlister

Pensum dokumenterer undervisning i fag og skal arkiveres og bevares. Fakultetene oppretter saksmappe i sak- og arkivsystemet og arkiverer pensumlistene for fagene.

## Oppgavesett til eksamen

Eksamensoppgaver arkiveres og bevares. Riksarkivaren har fattet eget vedtak om bevaring og kassasjon av eksamensbesvarelser med mer.

Instituttene og enhetene utarbeider oppgavesett og tilhørende sensorveiledninger og sørger for arkiveringen av disse dokumentene i sak- og arkivsystemet. Én saksmappe per institutt per semester, eventuelt én saksmappe per emnekode. Hver eksamensoppgave registreres som egen journalpost (X-notat), og sensorveiledning tilknyttes som vedlegg. Dokumentdato settes lik eksamensdato. Oppgavesett til eventuell kontinuasjonseksamen arkiveres i den saksmappen hvor det opprinnelige oppgavesettet ligger.

## Eksamensbesvarelser

Eksamensbesvarelser på papir eldre enn de fire siste semestre makuleres.

::: info Merk

Eksamensbesvarelser på papir eldre enn de fire siste semestre skal ikke overføres til fjernarkiv, Dora, for oppbevaring.

:::

Følgende rutine gjelder for eksamensbesvarelser på papir som skal oppbevares i fjernarkiv, Dora, før makulering.

1.  Eksamensbesvarelsene tas ut av konvoluttene, sorteres pr emnekode i stigende rekkefølge etter kandidatnummer og legges i arkivbokser. Eventuelle plastlommer fjernes. Boksene merkes med emnekode og kandidatnummer fra - til på framsiden/kortsiden av esken (ikke på lokket)

2.  Boksene stables i postbur/container (ta kontakt med transporten ved NTNU), eller i pappesker/flyttekasser som settes på paller (ikke mere enn 2 kasser i høyden). Merkes godt med innhold, avsender og mottaker. Arkivbokser og esker bestilles gjennom NTNUs innkjøpssystem. (Benytt gjerne Maske Arkivboks A4 6 cm, varenummer 2441002, og Arkivboks A4 10 cm, varenummer 2441004.)

3.  Kontakt DOKU før bestilling av transport for å avtale oversendelse av materialet

4.  Bestill transport hos Transportsentralen via Innsida (via system Lydia)

::: info Sending av arkivmateriale

Du finner mer informasjon på [Innsida.NTNU.no/Oppbevaring av arkivmateriale](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Oppbevaring+av+arkivmateriale).

:::

## Begrunnelse og klage på sensur

Studenter som har avlagt eksamen kan kreve begrunnelse for sensur eller klage på sensuren. Det opereres med ulike frister for ulike typer eksamen. Siden [Begrunnelse og klage](https://i.ntnu.no/wiki/-/wiki/Norsk/Begrunnelse+og+klage) på Innsida holdes oppdatert med korrekte frister.

::: info Frister

Gjeldende frister følger av lovverket, og det finner du alltid på Lovdata.no. For eksamen gjelder [Lov om universiteter og høyskoler (universitets- og høyskoleloven)](https://lovdata.no/lov/2024-03-08-9) og [Forskrift om studier ved Norges teknisk-naturvitenskapelige universitet (NTNU)](https://lovdata.no/forskrift/2015-12-08-1449).

:::

Behandling av klagesaker skal skje uten ugrunnet opphold, det vil si så raskt som mulig. Se forvaltningsloven §11 a. Om saken ikke er ferdigbehandlet i løpet av en måned skal fakultetet eller instituttet sende et foreløpig svar til studenten. Karakterfastsetting ved klagesensur kan ikke påklages, men studenten kan be om begrunnelse også etter klagesensur.

Studenter sender inn begrunnelse eller klage i Studentweb eller via eget nettskjema.

Skjema for begrunnelse eller klage mottas av DOKU. Skjema journalføres og arkiveres i sak- og arkivsystemet av en robotisert prosess, og fordeles til riktig fakultet eller institutt for videre saksbehandling.

Klager sendt inn i Studentweb overføres automatisk til sak- og arkivsystemet via integrasjonsløsning fra FS. Det samme skjer for svar på klage. Inngående klage og svar på klage knyttes sammen i en saksmappe som gjelder den enkelte student.

Krav om begrunnelse sendt inn i Studentweb blir saksbehandlet i Studentweb og Inspera. Det er ingen overføringer av dokumenter eller opplysninger til sak- og arkivsystemet.

Les mer om begrunnelse og klage på sensur på [Innsida.NTNU.no/Begrunnelse og klage](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Begrunnelse+og+klage).

## Bachelor- og masteroppgaver

NTNU skal arkivere bacheloroppgaver, masteroppgaver, hovedoppgaver og doktorgradsavhandlinger. Arkivering skjer i NTNUs institusjonelle arkiv NTNU Open.

Les mer om arkivering og publisering i NTNU Open på [Innsida.NTNU.no/Publisering og båndlegging av oppgaver](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Publisering+og+b%C3%A5ndlegging+av+oppgaver).

## Vitnemål

Papirkopi av utstedt og signert vitnemål arkiveres i fjernarkiv, Dora. Kopi av utstedt og signert vitnemål fra NTNU er arkivert i sak- og arkivsystemet fra 01.01.2016. Kopi av utstedt og signert vitnemål fra de tidligere høgskolene Sør-Trøndelag, Ålesund og Gjøvik er arkivert i sak- og arkivsystemet fra 01.01.2017.

Se internt notat datert 09.05.2016 med saksnummer 2016/11394.

For å søke etter vitnemål arkivert for perioden januar 2016 til april 2018 må du søke på studentens navn i feltet journalpostinnhold eller i feltet avsender eller mottaker. Registrering er utført manuelt.

For å søke etter vitnemål arkivert fra mai 2018 søker du på studentens navn enten i sakstittel eller i journalpostinnhold. Registrering er utført av robot.

Fra 1. juni 2022 fikk alle som fullførte et bachelorprogram, norsk masterprogram og PPU utstedt digitale vitnemål. Fra og med 1. juni 2023 innføres digitale vitnemål for alle. Det vil være noen fellesgradsprogram som fortsatt får vitnemål i papir. Det digitale vitnemålet og Diploma Supplement blir tilgjengelig i Studentweb og Vitnemålsportalen.

::: info Vitnemålsdatabasen fra SVT-fakultetet  
Vitnemålsdatabasen var primært et administrativt verktøy for å kunne se hvor i godkjenningsprosessen et vitnemål befant seg og når det ble sendt ut fra fakultetet. Kopi av alle vitnemål fra og med 2005 var registrert i databasen, unntatt ph.d.-vitnemålene som var registrert fra april 2007 og PPU-vitnemålene som var registrert fra september 2008. Vitnemålene fra SVT-fakultetets vitnemålsdatabase – cirka 11 000 – er hentet ut og registreres i sak- og arkivsystemet av en robotisert prosess. For å søke etter vitnemål fra den tidligere vitnemålsdatabasen, søker du på studentens navn enten i sakstittel eller i journalpostinnhold.

:::

## Politiattest

Politiattest kreves ved opptak til studier der studenter kan komme i kontakt med pasienter, klienter, barnehagebarn, elever eller andre sårbare grupper som del av klinisk undervisning eller praksisopplæring. Dette gjelder lærer- og lektorutdanninger, helseutdanninger, videreutdanninger og årsstudium som har praksis.

NTNU har fastsatt egne interne rutiner for mottak og behandling av politiattester med og uten merknader.

Les mer om opptak og politiattest på [NTNU.no/Studier/Opptak/Politiattest](http://www.ntnu.no/studier/opptak/politiattest). Se også internt notat i sak 2011/4408.

### Oppbevaring og makulering av politiattest

::: info Politiregisterforskriften § 37-2 – Oppbevaring av politiattest hos mottakeren

Politiattesten skal oppbevares utilgjengelig for uvedkommende. Politiattesten kan oppbevares så lenge det er nødvendig for formålet med politiattesten, men ikke utover det tidspunkt vedkommende slutter i den stillingen som ga grunnlaget for vandelskontroll. Politiattest som ikke lenger er nødvendig for formålet skal tilintetgjøres. Offentlige mottakere skal slette politiattesten i samsvar med § 16-2 annet ledd nr. 2.

:::

::: info Politiregisterforskriften § 16-2 – Nærmere om begrepet sletting

Sletting etter politiregisterloven eller denne forskriften innebærer at opplysningen skal fjernes fra registre eller andre systemer. Etter at opplysningene er fjernet fra registrene skal opplysningene behandles i samsvar med arkivlovgivningen ved at de enten

1.  Tilintetgjøres, dersom de kan arkivbegrenses

2.  Kasseres etter vedtak av Riksarkivaren, eller

3.  Bevares og avleveres til Arkivverket, jf arkivloven § 9 og § 10

:::

::: info Riksarkivarens kassasjonsvedtak nummer 898

Politiattester som innhentes i forbindelse med tilsettingssak er å betrakte som et saksdokument, jamfør forskrift 11.12.1998 nr. 1193 om offentlige arkiv § 2-6. Det må registreres i journal at slikt dokument er innhentet. Med hjemmel i § 9 i lov 04.12.1992 nr. 126 om arkiv fastsetter Riksarkivaren at slik attest kan kasseres når det ikke lenger foreligger et administrativt behov for å ha den tilgjengelig. Det er ikke nødvendig å skanne politiattesten inn i elektronisk arkivsystem.

:::

Det er ikke nødvendig å skanne selve politiattesten og knytte denne til sak- og arkivsystemet, men det bør framgå av journal at politiattest er innhentet, mottatt. Jamfør vedtak Riksarkivaren. Praktisk tilrettelegging, oppbevaring, arkivering og makulering av politiattest gjøres av DOKU i samråd med studie der bestemmelsene om politiattest gjelder.  
  
Ved avklaring 15.01.2018 er det kun politiattester med merknad som krever oppfølging og saksbehandling og som journalføres og arkiveres i sak- og arkivsystemet.

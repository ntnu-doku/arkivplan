---
date: '2021-02-05T00:00:00'
title: Høringer
status: Ferdig
category: Rutine
author: Geir Ekle
---

## Eksterne

Eksterne høringer kommer fra for eksempel departementer eller andre offentlige etater. Høringer kan gjelde lov- eller forskriftsendringer, instrukser, eksterne rundskriv, rapporter eller annet. DOKU journalfører og arkiverer høringer som sendes til NTNU. Høringssaken kan behandles i fellesadministrasjonen.

### Saksbehandlers ansvar

Saksbehandler må vurdere om det er behov for å samle inn uttalelser fra andre enheter. For å innhente uttalelser utarbeider saksbehandler et internt N-notat i saken som sendes til den aktuelle enheten. Saksfordeler på mottakende enhet fordeler til riktig saksbehandler. Saksbehandler som har fått fordelt henvendelsen må også vurdere om det er behov for å innhente uttalelser fra andre personer eller enheter. Svar skal registreres som et internt N-notat i saken. Saksbehandler sammenfatter svar og utarbeider høringssvar. Svar skal registreres enten som utgående brev i saken eller som inngående dokument med kvittering som inneholder høringssvaret.

Dersom en faglig uttalelse etterspørres, sendes saken til det mest relevante fakultetet. Saksbehandler på fakultet må vurdere om det er behov for å innhente uttalelse fra andre personer eller enheter. Saksbehandler sammenfatter og utarbeider høringssvar. Svar til ekstern part skal registreres som utgående brev i saken eller som inngående dokument med kvittering som inneholder høringssvaret.

Dersom flere fakultet er relevante i høringssaken, får andre fakultet saken i kopi med mulighet til å gi innspill til fakultetet som har fått saken. Dersom det er behov for mer omfattende koordinering, kan saken bringes tilbake for sentral koordinering, jamfør dekanmøte 13.02.2012 (sak 2012/17, dokument 13) og dekanmøte 29.08.2011 (sak 2011/36, dokument 33).

## Interne

Interne høringer går til en eller flere enheter. Dette kan være utkast til høringssvar, utarbeidelse av interne retningslinjer og regelverk, budsjettprosess eller annet.

### Saksbehandlers ansvar

Saksbehandler sender et internt N-notat. Dette notatet beskriver hva som forventes av mottakende enhet og frist for svar. Mottaker av N-notatet skal være enhet. Saksfordeler ved enhet fordeler til riktig saksbehandler. Svar sendes som et internt N-notat i saken. Saksbehandler sammenfatter mottatte svar fra enhetene og utarbeider et endelig notat, brev eller saksframlegg som registreres i saken.

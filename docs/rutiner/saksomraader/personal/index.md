---
date: '2021-09-20T00:00:00'
title: Personaldokumentasjon
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

## Fra 01.01.2014

Fra 01.01.2014 ble arkivdelen personaldokumentasjon, PERS, og elektronisk personalmappe i sak- og arkivsystemet innført. Fra samme dato ble de fysiske personalmappene avviklet.

Alle saker og dokumenter som gjelder en ansatts personaldokumentasjon skal kunne søkes fram i sak- og arkivsystemet via navn og eller ansattnummer fra fagsystemet Paga. For at dette prinsippet skal fungere, må ansattes navn være registrert i tittel på saksmappen med ansattnummer registrert som ordningsverdi i saksmappen.

::: info Registreringsrutiner

[Administrasjon arbeidsforhold](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-administrasjon-arbeidsforhold.pdf) (vedlegg)

[Arbeidsavtaler - forlengelser – opphør](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-arbeidsavtaler-forlengelser-opphoer.pdf) (vedlegg)

[Sykefravær - HMS - IA](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-sykefravaer-hms-ia.pdf) (vedlegg)

[Opprykk etter kompetanse](/arkivplan/vedlegg/rutiner/registrering/tematisk-standardtitler-opprykk-etter-kompetanse.pdf) (vedlegg)

:::

::: info Unntak

Personalsak vedrørende disiplinærreaksjoner, suspensjon, avskjed og konflikter, registreres som egen tematisk sak i personalarkivet. Tittel på tematisk sak tilpasses etter disiplinærforholdet. **Sakene skjermes og unntas offentlighet i sin helhet med tilgangskode U.** Kun saksbehandler som eier saken har da tilgang. For å gi flere lesetilgang til saken opprettes ad hoc tilgangsgruppe.

:::

[Opprette personalmappe](/arkivplan/vedlegg/rutiner/registrering/opprette-personalmappe.pdf) (vedlegg).

Sakene med dokumenter vil til sammen utgjøre den tilsattes elektroniske personalmappe. Kun de som har saksbehandlingsansvar skal ha tilgang til saker og dokumenter i personalarkivet.

Autorisering – lesetilgang til skjermet informasjon og dokumenter – for de som har saksbehandlingsansvar:

- Personlig

  - Skjerme egne dokumenter, lese og saksbehandle skjermede dokumenter mottatt fra andre

- For egen enhet

  - Lese alle skjermede dokumenter til eller fra egen enhet, og lese alle skjermede dokumenter som tilhører underliggende enheter

  - Ingen lesetilgang uten personlig autorisering

  - Kan få lesetilgang som medlem av tilgangsgruppe – faste og ad hoc, som kopimottaker og som saksansvarlig. Forutsetter personlig autorisering

Les mer om [tilgangskoder og skjerming av informasjon og dokumenter](/arkivplan/vedlegg/rutiner/registrering/veiledning-tilgangsstyring-personalmapper.pdf) (vedlegg).

Personaldokumentasjon finnes både i arkivdelen sak og arkivdelen personal. I arkivdelen personal arkiveres dokumentasjon som kun omhandler den ansatte, personalmappene. I arkivdelen sak arkiveres saker av prinsipiell art, presedenssaker, eller saker som berører grupper av ansatte. I tillegg kan det ligge personaldokumentasjon i fagsystemer.

## 08.12.2004 - 31.12.2013

Dokumenter mellom NTNU og ansatt ble journalført og arkivert i sak- og arkivsystemet fra desember 2004. Dokumentene ble arkivert som elektroniske kopier i personalmappesaken, mens papirdokumentene ble arkivert i den papirbaserte personalmappen.

Interne notater og utgående brev ble journalført og arkivert i elektronisk personalmappe, men i mange tilfeller ble ikke dokumentene overført til den papirbaserte personalmappen. Det er også tilfeller der dokumenter ikke er journalført og arkivert elektronisk, men er kun i den papirbaserte personalmappen.

Ved periodiseringen av arkivet i januar 2010 ble alle personalmapper med klassifiseringen 221 personalmappe overført til ny arkivdel NTNU-EMNE2. Den papirbaserte personalmappen var den gjeldende arkivversjonen fram til 31.12.2013.

::: advarsel Merk

Eksisterende fysiske personalmapper skal beholdes og ikke kasseres.

:::

## Personalarkiv i Paga

Fagsystemet Paga inneholder personaldokumentasjon på områder som lønn, skattetrekk, sykefravær, permisjoner, ferie og reiseregninger.

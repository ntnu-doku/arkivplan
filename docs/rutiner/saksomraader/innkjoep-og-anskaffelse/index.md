---
date: '2021-09-20T00:00:00'
title: Innkjøp og anskaffelse
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

Økonomiavdelingen, Seksjon for økonomitjenester, Innkjøp har utarbeidet sentrale rutiner for innkjøp og anskaffelser. Les mer om innkjøp og anskaffelser på Innsida [Innsida.ntnu.no/bestille](https://innsida.ntnu.no/bestille).

::: info Registreringsrutiner

Under arbeid 2021.

:::

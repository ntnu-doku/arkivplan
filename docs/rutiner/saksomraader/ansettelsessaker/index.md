---
date: '2021-09-20T00:00:00'
title: Ansettelsessaker
status: Ferdig
category: Rutine
author: Geir Ekle
---

Behandling av ansettelsessaker er regulert av:

- Arkivloven med forskrifter

- Forvaltningsloven med forskrifter

- Offentlighetsloven med forskrifter

- Statsansatteloven med forskrifter

- Arbeidsmiljøloven med forskrifter

Stillingssøknader er saksdokument og journalføringsplikten gjelder.

Deler av saksbehandlingen i tilsettingssaker skjer i rekrutteringssystemet Jobbnorge. Det er satt opp en integrasjon mellom Jobbnorge og sak- og arkivsystemet for automatisk overføring av utlysningstekst, offentlig søkerliste og utvidet søkerliste.

Følgende dokumenter kan inngå i en ansettelsessak:

- Anmodning om utlysing eller tilsetting som N-notat fra leder/enhet til ansettelsesmyndighet

- Utlysningstekst på alle målformer og språk

- Forslag til oppnevning av sakkyndig komite eller evalueringskomite

- Oppnevning av sakkyndig komite eller evalueringskomite

- Offentlig søkerliste

- Utvidet søkerliste

- Sakkyndig vurdering/faglig evaluering

- Pedagogisk vurdering

- Innstilling

- Protokoll eller vedtak til ansettelsesutvalg eller ansettelsesråd

- Tilbudsbrev

- Aksept eller avslag fra søker

- Eventuelle lønnskrav før arbeidsavtale er signert

::: info Merk

Søknaden til den som blir tilsatt overføres til personalmappe.

:::

::: info Registreringsrutiner for personaldokumentasjon?

Se siden [Personaldokumentasjon](/arkivplan/rutiner/saksomraader/personal)!

:::

## Søkerliste

Hvem som søker en stilling er som hovedregel offentlig, men søker kan unntas fra offentlighet dersom søkeren selv ber om dette. Ved vurderingen om en slik anmodning skal tas til følge, skal det legges vekt på om det knytter seg særlig offentlig interesse til stillingen. Jo større offentlig interesse, jo større skal terskelen være for å unnta søkeres navn fra offentlighet.

I utlysningsteksten skal det gjøres oppmerksom på at opplysninger om søkeren kan bli offentliggjort selv om søkeren har anmodet om ikke å bli oppført på søkerlisten. Dersom anmodningen ikke tas til følge, skal søkeren varsles om dette.

## Ansettelsessak utenfor rekrutteringssystemet

Dersom en stilling kunngjøres utenfor rekrutteringssystemet, må alle saksdokumenter registreres fortløpende i sak- og arkivsystemet.

## Studentassistent og læringsassistent

Utlysing av stillinger som studentassistent eller læringsassistent og lignende er også underlagt journalføringsplikt. Det opprettes som regel én samlesak per enhet per semester. I denne saken skal samme dokumenter som i en ordinær ansettelsessak inkluderes. Signert arbeidsavtale registreres i personalmappe.

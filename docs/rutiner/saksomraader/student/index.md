---
date: '2021-09-20T00:00:00'
title: Studentdokumentasjon
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

Alle saker og dokumenter som gjelder en student skal kunne søkes fram via studentens navn og eller fødselsnummer i sak- og arkivsystemet. For at dette prinsippet skal fungere, må studentens navn være registrert i tittel på saksmappe med fødselsnummer som ordningsverdi.

Det er utarbeidet felles registreringsrutiner i sak- og arkivsystemet for studentdokumentasjon som er tilpasset saksforholdet.

::: info Registreringsrutiner

[Eksamen](/arkivplan/vedlegg/rutiner/registrering/eksamen-v2.0.1.pdf) (vedlegg)

[Fusk](/arkivplan/vedlegg/rutiner/registrering/fusk-v2.1.0.pdf) (vedlegg)

[Master](/arkivplan/vedlegg/rutiner/registrering/master-v2.0.1.pdf) (vedlegg)

[Opptak](/arkivplan/vedlegg/rutiner/registrering/opptak-v2.0.1.pdf) (vedlegg)

[Politiattest](/arkivplan/vedlegg/rutiner/registrering/politiattest-v2.0.1.pdf) (vedlegg)

[Praksis](/arkivplan/vedlegg/rutiner/registrering/praksis-v2.1.1.pdf) (vedlegg)

[Skikkethet](/arkivplan/vedlegg/rutiner/registrering/skikkethet-v2.0.pdf) (vedlegg)

[Studieforløp](/arkivplan/vedlegg/rutiner/registrering/studieforloep-v2.2.1.pdf) (vedlegg)

[Tilrettelegging](/arkivplan/vedlegg/rutiner/registrering/tilrettelegging-v2.0.1.pdf) (vedlegg)

:::

Se også siden [Studie- og eksamensdokumentasjon](/arkivplan/rutiner/saksomraader/studie-og-eksamensdokumentasjon)!

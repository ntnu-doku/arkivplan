---
date: '2021-09-20T00:00:00'
title: Rektorvedtak
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

Rektorvedtak vil være avgjørelser som gjelder rettigheter og plikter til et ubestemt antall, eller en ubestemt krets av personer. Det vil kunne oppstå spørsmål om avgjørelsen er et vedtak. Med vedtak menes hvilke avgjørelser det dreier seg om. Enkeltvedtak vil falle utenfor, altså vedtak som gjelder en eller flere bestemte personer.

Eksempler på hva som kan være rektorvedtak (ikke uttømmende liste):

- Rektor får myndighet i en styresak til å fastsette nærmere bestemmelse om noe, for eksempel sidegjøremål

- Retningslinjer innenfor et område

- Utfyllende regler for sivilingeniør- og lektorutdanning

- Instruks for eksamenskandidater, eksamensinspektører og faglærers tilstedeværelse i lokalet under eksamen

- Emnebeskrivelser i Eksperter i Team

- Oppnevning av ekstern sensor i Eksperter i Team

- Opprettelse av ph.d.-program

- Endring av navn på studieprogram

- Vedta endringer i standardavtaler

- Adgangsbegrensninger i emner

- Oppnevning av opptakskomite

- Fastsette andre rangeringsregler enn de ordinære ved opptak til enkelte masterprogram

- Bestemmelser for fellesemnene ex.phil.-, ex.fac.- og perspektivemnet

- Handlingsplaner/politikk

Det er utarbeidet felles registreringsrutiner i sak- og arkivsystemet for rektorvedtak.

::: info Registreringsrutiner

[Rektorvedtak](/arkivplan/vedlegg/rutiner/registrering/rektorvedtak.pdf) (vedlegg)

[Søke etter og lese rektovedtak](/arkivplan/vedlegg/rutiner/registrering/rutine-for-aa-soeke-etter-og-lese-rektovedtak.pdf) (vedlegg)

:::

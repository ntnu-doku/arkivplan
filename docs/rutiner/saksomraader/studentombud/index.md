---
date: '2021-09-20T00:00:00'
title: Studentombud
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

Fra 2. oktober 2017 kom NTNUs første studentombud på plass. [Les mer om studentombudet på NTNU.no/studentombud](https://www.ntnu.no/studentombud). Studentombudet er opprettet som egen administrativ enhet og egen tilgangskode SO benyttes for saker unntatt offentlighet i sak- og arkivsystemet.

Studentombudet har taushetsplikt etter forvaltningsloven §13 om forhold hen blir kjent med gjennom sin rolle som ombud. Studentombudet har bare innsyn i taushetsbelagte opplysninger i den grad studenten det gjelder har gitt samtykke til innsyn.

Spørsmål om innsyn i saker hos studentombudet følger reglene i offentlighetsloven.

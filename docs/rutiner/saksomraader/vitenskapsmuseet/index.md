---
date: '2022-03-11T00:00:00'
title: Vitenskapsmuseet
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

::: info Registreringsrutiner

[Vitenskapsmuseet (VM)](/arkivplan/vedlegg/rutiner/registrering/vitenskapsmuseet-v3.1.0.pdf) (vedlegg)

:::

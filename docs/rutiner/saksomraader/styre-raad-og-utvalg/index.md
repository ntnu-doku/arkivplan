---
date: '2021-09-20T00:00:00'
title: 'Styre, råd og utvalg'
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

Dokumentasjon av virksomheten for styrer, råd og utvalg (SRU) av formell karakter journalføres og arkiveres. Eksempler på SRU er:

- Styret NTNU

- Fakultetsstyre og -råd,

- Instituttstyre og -råd,

- Ansettelsesråd og -utvalg

- Forskningsutvalg

- Studieutvalg

- Undervisningsutvalg

- Arbeidsutvalg

- Fagråd

- Programråd

- Samarbeidsorgan

Se egen oversikt over [NTNUs styre, utvalg og råd på Innsida](https://innsida.ntnu.no/raad-og-utvalg).

Referater, protokoller, vedtak og lignende før innføringen av digitalt sak- og arkivsystem i 2004/2005 er i all hovedsak arkivert på papir, og deretter i hovedsak digitalt. Fra og med 2012 skal dette materialet være digitalisert. Arkivering er utført enten som én saksmappe per SRU for en gitt periode eller som én saksmappe for hvert møte.

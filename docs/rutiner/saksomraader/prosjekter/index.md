---
date: '2021-09-20T00:00:00'
title: Ekstern- og internfinansierte prosjekter
status: Ferdig
category: Rutine
author: Geir Ekle
---

Prosjekteier vil som hovedregel være institutt eller fakultet. Saksbehandler og eller lokal prosjektstøtte tilknytter saksdokumenter i prosjektsaken. DOKU har arkivfaglig oppfølging.

Ekstern- og internfinansierte prosjekter kan deles inn i tre faser:

- Etablering

- Gjennomføring

- Avslutning

I disse tre fasene vil det pågå både vitenskapelig – faglig – og rent administrative aktiviteter. Fasene skaper ulike typer saksdokumenter som skal journalføres og arkiveres.

Dette gjelder også prosjektets etableringsfase. Egne saksmapper med søknader og tilhørende korrespondanse opprettes i sak- og arkivsystemet. I saksmappene skal man kunne finne:

- Søknader og anbud

- Prosjektklassifisering – vurdering av hvilken type prosjekt, bidragsfinansiert eller oppdragsfinansiert

- Notater rundt søknadsprosess

- Korrespondanse, inklusive e-post som har betydning for søknadsprosessen

- Svar på søknad

- Revidert søknad

- Svar på søknad, avslag på søknad

I prosjektets gjennomførings- og avslutningsfase skal blant annet følgende dokumenter kunne gjenfinnes i saksmappen:

- Tilsagnsbrev, innvilget søknad

- Underskrevet kontrakt

- Samarbeidskontrakter med andre enheter eller institusjoner

- Prosjektbudsjett

- Korrespondanse til eller fra oppdragsgiver som omhandler rettigheter plikter og kontraktsforhold

- Prosjektendringer

- Framdriftsrapporter

- Årsrapporter

- Sluttrapport

::: advarsel Merk

Regnskapsmateriale skal ikke arkiveres i sak- og arkivsystemet.

:::

Ansettelsessaker som blir til på grunnlag av påbegynt prosjekt registreres som egne saker. Det er utarbeidet felles registreringsrutiner for hvordan ansettelsessaker skal registreres.

---
date: '2021-09-20T00:00:00'
title: Ph.d.-dokumentasjon
status: Ferdig
category: Rutine
tags:
  - Behandle
  - Standard
author: Ole Vik
---

Alle saker og dokumenter som gjelder en doktorgradskandidat – en ph.d.-kandidat ved NTNU – skal kunne søkes fram via kandidatens navn i sak- og arkivsystemet. For at dette prinsippet skal fungere, må kandidatens navn være registrert i tittel på saksmappe.

Det er utarbeidet felles registreringsrutiner for ph.d.-dokumentasjon som er tilpasset saksforholdet.

::: info Registreringsrutiner

Kandidatmapper opprettes ved det fakultet der kandidaten har fått opptak til ph.d.-programmet – eventuelt skal bedømmes for graden (dr.philos). Personer ansatt som stipendiat ved NTNU vil ha både en kandidatmappe og en personalmappe, mens eksternfinansierte ph.d.-kandidater kun vil ha en kandidatmappe.

I kandidatmappene samler man det meste av dokumentasjon som angår den forskningsmessige delen av ph.d.-programmet. Eksempler på dokumentasjon som skal i kandidatmappen (listen er ikke uttømmende):

- Søknad om opptak – med eventuelt vedlegg

- Instituttets faglige vurdering

- Opptak og avslag

- Ph.d.-avtale

- Veilederrapport

- Framdriftsrapport

- Søknad om å få avhandlingen bedømt

- Medforfattererklæringer

- Bedømmelseskomiteens vurdering av avhandlingen

- Kunngjøring av disputas

- Bedømmelseskomiteens evaluering av prøveforelesning og disputas

- Kreering – tildeling av grad

Dersom det skal avholde et ph.d.-kurs, er dette en del av virksomheten som må dokumenteres. Invitasjonen som sendes ut skal arkiveres, og i tillegg skal kursprogrammet – pensumlisten – og eventuelle utstedte kursbevis arkiveres.

[Ph.d-dokumentasjon](/arkivplan/vedlegg/rutiner/registrering/phd.pdf) (vedlegg)

:::

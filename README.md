# NTNU Arkivplan

**For bidrag til eller spørsmål om innholdet på sidene, [kontakt DOKU](https://www.ntnu.no/adm/auv/ansatte-og-tjenester) direkte.**

Publiseringsløsning for NTNUs Arkivplan. Bruker [VuePress v1](https://v1.vuepress.vuejs.org/guide/) til publisering, Node til bygging. Se også [dokumentet om bidrag](./CONTRIBUTiNG.md).

## Bruk

Automasjonen som gjør mapper og filer om til publiseringsformat bruker [Node.js](https://nodejs.org/en/) og en rekke kommandoer som kan kjøres manuelt fra kommandolinjen i et terminalvindu. Systemet kan bygge strukturen for publisering uten menneskelig innblanding, hvis det er satt opp i en løsning hvor kommandoene kan kjøres når endringer skjer. Det generelle formatet tar formen `node run /<kommando/>`; kjør bare `node run` for beskrivelser av hver kommando. Alle kommandoer kjøres fra den øverste mappen, hvor `package.json` befinner seg.

Node.js v18 og [Pandoc](https://pandoc.org/) v2.11.2 eller høyere må være installert og tilgjengelig for å bruke systemet, versjoner før disse vil trolig gi feilmeldinger. Status for hver operasjon rapporteres forløpende i terminalvinduet og logges i `/logs/task.log`.

### TLDR

1. Kjør `npm run kilde`, men lukk alle Word-dokumenter i `/Innhold` først
2. Kjør `git add --all` og `git commit -S -m "kort forklaring"`
3. Kjør `git push`

### Forberedelser

Installer nødvendige bibliotek og rammeverk, gjennom kommandoen `npm install`. Programmet npm – "Node Package Manager" – følger med Node.js. Dette installerer alle skript oppgitt i `package.json`, med de definerte versjonene, til `/node_modules`.

### Innhold

Generer publiseringsformat i samme mappestruktur som mappen /Innhold har, hvor Word-filer omgjøres til Markdown-format, og andre filer kopieres. `node run innhold` gjør følgende:

1.  Finner all filer i /Innhold

2.  Itererer gjennom disse og avgjør

    a. Hvor publiseringsformat lagres: Et mappenavn en nettleser kjenner igjen

    b. Hvilken filtype det er: Bare .docx-filer konverteres

    c. Hva filnavnet er: Dette brukes som tittel hvis den ikke er angitt i dokumentet

3.  Hvis det er en .docx-fil lages en sti under /docs

4.  Hvis filen kan leses hentes metadata og innhold ut, tilpasses og lagres i Markdown-format som index.md under angitt sti

5.  Hvis det er en annen filtype kopieres den til angitt sti

Når prosessen er gjennomført vil alle filer under `/Innhold` ha en mappe under `/docs`, av samme navn, men tilpasset nettlesere.

### Indeks

Bygger en indeks av innholdet for bruk til søking. Lagres som `/docs/.vuepress/public/index.json` for enkel tilgang internt og eksternt, i JSON-format. Denne filen inneholder en kopi av alt tekstlig innhold og metadata.

### Backup

Lager en sikkerhetskopi av `/Innhold`, gjennom `node run backup`. Disse lagres i `/backup` som .zip-filer, gjennom komprimeringsløsningen 7-Zip. Filene navngis etter formatet yyyy-MM-dd_HH-mm-ss, for eksempel "2020-12-16_10-49-00.zip". Dette er en type overflødig sikkerhetskopi, da alle mapper og filer allerede har sikkerhetskopier og versjoner via SharePoint.

### Oppvask

Sletter alle genererte filer i `/docs`, gjennom `node run oppvask`. Dette inkluderer alle mapper og filer foruten `index.md`, `endringslogg.md`, `kategorier.md`, `emneknagger.md`, `package.json`, `package-lock.json`, `/.vuepress`, `/node_modules`. Disse unntakene er systemfiler eller automatisk generert innhold, nødvendig for publiseringsløsningen.

### PDF

Lager en PDF-kopi av arkivplanen i `/docs/.vuepress/public/utskrift.pdf`, samt en PDF-kopi av samlede PDF-vedlegg i `/docs/.vuepress/public/vedlegg.pdf`, gjennom `node run pdf`.

#### Tester

Disse kjører som `npm run` kommandoer;

- `test:links` sjekker om det er døde lenker i Arkivplanen og lagrer analysen under `tests/data/links.txt`
- `test:links-live` sjekker om det er døde lenker i [Arkiplanen live](https://ntnu.no/arkivplan) og lagrer analysen under `tests/data/links.txt`
- `test:search` analyserer effekten av søkmotorene som er implementert, og lagrer under `tests/data/search.txt`

Andre tester under `/tests` kjøres direkte i Node.js.

#### Lesbarhet

Gjennomfører en lesbarhetsanalyse av hvert dokument, som lagres med samme sti som innhold men under `/docs/.vuepress/public/lesbarhet`, og en samlet indeks i `/docs/.vuepress/public/readability.index.json`. Oppgaven kjøres asynkront på flere dokumenter samtidig, med løpende rapportering av akkumulert tid. **Merk**: Dette er en CPU-intensiv prosess, og vil bruke flere minutter på enkelte dokumenter.

#### Lesbarhetsrapport

Bruker data fra samlet indeks i `/docs/.vuepress/public/readability.index.json` til å lage en samletabell med lenke til hver rapport, navn på og lenke til side, beregnet klassetrinn, alder, lesetid og lengde beregnet i A4-sider.

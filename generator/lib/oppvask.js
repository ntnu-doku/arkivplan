const Logger = require("./logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync");
const start = performance.now();
const logger = new Logger();

/**
 * Clean /docs
 */
class Oppvask {
  /**
   * Remove generated files and folders
   */
  static process() {
    const files = walker("./docs", {
      fs: FS,
      depthLimit: 0,
      filter: (file) => {
        if (
          ![
            "index.md",
            "endringslogg.md",
            "kategorier.md",
            "emneknagger.md",
            "lenker.md",
            "package.json",
            "package-lock.json",
            ".vuepress",
            "node_modules",
          ].includes(path.basename(file.path))
        ) {
          return true;
        }
        return false;
      },
    });
    files.forEach((file, i) => {
      try {
        if (FS.existsSync(file.path)) {
          FS.rmSync(file.path, { recursive: true });
        }
        logger.entry({
          source: file.path,
          type: "deleted",
          start: start,
          i: i,
          n: files.length,
          store: true,
        });
      } catch (error) {
        logger.trace(error);
      }
    });
    this.removeIndex();
    this.removeAttachments();
  }

  /**
   * Remove /docs/.vuepress/public/index.js
   */
  static removeIndex() {
    if (!FS.existsSync("./docs/.vuepress/public/index.json")) {
      return;
    }
    try {
      FS.rmSync("./docs/.vuepress/public/index.json", { recursive: true });
      logger.entry({
        source: "./docs/.vuepress/public/index.json",
        type: "deleted",
        store: true,
      });
    } catch (error) {
      logger.trace(error);
    }
  }

  /**
   * Remove /docs/.vuepress/public/vedlegg
   */
  static removeAttachments() {
    if (!FS.existsSync("./docs/.vuepress/public/vedlegg")) {
      return;
    }
    try {
      FS.rmSync("./docs/.vuepress/public/vedlegg", { recursive: true });
      logger.entry({
        source: "./docs/.vuepress/public/vedlegg",
        type: "deleted",
        store: true,
      });
    } catch (error) {
      logger.trace(error);
    }
  }
}

module.exports = Oppvask;

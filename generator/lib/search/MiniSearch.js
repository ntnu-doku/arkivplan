const Search = require("./Search"),
  MiniSearchEngine = require("minisearch/dist/umd/index");

/**
 * @extends Search
 */
class MiniSearch extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    let engine = new MiniSearchEngine({
      idField: "path",
      fields: keys,
      searchOptions: {
        prefix: true,
        fuzzy: 0.35,
      },
      extractField: (document, fieldName) => {
        if (
          fieldName === "tags" &&
          document.hasOwnProperty("tags") &&
          document["tags"].length > 0
        ) {
          return document["tags"].join(" ");
        }
        return document[fieldName];
      },
      processTerm: function(term, _fieldName) {
        const stopwords = require(`stopwords-json/dist/no.json`);
        if (stopwords.includes(term)) {
          return false;
        }
        return term.toLowerCase().replace(/([a-z])\1/g, "$1");
      },
    });
    engine.addAll(content);
    return {
      engine: engine,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: "stopwords",
    };
  }

  /**
   * @inheritdoc
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = engine.search(query);
    data = data
      .map((item) => ({
        path: item.id,
        score: item.score,
        item: originals ? content.find((o) => o.path === item.id) : null,
        matches: matches ? item.match : null,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      time: Search.now() - start,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      if (
        !data[i].hasOwnProperty("matches") ||
        Object.keys(data[i].matches).length < 1
      ) {
        continue;
      }
      for (const [match, properties] of Object.entries(data[i].matches)) {
        for (let n = 0; n < properties.length; n++) {
          result[data[i].path][properties[n]] = Search.highlight({
            content: data[i].item[properties[n]],
            query: [match],
            ansi: ansi,
          });
        }
      }
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = MiniSearch;

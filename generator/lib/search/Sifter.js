const Search = require("./Search"),
  SifterEngine = require("sifter");

class Sifter extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({
    content = [],
    keys = [],
    query = "",
    originals = false,
    matches = false,
    limit = 5,
    highlight = false,
    ansi = false,
  } = {}) {
    const start = Search.now();
    let engine = new SifterEngine(content);
    let data = engine.search(query, {
      fields: keys,
    });
    const tokens = data.tokens;
    data = data.items
      .map((item) => ({
        path: content[item.id].path,
        score: item.score,
        item: originals ? content[item.id] : null,
      }))
      .slice(0, limit);
    return {
      score: true,
      fuzzy: false,
      time: Search.now() - start,
      locale: false,
      data: {
        results: data,
        count: `${data.length}/${content.length}`,
        time: 0,
      },
      highlit: highlight
        ? Sifter.matcher({
            data: data,
            query: tokens,
            ansi: ansi,
          })
        : null,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    let queries = [];
    let regexps = [];
    for (let i = 0; i < query.length; i++) {
      queries.push(query[i].string);
      regexps.push(query[i].regex);
    }
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      for (const [key, value] of Object.entries(data[i].item)) {
        let queries = [];
        for (let n = 0; n < regexps.length; n++) {
          const re = new RegExp(regexps[n].source, "ig");
          let reArray = [];
          while ((reArray = re.exec(String(value))) !== null) {
            queries.push(reArray[0]);
          }
        }
        if (queries.length > 0) {
          result[data[i].path][key] = Search.highlight({
            content: String(value),
            query: queries,
            ansi: ansi,
          });
        }
      }
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = Sifter;

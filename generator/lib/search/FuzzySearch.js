const Search = require("./Search"),
  FuzzySearchEngine = require("fz-search/dist/FuzzySearch");

/**
 * @extends Search
 */
class FuzzySearch extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    let engine = new FuzzySearchEngine({
      source: content,
      keys: keys,
      output_map: "root",
      score_test_fused: true,
      score_acronym: true,
      score_round: 0.025,
      thresh_include: 5,
    });
    return {
      engine: engine,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: false,
    };
  }

  /**
   * @inheritdoc
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = engine.search(query);
    data = data
      .map((item) => ({
        path: item.item.path,
        score: item.score,
        item: originals ? item.item : null,
        matches: matches ? engine.getMatchingField(item) : null,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      time: Search.now() - start,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!data[i].hasOwnProperty("matches")) {
        continue;
      }
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      result[data[i].path]["best"] = Search.highlight({
        content: data[i].matches,
        query: [query],
        ansi: ansi,
      });
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = FuzzySearch;

const Search = require("./Search"),
  FuseEngine = require("fuse.js/dist/fuse.basic.js");

/**
 * @extends Search
 */
class Fuse extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    let engine = new FuseEngine(content, {
      includeScore: true,
      includeMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      keys: keys,
    });
    return {
      engine: engine,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: false,
    };
  }

  /**
   * @inheritdoc
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = engine.search(query);
    data = data
      .map((item) => ({
        path: item.item.path,
        score: item.score,
        item: originals ? content.find((o) => o.path === item.item.path) : null,
        matches: matches ? item.matches : null,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: false,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      if (
        !data[i].hasOwnProperty("matches") ||
        data[i].matches === null ||
        data[i].matches.length < 1
      ) {
        continue;
      }
      for (let n = 0; n < data[i].matches.length; n++) {
        if (
          !data[i].matches[n].hasOwnProperty("indices") ||
          data[i].matches[n].indices.length < 1 ||
          !data[i].matches[n].hasOwnProperty("value") ||
          !data[i].matches[n].hasOwnProperty("key")
        ) {
          continue;
        }
        let query = [];
        for (let m = 0; m < data[i].matches[n].indices.length; m++) {
          query.push(
            data[i].matches[n].value.substring(
              data[i].matches[n].indices[m][0],
              data[i].matches[n].indices[m][1]
            )
          );
        }
        result[data[i].path][data[i].matches[n].key] = Search.highlight({
          content: data[i].item[data[i].matches[n].key],
          query: query,
          ansi: ansi,
        });
      }
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = Fuse;

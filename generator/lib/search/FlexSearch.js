const Search = require("./Search"),
  FlexSearchEngine = require("flexsearch");

/**
 * @extends Search
 */
class FlexSearch extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    let engine = new FlexSearchEngine({
      encode: "extra",
      tokenize: "full",
      threshold: 0,
      async: false,
      worker: false,
      cache: false,
      doc: {
        id: "path",
        field: keys,
      },
    });
    engine.add(content);
    return {
      engine: engine,
      score: false,
      fuzzy: false,
      time: Search.now() - start,
      locale: false,
    };
  }

  /**
   * @inheritdoc
   */
  static search({ engine = {}, query = "", content = [], limit = 5 } = {}) {
    const start = Search.now();
    let data = engine.search(query);
    data = data
      .map((item) => ({
        path: item.path,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      time: Search.now() - start,
    };
  }
}

module.exports = FlexSearch;

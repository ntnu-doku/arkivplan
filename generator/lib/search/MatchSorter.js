const Search = require("./Search"),
  { matchSorter: MatchSorterEngine } = require("match-sorter");

/**
 * @extends Search
 */
class MatchSorter extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({
    content = [],
    keys = [],
    query = "",
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = MatchSorterEngine(content, query, { keys: keys });
    data = data
      .map((item) => ({
        path: item.path,
      }))
      .slice(0, limit);
    return {
      score: false,
      fuzzy: false,
      time: Search.now() - start,
      locale: false,
      data: {
        results: data,
        count: `${data.length}/${content.length}`,
        time: 0,
      },
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      for (const [key, value] of Object.entries(data[i])) {
        result[data[i].path][key] = Search.highlight({
          content: value,
          query: [query],
          ansi: ansi,
        });
      }
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = MatchSorter;

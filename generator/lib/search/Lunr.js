const Search = require("./Search"),
  LunrEngine = require("lunr");
require("lunr-languages/lunr.stemmer.support")(LunrEngine);
require("lunr-languages/lunr.no")(LunrEngine);

/**
 * @extends Search
 */
class Lunr extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    let engine = LunrEngine(function() {
      this.use(LunrEngine.no);
      this.metadataWhitelist = ["position"];
      this.ref("path");
      for (let i = 0; i < keys.length; i++) {
        this.field(keys[i]);
      }
      for (let i = 0; i < content.length; i++) {
        this.add(content[i]);
      }
    });
    return {
      engine: engine,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: "full",
    };
  }

  /**
   * @inheritdoc
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = engine.query(function(q) {
      q.term(query, {
        usePipeline: false,
        editDistance: 3,
      });
    });
    data = data
      .map((item) => ({
        path: item.ref,
        score: item.score,
        item: originals ? content.find((o) => o.path === item.ref) : null,
        matches: matches ? item.matchData : null,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      time: Search.now() - start,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      if (
        !data[i].hasOwnProperty("matches") ||
        !data[i].matches.hasOwnProperty("metadata")
      ) {
        continue;
      }
      let query = {};
      for (const [key, values] of Object.entries(data[i].matches.metadata)) {
        for (const [property, value] of Object.entries(values)) {
          if (
            !value.position ||
            value.position.length < 1 ||
            typeof value.position[0] === "undefined"
          ) {
            continue;
          }
          if (!query.hasOwnProperty(property)) {
            query[property] = [];
          }
          query[property].push(
            data[i].item[property].substring(
              value.position[0][0],
              value.position[0][1]
            )
          );
        }
        for (const [property, queries] of Object.entries(query)) {
          result[data[i].path][property] = Search.highlight({
            content: data[i].item[property],
            query: queries,
            ansi: ansi,
          });
        }
      }
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = Lunr;

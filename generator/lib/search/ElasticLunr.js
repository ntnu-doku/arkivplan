const Search = require("./Search"),
  ElasticLunrEngine = require("elasticlunr-idream");
require("../../local_modules/elasticlunr-languages/lunr.stemmer.support")(
  ElasticLunrEngine
);
require("../../local_modules/elasticlunr-languages/lunr.no")(ElasticLunrEngine);

/**
 * @extends Search
 */
class ElasticLunr extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    const stopwords = require(`stopwords-json/dist/no.json`);
    ElasticLunrEngine.clearStopWords();
    ElasticLunrEngine.addStopWords(stopwords);
    let engine = ElasticLunrEngine(function() {
      // this.use(ElasticLunrEngine.no);
      for (let i = 0; i < keys.length; i++) {
        this.addField(keys[i]);
      }
    });
    for (let i = 0; i < content.length; i++) {
      content[i].id = i;
      if (content[i].hasOwnProperty("tags") && Array.isArray(content[i].tags)) {
        content[i].tags = content[i].tags.join(" ");
      }
      engine.addDoc(content[i]);
    }
    return {
      engine: engine,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: "stopwords",
    };
  }

  /**
   * @inheritdoc
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = engine.search(query);
    data = data
      .map((item) => ({
        path: content[item.ref].path,
        score: item.score,
        item: originals ? content[item.ref] : null,
        matches: matches ? item.positions : null,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      time: Search.now() - start,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      if (!data[i].hasOwnProperty("matches")) {
        continue;
      }
      for (const [key, values] of Object.entries(data[i].matches)) {
        let queries = [];
        for (let n = 0; n < values.length; n++) {
          queries.push(data[i].item[key].substring(values[n][0], values[n][1]));
          result[data[i].path][key] = Search.highlight({
            content: data[i].item[key],
            query: queries,
            ansi: ansi,
          });
        }
      }
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = ElasticLunr;

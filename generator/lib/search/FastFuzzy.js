const Search = require("./Search"),
  { Searcher: FastFuzzyEngine } = require("fast-fuzzy");

/**
 * @extends Search
 */
class FastFuzzy extends Search {
  /**
   * @inheritdoc
   */
  static bootstrap({ content = [], keys = [] } = {}) {
    const start = Search.now();
    let engine = new FastFuzzyEngine(content, {
      keySelector: function(item) {
        let result = [];
        for (let i = 0; i < keys.length; i++) {
          if (keys[i] === "tags" && Array.isArray(item[keys[i]])) {
            item[keys[i]] = item[keys[i]].join(" ");
          }
          result.push(item[keys[i]]);
        }
        return result;
      },
      returnMatchData: true,
    });
    return {
      engine: engine,
      score: true,
      fuzzy: true,
      time: Search.now() - start,
      locale: false,
    };
  }

  /**
   * @inheritdoc
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {
    const start = Search.now();
    let data = engine.search(query);
    data = data
      .map((item) => ({
        path: item.item.path,
        score: item.score,
        item: originals ? item.item : null,
        matches: matches
          ? Object.assign(item.match, { original: item.original })
          : null,
      }))
      .slice(0, limit);
    return {
      results: data,
      count: `${data.length}/${content.length}`,
      time: Search.now() - start,
    };
  }

  /**
   * @inheritdoc
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {
    const start = Search.now();
    if (data.length < 1) {
      return null;
    }
    let result = {};
    for (let i = 0; i < data.length; i++) {
      if (!result.hasOwnProperty(data[i].path)) {
        result[data[i].path] = {};
      }
      if (
        !data[i].hasOwnProperty("matches") ||
        Object.keys(data[i].matches).length < 1 ||
        !data[i].matches.hasOwnProperty("original") ||
        data[i].matches.original === ""
      ) {
        continue;
      }
      const key = Object.keys(data[i].item).find(
        (key) => data[i].item[key] === data[i].matches.original
      );
      result[data[i].path][key] = Search.highlight({
        content: data[i].item[key],
        query: [
          data[i].item[key].substring(
            data[i].matches.index,
            data[i].matches.index + data[i].matches.length
          ),
        ],
        ansi: ansi,
      });
    }
    return { matches: result, time: Search.now() - start };
  }
}

module.exports = FastFuzzy;

const SearchContext = require("../../local_modules/search-context"),
  Collapse = require("collapse-white-space");

/**
 * @interface
 */
class Search {
  /**
   * Instantiate search engine, populate data
   * @param {object} params
   * @param {array} params.content Array of objects to search
   * @param {array} params.keys Array of field-names
   * @param {string} params.query Term to search for
   * @param {boolean} params.debug Enter debug-mode, with extended data
   * @param {number} params.limit Hard limit on results
   * @returns {object} Search-engine interface
   * @returns {object} .data Engine
   * @returns {number} .time Time spent to perform build engine
   *
   * @abstract
   */
  static bootstrap({ content = [], keys = [] } = {}) {}

  /**
   * Perform search
   * @param {object} params
   * @param {array} params.content Array of objects to search
   * @param {array} params.keys Array of field-names
   * @param {string} params.query Term to search for
   * @param {boolean} params.debug Enter debug-mode, with extended data
   * @param {number} params.limit Hard limit on results
   * @returns {object} Search-results
   * @returns {object} .results Content matching the search-term
   * @returns {number} .count Amount of results
   * @returns {boolean} .score Whether results are comparatively scored
   * @returns {boolean} .fuzzy Whether test is typo-tolerant
   * @returns {number} .time Time spent to perform search
   *
   * @abstract
   */
  static search({
    engine = {},
    query = "",
    content = [],
    originals = false,
    matches = false,
    limit = 5,
  } = {}) {}

  /**
   * From given matches, highlight results
   * @param {object} params
   * @param {array} params.data Array of objects with search-data
   * @param {string} params.query Term searched for
   * @param {boolean} params.ansi Mark highlights with color for console
   * @returns {object} Highlit search-results
   * @returns {object} .matches[path] Keyed on path
   * @returns {object} .matches[path][property] Highlighted result
   * @returns {number} .time Time spent to perform highlighting
   */
  static matcher({ data = [], query = "", ansi = false } = {}) {}

  /**
   * Highlight text in context
   * @param {string} content Text to highlight
   * @param {array} query Terms to search for
   * @returns {string} Highlit results
   */
  static highlight({ content = "", query = [], ansi = false } = {}) {
    return SearchContext(Collapse(content), query, 80, function(string) {
      if (ansi) {
        return require("chalk").black.bgCyan(string);
      }
      return `<mark>${string}</mark>`;
    });
  }

  /**
   * Get current time in milliseconds
   *
   * @returns {number} Milliseconds since process started
   */
  static now() {
    return require("performance-now")();
  }
}

module.exports = Search;

const Logger = require("./logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  replaceAll = require("string.prototype.replaceall"),
  fileExtension = require("file-extension"),
  Matter = require("gray-matter"),
  { markdownToTxt } = require("markdown-to-txt");

replaceAll.shim();
const start = performance.now();
const logger = new Logger();

/**
 * Create an index of content and metadata
 */
class Indeks {
  /**
   * Find all Markdown-files in /docs and store them in /docs/.vuepress/public/index.json
   */
  static process() {
    const base = path.resolve("./");
    const target = `${base}/docs/.vuepress/public/index.json`
      .replace(base, "")
      .replaceAll("\\", "/");
    const files = walker(`${base}/docs`, {
      fs: FS,
      filter: (file) => {
        if (
          ![
            ".vuepress",
            "kategorier",
            "emneknagger",
            "node_modules",
            "package.json",
            "package-lock.json",
            "pnpm-lock.yaml",
          ].includes(path.basename(file.path))
        ) {
          return true;
        }
        return false;
      },
    });
    let index = [];
    files.forEach((file, i) => {
      if (fileExtension(file.path) !== "md") {
        return;
      }
      const relativePath = file.path.replace(base, "").replaceAll("\\", "/");
      let indexData = {};
      let URL = file.path
        .replace(base, "")
        .replace(path.basename(file.path), "")
        .replaceAll("\\", "/")
        .replace("/docs", "");
      if (URL !== "") {
        indexData.path = URL;
      }
      try {
        const fileContents = FS.readFileSync(file.path, { encoding: "utf8" });
        const parsed = Matter(fileContents);
        if (
          Object.keys(parsed.data).length < 1 ||
          Object.keys(parsed.content).length < 1 ||
          parsed.data.status !== "Ferdig"
        ) {
          return;
        }
        if (Object.keys(parsed.data).length > 0) {
          Object.assign(indexData, parsed.data);
        }
        if (Object.keys(parsed.content).length > 0) {
          Object.assign(indexData, { content: markdownToTxt(parsed.content) });
        }
        if (!["/docs/index.md", "/docs/CHANGELOG.md"].includes(relativePath)) {
          index.push(indexData);
        }
      } catch (error) {
        logger.trace(error);
      }
    });
    if (!FS.existsSync(`${base}/docs/.vuepress/public`)) {
      FS.mkdirSync(`${base}/docs/.vuepress/public`, { recursive: true });
    }
    FS.writeFileSync(`./${target}`, JSON.stringify(index, null, 2));
    logger.entry({
      message: `${target}`,
      source: "indeks",
      type: "saved",
      start: start,
      store: true,
    });
  }
}

module.exports = Indeks;

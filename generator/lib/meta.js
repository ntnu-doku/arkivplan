const Logger = require("./logger"),
  path = require("path"),
  fileExtension = require("file-extension"),
  { DateTime } = require("luxon"),
  AdmZip = require("adm-zip"),
  parser = require("xml-js"),
  YAML = require("js-yaml"),
  APP = require("./schema/app.json"),
  CORE = require("./schema/core.json"),
  CUSTOM = require("./schema/custom.json"),
  COVERPAGE = require("./schema/cover_page.json");

const logger = new Logger();

/**
 * Handle metadata in Microsoft Word document
 */
class Meta {
  /**
   * Get metadata from Microsoft Word document
   * @param {string} filename Path to file
   * @param {boolean} debug Output verbose debugging information
   * @returns {object}
   */
  static process({ filename: filename, debug: debug = false }) {
    if (debug) logger.entry({ message: "Meta.process()", source: filename });
    let metadata = Meta.getData({ filename: filename, debug: debug });
    if (Meta.verifyData(metadata)) {
      return { status: Meta.verifyData(metadata), content: "" };
    }
    metadata = Meta.filterData(metadata);
    if (
      path.basename(filename) ===
      "Bevarings- og kassasjonsplan for UH-sektoren.docx"
    ) {
      metadata.layout = "FUP";
    }
    let status = metadata.status.trim().toUpperCase();
    if (debug) logger.debug(`Meta.process() STATUS ${status}`);
    if (debug)
      logger.debug(
        `Meta.process() METADATA\n${JSON.stringify(metadata, null, 2)}`
      );
    metadata = YAML.safeDump(metadata);
    if (debug) logger.debug(`Meta.process() YAML\n${metadata}`);
    return { status: status, content: metadata };
  }

  /**
   * Find relevant metadata in the document
   * @param {string} filename Filename
   */
  static getData({ filename: filename, debug = false }) {
    let data = {};
    try {
      const zip = new AdmZip(filename);
      const zipEntries = zip.getEntries();
      for (const zipEntry of zipEntries) {
        if (
          [
            "docProps/core.xml",
            "docProps/app.xml",
            "docProps/custom.xml",
            "customXml/item1.xml",
          ].includes(zipEntry.entryName)
        ) {
          let props;
          const xml = zipEntry.getData().toString();
          const json = parser.xml2js(xml, {
            compact: true,
            trim: true,
            nativeType: true,
            ignoreDeclaration: true,
            spaces: 4,
          });
          if (zipEntry.entryName == "docProps/app.xml") {
            props = Meta.getDocumentProperties(json, APP);
          } else if (zipEntry.entryName == "docProps/core.xml") {
            props = Meta.getDocumentProperties(json, CORE);
          } else if (zipEntry.entryName == "docProps/custom.xml") {
            props = Meta.getDocumentProperties(json, CUSTOM);
          } else if (zipEntry.entryName == "customXml/item1.xml") {
            props = Meta.getDocumentProperties(json, COVERPAGE);
          }
          Object.assign(data, props);
          if (!data.hasOwnProperty("title")) {
            const extension = fileExtension(filename);
            const basename = path.basename(filename, `.${extension}`);
            data.title = basename;
          }
        }
      }
    } catch (error) {
      logger.trace(error);
    }
    return data;
  }

  /**
   * Find relevant properties in metadata from predefined schema
   * @param {object} obj Metadata
   * @param {object} props Schema
   * @returns {object}
   */
  static getDocumentProperties(obj, props) {
    let data = {};
    props.forEach((prop) => {
      let value;
      let path = null;
      let property = null;
      if (obj.hasOwnProperty(prop.path[0])) {
        path = prop.path[0];
      } else if (obj.hasOwnProperty(prop.path[0].replace(/^.*:/m, ""))) {
        path = prop.path[0].replace(/^.*:/m, "");
      }
      if (path === null) {
        return;
      }
      if (obj[path].hasOwnProperty(prop.path[1])) {
        property = prop.path[1];
      } else if (obj[path].hasOwnProperty(prop.path[1].replace(/^.*:/m, ""))) {
        property = prop.path[1].replace(/^.*:/m, "");
      }
      if (Array.isArray(obj[path][property])) {
        let search = obj[path][property].find((item) => {
          return item["_attributes"].name === prop.name;
        });
        data[prop.name.toLowerCase()] = search["vt:lpwstr"]["_text"];
      }
      if (
        property === null ||
        !prop.hasOwnProperty("path") ||
        !obj[path].hasOwnProperty(property) ||
        !obj[path][property].hasOwnProperty("_text")
      ) {
        return;
      }
      switch (prop.type) {
        case "number":
          value = Number(obj[path][property]._text);
          break;
        case "string":
          value = String(obj[path][property]._text);
        default:
          value = String(obj[path][property]._text);
      }
      data[prop.name] = value;
    });
    return data;
  }

  /**
   * Filter metadata
   * @param {object} obj Metadata
   * @returns {object}
   */
  static filterData(obj) {
    let data = { date: "" };
    if (obj.hasOwnProperty("title") && obj.title !== "") {
      data.title = obj.title;
    }
    if (
      obj.hasOwnProperty("publishDate") &&
      obj.publishDate !== "DATO" &&
      obj.publishDate !== ""
    ) {
      data.date = obj.publishDate;
    } else if (obj.hasOwnProperty("modified") && obj.modified !== "") {
      data.date = obj.modified;
    }
    if (!data.hasOwnProperty("date") || data.date == "") {
      data.date = DateTime.local().toISO();
    }
    if (obj.hasOwnProperty("status")) {
      data.status = obj.status;
    } else {
      data.status = "STATUS";
    }
    if (obj.hasOwnProperty("category") && obj.category !== "KATEGORI") {
      data.category = obj.category;
    }
    if (obj.hasOwnProperty("keywords") && obj.keywords !== "EMNEKNAGGER") {
      data.tags = obj.keywords.replace(/\s/g, "").split(",");
    }
    if (obj.hasOwnProperty("createdBy") || obj.hasOwnProperty("modifiedBy")) {
      if (obj.createdBy !== "FORFATTER" && obj.createdBy !== "") {
        data.author = obj.createdBy;
      } else if (obj.modifiedBy !== "FORFATTER" && obj.modifiedBy !== "") {
        data.author = obj.modifiedBy;
      } else {
        data.author = "DOKU";
      }
    }
    if (obj.hasOwnProperty("destination") && obj.destination !== "") {
      data.redirect = obj.destination;
    }
    return data;
  }

  /**
   * Verify that metadata contains given properties
   * @param {object} obj Metadata
   * @returns {string|boolean} String on failure, false on success
   */
  static verifyData(obj) {
    let result = [];
    if (!obj.hasOwnProperty("publishDate")) {
      result.push("publishDate");
    }
    if (!obj.hasOwnProperty("createdBy")) {
      result.push("createdBy");
    }
    if (!obj.hasOwnProperty("status")) {
      result.push("status");
    }
    if (!obj.hasOwnProperty("category")) {
      result.push("category");
    }
    if (result.length > 0) {
      return "MANGLER metadata." + result.join(",");
    }
    return false;
  }
}

module.exports = Meta;

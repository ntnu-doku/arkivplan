const log4js = require("log4js"),
  chalk = require("chalk"),
  stripAnsi = require("strip-ansi"),
  { DateTime } = require("luxon"),
  { msToTime } = require("./utilities"),
  { performance } = require("perf_hooks"),
  createCallsiteRecord = require("callsite-record");

/**
 * Log to console and file
 * @example
 * // Logs `[2021-01-14T16:34:16.071] [INFO] - Hi!`
 * logger.entry({ message: "Hi!" })
 * @example
 * // Logs `[2021-01-14T16:34:16.076] [DEBUG] - Debug! (C:\...\lib\logger.js:209:24)`
 * logger.entry({ message: "Debug!", level: "debug" })
 * @example
 * // Logs `[2021-01-14T16:34:16.077] [TRACE] - Trace!\n[Call Site/Stack Trace]`
 * logger.entry({ message: "Trace!", level: "trace" })
 */
class Logger {
  /**
   * Instantiate the class
   */
  constructor() {
    log4js.configure({
      appenders: {
        file: {
          type: "file",
          filename: "./logs/task.log",
          encoding: "utf-8",
          maxLogSize: 100000,
          keepFileExt: true,
          layout: { type: "pattern", pattern: "[%d] [%p] - %m" },
        },
        console: {
          type: "console",
          layout: { type: "pattern", pattern: "[%d] %[[%p]%] - %m" },
        },
        debug: {
          type: "console",
          layout: {
            type: "pattern",
            pattern: chalk`[%d] %[[%p]%] - %m {gray (%f:%l:%o)}`,
          },
        },
        trace: {
          type: "console",
          layout: { type: "pattern", pattern: `[%d] %[[%p]%] - %m\n%[%s%]` },
        },
      },
      categories: {
        default: { appenders: ["console", "file"], level: "trace" },
        file: { appenders: ["file"], level: "trace" },
        console: {
          appenders: ["console"],
          level: "trace",
        },
        warn: {
          appenders: ["console"],
          level: "warn",
        },
        debug: {
          appenders: ["debug"],
          level: "trace",
          enableCallStack: true,
        },
        trace: {
          appenders: ["trace"],
          level: "trace",
          enableCallStack: true,
        },
      },
    });
    this.file = log4js.getLogger("file");
    this.console = log4js.getLogger("console");
    this.debugger = log4js.getLogger("debug");
    this.stacker = log4js.getLogger("trace");
  }

  /**
   * Handle errors in detail with call stack
   * @param {object} error Error-message to expand
   */
  trace(error) {
    console.error(
      error.message,
      createCallsiteRecord({ forError: error }).renderSync()
    );
  }

  /**
   * Formatted indicator of time
   * @param {number} start Millisecond timestamp to subtract from current timestamp
   * @returns {string} Color-formatted status
   */
  time(start) {
    return chalk`{white ${msToTime(performance.now() - start)}}`;
  }

  /**
   * Formatted indicator of iteration
   * @param {number} i Index in zero-indexed iteration
   * @param {number} n Amount of items in iteration
   * @returns {string} Color-formatted status
   */
  iterator(i, n) {
    return chalk`{white [${i + 1}/${n}]}`;
  }

  /**
   * Formatted indicator of what file was saved where
   * @param {string} source Filename or path to original
   * @param {string} target Filename or path to target location
   * @returns {string} Color-formatted status
   */
  saved(source, target) {
    return `Lagret ${chalk.cyan(source)} som ${chalk.magenta(target)}`;
  }

  /**
   * Formatted indicator of what file was copied where
   * @param {string} source Filename or path to original
   * @param {string} target Filename or path to target location
   * @returns {string} Color-formatted status
   */
  copied(source, target) {
    return `Kopierte ${chalk.cyan(source)} til ${chalk.magenta(target)}`;
  }

  /**
   * Formatted indicator of file status
   * @param {string} source Filename or path to original
   * @param {string} status File-status, preventing manipulation
   * @returns {string} Color-formatted status
   */
  skipped(source, status = null) {
    return `${chalk.magentaBright("Utelot")} ${chalk.cyan(source)} ${
      status || status
    }`;
  }

  /**
   * Formatted indicator of file status
   * @param {string} source Filename or path to original
   * @returns {string} Color-formatted status
   */
  deleted(source) {
    return `${chalk.redBright("Slettet")} ${chalk.cyan(source)}`;
  }

  /**
   * Formatted indicator of what is happening
   * @param {string} message Message to send
   */
  notice(message) {
    this.console.info(chalk.whiteBright(message));
  }

  /**
   * @param {string} message Message to post to warning-channel
   */
  warn(message) {
    this.console.warn(chalk.yellowBright(message));
    this.debugger.warn(message);
  }

  /**
   * @param {string} message Message to post to debug-channel
   */
  debug(message) {
    this.debugger.debug(message);
  }

  /**
   * @param {string} message Message to post to trace-channel
   */
  stack(message) {
    this.stacker.trace(message);
  }

  /**
   * Add an entry to the log
   *
   * All parameters are optional, operations fail silently
   * @param {object} params
   * @param {string} params.message Message to write
   * @param {string} params.source Filename or path to format and prefix
   * @param {string} params.level Log-level (debug|info|warn|error|fatal)
   * @param {string} params.type Type of operation (saved|copied|skipped|deleted|notice)
   * @param {number} params.start Millisecond timestamp of when operation started
   * @param {number} params.i Index in iteration
   * @param {number} params.n Amount of items in iteration
   * @param {string} params.store Store to file
   */
  entry({
    message = "",
    source = "",
    level = "info",
    type = "notice",
    start = 0,
    i = null,
    n = null,
    store = false,
  } = {}) {
    if (
      typeof level !== "string" ||
      level.length < 1 ||
      !["trace", "debug", "info", "warn", "error", "fatal"].includes(level)
    ) {
      throw new Error(
        "Entry.level must be a string of trace|debug|info|warn|error|fatal"
      );
    }
    if (
      typeof type !== "string" ||
      type.length < 1 ||
      !["saved", "copied", "skipped", "deleted", "notice"].includes(type)
    ) {
      throw new Error(
        "Entry.type must be a string of saved|copied|skipped|deleted|notice"
      );
    }
    if (typeof start !== "number") {
      throw new Error("Entry.start must be a positive number");
    }
    let output = "";
    if (Number(i) >= 0 && Number(n) >= 1) {
      output += `${this.iterator(Number(i), Number(n))} `;
    }
    if (typeof source === "string" && source.length > 0) {
      output += `${this[type](source, message)} `;
    } else if (typeof message === "string" && message.length > 0) {
      output += `${this[type](message)} `;
    }
    if (start > 0) {
      output = output.trim();
      output += `, tok ${this.time(start)}`;
    }
    output = output.trim();
    if (level === "debug") {
      this.debugger[level](message || source);
      return;
    }
    if (level === "trace") {
      this.stacker[level](message || source);
      return;
    }
    this.console[level](output);
    if (store) {
      this.file[level](stripAnsi(output));
    }
  }
}

module.exports = Logger;

const Logger = require("./logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  fileExtension = require("file-extension"),
  { spawn, Pool, Worker } = require("threads"),
  dayjs = require("dayjs"),
  { msToTime } = require("./utilities"),
  chalk = require("chalk"),
  jsdom = require("jsdom"),
  Matter = require("gray-matter"),
  render = require("./readability/render"),
  beautify = require("js-beautify").html,
  Annotations = require("localized-readability/dist/annotations/language.nb-no");
const start = performance.now();
const now = dayjs();
const logger = new Logger();
const { JSDOM } = jsdom;
const base = path.resolve("./");
const pool = Pool(() => spawn(new Worker("./readability/worker")), {
  size: require("os").cpus().length - 1,
});
let readabilityIndex = [];

/**
 * Generate Readability-reports and index from Markdown-content
 */
class Readability {
  /**
   * Generate Readability-reports and index from Markdown-content
   * @param {array} files List of files to process
   * @param {boolean} debug Output verbose debugging information
   * @returns {object}
   */
  static async process({
    files: files = [],
    debug: debug = false,
    force: force = false,
  }) {
    logger.notice("Starter generasjon av lesbarhet-rapporter ...");
    if (files.length < 1) {
      files = walker(`${base}/docs`, {
        fs: FS,
        nodir: true,
        traverseAll: true,
        filter: (file) => {
          if (
            fileExtension(file.path) == "md" &&
            ![
              ".vuepress",
              "docs\\index.md",
              "docs\\kategorier.md",
              "docs\\emneknagger.md",
              "docs\\lenker.md",
              "docs\\endringslogg.md",
              "node_modules",
            ].some((v) => file.path.includes(v))
          ) {
            return true;
          }
          return false;
        },
      });
      files = files.map((file) =>
        file.path
          .replace(base, "")
          .replaceAll("\\", "/")
          .replace("/docs", "./docs")
      );
    }
    await Readability.iterate({ files: files, debug: debug, force: force });
    await pool.completed();
    await pool
      .terminate()
      .then(() => {
        logger.notice("Fullført generasjon av lesbarhet-rapporter");
      })
      .then(() => {
        logger.notice("Lagrer lesbarhetsindeks ...");
        if (!FS.existsSync(`${base}/docs/.vuepress/public`)) {
          FS.mkdirSync(`${base}/docs/.vuepress/public`, { recursive: true });
        }
        FS.writeFileSync(
          `${base}/docs/.vuepress/public/lesbarhet.index.json`,
          JSON.stringify(readabilityIndex, null, 2)
        );
        logger.entry({
          message: `${base}/docs/.vuepress/public/lesbarhet.index.json`,
          source: `Indeksert lesbarhetsanalyse`,
          type: "saved",
          start: start,
          store: true,
        });
      })
      .then(() => {
        console.log(
          chalk`{whiteBright ${"Total Time:"}} {cyan ${msToTime(
            performance.now() - start
          )}}`
        );
        process.exit(0);
      });
  }

  /**
   * Iterate through files and assign workers to process them
   * @param {array} files List of files to process
   * @param {boolean} debug Output verbose debugging information
   * @returns {void}
   */
  static async iterate({
    files: files,
    debug: debug = false,
    force: force = false,
  }) {
    const template = FS.readFileSync(
      `${base}/generator/lib/readability/standalone.html`,
      { encoding: "utf8" }
    );
    const dom = new JSDOM(template, { runScripts: "dangerously" });
    for (let i = 0; i < files.length; i++) {
      const index = {};
      const source = files[i];
      index.path = source.replace(`index.md`, "");
      const target = source
        .replace(`.${fileExtension(files[i])}`, ".html")
        .replace("./docs", "docs/.vuepress/public/lesbarhet");
      if (
        FS.existsSync(`${base}/${target}`) &&
        FS.statSync(`${base}/${target}`).size > 0
      ) {
        let fileTime = dayjs(FS.statSync(`${base}/${target}`).mtimeMs);
        if (!fileTime.isBefore(now, "day") && !force) {
          logger.entry({
            message: `${fileTime.format("YYYY-MM-DD")} ~ ${now.format(
              "YYYY-MM-DD"
            )}`,
            source: source,
            type: "skipped",
          });
          continue;
        }
      }
      const fileContents = FS.readFileSync(files[i], { encoding: "utf8" });
      const parsed = Matter(fileContents);
      index.title = parsed.data.title;
      const input = {
        lang: "nb-no",
        source: source,
        content: parsed.content,
        debug: debug,
      };
      if (!parsed.hasOwnProperty("content") || parsed.content.length < 10) {
        continue;
      }
      const task = pool.queue(async (process) => process(input));
      task.then(async (data) => {
        if (typeof data === "string") {
          logger.entry({
            message: data,
            source: source,
            type: "skipped",
            level: "warn",
            store: true,
          });
          return;
        }
        index.grade = data.consensus.grade;
        index.age = data.consensus.age;
        index.timeToRead = await Readability.timeToRead(data.count.words);
        index.pages = await Readability.pageCount(data.count.words);
        index.paragraphs = data.count.paragraphs;
        index.sentences = data.count.sentences;
        index.words = data.count.words;
        readabilityIndex.push(index);
        let html = beautify(
          "<!DOCTYPE html>\n" +
            render(dom.window.document, Annotations, data, source)
              .documentElement.outerHTML,
          {
            indent_size: "2",
            indent_char: " ",
            max_preserve_newlines: "-1",
            preserve_newlines: false,
            indent_inner_html: true,
          }
        );
        if (!FS.existsSync(`${base}/${target.replace("index.html", "")}`)) {
          FS.mkdirSync(`${base}/${target.replace("index.html", "")}`, {
            recursive: true,
          });
        }
        FS.writeFileSync(`${base}/${target}`, html);
        logger.entry({
          message: target,
          source: `${source} lesbarhetsanalyse`,
          type: "saved",
          start: start,
          store: true,
        });
      });
    }
  }

  /**
   * Count the time to read content in minutes
   * @param {number} words Amount of words in document
   * @param {number} wordsPerMinute Reading speed
   * @returns {string} Words and seconds
   */
  static async timeToRead(words, wordsPerMinute = 200) {
    let timeDecimal = words / wordsPerMinute;
    let minutes = Math.floor(timeDecimal);
    let decimalPortion = parseFloat(
      "0." + timeDecimal.toString().split(".")[1]
    );
    decimalPortion = (decimalPortion * 0.6).toFixed(2);
    let seconds = parseInt(decimalPortion.toString().split(".")[1]);
    return Number.parseFloat(`${minutes}.${seconds}`);
  }

  /**
   * Count document page-length
   * @param {number} words Amount of words in document
   * @param {number} wordsPerPage Words per page
   * @returns {number} Page-length approximation
   */
  static async pageCount(words, wordsPerPage = 375) {
    return Number.parseFloat(`${(words / wordsPerPage).toFixed(2)}`);
  }
}

module.exports = Readability;

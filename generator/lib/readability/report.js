const Logger = require("../logger"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  { msToTime } = require("../utilities"),
  chalk = require("chalk"),
  jsdom = require("jsdom"),
  json2html = require("node-json2html"),
  beautify = require("js-beautify").html;
const start = performance.now();
const logger = new Logger();
const { JSDOM } = jsdom;
const base = path.resolve("./");
const bodyTemplate = {
  "<>": "tr",
  html: [
    {
      "<>": "td",
      html: [
        {
          "<>": "a",
          html: function () {
            return this.path.replace("./docs", "").replace(/\/$/, "");
          },
          href: function () {
            return (
              this.path
                .replace("./docs", "/arkivplan/lesbarhet")
                .replace(/\/$/, "") + "/index.html"
            );
          },
        },
      ],
    },
    {
      "<>": "td",
      html: [
        {
          "<>": "a",
          html: "${title}",
          href: function () {
            return this.path.replace("./docs", "/arkivplan").replace(/\/$/, "");
          },
        },
      ],
    },
    {
      "<>": "td",
      html: "${grade}",
      class: function () {
        if (this.grade > 19) return "level-4";
        if (this.grade > 16) return "level-3";
        if (this.grade > 13) return "level-2";
        return "level-1";
      },
      title: function () {
        if (this.grade > 19) return "Svært vanskelig";
        if (this.grade > 16) return "Vanskelig";
        if (this.grade > 13) return "Ganske vanskelig";
        return "Normal";
      },
    },
    {
      "<>": "td",
      html: "${age}",
      class: function () {
        if (this.age >= 25) return "level-5";
        if (this.age >= 22) return "level-4";
        if (this.age >= 19) return "level-3";
        if (this.age >= 16) return "level-2";
        return "level-1";
      },
      title: function () {
        if (this.age >= 25) return "Ekstremt vanskelig";
        if (this.age >= 22) return "Svært vanskelig";
        if (this.age >= 19) return "Vanskelig";
        if (this.age >= 16) return "Ganske vanskelig";
        return "Normal";
      },
    },
    {
      "<>": "td",
      html: function () {
        return `${this.timeToRead} min`;
      },
      class: function () {
        if (this.timeToRead > 20) return "level-5";
        if (this.timeToRead > 15) return "level-4";
        if (this.timeToRead > 10) return "level-3";
        if (this.timeToRead > 5) return "level-2";
        return "level-1";
      },
      title: function () {
        if (this.timeToRead > 20) return "Ekstremt lang";
        if (this.timeToRead > 15) return "Svært lang";
        if (this.timeToRead > 10) return "Lang";
        if (this.timeToRead > 5) return "Ganske lang";
        return "Normal";
      },
    },
    {
      "<>": "td",
      html: "${pages}",
      class: function () {
        if (this.pages > 4) return "level-5";
        if (this.pages > 3) return "level-4";
        if (this.pages > 2) return "level-3";
        if (this.pages > 1) return "level-2";
        return "level-1";
      },
      title: function () {
        if (this.pages > 4) return "Ekstremt lang";
        if (this.pages > 3) return "Svært lang";
        if (this.pages > 2) return "Lang";
        if (this.pages > 1) return "Ganske lang";
        return "Normal";
      },
    },
  ],
};

class ReadabilityReport {
  static process({ debug: debug = false }) {
    logger.notice("Starter generasjon av samlet lesbarhets-rapport ...");
    const readabilityIndex = JSON.parse(
      FS.readFileSync(`${base}/docs/.vuepress/public/lesbarhet.index.json`, {
        encoding: "utf8",
      })
    );
    const template = FS.readFileSync(
      `${base}/generator/lib/readability/aggregate.html`,
      { encoding: "utf8" }
    );
    let table_header = json2html.transform(readabilityIndex[0], {
      "<>": "tr",
      html: [
        { "<>": "th", html: "Rapport" },
        { "<>": "th", html: "Side" },
        { "<>": "th", html: "Klassetrinn", "data-sort-default": "" },
        { "<>": "th", html: "Alder" },
        { "<>": "th", html: "Lesetid" },
        { "<>": "th", html: "Sider" },
      ],
    });
    let table_body = json2html.transform(readabilityIndex, bodyTemplate);
    const dom = new JSDOM(template, { runScripts: "dangerously" });
    let html = beautify(
      ReadabilityReport.render(
        dom.window.document,
        `<table><thead>${table_header}</thead><tbody>${table_body}</tbody></table>`
      ).documentElement.outerHTML,
      {
        indent_size: "2",
        indent_char: " ",
        max_preserve_newlines: "-1",
        preserve_newlines: false,
        indent_inner_html: true,
      }
    );
    if (!FS.existsSync(`${base}/docs/.vuepress/public`)) {
      FS.mkdirSync(`${base}/docs/.vuepress/public`, { recursive: true });
    }
    FS.writeFileSync(
      `${base}/docs/.vuepress/public/lesbarhetsrapport.html`,
      html
    );
    logger.entry({
      message: `${base}/docs/.vuepress/public/lesbarhetsrapport.html`,
      source: `Samlet lesbarhetsanalyse`,
      type: "saved",
      start: start,
      store: true,
    });
    console.log(
      chalk`{whiteBright ${"Total Time:"}} {cyan ${msToTime(
        performance.now() - start
      )}}`
    );
  }

  static render(document, html) {
    document.getElementById("header-main").innerHTML = "Lesbarhetsrapport";
    document.querySelector("content").innerHTML = html;
    return document;
  }
}

module.exports = ReadabilityReport;

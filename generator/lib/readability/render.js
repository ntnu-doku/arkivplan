/**
 * Render individual parts of Score
 * @param {object} data Generated data
 */
module.exports = function render(document, Annotations, data, title) {
  document.getElementsByTagName("title")[0].innerHTML = title;
  document.getElementById("header-main").innerHTML = Annotations.general.module;
  document.getElementById("header-score").innerHTML = Annotations.general.score;
  document.getElementById("header-interpretations").innerHTML =
    Annotations.general.interpretations;
  document.getElementById("header-counts").innerHTML =
    Annotations.general.counts;
  document.getElementsByTagName("content")[0].innerHTML = data.highlighted;
  document.querySelector("#consensus .description").innerHTML =
    Annotations.stats.consensus.description;
  document.querySelector("#consensus label.age").innerHTML =
    Annotations.general.age;
  document.querySelector("#consensus value.age").innerHTML = data.consensus.age;
  document.querySelector("#consensus label.grade").innerHTML =
    Annotations.general.grade;
  document.querySelector("#consensus value.grade").innerHTML =
    data.consensus.grade;
  document.querySelector("#automated-readability-index .title").innerHTML =
    Annotations.stats.automatedReadability.title;
  document.querySelector(
    "#automated-readability-index .description"
  ).innerHTML = Annotations.stats.automatedReadability.description;
  document
    .querySelector("#automated-readability-index .title")
    .setAttribute("title", Annotations.stats.automatedReadability.description);
  document.querySelector("#automated-readability-index label.age").innerHTML =
    Annotations.general.age;
  document.querySelector("#automated-readability-index value.age").innerHTML =
    data.interpretations.automatedReadability.age;
  document.querySelector("#automated-readability-index label.grade").innerHTML =
    Annotations.general.grade;
  document.querySelector("#automated-readability-index value.grade").innerHTML =
    data.interpretations.automatedReadability.grade;
  document.querySelector("#flesch .title").innerHTML =
    Annotations.stats.flesch.title;
  document.querySelector("#flesch .description").innerHTML =
    Annotations.stats.flesch.description;
  document
    .querySelector("#flesch .title")
    .setAttribute("title", Annotations.stats.flesch.description);
  document.querySelector("#flesch .value").innerHTML =
    data.interpretations.flesch;
  document.querySelector("#flesch-kincaid .title").innerHTML =
    Annotations.stats.fleschKincaid.title;
  document.querySelector("#flesch-kincaid .description").innerHTML =
    Annotations.stats.fleschKincaid.description;
  document
    .querySelector("#flesch-kincaid .title")
    .setAttribute("title", Annotations.stats.fleschKincaid.description);
  document.querySelector("#flesch-kincaid label.age").innerHTML =
    Annotations.general.age;
  document.querySelector("#flesch-kincaid value.age").innerHTML =
    data.interpretations.fleschKincaid.age;
  document.querySelector("#flesch-kincaid label.grade").innerHTML =
    Annotations.general.grade;
  document.querySelector("#flesch-kincaid value.grade").innerHTML =
    data.interpretations.fleschKincaid.grade;
  document.querySelector("#coleman-liau .title").innerHTML =
    Annotations.stats.colemanLiau.title;
  document.querySelector("#coleman-liau .description").innerHTML =
    Annotations.stats.colemanLiau.description;
  document
    .querySelector("#coleman-liau .title")
    .setAttribute("title", Annotations.stats.colemanLiau.description);
  document.querySelector("#coleman-liau label.age").innerHTML =
    Annotations.general.age;
  document.querySelector("#coleman-liau value.age").innerHTML =
    data.interpretations.colemanLiau.age;
  document.querySelector("#coleman-liau label.grade").innerHTML =
    Annotations.general.grade;
  document.querySelector("#coleman-liau value.grade").innerHTML =
    data.interpretations.colemanLiau.grade;
  document.querySelector("#smog .title").innerHTML =
    Annotations.stats.smog.title;
  document.querySelector("#smog .description").innerHTML =
    Annotations.stats.smog.description;
  document
    .querySelector("#smog .title")
    .setAttribute("title", Annotations.stats.smog.description);
  document.querySelector("#smog label.age").innerHTML = Annotations.general.age;
  document.querySelector("#smog value.age").innerHTML =
    data.interpretations.smog.age;
  document.querySelector("#smog label.grade").innerHTML =
    Annotations.general.grade;
  document.querySelector("#smog value.grade").innerHTML =
    data.interpretations.smog.grade;
  document.querySelector("#lix .title").innerHTML = Annotations.stats.lix.title;
  document.querySelector("#lix .description").innerHTML =
    Annotations.stats.lix.description;
  document
    .querySelector("#lix .title")
    .setAttribute("title", Annotations.stats.lix.description);
  document.querySelector("#lix .value").innerHTML = data.interpretations.lix;
  document.querySelector("#rix .title").innerHTML = Annotations.stats.rix.title;
  document.querySelector("#rix .description").innerHTML =
    Annotations.stats.rix.description;
  document
    .querySelector("#rix .title")
    .setAttribute("title", Annotations.stats.rix.description);
  document.querySelector("#rix label.grade").innerHTML =
    Annotations.general.grade;
  document.querySelector("#rix value.grade").innerHTML =
    data.interpretations.rix.grade;
  document.querySelector("#count label.paragraphs").innerHTML =
    Annotations.general.paragraphs;
  document.querySelector("#count value.paragraphs").innerHTML =
    data.count.paragraphs;
  document.querySelector("#count label.sentences").innerHTML =
    Annotations.general.sentences;
  document.querySelector("#count value.sentences").innerHTML =
    data.count.sentences;
  document.querySelector("#count label.words").innerHTML =
    Annotations.general.words;
  document.querySelector("#count value.words").innerHTML = data.count.words;
  document.querySelector("#count label.syllables").innerHTML =
    Annotations.general.syllables;
  document.querySelector("#count value.syllables").innerHTML =
    data.count.syllables;
  document.querySelector("#count label.polysillabic-words").innerHTML =
    Annotations.general.polysillabicWords;
  document.querySelector("#count value.polysillabic-words").innerHTML =
    data.count.polysillabicWords;
  document.querySelector("#count label.letters").innerHTML =
    Annotations.general.letters;
  document.querySelector("#count value.letters").innerHTML = data.count.letters;
  document.querySelector("#count label.characters").innerHTML =
    Annotations.general.characters;
  document.querySelector("#count value.characters").innerHTML =
    data.count.characters;
  document.querySelector("#count label.longwords").innerHTML =
    Annotations.general.longwords;
  document.querySelector("#count value.longwords").innerHTML =
    data.count.longwords;
  document.querySelector("#count label.periods").innerHTML =
    Annotations.general.periods;
  document.querySelector("#count value.periods").innerHTML = data.count.periods;
  document.querySelector("#count label.punctuations").innerHTML =
    Annotations.general.punctuations;
  document.querySelector("#count value.punctuations").innerHTML =
    data.count.punctuations;
  document.querySelector("#count label.whitespaces").innerHTML =
    Annotations.general.whitespaces;
  document.querySelector("#count value.whitespaces").innerHTML =
    data.count.whitespaces;
  return document;
};

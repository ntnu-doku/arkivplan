const Logger = require("../logger"),
  { performance } = require("perf_hooks"),
  { expose } = require("threads/worker"),
  Readability = require("localized-readability/dist/localized-readability"),
  Hypher = require("localized-readability/dist/hypher"),
  HyphenationPatterns = require(`localized-readability/dist/patterns/nb-no`),
  Annotations = require("localized-readability/dist/annotations/language.nb-no");
const start = performance.now();
const logger = new Logger();

expose(function process(input) {
  try {
    logger.notice(`Behandler ${input.source} ...`);
    const lang = input.lang;
    const Parser = Readability.parser;
    const Highlighter = Readability.highlighter;

    const data = {};
    data.setup = Parser.setup(input.content, Hypher, HyphenationPatterns);
    data.count = Parser.count(data.setup);
    data.statistics = Parser.statistics(data.count, lang);
    if (data.statistics.flesch < 1)
      return `kunne ikke behandles: Flesch Reading Ease < 1`;
    data.interpretations = Parser.interpretations(
      data.count,
      data.statistics,
      Annotations
    );
    data.consensus = Parser.consensus(data.interpretations);

    const Highlight = Highlighter.highlight(data.setup.nlcst, {
      paragraphs: true,
      sentences: true,
      words: true,
      Hypher: Hypher,
      HyphenationPatterns: HyphenationPatterns,
    });
    data.highlighted = Highlighter.stringify(Highlight);
    data.time = performance.now() - start;
    if (input.debug) {
      logger.notice(require("process-top")().toString());
    }
    return data;
  } catch (error) {
    logger.debug(input.source);
    logger.trace(error);
  }
  return;
});

const Logger = require("./logger"),
  replaceAll = require("string.prototype.replaceall"),
  matchAll = require("string.prototype.matchall"),
  MarkdownIt = require("markdown-it"),
  { markdownItTable } = require("markdown-it-table"),
  Formatter = require("smartypants").smartypantsu,
  rectify = require("rectify");
replaceAll.shim();
matchAll.shim();
const logger = new Logger();
// @see https://regex101.com/r/8pNnaG/1
const tableRegex =
  /((\r?\n){2}|^)([^\r\n]*\|[^\r\n]*(\r?\n)?)+(?=(\r?\n){2}|$)/gm;
const rectifyRegex = /`([^`]+)`/g;
const md = new MarkdownIt();
md.use(markdownItTable);

/**
 * Additional Markdown formatting
 */
class Markdown {
  /**
   * Apply Smarty-formatting to Markdown-formatted plain text
   * @param {string} str Markdown-formatted plain text
   * @param {boolean} debug Output verbose debugging information
   * @returns {string}
   *
   * @see https://www.npmjs.com/package/smartypants
   */
  static process({ str: str, debug: debug = false }) {
    if (typeof str !== "string" || str === "") {
      return str;
    }
    str = str.replace(/\/$/gm, "");
    str = str.replace(/\/`/gm, "`");
    str = str.replace(/\/--/gm, "--");
    let tableMatches = str.match(tableRegex);
    if (tableMatches !== null && tableMatches.length > 0) {
      for (let i = 0; i < tableMatches.length; i++) {
        str = str.replace(tableMatches[i], "\n\n" + md.render(tableMatches[i]));
      }
    }
    str = Formatter(str, 2);
    let rectifyMatches = str.match(rectifyRegex);
    if (rectifyMatches !== null && rectifyMatches.length > 0) {
      for (let i = 0; i < rectifyMatches.length; i++) {
        str = str.replace(rectifyMatches[i], rectify(rectifyMatches[i]));
      }
    }
    return str;
  }
}

module.exports = Markdown;

const Logger = require("./logger"),
  { performance } = require("perf_hooks"),
  { dev } = require("vuepress"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  fileExtension = require("file-extension"),
  puppeteer = require("puppeteer"),
  PDFjs = require("pdfjs");
const start = performance.now();
const logger = new Logger();

class PDF {
  static process() {
    PDF.generate()
      .then(() => {
        PDF.attachments();
      })
      .then(() => {
        process.exit(0);
      });
  }

  /**
   * Regenerate site and keep alive to capture
   */
  static async generate() {
    logger.notice("Åpner VuePress ...");
    try {
      const nCtx = await dev({
        sourceDir: "./docs",
        clearScreen: false,
        theme: "@vuepress/default",
      });
      try {
        await PDF.capture();
      } catch (error) {
        logger.trace(error);
      }
      logger.notice("Lukker VuePress");
      nCtx.devProcess.server.close();
    } catch (error) {
      logger.trace(error);
    }
    await new Promise((resolve) => {
      resolve();
    });
  }

  /**
   * Capture /print and store as PDF
   */
  static async capture() {
    logger.notice("Åpner Puppeteer ...");
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto("http://localhost:8080/arkivplan/print/", {
      timeout: 0,
      waitUntil: "load",
    });
    await page.evaluate(() => {
      document
        .querySelectorAll("a[href^='/']")
        .forEach(
          (element) =>
            (element.href = element.href.replace(
              "http://localhost:8080",
              "https://ntnu.no"
            ))
        );
    });
    await page.pdf({
      printBackground: true,
      path: "./docs/.vuepress/public/utskrift.pdf",
      format: "A4",
      preferCSSPageSize: true,
    });
    logger.entry({
      message: "/docs/.vuepress/public/utskrift.pdf",
      source: `/print`,
      type: "saved",
      start: start,
      store: true,
    });
    logger.notice(`Lukker Puppeteer ...`);
    await browser.close();
    await new Promise((resolve) => {
      resolve();
    });
  }

  /**
   * Merge attachments into one document
   */
  static async attachments() {
    logger.notice("Lagrer vedlegg som samlet PDF ...");
    const base = path.resolve("./");
    let files;
    try {
      files = walker(`${base}/docs/.vuepress/public/vedlegg`, {
        fs: FS,
        nodir: true,
      });
    } catch (error) {
      logger.trace(error);
      return;
    }
    const doc = new PDFjs.Document();
    for (let i = 0; i < files.length; i++) {
      const extension = fileExtension(files[i].path);
      const basename = path.basename(files[i].path, `.${extension}`);
      if (extension == "pdf") {
        let fileContents = FS.readFileSync(files[i].path);
        try {
          let file = new PDFjs.ExternalDocument(fileContents);
          doc.addPagesOf(file);
        } catch (error) {
          logger.trace(error);
        }
        logger.entry({
          message: "PDF-buffer",
          source: `${basename}.${extension}`,
          type: "copied",
          start: start,
          i: i,
          n: files.length,
          store: true,
        });
      }
    }
    doc.asBuffer((error, data) => {
      if (error) {
        logger.trace(error);
      } else {
        FS.writeFileSync("./docs/.vuepress/public/vedlegg.pdf", data, {
          encoding: "binary",
        });
        logger.entry({
          message: "/docs/.vuepress/public/vedlegg.pdf",
          source: `vedlegg`,
          type: "saved",
          start: start,
          store: true,
        });
      }
    });
    await new Promise((resolve) => {
      resolve();
    });
  }

  /**
   * Merge print and attachments into one document
   *
   * @deprecated Some attachments cannot be read as a document or image
   */
  static async merged() {
    logger.notice("Lagrer utskrift og vedlegg som samlet PDF ...");
    let files = [
      "./docs/.vuepress/public/utskrift.pdf",
      "./docs/.vuepress/public/vedlegg.pdf",
    ];
    const doc = new PDFjs.Document();
    for (let i = 0; i < files.length; i++) {
      const extension = fileExtension(files[i]);
      const basename = path.basename(files[i], `.${extension}`);
      let fileContents = FS.readFileSync(files[i]);
      let file = new PDFjs.ExternalDocument(fileContents);
      doc.addPagesOf(file);
      // let file = new PDFjs.Image(fileContents);
      // doc.image(file);
      logger.entry({
        message: "PDF-buffer",
        source: `${basename}.${extension}`,
        type: "copied",
        start: start,
        i: i,
        n: files.length,
        store: true,
      });
    }
    doc.asBuffer((error, data) => {
      if (error) {
        logger.trace(error);
      } else {
        FS.writeFileSync(
          "./docs/.vuepress/public/utskrift-med-vedlegg.pdf",
          data,
          {
            encoding: "binary",
          }
        );
        logger.entry({
          message: "/docs/.vuepress/public/utskrift-med-vedlegg.pdf",
          source: `vedlegg`,
          type: "saved",
          start: start,
          store: true,
        });
      }
    });
    await new Promise((resolve) => {
      resolve();
    });
  }
}

module.exports = PDF;

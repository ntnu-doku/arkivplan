const replaceAll = require("string.prototype.replaceall"),
  matchAll = require("string.prototype.matchall"),
  Logger = require("../lib/logger");
const logger = new Logger();

replaceAll.shim();
matchAll.shim();
const linkRegEx = /^!\[(?<alt>[^\[\]]*)\]\((?<path>.*?)\)/gm;
const pathRegEx = /\/vedlegg\/media\/(.+)/g;
const urlRegEx = /(?:\/vedlegg).+/g;
const extraRegEx = /{.*}/g;

/**
 * Clean references to media-files
 */
class Media {
  /**
   * Optimize media-links
   * @param {string} str Markdown-formatted plain text
   * @param {boolean} debug Output verbose debugging information
   * @returns {string}
   */
  static process({ str: str, debug: debug = false }) {
    if (typeof str !== "string" || str === "") {
      return str;
    }
    str = str.split("\\").join("/");
    let matches;
    matches = [...str.matchAll(linkRegEx)];
    if (debug) logger.entry({ message: `${matches.length} media items` });
    if (matches.length < 1) {
      return str;
    }
    for (let i = 0; i < matches.length; i++) {
      if (matches[i].groups.path.match(pathRegEx) === null) {
        if (debug) logger.debug(`EXTERNAL ${matches[i].groups.path}`);
        continue;
      }
      if (debug) logger.debug(`SUBJECT ${matches[i].groups.path}`);
      if (
        matches[i].groups.path.match(urlRegEx) === null &&
        matches[i].groups.path.match(urlRegEx).length > 0
      ) {
        if (debug) logger.debug(`NULL OR EMPTY ${matches[i].groups.path}`);
        continue;
      }
      const clean = matches[i].groups.path.match(urlRegEx)[0];
      if (debug) logger.entry({ message: `.${clean}` });
      str = str.replaceAll(matches[i].groups.path, `.${clean}`);
      str = str.replaceAll(extraRegEx, "");
    }
    return str;
  }
}

module.exports = Media;

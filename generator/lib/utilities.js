const cleanDeep = require("clean-deep");

class Utilities {
  /**
   * Convert milliseconds to human readable time
   * @param {Number} millisec Float containing milliseconds
   *
   * @see https://stackoverflow.com/a/32180863
   */
  static msToTime(millisec) {
    var milliseconds = millisec.toFixed(2);
    var seconds = (millisec / 1000).toFixed(1);
    var minutes = (millisec / (1000 * 60)).toFixed(1);
    var hours = (millisec / (1000 * 60 * 60)).toFixed(1);
    var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);
    if (seconds <= 0) {
      return milliseconds + " ms";
    } else if (seconds < 60) {
      return seconds + " sec";
    } else if (minutes < 60) {
      return minutes + " min";
    } else if (hours < 24) {
      return hours + " hours";
    } else {
      return days + " days";
    }
  }

  /**
   * Add an item node in the tree, at the right position
   * @param {String} node Path-property
   * @param {Array} treeNodes Tree-structure
   *
   * @see https://stackoverflow.com/a/7966101
   */
  static addToTree(node, treeNodes) {
    for (var i = 0; i < treeNodes.length; i++) {
      var treeNode = treeNodes[i];
      if (node.hasOwnProperty("path")) {
        if (node.path.indexOf(treeNode.path + "/") == 0) {
          Utilities.addToTree(node, treeNode.children);
          return;
        }
      } else if (node.hasOwnProperty("link")) {
        if (node.link.indexOf(treeNode.link + "/") == 0) {
          Utilities.addToTree(node, treeNode.children);
          return;
        }
      }
    }
    if (node.hasOwnProperty("path")) {
      treeNodes.push({
        title: node.title,
        path: node.path,
        key: node.key || null,
        children: [],
      });
    } else if (node.hasOwnProperty("link")) {
      treeNodes.push({
        text: node.text,
        link: node.link,
        key: node.key || null,
        children: [],
      });
    }
  }

  /**
   * Create the item tree
   * @param {Array} nodes Array of objects with title- and path-properties
   * @returns {Array}
   *
   * @see https://stackoverflow.com/a/7966101
   */
  static createTree(nodes) {
    var tree = [];
    for (var i = 0; i < nodes.length; i++) {
      var node = nodes[i];
      Utilities.addToTree(node, tree);
    }
    return tree;
  }

  /**
   * Sort data by path-property length and trim trailing slashes
   * @param {Array} data Array of objects with title- and path-properties
   * @returns {Array}
   */
  static menuSortandTrim(data) {
    if (typeof data === "undefined" || data.length < 1) {
      return [];
    }
    if (data[0].hasOwnProperty("path")) {
      data.sort(function (a, b) {
        if (a.path < b.path) {
          return -1;
        }
        if (a.path > b.path) {
          return 1;
        }
        return 0;
      });
    } else if (data[0].hasOwnProperty("link")) {
      data.sort(function (a, b) {
        if (a.link < b.link) {
          return -1;
        }
        if (a.link > b.link) {
          return 1;
        }
        return 0;
      });
    }
    for (let i = 0; i < data.length; i++) {
      if (data[i].hasOwnProperty("path")) {
        data[i].path = data[i].path.replace(/\/+$/g, "");
      } else if (data[i].hasOwnProperty("link")) {
        data[i].link = data[i].link.replace(/\/+$/g, "");
      }
    }
    return data;
  }

  /**
   * Add trailing slashes recursively to all path-properties
   * @param {object} obj Array of objects with title- and path-properties
   */
  static menuReinsertTrailingSlash(obj) {
    for (var key in obj) {
      if (typeof obj[key] == "object" && obj[key] !== null) {
        Utilities.menuReinsertTrailingSlash(obj[key]);
      } else {
        if (obj.hasOwnProperty("path") && obj.path.slice(-1) !== "/") {
          obj.path = obj.path + "/";
        }
      }
    }
  }
}

module.exports = {
  msToTime: Utilities.msToTime,
  createTree: Utilities.createTree,
  menuSortandTrim: Utilities.menuSortandTrim,
  menuReinsertTrailingSlash: Utilities.menuReinsertTrailingSlash,
  cleanDeep: cleanDeep,
};

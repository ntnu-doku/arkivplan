const Logger = require("./logger"),
  child_process = require("child_process"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  slugify = require("slugify"),
  replaceAll = require("string.prototype.replaceall"),
  fileExtension = require("file-extension"),
  Meta = require("./meta"),
  Attachments = require("./attachments"),
  Media = require("./media"),
  Markdown = require("./markdown");
replaceAll.shim();
const start = performance.now();
const logger = new Logger();

/**
 * Generate content from Microsoft Word documents
 */
class Innhold {
  /**
   * Generate Markdown-files from Microsoft Word documents
   * @param {boolean} debug Output verbose debugging information
   * @returns {void}
   */
  static process({ debug: debug = false }) {
    logger.notice("Start generering av innhold ...");
    const base = path.resolve("./");
    let files;
    try {
      files = walker(`${base}/Innhold`, {
        fs: FS,
        nodir: true,
      });
    } catch (error) {
      logger.trace(error);
      return;
    }
    for (let i = 0; i < files.length; i++) {
      const extension = fileExtension(files[i].path);
      const basename = path.basename(files[i].path, `.${extension}`);
      const self = path.dirname(files[i].path) + "/" + basename;
      let target = files[i].path
        .replace(base, "")
        .replace(`.${extension}`, "")
        .replaceAll("\\", "/")
        .replace("/Innhold", "");
      target = slugify(target, {
        locale: "nb",
        lower: true,
        remove: /[^\w\s$*_+~.()'"!\-:@/]/g,
      });
      if (extension == "docx") {
        let markdownFile;
        try {
          markdownFile = this.transformFile({
            file: files[i].path,
            mediaTarget: `${base}/docs${target}/vedlegg`,
            debug: debug,
          });
          if (!markdownFile) {
            continue;
          }
          if (FS.existsSync(self) && this.countChildren(self) > 0) {
            markdownFile.status = "FERDIG";
          }
        } catch (error) {
          logger.trace(error);
        }
        try {
          if (
            markdownFile.hasOwnProperty("status") &&
            ["FERDIG", "FLYTTET"].includes(markdownFile.status) &&
            markdownFile.hasOwnProperty("content") &&
            typeof markdownFile.content === "string"
          ) {
            if (!FS.existsSync(`${base}/docs${target}`)) {
              FS.mkdirSync(`${base}/docs${target}`, { recursive: true });
            }
            FS.writeFileSync(
              `${base}/docs${target}/index.md`,
              markdownFile.content
            );
            logger.entry({
              message: "/docs" + target + "/index.md",
              source: `${basename}.${extension}`,
              type: "saved",
              start: start,
              i: i,
              n: files.length,
              store: true,
            });
          } else {
            logger.entry({
              message: markdownFile.status,
              source: `${self
                .replace(base, "")
                .replaceAll("\\", "/")}.${extension}`,
              type: "skipped",
              level: "warn",
              start: start,
              i: i,
              n: files.length,
              store: true,
            });
          }
        } catch (error) {
          logger.trace(error);
        }
      } else {
        try {
          target = target.replace(/\/vedlegg/gi, "");
          if (
            !FS.existsSync(`${base}/docs/.vuepress/public/vedlegg${target}`)
          ) {
            FS.mkdirSync(`${base}/docs/.vuepress/public/vedlegg${target}`, {
              recursive: true,
            });
          }
          FS.copyFileSync(
            files[i].path,
            `${base}/docs/.vuepress/public/vedlegg${target}.${extension}`
          );
          logger.entry({
            message: `/docs/.vuepress/public/vedlegg${target}.${extension}`,
            source: `${basename}.${extension}`,
            type: "copied",
            start: start,
            i: i,
            n: files.length,
            store: true,
          });
        } catch (error) {
          logger.trace(error);
        }
      }
    }
  }

  /**
   * Retrieve content and metadata from file
   * @param {string} file Target file
   * @param {string} mediaTarget Where to output media-files
   * @param {boolean} debug Output verbose debugging information
   * @returns {object}
   */
  static transformFile({
    file: file,
    mediaTarget: mediaTarget,
    debug: debug = false,
  }) {
    if (!FS.existsSync(file)) {
      return { status: "MANGLER", content: "" };
    }
    let content,
      metadata,
      media = "";
    if (typeof mediaTarget !== "undefined" && mediaTarget != "") {
      media = `--extract-media="${mediaTarget}"`;
    }
    try {
      metadata = Meta.process({ filename: file, debug: debug });
      content = child_process
        .execSync(
          `pandoc --wrap=none --write=gfm-raw_html --markdown-headings=atx ${media} "${file}"`,
          { timeout: 10000, encoding: "UTF-8" }
        )
        .toString();
      content = Attachments.process({ str: content, debug: debug });
      content = Media.process({ str: content, debug: debug });
      content = Markdown.process({ str: content, debug: debug });
    } catch (error) {
      logger.trace(error);
    }
    return {
      status: metadata.status,
      content: `---\n${metadata.content}---\n\n${content}`,
    };
  }

  /**
   * Count files of type below target
   * @param {string} path Path to folder
   * @param {string} type Filetype
   * @returns {number}
   */
  static countChildren(path, type = "docx") {
    const files = walker(path, {
      fs: FS,
      nodir: true,
      depthLimit: 1,
    });
    let count = 0;
    for (let i = 0; i < files.length; i++) {
      const extension = fileExtension(files[i].path);
      if (extension == type) {
        count++;
      }
    }
    return count;
  }
}

module.exports = Innhold;

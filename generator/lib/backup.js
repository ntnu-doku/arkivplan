const Logger = require("./logger"),
  { performance } = require("perf_hooks"),
  { DateTime } = require("luxon"),
  _7z = require("7zip-min");
const start = performance.now();
const logger = new Logger();

/**
 * Create a zip-archive of /Innhold
 */
class Backup {
  /**
   * Use 7-zip to create a date-formatted file in /backups
   */
  static process() {
    const filename = DateTime.local().toFormat("yyyy-MM-dd_HH-mm-ss");
    _7z.pack("./Innhold", `./backups/${filename}.zip`, (error) => {
      if (error) {
        logger.trace(error);
      }
      logger.entry({
        message: `./backups/${filename}.zip`,
        source: "./Innhold",
        type: "saved",
        start: start,
        store: true,
      });
    });
  }
}

module.exports = Backup;

const replaceAll = require("string.prototype.replaceall"),
  matchAll = require("string.prototype.matchall"),
  fileExtension = require("file-extension"),
  slugify = require("slugify"),
  Logger = require("./logger"),
  Config = require("../../docs/.vuepress/config");
const logger = new Logger();

replaceAll.shim();
matchAll.shim();
const linkRegEx = /\[(?<alt>[^\[\]]*)\]\((?<path>.*?)\)/gm;
const pathRegEx = /[\\|\/]Publisering[\\|\/]Innhold[\\|\/](.+)/gm;
const attachmentRegEx = /Vedlegg\/.+/g;

/**
 * Clean URLs
 */
class Attachments {
  /**
   * Process text
   * @description Optimize internal and external links for relative use
   * @param {string} str Markdown-formatted plain text
   * @param {boolean} debug Output verbose debugging information
   * @returns {string}
   */
  static process({ str: str, debug: debug = false }) {
    if (typeof str !== "string" || str === "") {
      return str;
    }
    const base = Config.base.replace(/\/+$/g, "");
    if (debug) logger.entry({ message: `Base ${base}` });
    let linkMatches, pathMatches;
    linkMatches = [...str.matchAll(linkRegEx)];
    pathMatches = [...str.matchAll(pathRegEx)];
    if (debug) logger.entry({ message: `${linkMatches.length} links` });
    if (debug)
      logger.entry({ message: `${pathMatches.length} internal links` });
    if (linkMatches.length < 1) {
      return str;
    }
    for (let i = 0; i < linkMatches.length; i++) {
      if (linkMatches[i].groups.path.match(pathRegEx) === null) {
        if (debug) logger.debug(`EXTERNAL ${linkMatches[i].groups.path}`);
        continue;
      }
      if (debug) logger.debug(`INTERNAL ${linkMatches[i].groups.path}`);
      let clean = pathRegEx.exec(linkMatches[i].groups.path)[1];
      const extension = fileExtension(clean);
      if (extension === "docx") {
        clean = clean.replace("\\", "/");
        clean = clean.replace(`.${extension}`, "");
        clean =
          base +
          "/" +
          slugify(decodeURI(clean), {
            locale: "nb",
            lower: true,
            remove: /[^\w\s$*_+~.()'"!\-:@/]/g,
          });
      } else if (
        Array.isArray(clean.match(attachmentRegEx)) &&
        clean.match(attachmentRegEx).length > 0 &&
        clean.match(attachmentRegEx)[0] !== null
      ) {
        clean = clean.replace(/\/vedlegg/gi, "");
        clean =
          base +
          "/vedlegg/" +
          slugify(decodeURI(clean), {
            locale: "nb",
            lower: true,
            remove: /[^\w\s$*_+~.()'"!\-:@/]/g,
          });
      }
      if (typeof clean === "undefined" || clean === "") {
        continue;
      }
      if (debug) logger.entry({ message: `${clean}` });
      str = str.replaceAll(linkMatches[i].groups.path, `${clean}`);
    }
    str = str.replace(/([^:])(\/{2,})/g, "$1/");
    return str;
  }
}

module.exports = Attachments;

var fs = require('fs')
var context = require('./')
var query = process.argv.slice(2)
var text = fs.readFileSync(__dirname + '/README.md', 'utf-8')
var stat = context.stats(text, query)
console.log(stat)
console.log(
  context.highlight(context.context(text, stat.group, query, 80), query)
)


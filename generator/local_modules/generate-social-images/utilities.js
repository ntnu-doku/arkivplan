/**
 * Get process arguments
 * @returns object
 * @see https://stackoverflow.com/a/54098693
 */
function getArgs() {
  const args = {};
  process.argv.slice(2, process.argv.length).forEach((arg) => {
    if (arg.slice(0, 2) === "--") {
      const longArg = arg.split("=");
      const longArgFlag = longArg[0].slice(2, longArg[0].length);
      const longArgValue = longArg.length > 1 ? longArg[1] : true;
      if (typeof longArgValue === "string") {
        args[longArgFlag] = longArgValue.toLowerCase();
      } else {
        args[longArgFlag] = longArgValue;
      }
    } else if (arg[0] === "-") {
      const flags = arg.slice(1, arg.length).split("");
      flags.forEach((flag) => {
        args[flag] = true;
      });
    }
  });
  return args;
}

/**
 * Convert milliseconds to human readable time
 * @param {Number} millisec Float containing milliseconds
 *
 * @see https://stackoverflow.com/a/32180863
 */
function msToTime(millisec) {
  var milliseconds = millisec.toFixed(2);
  var seconds = (millisec / 1000).toFixed(1);
  var minutes = (millisec / (1000 * 60)).toFixed(1);
  var hours = (millisec / (1000 * 60 * 60)).toFixed(1);
  var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);
  if (seconds <= 0) {
    return milliseconds + " ms";
  } else if (seconds < 60) {
    return seconds + " sec";
  } else if (minutes < 60) {
    return minutes + " min";
  } else if (hours < 24) {
    return hours + " hours";
  } else {
    return days + " days";
  }
}

module.exports = {
  getArgs: getArgs,
  msToTime: msToTime,
};

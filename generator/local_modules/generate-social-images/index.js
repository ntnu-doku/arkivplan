const FS = require("fs"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  chalk = require("chalk-cjs"),
  walker = require("klaw-sync"),
  matter = require("gray-matter"),
  readChunk = require("read-chunk"),
  imageType = require("image-type"),
  sizeOf = require("image-size"),
  { getArgs, msToTime } = require("./utilities"),
  { createImage } = require("./create-image"),
  config = require("../../../docs/.vuepress/config.js");
const args = getArgs();
const start = performance.now();

let base = "../../../",
  source = "/docs/.vuepress/public/assets/img",
  metadata = config,
  background,
  watermark;
if (args.hasOwnProperty("base") && args.base !== "") {
  base = args.base;
}
base = path.resolve(base).replace(/\\/g, "/");
if (args.debug) console.log(chalk.cyanBright("BASE"), base);

if (args.hasOwnProperty("source") && args.source !== "") {
  source = args.source;
}
if (args.debug) console.log(chalk.cyanBright("SOURCE"), source);

if (args.hasOwnProperty("metadata") && args.metadata !== "") {
  metadata = args.metadata;
}
if (args.debug) console.log(chalk.cyanBright("METADATA"), metadata);

if (
  args.hasOwnProperty("background") &&
  args.background !== "" &&
  FS.existsSync(`${base}${args.background}`)
) {
  let backgroundImage = `${base}${args.background}`;
  let backgroundImageMimeType = imageType(
    readChunk.sync(backgroundImage, 0, 12)
  );
  let backgroundImageContents = FS.readFileSync(backgroundImage, {
    encoding: "base64",
  });
  background = `data:${backgroundImageMimeType};base64,${backgroundImageContents}`;
  if (args.debug)
    console.log(
      chalk.cyanBright("BACKGROUND"),
      backgroundImageMimeType,
      backgroundImageContents.length
    );
} else {
  console.error(
    chalk.redBright("Missing background", `${base}${args.background}`)
  );
  return;
}
if (
  args.hasOwnProperty("watermark") &&
  args.watermark !== "" &&
  FS.existsSync(`${base}${args.watermark}`)
) {
  let watermarkImage = `${base}${args.watermark}`;
  let watermarkImageMimeType = imageType(readChunk.sync(watermarkImage, 0, 12));
  let watermarkImageContents = FS.readFileSync(watermarkImage, {
    encoding: "base64",
  });
  watermark = `data:${watermarkImageMimeType};base64,${watermarkImageContents}`;
  if (args.debug)
    console.log(
      chalk.cyanBright("WATERMARK"),
      watermarkImageMimeType,
      watermarkImageContents.length
    );
}

console.log(chalk.magenta("GENERATING SOCIAL IMAGES ..."));

const mainImage = createImage({
  debug: args.debug ?? null,
  background: background,
  watermark: watermark,
  title: metadata.title,
  author: metadata.themeConfig.author.name,
  site: metadata.themeConfig.domainAlt,
});
FS.writeFile(
  `${base}${source}/social/social.jpg`,
  mainImage,
  "base64",
  function (err) {
    if (err) console.error(err);
    if (args.debug)
      console.log(
        chalk.cyanBright("WROTE"),
        `${source}/social/social.jpg in`,
        chalk.whiteBright(msToTime(performance.now() - start))
      );
  }
);

const pageFolders = walker(`${base}/docs`, {
  fs: FS,
  nofile: true,
  depthLimit: -1,
  filter: (file) => {
    if (
      ![
        ".vuepress",
        "kategorier",
        "emneknagger",
        "node_modules",
        "package.json",
        "package-lock.json",
        "pnpm-lock.yaml",
      ].includes(path.basename(file.path))
    ) {
      return true;
    }
    return false;
  },
});
let pages = [];
for (let i = 0; i < pageFolders.length; i++) {
  let pagePath = pageFolders[i].path;
  if (FS.existsSync(`${pagePath}/index.md`)) {
    let metadata = matter(
      FS.readFileSync(`${pagePath}/index.md`, { encoding: "utf8" })
    ).data;
    const imageFiles = walker(pagePath, {
      fs: FS,
      nodir: true,
      depthLimit: 0,
      filter: (item) => {
        if (
          [".jpg", ".jpeg", ".png"].includes(path.extname(item.path)) &&
          path.basename(item.path) != "social.jpg"
        ) {
          return true;
        }
        return false;
      },
    });
    let images = [];
    for (let n = 0; n < imageFiles.length; n++) {
      const dimensions = sizeOf(imageFiles[n].path);
      if (dimensions.width >= 1200 && dimensions.height >= 630) {
        images.push(imageFiles[n].path);
      }
    }
    pagePath = pagePath.replace(/\\/g, "/").replace(base, "");
    if (metadata.hasOwnProperty("title") && metadata.title !== "") {
      pages.push({ path: pagePath, metadata: metadata, images: images });
    }
  }
}
console.log(
  chalk.magentaBright("PROCESSING"),
  pages.length,
  `Page(s) in ${base}${source}`
);

for (let i = 0; i < pages.length; i++) {
  const start = performance.now();
  let customBackground = null;
  let target = `${base}${source}/social/${pages[i].path.replace("/docs/", "")}`;
  try {
    if (pages[i].hasOwnProperty("images") && pages[i].images.length > 0) {
      let backgroundImage = pages[i].images[0];
      let backgroundImageMimeType = imageType(
        readChunk.sync(pages[i].images[0], 0, 12)
      );
      let backgroundImageContents = FS.readFileSync(backgroundImage, {
        encoding: "base64",
      });
      customBackground = `data:${backgroundImageMimeType};base64,${backgroundImageContents}`;
    }
    const image = createImage({
      debug: args.debug ?? null,
      background: customBackground ?? background,
      watermark: watermark,
      title: pages[i].metadata.title,
      subtitle: pages[i].metadata.subtitle ?? "",
      author: metadata.themeConfig.author.name,
      site: metadata.themeConfig.domainAlt,
    });
    if (!FS.existsSync(target)) {
      FS.mkdirSync(target, { recursive: true });
    }
    FS.writeFile(`${target}/social.jpg`, image, "base64", function (err) {
      if (err) console.error(err);
      if (args.debug)
        console.log(
          chalk.cyanBright("WROTE"),
          `${pages[i].path}/social.jpg in`,
          chalk.whiteBright(msToTime(performance.now() - start))
        );
    });
  } catch (err) {
    console.error(err);
  }
}

process.on("exit", function () {
  console.log(
    chalk.magentaBright("PROCESSED"),
    pages.length,
    `Page(s) in ${base}${source} in`,
    chalk.whiteBright(msToTime(performance.now() - start))
  );
});

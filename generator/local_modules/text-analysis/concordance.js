/**
 * Alphabetical list of principal words in test
 * @see https://github.com/shiffman/A2Z-F17/blob/master/week5-analysis/05_node_concordance/concordance.js
 */
class Concordance {
  constructor() {
    this.dict = {};
    this.keys = [];
    this.associativeDict = [];
  }

  /**
   * Tokenize text
   * @param {string} text Text to split
   * @returns {array} Array of tokens
   */
  split(text) {
    const NLP = require("../../../node_modules/wink-nlp-utils");
    const tokens = NLP.string.tokenize(text, true);
    let words = [];
    for (let i = 0; i < tokens.length; i++) {
      if (tokens[i].tag == "word") {
        words.push(tokens[i].value);
      }
    }
    return words;
  }

  /**
   * Exclude stopwords and only retain words
   * @param {string} token Token to validate
   * @param {string} locale ISO 639-1 language-code
   * @returns {boolean}
   */
  validate(token, locale) {
    const stopwords = require(`stopwords-json/dist/${locale}.json`);
    if (stopwords.includes(token)) {
      return false;
    }
    return /\S{2,}/.test(token);
  }

  /**
   * Populate .dict and .keys
   * @param {string} data Text to process
   * @param {string} locale ISO 639-1 language-code
   */
  populate(data, locale) {
    const tokens = this.split(data);
    for (let i = 0; i < tokens.length; i++) {
      const token = tokens[i].toLowerCase();
      if (this.validate(token, locale)) {
        this.increment(token);
      }
    }
  }

  /**
   * Get keys
   * @returns {array}
   */
  getKeys() {
    return this.keys;
  }

  /**
   * @param {string} word Word to count
   * @returns {number}
   */
  getCount(word) {
    return this.dict[word];
  }

  /**
   * @param {string} word Word to increment count for
   */
  increment(word) {
    if (!this.dict[word]) {
      this.dict[word] = 1;
      this.keys.push(word);
    } else {
      this.dict[word]++;
    }
  }

  /**
   * Sort array of keys by counts
   */
  sortByCount() {
    let concordance = this;
    function sorter(a, b) {
      return concordance.getCount(b) - concordance.getCount(a);
    }
    this.keys.sort(sorter);
  }

  /**
   * Create associative dictionary
   */
  setAssociativeDict() {
    for (const [key, value] of Object.entries(this.dict)) {
      this.associativeDict.push({ key: key, count: value });
    }
  }

  /**
   * Sort associative dictionary by count
   */
  sortAssociativeDictByCount() {
    this.associativeDict.sort(function (a, b) {
      return b.count - a.count;
    });
  }

  /**
   * Find and rank most frequent words in string
   * @param {string} data Text to process
   * @param {string} locale ISO 639-1 language-code
   */
  process(text, locale = "en") {
    if (typeof text !== "string" || text.length < 1) {
      throw new Error(
        "Text passed to Concordance.process() must be a non-empty string"
      );
    }
    this.populate(text, locale);
    this.sortByCount();
    this.setAssociativeDict();
    this.sortAssociativeDictByCount();
  }

  /**
   * Concatenate all content
   * @param {array} Array of objects with search-data
   * @returns {string}
   */
  static groupContent(data) {
    data = data.slice(0, 2);
    let content = "";
    for (let i = 0; i < data.length; i++) {
      if (
        data[i].hasOwnProperty("content") &&
        typeof data[i].content === "string" &&
        data[i].content.length > 0
      ) {
        content += data[i].content + " ";
      }
    }
    return content.trim();
  }
}

module.exports = Concordance;

# Endringslogg

Alle nevneverdige endringer i systemet for publisering av arkivplanen vil oppføres i dette dokument. Dette gjelder ikke innholdet i arkivplanen, som har [en egen endringslogg](./docs/endringslogg.md).

Formatet er basert på [Lag en Endringslogg](https://olevik.github.io/keep-a-changelog/), og følger [Semantisk Versjonering](https://semver.org/spec/v2.0.0.html). Gitt et versjonsnummer i formen STØRRE.MINDRE.FIKS betyr det at:

STØRRE versjon økes ved substansielle endringer, inkompatibelt med eksisterende løsninger.
MINDRE versjon økes ved tillegg til eller nye løsninger, kompatibelt med eksisterende løsninger.
FIKS versjon økes ved ubetydelige endringer, som ikke endrer løsningen substansielt.

## [4.2.4] - 2025-01-27

### Fikset

- Satt språk til Norsk Bokmål i lesbarhetsrapport

## [4.2.3] - 2025-01-27

### Endret

- Forbedringer i generering av lesbarhetsrapporter
- Forbedret metadata-beskrivelse

### Fikset

- Satt språk til Norsk Bokmål

## [4.2.2] - 2025-01-21

### Fikset

- Lenker i `/utskrift.pdf`
- Interne lenker og autonavigasjon

## [4.2.1] - 2025-01-21

### Fikset

- Generering av bilder for hver side for deling

## [4.2.0] - 2025-01-21

### Lagt til

- Utvidede [metadata](https://www.w3.org/TR/2011/WD-html5-author-20110809/the-meta-element.html), inkludert [OpenGraph](https://ogp.me/) og [Schema.org](https://schema.org/)
- Kanoniske URL-er
- Generering av bilder for hver side for deling

### Fikset

- Nettsidekart (`/sitemap.xml`)
- Mer robust løsning for forfatternavn
- Korrigerte sidenavigasjon, tittel på hovedside
- Autosøk gjennom `?search=`

## [4.1.0] - 2025-01-10

### Lagt til

- Utforming for [Bevarings- og kassasjonsplan for UH-sektoren](https://ntnu.no/arkivplan/regelverk/bevaring-og-kassasjon/bevarings-og-kassasjonsplan-for-uh-sektoren/), med spesialtilpasning av stor tabell, samt [større versjon](https://ntnu.no/arkivplan/assets/fup.html)
- Live lenkesjekk, da lokal utgave ikke finner alle feil eller er særlig pålitelig

### Endret

- Noen mindre kodetilpasninger
- Fontstørrelse for tabeller ved utskrift

## [4.0.0] - 2024-08-06

### Endret

- Oppdaterte Yarn til versjon 4.4.0
- Tving UTF8 ved Pandoc-konvertering

## [3.1.9] - 2023-11-06

### Endret

- Gjorde `elasticlunr-languages` fullstendig lokal
- Tilpasset konfigurasjon for å unngå feil ved automasjon i GitLab
- Oppdaterte Node-versjon til 18 i `.gitlab-ci.yml`

## [3.1.8] - 2023-09-16

### Endret

- Pandoc til å bruke `GFM` fremfor `Markdown`
- Konverter tabeller til HTML før tekstkonvertering
- Utvidet bredde for innhold
- Reduserte cellemarginer i tabeller
- Skjulte menyknapp og søkeknapp ved utskrift
- La til `--openssl-legacy-provider` som [backup-løsning](https://github.com/microsoft/vscode/issues/136599) for utvikling

## [3.1.7] - 2023-03-22

### Endret

- Oppdaterte Node-versjon til 18
- La til `--openssl-legacy-provider` som [backup-løsning](https://github.com/microsoft/vscode/issues/136599) for PDF-generering

## [3.1.6] - 2022-03-15

### Endret

- Strømmende rapportering i søkemotortest
- Bygg lokalt til /public/arkivplan for lenketest

## [3.1.5] - 2022-03-08

### Fikset

- Unødvendig merking av resultater i søketest
- Lenker og lenketest

## [3.1.4] - 2022-03-08

### Fikset

- Korrigerte lenker

## [3.1.3] - 2022-03-03

### Fikset

- SSR for dokumenter

## [3.1.2] - 2021-11-12

### Fikset

- Byttet fra NPM til Yarn for behandling av bibliotek

## [3.1.1] - 2021-11-11

### Fikset

- Mer responsive bokser på forsiden

## [3.1.0] - 2021-09-20

### Lagt til

- Bokser på forsiden med snarveier til brukerkategorier
- [Samlet rapport](https://ntnu.no/arkivplan/lesbarhetsrapport.html) fra lesbarhetsindeks

### Fikset

- Linje under lenker
- Nummerering i innholdsliste
- Genererte lenker til interne dokumenter, igjen

## [3.0.1] - 2021-08-12

### Fikset

- Interne lenker avhengig av dybde i venstremeny

## [3.0.0] - 2021-08-10

### Lagt til

- Automatisk innholdsfortegnelse
- Lenke til siden ved overskriften
- Lenketest
- Konverteringstest
- Lesbarhetsanalyser
- Funksjonalitet for videresending ved flytting av innhold
- [/lenker](https://ntnu.no/arkivplan/lenker) gir lenker for viderekobling via https://www.ntnu.no/arkivplan

### Endret

- Lenke til "Hjelp oss å forbedre denne siden" erstattet med boks med lenke til kontaktside
- Forfatter erstattet med "DOKU" universelt
- Begrens venstremeny til lenker til sider
- Metodesignatur i `generator/lib/innhold.js`s `process()` og `.transformFile()` for å støtte debug
- Byttet ut `FS-Extra` med `FS`

### Fikset

- Genererte lenker til interne dokumenter
- Søkeboks som liker å gjemme seg
- Eksterne lenker

## [2.0.4] - 2021-05-07

### Fikset

- Korrigerte lenkebehandling

### Endret

- Node økt til v16.0.0

## [2.0.3] - 2021-04-27

### Fikset

- Fang metadata fra dokumenter formatert av Word Online

## [2.0.2] - 2021-04-16

### Fikset

- API-dokumentasjon

## [2.0.1] - 2021-04-09

### Endret

- Bedre håndtering av interaksjon med søkefelt og -resultater
- VuePress økt til v1.8.2

### Fikset

- Klikk på søkeresultater
- Kompilasjon

## [2.0.0] - 2021-03-21

### Lagt til

- Søkemotor-sammenligning og analyse
- Søkeresultater i kontekst
- PDF-generator

### Endret

- Lenke til DOKU i meny
- [MiniSearch](https://github.com/lucaong/minisearch) som søkemotor
- Node økt til v15.12.0

### Fikset

- Automatisk formatering: Norsk bruker normal, ikke lang, tankestrek
- Kode-kvalitet

## [1.2.0] - 2021-02-12

### Endret

- Metadata: Foretrekk original forfatter fremfor siste endringer
- Refaktor: Metadata

### Lagt til

- Test for metadata
- Prosess for metadata

### Fikset

- Tegn for kode i innhold
- Kodekvalitet

## [1.1.0] - 2021-02-09

### Endret

- Refaktor: Klasser og metoder
- JSDoc-kommentarer

### Fikset

- Lenker fra søk
- Lenker til vedlegg

## [1.0.1] - 2021-02-06

### Fikset

- Tilpassede lenker
- Print-stiler
- Pandoc-format: `markdown+footnotes-grid_tables`

## [1.0.0] - 2021-02-06

### Lagt til

- Første offentlige publisering av ny utgave av NTNUs arkivplan
